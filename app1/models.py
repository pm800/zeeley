from __future__ import unicode_literals
from django.db import models
from time import time
from django.contrib.auth.models import User
#from django.contrib.postgres.fields import ArrayField
from django import forms # __IGNORE_WARNING__
import ast
import datetime
from django.core.validators import RegexValidator
from django.utils import timezone # __IGNORE_WARNING__

from django.utils.encoding import python_2_unicode_compatible


# from gcm.models import AbstractDevice

# class MyDevice(AbstractDevice):
#     pass



class ListField(models.TextField):
    #__metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)


    def from_db_value(self, value, expression, connection, context):
        if value is None:
            return value
        return ast.literal_eval(value)


    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return unicode(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)

# Create your models here.
def get_upload_file_name(instance, filename):
    return "uploaded_files/%s_%s" % (str(time()).replace('.','_'),filename)

# class StringListField(models.Field):



#     __metaclass__=models.SubfieldBase
#     SPLIT_CHAR = u'\v'
#     def __init__(self,*args,**kwargs):
#         self.internal_type=kwargs.pop('internal_type','CharField')
#         super(StringListField,self).__init__(*args,**kwargs)

#     def to_python(self,value):
#         if isinstance(value,list):
#             return value
#         if value is None:
#             return []
#         return value.split(self.SPLIT_CHAR)

#     def get_internal_type(self):
#         return self.internal_type

#     def get_db_prep_lookup(self, lookup_type, value):
#         #SQL WHERE
#         raise NotImplementedError()

#     def get_db_prep_save(self,value):
#         return self.SPLIT_CHAR.join(value)

#     def formfield(self,**kwargs):
#         assert not kwargs, kwargs
#         return forms.MultipleChoiceField(choices=self.choices)


class regs(models.Model):#??? no use in views
    First_Name = models.CharField(max_length=30,null=True)
    Username = models.CharField(max_length=30)
    Last_Name = models.CharField(max_length=30,null=True)
    City = models.CharField(max_length=30,null=True)
    Date = models.IntegerField(default=0)
    Month = models.IntegerField(default=0)
    Year = models.IntegerField(default=0)

    email = models.EmailField()
    password = models.CharField(max_length=30)
    confirm_password = models.CharField(max_length=30)
    male = models.BooleanField(default=False)
    female = models.BooleanField(default=False)
    other = models.BooleanField(default=False)
    #t=models.FileField(upload_to=get_upload_file_name,blank=True)Sport = models







    S_Cricket = models.BooleanField(default=False)
    S_Badminton = models.BooleanField(default=False)
    S_Volleyball = models.BooleanField(default=False)
    S_Football = models.BooleanField(default=False)
    S_Chess = models.BooleanField(default=False)
    S_Hockey = models.BooleanField(default=False)
    S_Table_Tennis = models.BooleanField(default=False)
    S_Lawn_Tennis = models.BooleanField(default=False)
    S_Kabaddi = models.BooleanField(default=False)
    S_Kho_Kho = models.BooleanField(default=False)







    M_Guitar = models.BooleanField(default=False)
    M_Sitar = models.BooleanField(default=False)
    M_Drum = models.BooleanField(default=False)
    M_Tabla = models.BooleanField(default=False)
    M_Flute = models.BooleanField(default=False)
    M_Violin = models.BooleanField(default=False)
    M_Piano = models.BooleanField(default=False)
    M_Regional = models.BooleanField(default=False)
    M_Organ_Pad = models.BooleanField(default=False)
    M_Western = models.BooleanField(default=False)
    M_Classical = models.BooleanField(default=False)
    M_Vocals = models.BooleanField(default=False)
    M_Harmonium = models.BooleanField(default=False)



    T_Manali = models.BooleanField(default=False)
    T_Kullu = models.BooleanField(default=False)
    T_Switzerland = models.BooleanField(default=False)
    T_Kashmir = models.BooleanField(default=False)
    T_Shimla = models.BooleanField(default=False)
    T_London = models.BooleanField(default=False)
    T_Mumbai = models.BooleanField(default=False)
    T_Kolkata = models.BooleanField(default=False)
    T_Chennai = models.BooleanField(default=False)
    T_Delhi = models.BooleanField(default=False)
    T_Ooty = models.BooleanField(default=False)
    T_Goa = models.BooleanField(default=False)
    T_NewYork = models.BooleanField(default=False)
    T_Malaysia = models.BooleanField(default=False)
    T_Darjeeling = models.BooleanField(default=False)
    T_Nainital = models.BooleanField(default=False)
    T_Agra = models.BooleanField(default=False)
    T_Jaipur_Udaipur = models.BooleanField(default=False)
    T_Kanyakumari = models.BooleanField(default=False)
    T_Ajanta_Ellora = models.BooleanField(default=False)
    T_Mysore = models.BooleanField(default=False)
    T_Rishikesh = models.BooleanField(default=False)
    T_Gujarat = models.BooleanField(default=False)
    T_Adman_Islands = models.BooleanField(default=False)
    T_Meghalaya = models.BooleanField(default=False)
    T_Almora = models.BooleanField(default=False)
    T_Wagah_Border = models.BooleanField(default=False)
    T_Arunachal_Pradesh = models.BooleanField(default=False)

    def __unicode__(self):
        return self.First_Name


class student(models.Model):
    #student_id = models.IntegerField(default=id)
    ##
    #date_created = models.DateTimeField('date_created')

    ##
    user=models.OneToOneField(User,  blank=True) #listf  #pahle
    online=models.BooleanField(default=False,  blank=True)

    n_notif=models.IntegerField(default=0,  blank=True)
    n_msg=models.IntegerField(default=0,  blank=True)
    msg_list = ListField(default=[],  blank=True) # list of ids of pesons who sent message  (only one id for a person)
    msg_sdr = models.ManyToManyField(u'self', symmetrical=False, through='ReMsgr', related_name = 'msg_rcr',  blank=True)
    n_frqst=models.IntegerField(default=0,  blank=True)
    logout = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,  blank=True) # last datetime when user is online

    status = models.CharField(max_length=100,null=True,  blank=True)
    first_name=models.CharField(max_length=100,null=True,  blank=True)
    last_name=models.CharField(max_length=100,null=True,  blank=True)
    date = models.IntegerField(default=0,  blank=True)
    month = models.IntegerField(default=0,  blank=True)
    year = models.IntegerField(default=0,  blank=True)
    male = models.BooleanField(default=False,  blank=True)
    female = models.BooleanField(default=False,  blank=True)
    latitude = models.DecimalField(max_digits=8,decimal_places=5,default=25.26276,  blank=True)
    longitude = models.DecimalField(max_digits=8,decimal_places=5,default=82.99078,  blank=True)

    post = ListField(default=[],  blank=True)# list of (postnumber)
    #post_2 = models.ManyToManyField(u'self', related_name = '')
    work = models.TextField(default="",  blank=True)
    work_as = models.TextField(default="",  blank=True)
    school = models.TextField(default="",  blank=True)
    college = models.TextField(default="",  blank=True)
    party = models.TextField(default="",  blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=15, validators=[phone_regex], blank=True) # validators should be a list

    current_place = models.TextField(default="",  blank=True)
    current_intrest = models.TextField(default="",  blank=True)
    hometown = models.TextField(default="",  blank=True)
    prof_pic = ListField(default=[],  blank=True)# list of (prof_pic number) # /file_p8_25.jpg
    #prof_pic_2 = models.ManyToManyField(u'self', related_name = '')
    cover_pic = ListField(default=[],  blank=True)# list of (cover_pic number) # /file_c8.26.jpg
    #cover_pic_2 = models.ManyToManyField(u'self', related_name = '')
    cov_fix = ListField(default=[0])
    #cov_fix_2 = models.ManyToManyField(u'self', related_name = '')

    priv_prof = models.IntegerField(default=1,  blank=True)
    priv_mess = models.IntegerField(default=1,  blank=True)
    priv_frnd = models.IntegerField(default=1,  blank=True)
    priv_email = models.IntegerField(default=1,  blank=True)
    priv_goups = models.IntegerField(default=1,  blank=True)
    priv_blokd = models.IntegerField(default=1,  blank=True)


    interest = ListField(default=[0],  blank=True)# list of ids of inetrest objects
    interest_2 = models.ManyToManyField('intrests', through='StudentInterest', related_name = 'intrest_memb_id_2',  blank=True)
    travels = ListField(default=[0],  blank=True)#??? no use
    #travels_2 = models.ManyToManyField(u'self', related_name = '')

    #post = ArrayField(models.CharField(max_length=100),blank=True)
    # date = models.DateField(_("Date"), default=datetime.date.today)

    tagword = models.CharField(max_length=100,null=True,default="",  blank=True)

    priv_map = models.IntegerField(default=0,  blank=True)
    priv_dist = models.IntegerField(default=0,  blank=True)


    block_prof = ListField(default=[],  blank=True)
    block_prof_2 = models.ManyToManyField(u'self', symmetrical=False, related_name = 'blocked_prof',  blank=True)
    block_location = ListField(default=[],  blank=True)
    block_location_2 = models.ManyToManyField(u'self', symmetrical=False, related_name = 'blocked_location',  blank=True)
    block_msg = ListField(default=[],  blank=True)# list if ids of student who can't message to user
    block_msg_2 = models.ManyToManyField(u'self', symmetrical=False, related_name = 'blocked_msg', blank=True)
    block_invite = ListField(default=[],  blank=True)
    block_invite_2 = models.ManyToManyField(u'self', symmetrical=False, related_name = 'blocked_invite',  blank=True)

    priv_string = models.CharField(max_length=10,null=True,  blank=True) # j.priv_string = string 0, 1, 'stud.priv_map'+'stud.priv_dist'+'0' or '1'

    fmsg = models.TextField(default="",  blank=True)
    fnotify = models.TextField(default="",  blank=True)
    fhitcomnotif= ListField(default=[],  blank=True)#??? no use
    #fhitcomnotif_2 = models.ManyToManyField(u'self', related_name = '')
    fcomnotif = ListField(default=[],  blank=True)#???   no use
    #fcomnotif_2 = models.ManyToManyField(u'self', related_name = '')
    f_list = ListField(default=[],  blank=True)#list of leaders ids
    f_list_2 = models.ManyToManyField(u'self', symmetrical=False, through='ReFlist', related_name = 'accept_f_list_2',  blank=True) #.c
    add_f_list = ListField(default=[],  blank=True)#???
    add_f_list_2 = models.ManyToManyField(u'self',related_name='sdfsdf', symmetrical=False, blank=True)
    accept_f_list = ListField(default=[],  blank=True)# list of follower's ids
    #accept_f_list_2 = models.ManyToManyField(u'self', symmetrical=False, related_name = '', null=True, blank=True)





    cov_r =models.IntegerField(default=0,  blank=True)#???
    cov_g =models.IntegerField(default=86,  blank=True)
    cov_b =models.IntegerField(default=134,  blank=True)
    groups = ListField(default=[],  blank=True) #?? list of ids of create_group objects in which user is present (not sure) or gropus created by user #???
    #groups_2 = models.ManyToManyField('create_group', related_name = 'cg_memb_id_2',  blank=True) #??? f or m
    team_to_chat = ListField(default=[],  blank=True)#?? list of ids of teams
    #team_to_chat_2 = models.ManyToManyField('team', related_name = 'team_memb_2', blank=True)
    shared_posts = ListField(default=[],  blank=True)# list of ids of posts shared by user.
    #shared_posts_2 = models.ManyToManyField('posts', related_name = 'share_students',  blank=True)
    class Meta:
        ordering=['first_name']


    def __unicode__(self):
        return self.first_name

class StudentInterest(models.Model):
    student = models.ForeignKey(student, null=True, blank=True)
    interest = models.ForeignKey('intrests', null=True, blank=True)
    dt = models.DateTimeField(null=True, blank=True)

class ReMsgr(models.Model):
    rcr = models.ForeignKey(student, related_name='a', null=True, blank=True)
    sdr = models.ForeignKey(student, related_name='b', null=True, blank=True)
    dt = models.DateTimeField(null=True, blank=True)

class ReFlist(models.Model):
    follower = models.ForeignKey(student, related_name='c', null=True, blank=True)
    leader = models.ForeignKey(student, related_name='d', null=True, blank=True)
    dt = models.DateTimeField(null=True, blank=True)
####################################################################
class posts(models.Model):
    identity = models.IntegerField(default=0,  blank=True) #crator's id   #listf
    creator = models.ForeignKey(student,related_name='all_posts', blank=True, null=True)
    profile_picture= models.BooleanField(default=False,  blank=True)
    cover_picture= models.BooleanField(default=False,  blank=True)


    photos = models.IntegerField(default=0,  blank=True) # photos = photo number
    hits = models.IntegerField(default=0,  blank=True)
    hitters = ListField(default=[],  blank=True)
    hitters_2 = models.ManyToManyField(student, related_name = 'hit_posts', blank=True)
    comnt = ListField(default=[],  blank=True)#list of ids of commentors.[2,45,2,66,2]
    comnt_2 = models.ManyToManyField(student, related_name = 'comnt_posts',  blank=True)
    share = models.ManyToManyField(student, related_name='shared_posts_2', blank=True)

    comments = models.IntegerField(default=0,  blank=True) # total no. of comments
    say = models.TextField(default="",  blank=True)
    category = models.TextField(default="",  blank=True)# empty or dating
    anonymous = models.BooleanField(default=False,  blank=True)# generally for dating category
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)


    def __unicode__(self):
        return str(self.photos) #+++++
        #return "posts"

class postcomment(models.Model): #done
    c_identity = models.IntegerField(default=0,  blank=True)#id of  comment creator. #listf
    creator = models.ForeignKey(student, null=True, blank=True, related_name='postcomments')
    c_intity = models.IntegerField(default=0,  blank=True) #id of post creator.  #listf
    p_creator = models.ForeignKey(student, null=True, blank=True, related_name='all_comnts_on_my_posts')
    c_photos = models.IntegerField(default=0,null=True,  blank=True)# post id on which commenting     #listf
    post = models.ForeignKey(posts, null=True, blank=True, related_name='postcomments', on_delete=models.CASCADE)
    c_say = models.TextField(default="",null=True,  blank=True)#c_say= comment.
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)
    def __unicode__(self):
        return str(self.c_photos) #+++
        #return 'postcomment'

class message(models.Model):
    m_id = models.IntegerField(default=0,  blank=True) # messenger's id #listf
    sender = models.ForeignKey(student, null=True, blank=True, related_name='msgs')
    m_friend = models.IntegerField(default=0,  blank=True) # message reciever's id #listf
    receiver =  models.ForeignKey(student, null=True, blank=True, related_name='r_msgs')
    m_message = models.TextField(default="",  blank=True) # message
    m_admin = models.BooleanField(default=False,  blank=True)#???
    m_seen = models.BooleanField(default=False,  blank=True)#lll
    m_photo = models.BooleanField(default=False,  blank=True)#if message is a photo
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)
    def __unicode__(self):
        return str(self.m_id) #++++++++++++++++++++++++++++++++++++++++++++++++++++
        #return 'message'

class create_group(models.Model):
    cg_name =models.CharField(max_length=70,  blank=True)
    cg_lat = models.DecimalField(max_digits=8,decimal_places=5,  blank=True)
    cg_long = models.DecimalField(max_digits=8,decimal_places=5,  blank=True)
    cg_memb_id = ListField(default=[],  blank=True)
    cg_memb_id_2 = models.ManyToManyField(student,blank=True, related_name = 'groups_2')

    def __unicode__(self):
        return self.cg_name

@python_2_unicode_compatible
class intrests(models.Model):
    intrest_name = models.CharField(max_length=70,  blank=True)
    intrest_posts = ListField(default=[],  blank=True)# list of ids of posts on this inerest.
    intrest_posts_2 = models.ManyToManyField('posts', related_name = 'interests',  blank=True)#??? forei or many
    intrest_memb_id = ListField(default=[],  blank=True)# list of ids(unique) of students who have this iterest in present or past
   #### intrest_memb_id_2 = models.ManyToManyField(student, related_name = '')
    intrest_category = models.CharField(max_length=70,  blank=True)
    intrest_catelog = models.CharField(max_length=70,  blank=True)
    tagword = models.CharField(max_length=100,null=True,default="",  blank=True)
    def __unicode__(self):
        return self.intrest_name

    def __str__(self):
        return self.intrest_name


class create_city(models.Model):
    city_name =models.CharField(max_length=70,  blank=True)
    city_lat = models.DecimalField(max_digits=8,decimal_places=5,  blank=True)
    city_long = models.DecimalField(max_digits=8,decimal_places=5,  blank=True)
    city_memb_id = ListField(default=[],  blank=True)
    city_memb_id_2 = models.ManyToManyField(student, related_name = 'cities', blank=True)
    city_descript = models.TextField(default="",  blank=True)
    city_posts = ListField(default=[],  blank=True)
    city_posts_2 = models.ManyToManyField(posts, related_name = 'cities',  blank=True)
    tagword = models.CharField(max_length=100,null=True,default="",  blank=True)
    def __unicode__(self):
        return self.city_name

class create_subjects(models.Model):
    subjects_name =models.CharField(max_length=70,  blank=True)
    subjects_memb_id = ListField(default=[],  blank=True)
    subjects_memb_id_2 = models.ManyToManyField(student, related_name = 'subjects',  blank=True)
    subjects_descript = models.TextField(default="",  blank=True)
    subjects_posts = ListField(default=[],  blank=True)
    subjects_posts_2 = models.ManyToManyField(posts, related_name = 'subjects',  blank=True)
    tagword = models.CharField(max_length=100,null=True,default="",  blank=True)
    def __unicode__(self):
        return self.subjects_name

class player(models.Model):
    p_id = models.IntegerField(default=0)# student's id #listf
    student = models.OneToOneField(student, blank=True, null=True, related_name='player')
    p_votes = models.IntegerField(default=0)
    p_rating = models.IntegerField(default=0)
    p_team = models.IntegerField(default=0)# team id or game id #listf #listf
    team = models.ForeignKey('team', blank=True, null=True, related_name='players')
    game = models.ForeignKey('game',blank=True, null=True, related_name='players')
    p_game = models.CharField(max_length=70,  blank=True)# basically a interest_name

    def __unicode__(self):
        return str(self.p_id)#+++
        #return 'player'
class team(models.Model):
    team_name = models.CharField(max_length=100,  blank=True)
    team_capt = models.IntegerField(default=0)# team creator
    team_category = models.CharField(max_length=70,  blank=True)# is basically a intrest_name of a intrest
    team_memb = ListField(default=[],  blank=True)
    team_memb_2 = models.ManyToManyField(student, blank=True, related_name = 'team_to_chat_2')
    team_invites = ListField(default=[],  blank=True)# list of ids of invited students
    team_invites_2 = models.ManyToManyField(student, related_name = 'team_invites',  blank=True)
    team_matches = models.IntegerField(default=0)
    team_wins = models.IntegerField(default=0)
    team_lose =models.IntegerField(default=0)
    team_logo = models.TextField(default="",  blank=True)
    team_group = models.IntegerField(default=0)# id of create_group object or id of trip

    game_name = models.TextField(default="",  blank=True)

    game_server = models.TextField(default="",  blank=True)

    def __unicode__(self):
        return self.team_name


class trip(models.Model):
    trip_name = models.CharField(max_length=100,null=True,  blank=True)
    trip_from = models.DateTimeField(null=True,default=datetime.datetime.today)
    trip_to = models.DateField(default=datetime.date.today)
    trip_city = models.CharField(max_length=50,null=True,  blank=True)
    trip_city_from = models.CharField(max_length=50,  blank=True)
    trip_mode = models.CharField(max_length=10,null=True,  blank=True)
    trip_memb =ListField(default=[],  blank=True)# list of ids of trip creator and #not sure# other members
    trip_memb_2 = models.ManyToManyField(student, related_name = 'trips', blank=True)
    trip_invites =ListField(default=[],  blank=True)# list of ids of students who request to join the trip
    trip_invites_2 = models.ManyToManyField(student, related_name = 'trip_invites', blank=True)
    trip_n_membs =models.IntegerField(default=0)# #ns# max# no. of menbers in trip
    trip_admin =models.IntegerField(default=6) # trip creator

    from_latitude = models.DecimalField(max_digits=11,decimal_places=8,default=25.26370359)#??? default is of
    from_longitude = models.DecimalField(max_digits=11,decimal_places=8,default=82.98598559)

    to_latitude = models.DecimalField(max_digits=11,decimal_places=8,default=25.26370359)
    to_longitude = models.DecimalField(max_digits=11,decimal_places=8,default=82.98598559)
    booking = models.BooleanField(default=False)
    remark = models.TextField(default="",  blank=True)


    def __unicode__(self):
        return self.trip_name


class band(models.Model):
    band_name = models.CharField(max_length=100,  blank=True)
    band_descript = models.TextField(default="",  blank=True)
    band_V_piano = models.IntegerField(default=0)
    band_V_guitar = models.IntegerField(default=0)
    band_V_singer = models.IntegerField(default=0)
    band_V_violin = models.IntegerField(default=0)
    band_V_bass = models.IntegerField(default=0)
    band_V_drum = models.IntegerField(default=0)
    band_V_flute = models.IntegerField(default=0)
    band_V_saxophone = models.IntegerField(default=0)
    band_V_tabla = models.IntegerField(default=0)
    band_V_other = models.IntegerField(default=0)
    band_other = models.CharField(max_length=100,  blank=True)
    band_memb =ListField(default=[],  blank=True)#list of ids of student
    band_memb_2 = models.ManyToManyField(student, related_name = 'bands',blank=True)
    band_memb_work =ListField(default=[],  blank=True)# list of intgers(denoting his work)
    # band_memb_work_2 = models.ManyToManyField(student, related_name = '')
    band_posts =ListField(default=[],  blank=True)
    band_posts_2 = models.ManyToManyField(posts, related_name = 'bands', blank=True)  #??? rf (relation field)
    band_hits = models.IntegerField(default=0)
    band_group = models.IntegerField(default=0)
    band_hitters =ListField(default=[],  blank=True)
    band_hitters_2 = models.ManyToManyField(student,blank=True, related_name = 'hit_bands')
    tagword = models.CharField(max_length=100,null=True,default="",  blank=True)
    def __unicode__(self):
        return self.band_name


class groupchat(models.Model):
    gc_id = models.IntegerField(default=0)# student id #listf
    creator = models.ForeignKey(student, blank=True, null=True, related_name='groupchats')
    gc_group = models.IntegerField(default=0)# create_group id  #listf
    group = models.ForeignKey(create_group, blank=True, null=True, related_name='groupchats')
    gc_message = models.TextField(default="",  blank=True)
    admin = models.BooleanField(default=False)
    gc_interest = models.TextField(default=0)# interest name
    def __unicode__(self):
        return str(self.gc_id) #+++
        #return 'groupchat'

class teamchat(models.Model):
    teamc_id = models.IntegerField(default=0)# student id #listf
    creator = models.ForeignKey(student, blank=True, null=True, related_name='teamchats')
    teamc_group = models.IntegerField(default=0)# team id  #listf
    team = models.ForeignKey(team, blank=True, null=True, related_name='teamchats')
    teamc_message = models.TextField(default="",  blank=True)
    admin = models.BooleanField(default=False)
    teamc_photo = models.BooleanField(default=False)
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)
    def __unicode__(self):
        return str(self.teamc_id)#+++ need


class startup(models.Model):
    startup_name = models.CharField(max_length=100,  blank=True)
    startup_descript = models.TextField(default="",  blank=True)
    startup_V_ceo = models.IntegerField(default=0)
    startup_V_techop = models.IntegerField(default=0)
    startup_V_salesmark = models.IntegerField(default=0)
    startup_V_hr = models.IntegerField(default=0)
    startup_V_busdev = models.IntegerField(default=0)
    startup_V_custser = models.IntegerField(default=0)
    startup_V_salesman = models.IntegerField(default=0)
    startup_V_rnd = models.IntegerField(default=0)
    startup_V_adm = models.IntegerField(default=0)
    startup_V_other = models.IntegerField(default=0)
    startup_other = models.CharField(max_length=100,  blank=True)
    startup_memb =ListField(default=[],  blank=True)# list of student ids
    startup_memb_2 = models.ManyToManyField(student, related_name = 'startups',  blank=True)
    startup_memb_work =ListField(default=[],  blank=True)# list of integers denoting his work
    # startup_memb_work_2 = models.ManyToManyField(student, related_name = '')
    startup_posts =ListField(default=[])
    startup_posts_2 = models.ManyToManyField(posts, related_name = 'startups', blank=True)



    startup_hits = models.IntegerField(default=0)
    startup_group = models.IntegerField(default=0)
    startup_hitters =ListField(default=[],  blank=True)
    startup_hitters_2 = models.ManyToManyField(student, related_name = 'hit_startups',  blank=True)
    tagword = models.CharField(max_length=100,null=True,default="",  blank=True)

    def __unicode__(self):
        return self.startup_name



class game(models.Model):
    game_name = models.CharField(max_length=100)
    game_capt = models.IntegerField(default=0)
    game_category = models.CharField(max_length=70)#i.current_intrest
    game_memb = ListField(default=[],  blank=True)
    game_memb_2 = models.ManyToManyField(student, related_name = 'games',  blank=True)
    game_limit = models.IntegerField(default=1)
    game_logo = models.TextField(default="",  blank=True)
    game_group = models.IntegerField(default=0)

    def __unicode__(self):
        return self.game_name



class create_cafe(models.Model):
    cafe_name =models.CharField(max_length=70,  blank=True)
    cafe_lat = models.DecimalField(max_digits=8,decimal_places=5,  blank=True)
    cafe_long = models.DecimalField(max_digits=8,decimal_places=5,  blank=True)
    cafe_reviewer_id = ListField(default=[],  blank=True)
    cafe_reviewer_id_2 = models.ManyToManyField(student, related_name = 'review_cafes',  blank=True)
    cafe_address = models.TextField(default="",  blank=True)
    cafe_posts = ListField(default=[],  blank=True)
    cafe_posts_2 = models.ManyToManyField(posts, related_name = 'cafes', blank=True)
    cafe_review = models.DecimalField(max_digits=3,decimal_places=2,null=True,  blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=15, validators=[phone_regex], blank=True)

    tagword = models.CharField(max_length=100,null=True,default="")

    def __unicode__(self):
        return self.cafe_name

class create_film(models.Model):
    film_name =models.CharField(max_length=70,  blank=True)
    film_reviewer_id = ListField(default=[],  blank=True)
    film_reviewer_id_2 = models.ManyToManyField(student, related_name = 'review_films',  blank=True)
    film_descript = models.TextField(default="",  blank=True)
    film_posts = ListField(default=[],  blank=True)# ids of posts
    film_posts_2 = models.ManyToManyField(posts, related_name = 'films',  blank=True)
    film_memb= ListField(default=[],  blank=True)
    film_memb_2 = models.ManyToManyField(student, related_name = 'films',  blank=True)
    film_review = models.DecimalField(max_digits=3,decimal_places=2,null=True,  blank=True)
    tagword = models.CharField(max_length=100,null=True,default="",  blank=True)

    def __unicode__(self):
        return self.film_name


class feedback(models.Model):
    feed = models.TextField(default="",  blank=True)

    def __unicode__(self):
        return str(self.id)#+++need





class quick(models.Model):
    quick_name = models.TextField(default="",  blank=True)
    quick_from = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time(),  blank=True)
    quick_to = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time(),  blank=True)
    quick_memb =ListField(default=[],  blank=True)
    quick_memb_2 = models.ManyToManyField(student, related_name = 'quicks', blank=True)
    quick_group = models.IntegerField(default=0)



    def __unicode__(self):
        return self.quick_name

class pizza(models.Model):
    family_name = models.TextField(default="",  blank=True)
    hour_from = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    hour_to = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    family_memb =ListField(default=[],  blank=True)
    family_memb_2 = models.ManyToManyField(student, related_name = 'pizzas',  blank=True)
    pizza_type=models.CharField(max_length=20,null=True,  blank=True)
    pizza_coupon = models.CharField(max_length=5,null=True,  blank=True)
    pizza_group = models.IntegerField(default=0)# create_group object's id



    def __unicode__(self):
        return self.family_name


class cab_share(models.Model):

    from_latitude = models.DecimalField(max_digits=10,decimal_places=8,default=25.26370359)
    from_longitude = models.DecimalField(max_digits=10,decimal_places=8,default=82.98598559)
    place_from = models.CharField(max_length=100,null=True,  blank=True)
    to_latitude = models.DecimalField(max_digits=10,decimal_places=8,default=25.26370359)
    to_longitude = models.DecimalField(max_digits=10,decimal_places=8,default=82.98598559)
    place_to = models.CharField(max_length=100,null=True,  blank=True)
    depart_time = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    cab_memb =ListField(default=[],  blank=True)
    cab_memb_2 = models.ManyToManyField(student, related_name = 'cabs',  blank=True)
    cab_coupon = models.BooleanField(default=0)
    cab_limit = models.IntegerField(default=1)






    def __unicode__(self):
        return str(self.id) #+++need



#+++@python_2_unicode_compatible
class Vote(models.Model):
    user = models.OneToOneField(User, related_name='votes', null=True,  blank=True) #listf
    interest = models.OneToOneField(intrests, related_name='lsls', null=True, blank=True) #listf
    score = models.FloatField(  blank=True)
    def __unicode__(self):
        return u'vote' #+++ need

    '''def __str__(self):#???
        return self.self.score'''




class user_device(models.Model):
    """docstring for user_device"""
    dev_id = models.OneToOneField(student,related_name='sdfsdfsdd',  blank=True) #listf
    reg_key = models.TextField(default="",  blank=True)

    def __unicode__(self):
        return str(self.reg_key) #+++ need



class notification(models.Model):
    """docstring for user_device"""
    notif_from = models.IntegerField(default=0)
    notif_to = models.IntegerField(default=0)
    notif = models.TextField(default="",  blank=True)
    purpose = models.CharField(max_length=20,null=True,  blank=True)
    purpose_id = models.IntegerField(default=0) #id of some model object
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)#auto_now : for each time ojcet saved. #add: for first time only when object is created

    def __unicode__(self):
        return str(self.id) #+++
        #return 'n'
