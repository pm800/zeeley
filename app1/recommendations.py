from django.contrib.auth.models import User
from recommends.providers import RecommendationProvider
from recommends.providers import recommendation_registry

from .models import intrests,Vote


class InterestRecommendationProvider(RecommendationProvider):
    def get_users(self):
        return User.objects.filter(is_active=True, votes__isnull=False).distinct()

    def get_items(self):
        return intrests.objects.all()

    def get_ratings(self, obj):
        return Vote.objects.filter(product=obj)

    def get_rating_score(self, rating):
        return rating.score

    def get_rating_user(self, rating):
        return rating.user

    def get_rating_item(self, rating):
        return rating.intrests


recommendation_registry.register(Vote, [intrests], InterestRecommendationProvider)