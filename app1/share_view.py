def Cab_sharing(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        cabs =[]
        all_cabs = cab_share.objects.all()

        for tn in all_cabs:
            if timezone.now() > tn.depart_time:
                tn.delete()

        your_cabs = cab_share.objects.filter(cab_memb__contains=i.id)
        count = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        m = intrests.objects.filter(intrest_name__iexact="Cab_sharing").first()
        dist = 500
        zipped = localite(request,m.intrest_name,dist)
        if request.method=="GET":
            
            place_from = request.GET["place_from"]
            place_to = request.GET["place_to"]
            from_lat = request.GET["from_lat"]
            from_long = request.GET["from_long"]
            to_lat = request.GET["to_lat"]
            to_long = request.GET["to_long"]
            max_memb = request.GET["max_memb"]
            date = request.GET["date"]
            time = request.GET["time"]
            d_time = change_date_formatt(date,time)
            # coupon =request.GET["coupon"]

            shared = cab_share.objects.filter(place_from=place_from,place_to=place_to,cab_memb__contains =i.id)
            for shr in shared:
                if len(shr.cab_memb) ==1:
                    shr.delete()

            c_share = cab_share(place_from=place_from,place_to=place_to,from_latitude=from_lat,from_longitude=from_long,to_latitude=to_lat,to_longitude=to_long,cab_limit=max_memb, depart_time=d_time,cab_coupon=False)
            c_share.save()
            
            # cabs = cab_share.objects.filter(place_from=place_from,place_to=pace_to,depart_time=date+" "+time)
            c_share.cab_memb.append(i.id)
            c_share.save()
            cabs = cab_share.objects.filter(place_from=place_from,place_to=place_to).exclude(cab_memb__contains=i.id)


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "m" : m,
            "your_cabs" :your_cabs,
            # "f" : f,
            # "other" : other,
            "zip" : zipped,
            # "zip_team" :zipped_team,
            "cabs" : cabs,
            # "l" : l,
            
        }
   
        return render(request,"Cab_Sharing.html",context)
    else:
        return HttpResponse ("Login first!!!")



def Cotravelling(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        trips =[]
        all_trips = trip.objects.all()

        for tn in all_trips:
            if datetime.now(pytz.utc) > tn.trip_from:
                tn.delete()

        your_trips = trip.objects.filter(trip_memb__contains=i.id).exclude(trip_city_from="Cafe").exclude(trip_city_from="Film")
        count = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        m = intrests.objects.filter(intrest_name__iexact="Travelling").first()
        dist = 500
        zipped = localite(request,m.intrest_name,dist)
        if request.method=="GET":
            
            place_from = request.GET["place_from"]
            place_to = request.GET["place_to"]
            from_lat = request.GET["from_lat"]
            from_long = request.GET["from_long"]
            to_lat = request.GET["to_lat"]
            to_long = request.GET["to_long"]
            max_memb = request.GET["max_memb"]
            date_from = change_date_formt2(request.GET["date"])
            date_to = date_from

            # coupon =request.GET["coupon"]

            shared = trip.objects.filter(trip_city_from=place_from,trip_city=place_to,trip_memb__contains =i.id,trip_from=date_from)
            for shr in shared:
                if len(shr.trip_memb) ==1:
                    shr.delete()

            tour = trip(trip_city_from=place_from,trip_city=place_to,from_latitude=from_lat,from_longitude=from_long,to_latitude=to_lat,to_longitude=to_long,trip_n_membs=max_memb, trip_from=date_from,trip_to=date_to)
            tour.save()
            
            # cabs = cab_share.objects.filter(place_from=place_from,place_to=pace_to,depart_time=date+" "+time)
            tour.trip_memb.append(i.id)
            tour.save()
            trips = trip.objects.filter(trip_city_from=place_from,trip_city=place_to).exclude(trip_memb__contains=i.id)


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "m" : m,
            "your_trips" :your_trips,
            # "f" : f,
            # "other" : other,
            "zip" : zipped,
            # "zip_team" :zipped_team,
            "trips" : trips,
            # "l" : l,
            
        }
   
        return render(request,"Cotravelling.html",context)
    else:
        return HttpResponse ("Login first!!!")

def Rest_share(request):
    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        c = i.id
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif


        tri =trip.objects.filter(trip_city_from='Cafe')
        for tn in tri:
            if datetime.now(pytz.utc) > tn.trip_from :
                tn.delete()

        my_trip = tri.filter(trip_memb__contains=i.id)
        

        more = []

        if request.method=="GET":
            if request.GET["restaurant"]:
                
                t_name = request.GET["restaurant"]
                date = request.GET["date"]
                remark = request.GET["remark"]
                d=date.split("/")
                # q_from=change_date_formt(q_from)
                t_from = d[2]+'-'+d[0]+'-'+d[1]
                
                f_type = request.GET["food_type"]
                t=trip(trip_name=t_name,trip_from=t_from,trip_n_membs=int(f_type),trip_city_from="Cafe",remark=remark)
                t.trip_memb.append(i.id)
                t.save()

                more = tri.filter(trip_name=t_name,trip_mode=t_mode)


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "your_trip":my_trip,
            "trips":more,
            
        }


        
        return render(request,"Restaurants.html",context)

    else:
        return HttpResponse ("Login first!!!")