def Cotravelling(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        trips =[]
        membs = []
        all_trips = trip.objects.all()
        near_trips = []
        for tn in all_trips:
            membs = membs+tn.trip_memb
            if i.id not in tn.trip_memb:
                distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
                if distance <= 15000:
                    near_trips.append(tn)
            if datetime.now(pytz.utc) > tn.trip_from:
                tn.delete()
        membs = list(set(membs))
        your_trips = trip.objects.filter(trip_memb__contains=i.id).exclude(trip_city_from="Cafe").exclude(trip_city_from="Film").order_by("-id")
        count = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        a = student.objects.filter(id__in=membs)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        m = intrests.objects.filter(intrest_name__iexact="Travelling").first()
        dist = 500
        zipped = localite(request,m.intrest_name,dist)



        if request.method=="GET":
            if request.GET["place_from"]:
            
                place_from = request.GET["place_from"]
                place_to = request.GET["place_to"]
                from_lat = request.GET["from_lat"]
                from_long = request.GET["from_long"]
                to_lat = request.GET["to_lat"]
                to_long = request.GET["to_long"]
                max_memb = request.GET["max_memb"]
                remark = request.GET["remark"]
                date_from = change_date_formt2(request.GET["date"])

                date_to = date_from

                # coupon =request.GET["coupon"]

                shared = trip.objects.filter(trip_city_from=place_from,trip_city=place_to,trip_memb__contains =i.id,trip_from=date_from)
                for shr in shared:
                    if len(shr.trip_memb) ==1:
                        shr.delete()
                trip_name = i.first_name + "'s Travelling Troop"
                tour = trip(trip_name =trip_name, trip_city_from=place_from,trip_city=place_to,from_latitude=from_lat,from_longitude=from_long,to_latitude=to_lat,to_longitude=to_long,trip_n_membs=max_memb, trip_from=date_from,trip_to=date_to,remark=remark)
                tour.save()
                
                # cabs = cab_share.objects.filter(place_from=place_from,place_to=pace_to,depart_time=date+" "+time)
                tour.trip_memb.append(i.id)
                tour.save()
                
                z="Done"

                ctx={"z":z}
            

                return HttpResponse(json.dumps(ctx), content_type='application/json')

        if your_trips:
            trips = trip.objects.filter(trip_city_from=your_trips.last().trip_city_from,trip_city=your_trips.last().trip_city).exclude(trip_memb__contains=i.id)
        
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "m" : m,
            "your_trips" :your_trips,
            # "f" : f,
            # "other" : other,
            "zip" : zipped,
            # "zip_team" :zipped_team,
            "near_trips" : near_trips,
            "trips" : trips,
            # "l" : l,
            
        }
   
        return render(request,"Cotravelling.html",context)
    else:
        return HttpResponse ("Login first!!!")

def Rest_share(request):
    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        c = i.id
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        membs = []
        near_trips =[]
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif

        m = intrests.objects.filter(intrest_name__iexact="Travelling").first()
        dist = 500
        zipped = localite(request,m.intrest_name,dist)


        tri =trip.objects.filter(trip_city_from='Cafe')
        for tn in tri:
            membs = membs+tn.trip_memb
            if i.id not in tn.trip_memb:
                distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
                if distance <= 15000:
                    near_trips.append(tn)

            if datetime.now(pytz.utc) > tn.trip_from :
                tn.delete()

        membs = list(set(membs))
        a = student.objects.filter(id__in=membs)
        my_trip = tri.filter(trip_memb__contains=i.id)
        

        more = []

        if request.method=="GET":
            if request.GET["restaurant"]:
                
                t_name = request.GET["restaurant"]
                date = request.GET["date"]
                remark = request.GET["remark"]
                d=date.split("/")
                # q_from=change_date_formt(q_from)
                t_from = change_date_formt2(date)
                
                f_type = request.GET["food_type"]

                shared = trip.objects.filter(trip_name=t_name,trip_city_from="Cafe",trip_memb__contains =i.id,trip_from=t_from)
                for shr in shared:
                    if len(shr.trip_memb) ==1:
                        shr.delete()
                t=trip(trip_name=t_name,trip_from=t_from,trip_n_membs=int(f_type),trip_city_from="Cafe",from_latitude=i.latitude,from_longitude=i.longitude,remark=remark)
                t.trip_memb.append(i.id)
                t.save()

                
                z="Done"

                ctx={"z":z}
            

                return HttpResponse(json.dumps(ctx), content_type='application/json')
        if my_trip:
            more = tri.filter(trip_name=my_trip.last().trip_name)

        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "your_trip":my_trip,
            "trips":more,
            "near_trips":near_trips,
            "m" : m,
            "zipped":zipped,
            
        }


        
        return render(request,"Restaurants.html",context)

    else:
        return HttpResponse ("Login first!!!")



def trip_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
            myteam = team.objects.filter(team_memb__contains=i.id,team_category="Travelling")
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            t_share.trip_memb.append(i.id)
            t_share.save()
            p.n_notif=p.n_notif+1


            
            p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> joined your trip. Please contact him soon.</td></tr><br>"+p.fnotify
            p.save()

            t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="Travelling")
            if t:
                m=t.first()
                
            else:
                t_n=i.first_name+"'s' Travelling "+"Troop"
                m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Travelling",team_group=t_share.id)
                m.team_memb.append(t_share.trip_memb[0])
                m.save()
            m.team_memb.append(i.id)
            m.save()


            notif_msg = str(i.first_name) + " has joined your travelling trip to "+str(t_share.trip_city).split(',')[0]+". Please contact him soon."
            new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Travelling",purpose_id=t_share.id)
            new_notif.save()

            if user_device.objects.filter(dev_id=p.id):
                ud = user_device.objects.filter(dev_id=p.id).first() 
                url='https://fcm.googleapis.com/fcm/send'
                data ={ "notification": {"title": "Zeeley Traveller", "text": notif_msg  },  "to" : str(ud.reg_key),
                    "data":{"team_id":t_share.id}}
                header={'Content-Type':'application/json','Authorization':'key=AIzaSyAR2UQhb8lQ7nsrCsGcMzf7W0VuOzzrmig'}
                r = requests.post(url, data=json.dumps(data), headers=header)
                state = "Done"
            if my_trip:
                my_trip.trip_memb.remove(i.id)
                my_trip.save()
                if not my_trip.trip_memb:
                    my_trip.delete()
            if myteam:
                myteam.first().team_memb.remove(i.id)
                myteam.first().save()
                if not myteam.first().team_memb:
                    myteam.first().delete()
            
            

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Rest_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_name=t_share.trip_name,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
            
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            t_share.trip_memb.append(i.id)
            t_share.save()
            p.n_notif=p.n_notif+1


            
            p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> joined your trip to the restaurant "+t_share.trip_name+". Please contact him soon.</td></tr><br>"+p.fnotify
            p.save()

            t = team.objects.filter(team_group=t_share.id,team_category="Travelling")
            if t:
                m=t.first()
                
            else:
                t_n=i.first_name+"'s' Restaurant "+"Troop"
                m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Restaurant",team_group=t_share.id)
                m.team_memb.append(t_share.trip_memb[0])
                m.save()
            m.team_memb.append(i.id)
            m.save()

            notif_msg = str(i.first_name) + " has joined your trip to the restaurant "+str(t_share.trip_name)+". Please contact him soon."
            new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose=i.current_intrest,purpose_id=t_share.id)
            new_notif.save()

            if user_device.objects.filter(dev_id=p.id):
                ud = user_device.objects.filter(dev_id=p.id).first() 
                url='https://fcm.googleapis.com/fcm/send'
                data ={ "notification": {"title": "Zeeley Restaurants", "text": notif_msg  },  "to" : str(ud.reg_key),
                    "data":{"team_id":t_share.id}}
                header={'Content-Type':'application/json','Authorization':'key=AIzaSyAR2UQhb8lQ7nsrCsGcMzf7W0VuOzzrmig'}
                r = requests.post(url, data=json.dumps(data), headers=header)
                state = "Done"
            if my_trip:
                my_trip.trip_memb.remove(i.id)
                my_trip.save()
                if not my_trip.trip_memb:
                    my_trip.delete()
            if myteam:
                myteam.first().team_memb.remove(i.id)
                myteam.first().save()
                if not myteam.first().team_memb:
                    myteam.first().delete()

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Rest_edit(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method == "GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            c_share = trip.objects.filter(id=trip_id).first()
            if c_share.trip_memb[0]==i.id:
                memb_del = request.GET["memb_remove"]
                p=student.objects.filter(id=int(memb_del)).first()
                c_share.trip_memb.remove(p.id)
                c_share.save()
                
                

                p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> removed you from his trip to the restaurant "+c_share.trip_name+". Please contact him soon.</td></tr><br>"+p.fnotify
                p.save()

                notif_msg = str(i.first_name) + " removed you from his trip to the restaurant "+c_share.trip_name+". Please contact him soon."
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose=i.current_intrest,purpose_id=c_share.id)
                new_notif.save()

                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Restaurants", "text": notif_msg  },  "to" : str(ud.reg_key),
                        "data":{"team_id":t_share.id}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAR2UQhb8lQ7nsrCsGcMzf7W0VuOzzrmig'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"

                if not c_share.trip_memb:
                    c_share.delete()
                    z="trip deleted"
                else:
                    c_share.pk=None
                    c_share.save()
                    c_share.trip_memb=[p.id]
                    c_share.save()
                    z="Removed"

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')

def trip_edit(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method == "GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            c_share = trip.objects.filter(id=int(trip_id)).first()
            if c_share.trip_memb[0]==i.id:

                if request.GET["remark"]:
                    remark = request.GET["remark"]
                    c_share.remark = remark
                    c_share.save()

                if request.GET["memb_remove"]:
                    memb_del = request.GET["memb_remove"]
                    myteam = team.objects.filter(team_memb__contains=memb_del,team_category="Travelling")
                    if myteam:
                        myteam.first().team_memb.remove(int(memb_del))
                        myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()

                    p = student.objects.filter(id=int(memb_del)).first()
                    c_share.trip_memb.remove(p.id)
                    
                    c_share.save()
                    
                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> removed you from his trip to the restaurant "+c_share.trip_name+". Please contact him soon.</td></tr><br>"+p.fnotify
                    p.save()

                    notif_msg = str(i.first_name) + " removed you from his trip to the restaurant "+c_share.trip_name+". Please contact him soon."
                    new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose=i.current_intrest,purpose_id=c_share.id)
                    new_notif.save()

                    if user_device.objects.filter(dev_id=p.id):
                        ud = user_device.objects.filter(dev_id=p.id).first() 
                        url='https://fcm.googleapis.com/fcm/send'
                        data ={ "notification": {"title": "Zeeley Restaurants", "text": notif_msg  },  "to" : str(ud.reg_key),
                            "data":{"team_id":t_share.id}}
                        header={'Content-Type':'application/json','Authorization':'key=AIzaSyAR2UQhb8lQ7nsrCsGcMzf7W0VuOzzrmig'}
                        r = requests.post(url, data=json.dumps(data), headers=header)
                        state = "Done"

                    z="removed"
                    if not c_share.trip_memb:
                        c_share.delete()
                        z="trip deleted"
                    

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')