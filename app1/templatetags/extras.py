from django import template
from datetime import datetime
from PIL import Image
# from app1.recommendations import InterestRecommendationProvider

register = template.Library()


@register.filter
def gory(things, id):
    return things.filter(id=id)

@register.filter
def multiply(value, arg):
    return (value*arg)

@register.filter
def delay(value):
    last_visit_time = value.tzinfo
    time=(datetime.now(last_visit_time)-value).seconds
    return (time)

@register.filter
def remainder(value, arg):
    return (value%arg)

@register.filter
def height(value):
    k=value.split("media/")
    infile = "/home/mohan/lifer/baba1/media_in_env/media_root/"+k[1]
    im = Image.open(infile)
    width, height = im.size
    return ((height*270.00)/width)

@register.filter
def mutual(value, arg):
    b = [val for val in value.f_list if val in arg.f_list]
    return (len(b))
@register.filter
def people(value, arg):
    t = value.filter(intrest_catelog=str(arg))
    z=[]
    for k in t:
        z =z+ [x for x in k.intrest_memb_id if x not in z]
    return (len(z))

@register.filter
def search(value, arg):
    return value.filter(id=int(arg)).first()

# @register.filter
# def similar(value):
#     return get_items
@register.filter
def age(value):
    return datetime.today().year - int(value)

@register.filter
def split_text(value, arg2):
    if "," in value:
        return value.split(",")[int(arg2)]
    else:
        value1 =value+","
        return value1.split(",")[int(arg2)]

