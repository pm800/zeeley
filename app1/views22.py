from django.shortcuts import render, render_to_response
from .forms import regs_form
from .forms import student_form,trip_form
from .models import student,posts,postcomment,message,create_group,create_city,intrests,create_subjects,team,player,trip,band,groupchat,startup
from .models import regs
from django.conf import settings
from django.http import HttpResponse
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q
import operator
import os,sys
from PIL import Image
from datetime import datetime
#from PIL import ThePIL

try:
    from django.utils import simplejson as json
except ImportError:
    import json

import os,sys

# Create your views here.


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def edit(request):
    form = regs_form(request.POST or None)
    head = 'Edit Your Profile'
    title="Welcome"
    active = True
    u = regs.objects.filter(id=2).first()
    if request.method=='POST':
        post = request.POST
        
        u.First_Name = post["First_Name"]

        u.Last_Name = post["Last_Name"]
        u.City = post["City"]

        u.save(update_fields=['First_Name','Last_Name','City'])


    title = u.First_Name+" "+u.Last_Name

    # q = regs.objects.get(id=0)
    # # q.First_Name = i.First_Name
    # q.delete()
    
    #i=form.save(commit=False)
    # # m=i.save()
    # # regs.objects.all()
    # n=i.id
    # fn =i.First_Name
    # ln =i.Last_Name
    # ci =i.City
    # i.id=n
    # i.First_Name=fn
    # i.Last_Name=ln
    # i.City=ci



    # if form.is_valid():
    #     i.First_Name = form.cleaned_data.get("First_Name")
    #     i.Last_Name = form.cleaned_data.get("Last_Name")
    #     i.City = form.cleaned_data.get("City")

    # print i.First_Name

    # #i = form(First_Name="Nitin", Last_Name = "Vishwari", City ="Varanasi")
    # # first_name = form.cleaned_data.get("First_Name")
    # # if not first_name:
    # #     first_name = "User"
    #i.save()
    
    # i.First_Name = first_name
    
    
    # first_name=i.First_Name
    # if first_name:
    

    # q=regs.objects.get(id=1)
    # q.First_Name=title
    # q.save()

    context = {
        "title" : title,
        "form" : form,
        "head" : head
    }


    # if form.is_valid(): 
    #     instance = form.save(commit=False)
    # # if not instance._name:
    # #     instance.full_name = "mohan"
    #     instance.save()
    #     context ={
    #         "title" : "Thank You!!!"
    #     }
    return render(request,"edit_prof.html",context)
def prof(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        f = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).order_by("-id")
        count = str(c)
        title = i.first_name+" "+i.last_name
        if request.method=='POST':
            
            if request.POST.get('searchbox3'):
                ci = request.POST.get('searchbox3')
                # v = posts.objects.filter(photos=h).first()
                i.current_intrest = ci
                i.save()
                j=intrests.objects.filter(intrest_name=ci).first()
                if not c in j.intrest_memb_id:
                    j.intrest_memb_id.append(c)
                    j.save()


                if j.intrest_category=="pg":
                    if not player.objects.filter(p_id=c,p_game=ci):
                        p=player(p_id=c,p_game=ci)
                        p.save()


            if request.POST.get('work'):
                work = request.POST.get('work')
                # v = posts.objects.filter(photos=h).first()
                i.work = work
                i.save()
            if request.POST.get('hometown'):
                hometown = request.POST.get('hometown')
                # v = posts.objects.filter(photos=h).first()
                i.hometown = hometown
                i.save()
            if request.POST.get('currentplace'):
                currentplace = request.POST.get('currentplace')
                # v = posts.objects.filter(photos=h).first()
                i.current_place = currentplace
                i.save()

        
        if i.male:
            l="his"

        if i.female:
            l="her"

        active = True
        t = i.post
        
        a = student.objects.filter(id__in=i.f_list)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        if len(i.cover_pic)>0:
            j=i.cover_pic[len(i.cover_pic)-1]
        else:
            j=0
        if len(i.prof_pic)>0:
            s=i.prof_pic[len(i.prof_pic)-1]
        else:
            s=0

        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"








        
        context = {
            "title" : title,
            "count" : i.id,
            "t" : t,
            "l" : l,
            "f" : f,
            "i" : i,
            "e" : i.f_list,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "s" : s,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "page" : pc,
            #"form" : form
        }


        return render(request,'profile1.html',context)



        #response = render_to_response("profile1.html",context)


        # visits = int(request.COOKIES.get('visits', '0'))

        # # Does the cookie last_visit exist?
        # if 'last_visit' in request.COOKIES:
        #     # Yes it does! Get the cookie's value.
        #     last_visit = request.COOKIES['last_visit']
        #     # Cast the value to a Python date/time object.
        #     last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        #     # If it's been more than a day since the last visit...
        #     if (datetime.now() - last_visit_time).seconds > 10:
        #         # ...reassign the value of the cookie to +1 of what it was before...
        #         response.set_cookie('visits', visits+1)
        #         print visits
        #         # ...and update the last visit cookie, too.
        #         response.set_cookie('last_visit', datetime.now())
        # else:
        #     # Cookie last_visit doesn't exist, so create it to the current date/time.
        #     response.set_cookie('last_visit', datetime.now())
        #     print "set_cookie"








        #return response





    else:
        return HttpResponse("Login first!!!")


    
    


def Upload(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    k=len(i.post)+len(i.prof_pic)+len(i.cover_pic)
    p="/home/raftar/ttl/baba1/media_in_env/media_root/file_p" + str(c)+".jpg"

    #os.rename(p,p+".k"+str(k+1))
    i.prof_pic.append(k+1)
    i.save()
    m=posts(identity=c,photos=k+1,hits=0,comments=0,profile_picture=True)
    m.save()
    for count, x in enumerate(request.FILES.getlist("files")):
        def process(f):
            with open('/home/raftar/ttl/baba1/media_in_env/media_root/file_p' + str(c)+'_'+str(k+1)+".jpg", 'wb+') as destination:
                with open('/home/raftar/ttl/baba1/media_in_env/media_root/file_' + str(c)+'.jpg', 'wb+') as destination1:
                    for chunk in f.chunks():
                        destination.write(chunk)
                        destination1.write(chunk)

        process(x)

        infile = '/home/raftar/ttl/baba1/media_in_env/media_root/file_' + str(c)+'.jpg'

        outfile = os.path.splitext(infile)[0] + ".thumbnail"
        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail((128,128),Image.ANTIALIAS)
                im.save(outfile, "JPEG")
            except IOError:
                print "cannot create thumbnail for", infile
        


    return HttpResponseRedirect('/prof/')


def Upload_cover(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    l=len(i.post)+len(i.prof_pic)+len(i.cover_pic)
    #p="/home/raftar/ttl/baba1/media_in_env/media_root/file_c" + str(c)+'.jpg'
    #os.rename(p+".0",p+"."+str(l+1))
    i.cover_pic.append(l+1)
    i.save()
    m=posts(identity=c,photos=l+1,hits=0,comments=0,cover_picture=True)
    m.save()
    for count, x in enumerate(request.FILES.getlist("files")):
        def process(f):
            with open('/home/raftar/ttl/baba1/media_in_env/media_root/file_c' + str(c)+'_'+str(l+1)+'.jpg', 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

        process(x)

        infile = '/home/raftar/ttl/baba1/media_in_env/media_root/file_c' + str(c) +'_'+str(l+1) +'.jpg'

        outfile = os.path.splitext(infile)[0] + ".thumbnail"


        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail((300,300),Image.ANTIALIAS)
                im.save(outfile, "JPEG")
            except IOError:
                print "cannot create thumbnail for", infile
        
        do=1
        return HttpResponseRedirect('/covfix/')


def Upload_post(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    t = student.objects.get(id=c)
    p=len(t.post)+len(t.prof_pic)+len(t.cover_pic)
    t.post.append(p+1)
    t.save()
    cat=""
    if request.method=='POST':
        if request.POST.get("category"):
            cat = request.POST["category"]
        
        if request.POST["say"]:
            say= request.POST["say"]
            if cat:
                say=say+" #"+cat+" "
           

            if request.FILES.getlist("files"):

                m=posts(identity=c,photos=p+1,hits=0,comments=0,say=say,category=cat)
                m.save()
            else:
                m=posts(identity=c,photos=-p-1,hits=0,comments=0,say=say,category=cat)
                m.save()

            if "#" in say:
                said=say.split("#")
                saying=said[1].split(" ")
                if intrests.objects.filter(Q(intrest_name__iexact=saying[0])|Q(intrest_name__iexact=cat)):
                    link_intrst=intrests.objects.get(Q(intrest_name__iexact=saying[0])|Q(intrest_name__iexact=cat))
                    link_intrst.intrest_posts.append(m.id)
                    link_intrst.save()

                if create_city.objects.filter(Q(city_name__iexact=saying[0])|Q(city_name__iexact=cat)):
                    link_city=create_city.objects.get(Q(city_name__iexact=saying[0])|Q(city_name__iexact=cat))
                    link_city.city_posts.append(m.id)
                    link_city.save()

                if create_subjects.objects.filter(Q(subjects_name__iexact=saying[0])|Q(subjects_name__iexact=cat)):
                    link_subjects=create_subjects.objects.get(Q(subjects_name__iexact=saying[0])|Q(subjects_name__iexact=cat))
                    link_subjects.subjects_posts.append(m.id)
                    link_subjects.save()
            if request.POST.get("onoffswitch"):
                if request.POST["onoffswitch"]=="on":
                    print request.POST["onoffswitch"]
                    m.anonymous=True
                    m.save()
                else:
                    m.anonymous=False
                    m.save()

            
        elif request.FILES.getlist("files"):
            m=posts(identity=c,photos=p+1,hits=0,comments=0,category=cat)
            m.save()
            if cat:
                if intrests.objects.filter(intrest_name__iexact=cat):
                    link_intrst=intrests.objects.get(Q(intrest_name__iexact=saying[0])|Q(intrest_name__iexact=cat))
                    link_intrst.intrest_posts.append(m.id)
                    link_intrst.save()

                if create_city.objects.filter(city_name__iexact=cat):
                    link_city=create_city.objects.get(Q(city_name__iexact=saying[0])|Q(city_name__iexact=cat))
                    link_city.city_posts.append(m.id)
                    link_city.save()

                if create_subjects.objects.filter(subjects_name__iexact=cat):
                    link_subjects=create_subjects.objects.get(Q(subjects_name__iexact=saying[0])|Q(subjects_name__iexact=cat))
                    link_subjects.subjects_posts.append(m.id)
                    link_subjects.save()
            if request.POST.get("onoffswitch"):
                if request.POST["onoffswitch"]=="on":
                    print request.POST["onoffswitch"]
                    m.anonymous=True
                    m.save()

                else:
                    m.anonymous=False
                    m.save()

        else:
            return HttpResponse("No post to upload")

    
    

    for count, x in enumerate(request.FILES.getlist("files")):
        def process(f):
            with open('/home/raftar/ttl/baba1/media_in_env/media_root/file_' + str(c)+ '_'+ str(p+1)+'.jpg', 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

        process(x)

    return HttpResponseRedirect('/prof/')

    infile = '/home/raftar/ttl/baba1/media_in_env/media_root/file_' + str(c) +'_'+str(p+1) +'.jpg'

    outfile = os.path.splitext(infile)[0] + ".thumbnail"
    if infile != outfile:
        try:
            im = Image.open(infile)
            im.thumbnail((128,128),Image.ANTIALIAS)
            im.save(outfile, "JPEG")
        except IOError:
            print "cannot create thumbnail for", infile



def register_user(request):

    form = student_form(request.POST or None)
    title="Welcome"
    context = {
        "form" : form,
        "title" : title
    }


    if request.user.is_authenticated():
        if not request.user.is_superuser:
            return HttpResponseRedirect('/prof/')
        # return render(request,'.html',context)
    #     return HttpResponse("user.is_authenticated!!!")
        # else:
        #     return HttpResponseRedirect('/prof/')
    if request.POST:
        username=request.POST['username']
        password=request.POST['password']
        password1=request.POST['confirm_password']
        email=request.POST['email']
        
        first_name=request.POST['first_name']
        last_name=request.POST['last_name']


        

        date=request.POST.get('date')
        month=request.POST.get('month')
        year=request.POST.get('year')

        male = request.POST.get('male')

        # female = request.POST.get('male')
        # other = request.POST.get('m')
        check = request.POST.get('check')

        USER=User.objects.filter(username=username)

        if USER:
            #messages.error(request,"User already exist")
            #redirect here
            return HttpResponse("User already exist!!!")
        if password==password1:
            if check:
                User.objects.create_user(username=email,password=password,email=email)
                Us=User.objects.get(username=email)
                #### 
                a=student.objects.all()
                p=[]
                for c in a:
                    p.append(c.id)
                    
                s=student(user=Us,first_name=first_name,last_name=last_name,date=date,month=month,year=year,post=[],add_f_list=p)
                s.save()
                s.prof_pic.append("0")

                infile = '/home/raftar/ttl/baba1/media_in_env/media_root/default.jpg'
                outfile = '/home/raftar/ttl/baba1/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'

                
                if infile != outfile:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

                infile1 = '/home/raftar/ttl/baba1/media_in_env/media_root/defaultcover.jpg'
                outfile1 = '/home/raftar/ttl/baba1/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

                
                if infile1 != outfile1:
                    try:
                        im = Image.open(infile1)
                        im.thumbnail((1600,900),Image.ANTIALIAS)
                        im.save(outfile1, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile1

                thumb(outfile)
                thumb(outfile1)

                for c in a:
                    c.add_f_list.append(s.id)
                    c.save()
                
                if male == "male":
                    s.male = True
                    s.save()
                if male=="female":
                    s.female = True
                    s.save()
                if male=="other":
                    s.other = True
                    s.save()
            else :
               return HttpResponse("Check out the terms and conditions!!!") 

    return render(request,"home.html",context)


def login_user(request):
    state = ""
    username = password = ''
    if request.user.is_authenticated():
        #state="Already logged in, "+request.user.username
        if request.user.is_superuser:
            return HttpResponseRedirect('/admin')
        # profile = Profile.objects.get(user=request.user)
        # if(profile.level==1):
        #     return HttpResponseRedirect(reverse('programme_list', args=(str(dept.objects.filter(head=profile)[0].dept_code),)))
        return HttpResponseRedirect('/prof/')
    if request.POST:
        username = request.POST['usernam']
        password = request.POST['pass']

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_superuser:
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                i.online=True
                i.save()

                return HttpResponseRedirect('/admin')
            elif user.is_active:
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                i.online=True
                i.save()
                # profile = Profile.objects.get(user=request.user)
                # if profile.level==1:
                #     return HttpResponseRedirect(reverse('programme_list', args=(str(dept.objects.filter(head=profile)[0].dept_code),)))
                # elif profile.level==8:
                #     return HttpResponseRedirect('/Faculty/Dashboard')
                return HttpResponseRedirect('/prof/')
            else:
                # state = "Your account is not active, please contact the site admin."
                # messages.error(request,state)
                return HttpResponse("Your account is not active, please contact the site admin.")
        else:
            # state = "Your username and/or password were incorrect."
            # messages.error(request,state)
            return HttpResponse("Your username and/or password were incorrect.")


    return render(request, 'login.html',{})


def logout_user(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        i.online=False
        i.logout=datetime.now().replace(microsecond=0)
        i.save()
        logout(request)
        state="You have been successfully logged out."
        messages.success(request,state)
    else:
        state="Oops ! You are already logged out ! Try logging in again"
        messages.error(request,state)
    return render(request, 'logout.html')



def Hit(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    k=""

    if request.method=='POST':
        h = str(request.POST.get('slug'))
        j = str(request.POST.get('sluge'))


        v = posts.objects.filter(identity=j,photos=h).first()
        
        
        if c in v.hitters:
            v.hits = v.hits-1
            i=v.hitters.index(c)
            del v.hitters[i]
            k="Hit"
            
            
            
            
        else:
            v.hits = v.hits+1
            v.hitters.append(c)
            k="Unhit"
            if j!=str(c):
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1

                p.fnotify="<tr><td width='15%'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td width='70%'><a href='../"+str(c)+"' > <b style='color:black;'>"+str(i.first_name)+"</b></a> hitted your <a href='../prof/#"+str(j)+"."+str(h)+"liked'>post</a></td><td width='15%'><img src='/media/file_"+str(v.identity)+"_"+str(v.photos)+".thumbnail' style='width:50px;height:50px;'></td></tr><br>"+p.fnotify
                p.save()
                
        v.save()
        
        z=str(v.hits)
        ctx= {'z':z,'k':k,}
    
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def comment(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id

    if request.method=='POST':
        h = request.POST.get('coment')
        k = request.POST.get('slug')
        j = request.POST.get('sluge')
        s = posts.objects.filter(identity=j,photos=k).first()
        m = postcomment(c_identity=i.id,c_photos=s.id,c_say=h,c_intity=j)
        m.save()
        s.comments=s.comments+1
        s.comnt.append(c)
        s.save()
        n=s.comments
        l = student.objects.filter(id=m.c_identity).first()
        z="/media/file_"+str(l.id)+".thumbnail"+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
        if j!=str(c):
            p=student.objects.filter(id=j).first()
            p.n_notif=p.n_notif+1

            p.fnotify="<tr><td width='15%'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td width='85%'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><br>"+p.fnotify
            p.save()

    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z,'n':n}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def opine(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    if request.method=='POST':
        j = request.POST.get('sluge')
        k = request.POST.get('slug')
        s = posts.objects.filter(identity=j,photos=k).first()
        m = postcomment.objects.filter(c_intity=j,c_photos=s.id)
        if m:
            for c in m:
                l = student.objects.filter(id=c.c_identity).first()
                z=z+"/media/file_"+str(l.id)+".thumbnail"+"@#$"+l.first_name+" "+l.last_name+"@#$"+c.c_say+"@#$@#$"
            
        else :
            z="NO"


    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z}
    # ctx = 'yes'
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def pr(request,prof_id=5):
    
    b= student.objects.filter(user=request.user).first()
    e=student.objects.filter(id__in=b.f_list)
    i=student.objects.get(id=prof_id)
    a=student.objects.filter(id__in=i.f_list)
    if not i:
        return HttpResponse("No such Url exists!!!")
    c = i.id
    f = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).order_by("-id")
    count = str(c)
    title = i.first_name+" "+i.last_name
    if i.male:
        l="his"

    if i.female:
        l="her"

    
    t = i.post

    if len(i.cover_pic)>0:
        j=i.cover_pic[len(i.cover_pic)-1]
    else:
        j=0

    page=intrests.objects.filter(intrest_name=i.current_intrest).first()
    if page:
        pc=page.intrest_category
    else:
        pc="pg"
    

    
    context = {
        "title" : title,
        "count" : count,
        "t" : t,
        "l" : l,
        "f" : f,
        "i" : i,
        "j" : j,
        "a" : a,
        "e" : e,
        "page" : pc,
        "cov_r":i.cov_r,
        "cov_g":i.cov_g,
        "cov_b":i.cov_b,
        #"form" : form
    }
    return render(request,"profile10.html",context)

def all_pr(request):
    a=student.objects.all()
    i=student.objects.filter(user=request.user).first()
    c = i.id
    f = posts.objects.filter(identity=c)
    count = str(c)
    title = i.first_name+" "+i.last_name
    t = i.post
    context = {
        "title" : title,
        "count" : count,
        "t" : t,
        #"l" : l,
        "f" : f,
        "i" : i,
        "a" : a,
        "b" : i.add_f_list,
        "d" : i.accept_f_list,
        "e" : i.f_list,
        #"form" : form
    }



    return render(request,"profile9.html",context)

def send_request(request,req_id=1):
    i=student.objects.filter(user=request.user).first()
    j=student.objects.filter(id=req_id).first()
    if j.id in i.add_f_list:
        j.accept_f_list.append(i.id)
        #i.sent_f_list.append(j.id)
        l=j.add_f_list.index(i.id)
        del j.add_f_list[l]
        k=i.add_f_list.index(j.id)
        del i.add_f_list[k]
        j.n_frqst=j.n_frqst+1
        i.save()
        j.save()

    return HttpResponseRedirect('/friends')

def accept_request(request,req_id=1):
    i=student.objects.filter(user=request.user).first()
    j=student.objects.filter(id=req_id).first()
    if j.id in i.accept_f_list:
        j.f_list.append(i.id)
        i.f_list.append(j.id)
        # l=j.sent_f_list.index(i.id)
        # del j.sent_f_list[l]
        k=i.accept_f_list.index(j.id)
        del i.accept_f_list[k]
        i.save()
        j.save()

    return HttpResponseRedirect('/friends')

def del_request(request,req_id=1):
    i=student.objects.filter(user=request.user).first()
    j=student.objects.filter(id=req_id).first()
    if j.id in i.accept_f_list:
        k=i.accept_f_list.index(j.id)
        del i.accept_f_list[k]
        i.save()
        

    return HttpResponseRedirect('/halua/get/all')
def interests(request):
    a=student.objects.all()
    i=student.objects.filter(user=request.user).first()
    c = i.id
    f = posts.objects.filter(identity=c)
    count = str(c)
    title = i.first_name+" "+i.last_name
    t = i.post
    context = {
        "title" : title,
        "count" : count,
        "t" : t,
        #"l" : l,
        "f" : f,
        "i" : i,
        "a" : a,
        "b" : i.add_f_list,
        "d" : i.accept_f_list,
        "e" : i.f_list,
        "cov_r":i.cov_r,
        "cov_g":i.cov_g,
        "cov_b":i.cov_b,
        #"form" : form
    }



    return render(request,"interest.html",context)

def photo(request):
    a=student.objects.all()
    i=student.objects.filter(user=request.user).first()
    c = i.id
    f = posts.objects.filter(identity=c)
    count = str(c)
    title = i.first_name+" "+i.last_name
    t = i.post
    context = {
        "title" : title,
        "count" : count,
        "t" : t,
        #"l" : l,
        "f" : f,
        "i" : i,
        "a" : a,
        "b" : i.add_f_list,
        "d" : i.accept_f_list,
        "e" : i.f_list,
        "cov_r":i.cov_r,
        "cov_g":i.cov_g,
        "cov_b":i.cov_b,
        #"form" : form
    }




    return render(request,"photos.html",context)


def frn(request):
    a=student.objects.all()
    i=student.objects.filter(user=request.user).first()
    c = i.id
    h=student.objects.filter(id__in=i.f_list)
    b=student.objects.filter(id__in=i.add_f_list)
    d=student.objects.filter(id__in=i.accept_f_list)
    f = posts.objects.filter(identity=c)
    count = str(c)
    title = i.first_name+" "+i.last_name
    t = i.post
    context = {
        "title" : title,
        "count" : count,
        "t" : t,
        #"l" : l,
        "f" : f,
        "i" : i,
        "a" : a,
        "b" : b,
        "d" : d,
        "e" : i.f_list,
        "h" : h,
        "cov_r":i.cov_r,
        "cov_g":i.cov_g,
        "cov_b":i.cov_b,
        #"form" : form
    }


    return render(request,"freinds.html",context)

def Msg(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id

    if request.method=='POST':
        k = request.POST.get('slug')
        j = request.POST.get('sluge')
        m = message(m_id=c,m_friend=j,m_message=k)
        m.save()
        p=student.objects.filter(id=j).first()
        if not i.id in p.msg_list:
            p.msg_list.append(i.id)
            p.n_msg=p.n_msg+1
            
        else:
            p.msg_list.remove(i.id)
            p.msg_list.append(i.id)
            p.n_msg=p.n_msg+1

        p.fmsg=""
        for x in p.msg_list:
            msgs=message.objects.filter(m_id=x,m_friend=j).last()
            y=student.objects.filter(id=x).first()
            p.fmsg="<img src='/media/file_"+str(y.id)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'>"+"<b>"+str(y.first_name)+"</b>:"+str(msgs.m_message)+"<br>"+p.fmsg

        
        p.save()
        
        # l = student.objects.filter(id=m.c_identity).first()
        # z="/media/file_"+str(l.id)+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
        z="<img src='/media/file_"+str(m.m_id)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'>"+"  "+m.m_message

    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')



def Msglist(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""

    if request.method=='POST':
        j = request.POST.get('sluge')
        m = message.objects.filter(Q(m_id=c,m_friend=j) | Q(m_id=j,m_friend=c))
        # n = message.objects.filter(m_id=j,m_friend=c)
        # l = student.objects.filter(id=m.c_identity).first()
        # z="/media/file_"+str(l.id)+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
        for l in m:
            if not l.m_admin:
                if l.m_id==i.id:
                    z=z+"<div class='full'><p class='sender'>"+l.m_message+"</p></div><br>"
                else:
                    z=z+"<div><img src='/media/file_"+str(l.m_id)+".thumbnail' style='width:35px;height:35px;border-radius:35px;'>"+"<p class='receiver'>"+l.m_message+"<br></p></div><br>"
            else:
                z=z+"<center clss='admin'>"+l.m_message+"</center>"


        # for k in n:
        #     z=z+"<img src='/media/file_"+str(k.m_id)+"' style='width:50px;height:50px;border-radius:50px;'>"+"  "+k.m_message+"<br>"

    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Frqst(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    i.n_frqst=0
    k = i.accept_f_list
    if len(k):
        for l in k:
            j=student.objects.filter(id=l).first()
            z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a friend request.</td></tr>"

            #<tr><td width="55px">
            # <img src="{%static 'image/pro.jpg'%}" style="width:50px;height:50px;border-radius:50px;"></td><td>
            # Mark Zuckerburg sent you a friend request
            # <br>
            # Accept!!</td>
    else:
        z="No request.."
        
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Fmsg(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    if i.fmsg:
        i.n_msg=0
        z = i.fmsg
    # if len(k):
    #     for l in k:
    #         j=student.objects.filter(id=l).first()
    #         z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+"' style='width:50px;height:50px;border-radius:50px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a message.</td></tr>"

            #<tr><td width="55px">
            # <img src="{%static 'image/pro.jpg'%}" style="width:50px;height:50px;border-radius:50px;"></td><td>
            # Mark Zuckerburg sent you a friend request
            # <br>
            # Accept!!</td>
    else:
        z="No recent messages"
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Fnotif(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    k="<tr><td width='15%'>"
    if i.fnotify:
        i.n_notif=0
        z = i.fnotify
        
        s=i.fnotify.split(k)
        if len(s)>6:
            i.fnotify=""
            for n in s[1:5]:
                i.fnotify=i.fnotify+k+n
    # if len(k):
    #     for l in k:
    #         j=student.objects.filter(id=l).first()
    #         z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+"' style='width:50px;height:50px;border-radius:50px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a message.</td></tr>"

            #<tr><td width="55px">
            # <img src="{%static 'image/pro.jpg'%}" style="width:50px;height:50px;border-radius:50px;"></td><td>
            # Mark Zuckerburg sent you a friend request
            # <br>
            # Accept!!</td>
    else:
        z="No recent notifications"
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def homepg(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=datetime.now().replace(microsecond=0)
        i.save()
        c = i.id
        capt=""
        tm=""
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="pg":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by('-id')
            
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    # v = posts.objects.filter(photos=h).first()
                    i.current_intrest = ci
                    i.save()
                    find=intrests.objects.filter(intrest_name=ci).first()

                    if not c in find.intrest_memb_id:
                        find.intrest_memb_id.append(c)
                        find.save()

                    if find.intrest_category=="pg":
                        if not player.objects.filter(p_id=c,p_game=ci):
                            p=player(p_id=c,p_game=ci)
                            p.save()

                    return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
            if play:
                tm=team.objects.filter(id=play).first()
                if tm.team_capt==i.id:
                    capt=c

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:

                last_visit_time = k.logout.tzinfo
                time=(datetime.now(last_visit_time)-k.logout).seconds
                tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = student.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "count" : count,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"homepage.html",context)
            

        else:
            state="Make any team game as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homepage.html",{})
            


    else:
        return HttpResponse("Login first!!!")


    
    


def database(request):
    a=posts.objects.all()
    for c in a:
        c.comnt=[]
        c.comments=0
        z=postcomment.objects.filter(c_intity=c.identity,c_photos=c.photos)
        for k in z:

            c.comnt.append(k.c_identity)
        c.comments=len(c.comnt)
        c.save()

    return HttpResponse("database Updated")


def group(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(identity=c)
        count = str(c)
        title = i.first_name+" "+i.last_name
        if request.method=='POST':
            group_name=request.POST['group_name']
            group_lat = request.POST['group_lat']
            group_long=request.POST['group_long']
            m=create_group(cg_name=group_name,cg_lat=group_lat,cg_long=group_long,cg_memb_id=[c])
            m.save()
            p=m.id
            i.groups.append(p)
            i.save()
        
            for count, x in enumerate(request.FILES.getlist("files")):
                def process(f):
                    with open('/home/raftar/ttl/baba1/media_in_env/media_root/file_g' + str(c)+ '_'+ str(p)+".jpg", 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)

                process(x)

                infile='/home/raftar/ttl/baba1/media_in_env/media_root/file_g' + str(c)+ '_'+ str(p)+".jpg"
                thumb(infile)


            return HttpResponseRedirect("/homepg/")

        if i.male:
            l="his"

        if i.female:
            l="her"

        active = True
        t = i.post
        
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "count" : count,
            "t" : t,
            "l" : l,
            "f" : f,
            "i" : i,
            "e" : i.f_list,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
        return render(request,"group.html",context)
    else:
        return HttpResponse("Login first!!!")
    



def Rgba(request):
    i=student.objects.filter(user=request.user).first()
    if request.method=='POST':
        red=request.POST['r']
        green = request.POST['g']
        blue=request.POST['b']
        i.cov_r=red
        i.cov_g=green
        i.cov_b=blue
        i.save()

    ctx="("+str(i.cov_r)+","+str(i.cov_g)+","+str(i.cov_b)+")"
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Joingroup(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    g=create_group.objects.exclude(id__in=i.groups)
    if request.method=='POST':
        gr=request.POST.getlist("sel_group")
        print gr
        for k in gr:
            i.groups.append(int(float(k)))
            l=create_group.objects.get(id=int(float(k)))
            l.cg_memb_id.append(c)
            l.save()


        
        i.save()
        return HttpResponseRedirect("/prof/")

    p=i.n_frqst
    q=i.n_msg
    r=i.n_notif
    context={
        "p" : p,
        "q" : q,
        "r" : r,
        "g" :g,
    }

    return render(request,"nogrp.html",context)


def Share(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z="NOT DONE"

    if request.method=='POST':
        h = str(request.POST.get('slug'))
        j = str(request.POST.get('sluge'))
        p = posts.objects.filter(identity=j,photos=h).first()
        i.shared_posts.append(p.id)
        i.save()
        z="DONE"


    ctx={"z":z}

    return HttpResponse(json.dumps(ctx), content_type='application/json')




def Addfriend(request):
    i=student.objects.filter(user=request.user).first()
    f=student.objects.filter(id__in=i.add_f_list)
    if request.method=='POST':
        gr=request.POST.getlist("sel_group")
        for k in gr:
            j=student.objects.filter(id=k).first()
            j.accept_f_list.append(i.id)
            #i.sent_f_list.append(j.id)
            l=j.add_f_list.index(i.id)
            del j.add_f_list[l]
            k=i.add_f_list.index(j.id)
            del i.add_f_list[k]
            j.n_frqst=j.n_frqst+1
            i.save()
            j.save()

        return HttpResponseRedirect("/prof/")




    p=i.n_frqst
    q=i.n_msg
    r=i.n_notif
    context={
        "p" : p,
        "q" : q,
        "r" : r,
        "f" : f,
    }

    return render(request,"nofrnd.html",context)


def Create_city(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(identity=c)
        count = str(c)
        title = i.first_name+" "+i.last_name
        if request.method=='POST':
            city_name=request.POST['city_name']
            city_lat = request.POST['city_lat']
            city_long=request.POST['city_long']
            city_des = request.POST['city_descript']
            m=create_city(city_name=city_name,city_lat=city_lat,city_long=city_long,city_descript=city_des)
            m.save()
            p=m.id
            
        
        for count, x in enumerate(request.FILES.getlist("files")):
            def process(f):
                with open('/home/raftar/ttl/baba1/media_in_env/media_root/file_city' + '_'+ str(p)+".jpg", 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)

            process(x)

            infile='/home/raftar/ttl/baba1/media_in_env/media_root/file_city' + '_'+ str(p)+".jpg"
            thumb(infile)


        if i.male:
            l="his"

        if i.female:
            l="her"

        active = True
        t = i.post
        
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "count" : count,
            "t" : t,
            "l" : l,
            "f" : f,
            "i" : i,
            "e" : i.f_list,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
        return render(request,"city.html",context)
    else:
        return HttpResponse("Login first!!!")
    



def City(request):
    city_name = request.GET.get('q','')
    j = create_city.objects.filter(city_name__iexact=city_name).first()
    i=student.objects.filter(user=request.user).first()
    c = i.id
    f = posts.objects.filter(id__in=j.city_posts).order_by("-id")
    count = str(c)
    title = i.first_name+" "+i.last_name
    a = student.objects.all()
    p=i.n_frqst
    q=i.n_msg
    r=i.n_notif
    page=intrests.objects.filter(intrest_name=i.current_intrest).first()
    if page:
        pc=page.intrest_category
    else:
        pc="pg"

    form = trip_form(request.POST or None)
    if request.method=="POST":
        t_name = request.POST["tourname"]
        t_from = request.POST["trip_from"]
        t_to = request.POST["trip_to"]
        t_mode = request.POST["mode"]
        t_memb = request.POST["memb"]
        t=trip(trip_name=t_name,trip_from=t_from,trip_to=t_to,trip_mode=t_mode,trip_n_membs=t_memb,trip_city=j.city_name)
        t.trip_memb.append(i.id)
        t.save()
        j.city_memb_id.append(i.id)
        j.save()
        state="Make any sports as ur Intrest!!!"
        messages.success(request,state)

    context = {
        "title" : title,
        "count" : count,
        "i" : i,
        "a" : a,
        "e" : i.f_list,
        "p" : p,
        "q" : q,
        "r" : r,
        "j" : j,
        "f" : f,
        "page": pc,
        "form":form,
    }

    return render(request,"place.html",context)



def create_new_intrest(request):
    in_names=request.GET.get('q','')
    in_name=in_names.split(".")
    if in_name:
        m = intrests(intrest_name=in_name[0],intrest_category=in_name[1])
        m.save()
        return HttpResponse("DONE")
    else:
        return HttpResponse("Nothing to Create Intrest")


def Open_intrest(request):
    in_name=request.GET.get('q','')
    m = intrests.objects.get(intrest_name__iexact=in_name)
    i=student.objects.filter(user=request.user).first()
    c = i.id
    f = posts.objects.filter(id__in=m.intrest_posts).order_by("-id")
    count = str(c)
    title = i.first_name+" "+i.last_name
    a = student.objects.all()
    p=i.n_frqst
    q=i.n_msg
    r=i.n_notif
    k=str(m.intrest_name)+".html"
    l=student.objects.filter(id__in=m.intrest_memb_id)
    context = {
        "title" : title,
        "count" : count,
        "i" : i,
        "a" : a,
        "e" : i.f_list,
        "p" : p,
        "q" : q,
        "r" : r,
        "m" : m,
        "f" : f,
        "l" : l,
    }

    return render(request,k,context)

def Search(request):
    if request.method=='POST':
        k=request.POST.get('searchbox1')
        print k
        if student.objects.filter(first_name__iexact=k):
            m=student.objects.get(first_name__iexact=k)
            return HttpResponseRedirect("/"+str(m.id)+"/")
        
        elif intrests.objects.filter(intrest_name__iexact=k):
            n=intrests.objects.get(intrest_name__iexact=k)
            return HttpResponseRedirect("/open_intrest/?q="+str(n.intrest_name))
        
        elif create_city.objects.filter(city_name__iexact=k):
            o=create_city.objects.get(city_name__iexact=k)
            return HttpResponseRedirect("/city/?q="+str(o.city_name))

        elif create_subjects.objects.filter(subjects_name__iexact=k):
            o=create_subjects.objects.get(subjects_name__iexact=k)
            return HttpResponseRedirect("/subject/?q="+str(o.subjects_name))
        else:
            return HttpResponse("No match found...")

        

def Searching(request):
    z=""
    s=""
    srcnt=0
    if request.method=='POST':
        k=request.POST.get('typed')
        print k

        if student.objects.filter(first_name__istartswith=k):
            m=student.objects.filter(first_name__istartswith=k)
            s=m.first().first_name
            for c in m:
                srcnt=srcnt+1
                z=z+"<a href='/"+str(c.id)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/"+str(c.id)+"\"' style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/file_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.first_name)+" "+str(c.last_name)+"</b></h4>Person<br>16 Mutual Friends</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"


        if intrests.objects.filter(intrest_name__istartswith=k):
            n=intrests.objects.filter(intrest_name__istartswith=k)
            if not s:
                s=n.first().intrest_name
            for c in n:
                srcnt=srcnt+1
                z=z+"<a href='/open_intrest/?q="+str(c.intrest_name)+"'><div><tr id='srch"+str(srcnt)+"' onclick='document.location=\"/open_intrest/?q="+str(c.intrest_name)+"\"' style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/intrest/"+str(c.intrest_name)+".thumbnail' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.intrest_name)+"</b></h4>Intrest<br>"+str(len(c.intrest_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"
    
            

        if create_city.objects.filter(city_name__istartswith=k):
            o=create_city.objects.filter(city_name__istartswith=k)
            if not s:
                s=o.first().city_name
            for c in o:
                srcnt=srcnt+1
                z=z+"<a href='/city/?q="+str(c.city_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/city/?q="+str(c.city_name)+"\"' style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/file_city_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.city_name)+"</b></h4>Intrest<br>"+str(len(c.city_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"

        if create_subjects.objects.filter(subjects_name__istartswith=k):
            o=create_subjects.objects.filter(subjects_name__istartswith=k)
            if not s:
                s=o.first().subjects_name
            for c in o:
                srcnt=srcnt+1
                z=z+"<a href='/subject/?q="+str(c.subjects_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/subject/?q="+str(c.subjects_name)+"\"' style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/file_sub_"+str(c.id)+".jpg"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.subjects_name)+"</b></h4>Study<br>"+str(len(c.subjects_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"



        
        
        
    ctx= {'z':z,'s':s,}
    
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Searching_Intrest(request):
    z=""
    s=""
    
    if request.method=='POST':
        k=request.POST.get('typed')
        print k

        if intrests.objects.filter(intrest_name__istartswith=k):
            n=intrests.objects.filter(intrest_name__istartswith=k)
            s=n.first().intrest_name
            
            for c in n:
                z=z+"<div><tr style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/intrest/"+str(c.intrest_name)+".jpg' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.intrest_name)+"</b></h4>Intrest<br>"+str(len(c.intrest_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"
    


    ctx= {'z':z,'s':s,}
    
    return HttpResponse(json.dumps(ctx), content_type='application/json')


    
    


def Open(request):
    name=request.GET.get('q','')
    if intrests.objects.filter(intrest_name__iexact=name):
        return HttpResponseRedirect("/open_intrest/?q="+name)
    elif create_city.objects.filter(city_name__iexact=name):
        return HttpResponseRedirect("/city/?q="+name)
    

    else :
        return HttpResponse("No such page...")





def add_subjects(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(identity=c)
        count = str(c)
        title = i.first_name+" "+i.last_name
        if request.method=='POST':
            subject_name = request.POST['subject_name']
            subject_des = request.POST['subject_descript']

            m=create_subjects(subjects_name=subject_name,subjects_descript=subject_des)
            m.save()
            p=m.id
            
        
        for count, x in enumerate(request.FILES.getlist("files")):
            def process(f):
                with open('/home/raftar/ttl/baba1/media_in_env/media_root/file_sub' + '_'+ str(p)+".jpg", 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)

            process(x)   
            infile='/home/raftar/ttl/baba1/media_in_env/media_root/file_sub' + '_'+ str(p)+".jpg"
            thumb(infile)
        active = True
        t = i.post
        
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "count" : count,
            "t" : t,
            "f" : f,
            "i" : i,
            "e" : i.f_list,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
    else:
        return HttpResponse("Login first!!!")
    return render(request,"newsub.html",context)


def Subject(request):
    sub_name = request.GET.get('q','')
    j = create_subjects.objects.filter(subjects_name__iexact=sub_name).first()
    i=student.objects.filter(user=request.user).first()
    c = i.id
    f = posts.objects.filter(id__in=j.subjects_posts).order_by("-id")
    count = str(c)
    title = i.first_name+" "+i.last_name
    a = student.objects.all()
    p=i.n_frqst
    q=i.n_msg
    r=i.n_notif
    context = {
        "title" : title,
        "count" : count,
        "i" : i,
        "a" : a,
        "e" : i.f_list,
        "p" : p,
        "q" : q,
        "r" : r,
        "j" : j,
        "f" : f,
    }

    return render(request,"subject.html",context)

def Create_team(request):
    if request.user.is_authenticated() :
        t_n=""
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            t_n=request.POST['team_name']
            t_l=request.POST['team_logo']
            t_g=request.POST['team_group']
            print (t_g)
            m = team(team_name=t_n,team_capt=c,team_logo=t_l,team_category=i.current_intrest,team_group=t_g)
            m.team_memb.append(c)
            m.save()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            play.p_team=m.id
            play.save()
            

            k=team.objects.filter(team_name=t_n).first()
            return HttpResponseRedirect("/make_team/?q="+str(k.id))
        else:
            return HttpResponse("No team created....")

def Make_team(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if team.objects.filter(id=team_id):
            t=team.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            c = i.id
            b=[]
            
            count = str(c)
            title = i.first_name+" "+i.last_name
            gr=create_group.objects.filter(id=t.team_group).first()
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            memb = g.intrest_memb_id
            for p in memb:
                if p in gr.cg_memb_id:
                    playe=player.objects.filter(p_id=p).first()
                    if not playe.p_team:
                        b.append(p)

            a = student.objects.filter(id__in=b)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "count" : count,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"createteam.html",context)
        else:
            return HttpResponse("No such team found!!!")

    else:
        return HttpResponse("Login first!!!")
    

def Invite(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['teamid']
            m = team.objects.filter(id=l).first()
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1


                p.notify=""
                p.fnotify="<tr><td width='15%'><img src='/static/"+str(m.team_logo)+"' style='width:70%;height:50px;border-radius:50px;'></td><td width='85%'><a href='../view_team/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.team_name)+"</a></b> invited you to play in their team.<br> <a href='../accept/?q=Invite."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a><br></td>"+p.fnotify
                p.save()


        return HttpResponseRedirect("/homepg/")

def View_team(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if team.objects.filter(id=team_id):
            t=team.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            c = i.id
            if request.method=='POST':
                k=request.POST['player']
                if play.p_team:
                    return HttpResponse("Leave pevious team first!!!")
                else:
                    if not c in t.team_memb:
                        t.team_memb.append(c)
                        t.save()
                        print(t.id)
                        play.p_team=t.id
                        play.save()
                    return HttpResponseRedirect("/homepg/")


            count = str(c)
            title = i.first_name+" "+i.last_name
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            a = student.objects.filter(id__in=t.team_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "count" : count,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"visit.html",context)
        else:
            return HttpResponse("No such team found!!!")


    else:
        return HttpResponse("Login first!!!")


def Homedouble(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="double":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by('-id')
            
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            teams=team.objects.all()

            

            
            if request.method=='POST':
                
                if request.POST.get('currentintrest'):
                    ci = request.POST.get('currentintrest')
                    # v = posts.objects.filter(photos=h).first()
                    i.current_intrest = ci
                    i.save()
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = student.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "count" : count,
                "t" : t,
                "l" : l,
 