"""ram URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class   -based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import patterns,include, url
from django.conf.urls.static import static
from django.contrib import admin
from app1 import views

admin.autodiscover()


urlpatterns=[
    url(r'^$', views.register_user, name='home'),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^contact/$', views.contact, name='contact'),
    # url(r'^', include('registration.backends.default.urls')),
    url(r'^login/', views.login_user, name='login_user'),
    url(r'^logout/$', views.logout_user, name='logout_user'),
    url(r'^prof', views.prof, name='prof'),
    url(r'^halua/', include('app1.urls')),
    url(r'^(?P<prof_id>\d+)/','app1.views.pr'),
    url(r'^upload/', views.Upload, name='Upload'),
    url(r'^uploadpost/', views.Upload_post, name='Upload_post'),
    url(r'^hit/', views.Hit, name='Hit'),
    url(r'^msg/', views.Msg, name='Msg'),
    url(r'^cover/', views.Upload_cover, name='Upload_cover'),
    url(r'^msglist/', views.Msglist, name='Msg'),
    url(r'^comment/', views.comment, name='Comment'),
    url(r'^opine/', views.opine, name='Opine'),
    url(r'^sendrequest/(?P<req_id>\d+)/', views.send_request, name='S_Request'),
    url(r'^acceptrequest/(?P<req_id>\d+)/', views.accept_request, name='A_Request'),
    url(r'^deleterequest/(?P<req_id>\d+)/', views.del_request, name='D_Request'),
    #url(r'^accounts/', include('allaccess.urls')),
    # url(r'^edit_prof/', views.edit, name='edit'),
    url(r'^interest', views.interests, name='interests'),
    url(r'^photo', views.photo, name='photo'),
    url(r'^friends', views.frn, name='frn'),
    url(r'^homepg', views.homepg, name='homepg'),
    url(r'^frqst/', views.Frqst, name='Frqst'),
    url(r'^fmsg/', views.Fmsg, name='Fmsg'),
    url(r'^fnotif/', views.Fnotif, name='Fnotif'),
    url(r'^data/', views.database, name='Database'),
    url(r'^group/', views.group, name='group'),
    url(r'^rgba/', views.Rgba, name='Rgba'),
    url(r'^joingroup/', views.Joingroup, name='Joingroup'),
    url(r'^share/', views.Share, name='Share'),
    url(r'^addfriend/', views.Addfriend, name='Addfriend'),
    url(r'^create_city/', views.Create_city, name='Create_city'),
    url(r'^city/',views.City,name='City'),
    url(r'^new_intrest/',views.create_new_intrest,name='create_new_intrest'),
    url(r'^open_intrest/',views.Open_intrest,name='Open_intrest'),
    url(r'^search/',views.Search,name='Search'),
    url(r'^searching/',views.Searching,name='Searching'),
    url(r'^open/',views.Open,name='Open'),
    url(r'^add_sub/',views.add_subjects,name='add_subject'),
    url(r'^subject/',views.Subject,name='Subject'),
    url(r'^create_team/',views.Create_team,name='Create_team'),
    url(r'^searching_intrest/',views.Searching_Intrest,name='Searching_Intrest'),
    url(r'^make_team/',views.Make_team,name='Make_team'),
    url(r'^invite/',views.Invite,name='Invite'),
    url(r'^view_team/',views.View_team,name='View_team'),
    url(r'^homedouble/',views.Homedouble,name='Homedouble'),
    url(r'^hometour/',views.Hometour,name='Hometour'),
    url(r'^invitechallenger/',views.Invitechallenger,name='Invitechallenger'),
    url(r'^invitepartner/',views.Invitepartner,name='Invitepartner'),
    url(r'^leaveteam/',views.Leaveteam,name='Leaveteam'),
    url(r'^viewtraveller/',views.Viewtraveller,name='Viewtraveller'),
    url(r'^accept/',views.Accept,name='Accept'),
    url(r'^challenge/',views.Challenge,name='Challenge'),
    url(r'^homeband/',views.Homeband,name='Homeband'),
    url(r'^band/',views.Band,name='Band'),
    url(r'^thumb/',views.thumbonly,name='thumbonly'),
    url(r'^groupcht/',views.Groupcht,name='Groupcht'),
    url(r'^groupmsg/',views.Groupmsg,name='Groupmsg'),
    url(r'^teamcht/',views.Teamcht,name='Teamcht'),
    url(r'^teammsg/',views.Teammsg,name='Teammsg'),
    url(r'^sendnote/',views.sendnote,name='sendnote'),
    url(r'^homestartup/',views.Homestartup,name='Homestartup'),
    url(r'^updatergb/',views.updatergb,name='updatergb'),
    url(r'^thumbing/',views.thumbing,name='thumbing'),
    url(r'^defaulttheme/',views.defaulttheme,name='defaulttheme'),
    url(r'^covfix/',views.covfix,name='covfix'),
    url(r'^setting/',views.setting,name='setting'),
    url(r'^view_interest/',views.sports,name='sport'),
    url(r'^study/',views.study,name='study'),
    url(r'^travel/',views.travel,name='travel'),
    url(r'^about/',views.about,name='about'),
    url(r'^privacy/',views.privacy,name='privacy'),
    url(r'^homegroup/',views.homegroup,name='homegroup'),
    url(r'^homequick/',views.homequick,name='homequick'),
    url(r'^create_game/',views.Create_game,name='Create_game'),
    url(r'^make_game/',views.Make_game,name='Make_game'),
    url(r'^invitegamer/',views.Invitegamer,name='Invitegamer'),
    url(r'^view_game/',views.View_game,name='Viewgame'),
    url(r'^leavegame/',views.Leavegame,name='Leavegame'),
    url(r'^make_quick/',views.make_quick,name='make_quick'),
    url(r'^delnotif/',views.clearnotification,name='clearnotification'),
    url(r'^add_as_interest/',views.add_as_interest,name='add_as_interest'),
    

    #url(r'^accounts/', include('allauth.urls')),
    
    #url(r'^covchange/',views.covchange,name='covchange'),


]




if settings.DEBUG :
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)


