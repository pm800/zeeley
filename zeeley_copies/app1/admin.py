from django.contrib import admin ##
#from django.contrib.auth.models import User

# Register your models here.
from .forms import student_form, trip_form 
from .models import student, regs,posts, postcomment, message, create_group, intrests, create_city, create_subjects, player, team, trip, band, groupchat, teamchat, startup, game, create_cafe, create_film, feedback, quick, pizza, cab_share, Vote, user_device, notification , mm
 
class SignUpAdmin(admin.ModelAdmin):
	
	form = student_form
	#list_diplay = ["__unicode__", "timestamp", "updated"]
# 	#class Meta:
# 	#	model = SignUp

class TripAdmin(admin.ModelAdmin):
	
	form = trip_form

class studentAdmin(admin.ModelAdmin):
    list_display = ['logout','first_name','user']
    '''fieldsets = [
        (None,               {'fields': ['first_name']}),
    ]'''
class IntrestAdmin(admin.ModelAdmin):
    list_display = ['intrest_name','tagword','intrest_catelog','intrest_category']
class PlayerAdmin(admin.ModelAdmin):
    list_display = ['p_id','p_game','p_team']
class TeamAdmin(admin.ModelAdmin):
    list_display = ['team_name','team_group','team_category']
class GameAdmin(admin.ModelAdmin):
    list_display = ['game_name','game_group','game_category']
class NotificationAdmin(admin.ModelAdmin):
    list_display = ['notif_from','notif_to','post_time','purpose_id','purpose']
class PostsAdmin(admin.ModelAdmin):
    list_display = ['identity','photos','profile_picture','cover_picture','anonymous','category']
class TripTripAdmin(admin.ModelAdmin):
    list_display = ['trip_name','trip_mode']
#admin.site.register(student,SignUpAdmin) #+++
admin.site.register(mm)
admin.site.register(student,studentAdmin)
admin.site.register(regs)
admin.site.register(posts,PostsAdmin)
admin.site.register(postcomment)
admin.site.register(message)
admin.site.register(create_group)
admin.site.register(intrests,IntrestAdmin)
admin.site.register(create_city)
admin.site.register(create_subjects)
admin.site.register(player,PlayerAdmin)
admin.site.register(team,TeamAdmin)
admin.site.register(trip,TripTripAdmin)
admin.site.register(band)
admin.site.register(groupchat)
admin.site.register(teamchat)
admin.site.register(startup)
admin.site.register(game,GameAdmin)
admin.site.register(create_cafe)
admin.site.register(create_film)
admin.site.register(feedback)
admin.site.register(quick)
admin.site.register(pizza)
admin.site.register(cab_share)
admin.site.register(Vote)
admin.site.register(user_device)
admin.site.register(notification,NotificationAdmin)

#,SignUpAdmin
