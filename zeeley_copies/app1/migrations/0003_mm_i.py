# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0002_auto_20161217_1547'),
    ]

    operations = [
        migrations.AddField(
            model_name='mm',
            name='i',
            field=models.IntegerField(null=True),
        ),
    ]
