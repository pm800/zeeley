# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0003_mm_i'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mm',
            name='i',
        ),
        migrations.AddField(
            model_name='mm',
            name='h',
            field=models.IntegerField(default=0),
        ),
    ]
