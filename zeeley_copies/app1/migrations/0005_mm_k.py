# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0004_auto_20161217_1552'),
    ]

    operations = [
        migrations.AddField(
            model_name='mm',
            name='k',
            field=models.IntegerField(default=0),
        ),
    ]
