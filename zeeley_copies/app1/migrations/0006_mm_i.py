# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0005_mm_k'),
    ]

    operations = [
        migrations.AddField(
            model_name='mm',
            name='i',
            field=models.IntegerField(null=True),
        ),
    ]
