# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0006_mm_i'),
    ]

    operations = [
        migrations.AddField(
            model_name='mm',
            name='c',
            field=models.CharField(max_length=19, null=True),
        ),
    ]
