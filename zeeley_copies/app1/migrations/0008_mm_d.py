# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0007_mm_c'),
    ]

    operations = [
        migrations.AddField(
            model_name='mm',
            name='d',
            field=models.DateTimeField(null=True),
        ),
    ]
