# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0008_mm_d'),
    ]

    operations = [
        migrations.AddField(
            model_name='mm',
            name='year_in_school',
            field=models.CharField(default='FR', max_length=2, choices=[('FR', 'Freshman'), ('SO', 'Sophomore'), ('JR', 'Junior'), ('SR', 'Senior')]),
        ),
    ]
