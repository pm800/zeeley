# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0009_mm_year_in_school'),
    ]

    operations = [
        migrations.AddField(
            model_name='mm',
            name='mychoices',
            field=models.CharField(max_length=2, null=True, choices=[('Audio', (('vinyl', 'Vinyl'), ('cd', 'CD'))), ('Video', (('vhs', 'VHS Tape'), ('dvd', 'DVD'))), ('unknown', 'Unknown')]),
        ),
    ]
