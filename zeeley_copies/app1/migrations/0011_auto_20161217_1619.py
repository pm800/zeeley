# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0010_mm_mychoices'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mm',
            name='mychoices',
            field=models.CharField(max_length=2, null=True, choices=[('Audio', (('vinyl', ('Vinyl', 'vv')), ('cd', 'CD'), 'pm')), ('Video', (('vhs', 'VHS Tape'), ('dvd', 'DVD'))), ('unknown', 'Unknown')]),
        ),
    ]
