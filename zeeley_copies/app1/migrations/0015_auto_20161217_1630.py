# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0014_auto_20161217_1627'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mm',
            name='mychoices',
            field=models.CharField(max_length=2, null=True, choices=[('Audio', (('vinyl', 'Vinyl'), ('cd', 'CD'))), ('Video', (('vhs', 'VHS Tape'), ('dvd', 'DVD'))), ('unknown', 'Unknown')]),
        ),
    ]
