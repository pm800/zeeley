# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0015_auto_20161217_1630'),
    ]

    operations = [
        migrations.AddField(
            model_name='mm',
            name='mc',
            field=models.IntegerField(default=7, choices=[(1, 'one'), (2, 'two')]),
        ),
        migrations.AlterField(
            model_name='mm',
            name='mychoices',
            field=models.CharField(max_length=10, null=True, choices=[('Audio', (('vinyl', 'Vinyl'), ('cd', 'CD'))), ('Video', (('vhs', 'VHS Tape'), ('dvd', 'DVD'))), ('unknown', 'Unknown')]),
        ),
        migrations.AlterField(
            model_name='mm',
            name='year_in_school',
            field=models.CharField(default='JR', max_length=2, choices=[('FR', 'Freshman'), ('JR', 'Junior'), ('SR', 'Senior')]),
        ),
    ]
