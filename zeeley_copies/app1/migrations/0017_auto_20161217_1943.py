# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0016_auto_20161217_1803'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mm',
            name='h',
        ),
        migrations.RemoveField(
            model_name='mm',
            name='k',
        ),
        migrations.AddField(
            model_name='mm',
            name='f',
            field=models.FileField(upload_to=b'', blank=True),
        ),
        migrations.AddField(
            model_name='mm',
            name='im',
            field=models.ImageField(upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='mm',
            name='c',
            field=models.CharField(default='abc', max_length=19, null=True),
        ),
        migrations.AlterField(
            model_name='mm',
            name='d',
            field=models.DateTimeField(default=datetime.datetime(2016, 12, 17, 14, 13, 37, 213357, tzinfo=utc), null=True),
        ),
        migrations.AlterField(
            model_name='mm',
            name='i',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='mm',
            name='mc',
            field=models.IntegerField(default=2, choices=[(1, 'one'), (2, 'two')]),
        ),
    ]
