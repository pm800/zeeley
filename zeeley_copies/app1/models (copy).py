from django.db import models
from time import time
from django.contrib.auth.models import User
#from django.contrib.postgres.fields import ArrayField
from django import forms
from django.db import models
import ast
import datetime
from django.core.validators import RegexValidator
from django.utils import timezone
class ListField(models.TextField):
    #__metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)


    def from_db_value(self, value, expression, connection, context):
        if value is None:
            return value
        return ast.literal_eval(value)


    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return unicode(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)

# Create your models here.
def get_upload_file_name(instance, filename):
	return "uploaded_files/%s_%s" % (str(time()).replace('.','_'),filename)

# class StringListField(models.Field):



#     __metaclass__=models.SubfieldBase
#     SPLIT_CHAR = u'\v'
#     def __init__(self,*args,**kwargs):
#         self.internal_type=kwargs.pop('internal_type','CharField')
#         super(StringListField,self).__init__(*args,**kwargs)

#     def to_python(self,value):
#         if isinstance(value,list):
#             return value
#         if value is None:
#             return []
#         return value.split(self.SPLIT_CHAR)

#     def get_internal_type(self):
#         return self.internal_type

#     def get_db_prep_lookup(self, lookup_type, value):
#         #SQL WHERE
#         raise NotImplementedError()

#     def get_db_prep_save(self,value):
#         return self.SPLIT_CHAR.join(value)

#     def formfield(self,**kwargs):
#         assert not kwargs, kwargs
#         return forms.MultipleChoiceField(choices=self.choices)

        
class regs(models.Model):
    First_Name = models.CharField(max_length=30,null=True)
    Username = models.CharField(max_length=30)
    Last_Name = models.CharField(max_length=30,null=True)
    City = models.CharField(max_length=30,null=True)
    Date = models.IntegerField(default=0)
    Month = models.IntegerField(default=0)
    Year = models.IntegerField(default=0)
     
    email = models.EmailField()
    password = models.CharField(max_length=30)
    confirm_password = models.CharField(max_length=30)
    male = models.BooleanField(default=False)
    female = models.BooleanField(default=False)
    other = models.BooleanField(default=False)
    #t=models.FileField(upload_to=get_upload_file_name,blank=True)Sport = models






    
    S_Cricket = models.BooleanField(default=False)
    S_Badminton = models.BooleanField(default=False)
    S_Volleyball = models.BooleanField(default=False)
    S_Football = models.BooleanField(default=False)
    S_Chess = models.BooleanField(default=False)
    S_Hockey = models.BooleanField(default=False)
    S_Table_Tennis = models.BooleanField(default=False)
    S_Lawn_Tennis = models.BooleanField(default=False)
    S_Kabaddi = models.BooleanField(default=False)
    S_Kho_Kho = models.BooleanField(default=False)







    M_Guitar = models.BooleanField(default=False)
    M_Sitar = models.BooleanField(default=False)
    M_Drum = models.BooleanField(default=False)
    M_Tabla = models.BooleanField(default=False)
    M_Flute = models.BooleanField(default=False)
    M_Violin = models.BooleanField(default=False)
    M_Piano = models.BooleanField(default=False)
    M_Regional = models.BooleanField(default=False)
    M_Organ_Pad = models.BooleanField(default=False)
    M_Western = models.BooleanField(default=False)
    M_Classical = models.BooleanField(default=False)
    M_Vocals = models.BooleanField(default=False)
    M_Harmonium = models.BooleanField(default=False)



    T_Manali = models.BooleanField(default=False)
    T_Kullu = models.BooleanField(default=False)
    T_Switzerland = models.BooleanField(default=False)
    T_Kashmir = models.BooleanField(default=False)
    T_Shimla = models.BooleanField(default=False)
    T_London = models.BooleanField(default=False)
    T_Mumbai = models.BooleanField(default=False)
    T_Kolkata = models.BooleanField(default=False)
    T_Chennai = models.BooleanField(default=False)
    T_Delhi = models.BooleanField(default=False)
    T_Ooty = models.BooleanField(default=False)
    T_Goa = models.BooleanField(default=False)
    T_NewYork = models.BooleanField(default=False)
    T_Malaysia = models.BooleanField(default=False)
    T_Darjeeling = models.BooleanField(default=False)
    T_Nainital = models.BooleanField(default=False)
    T_Agra = models.BooleanField(default=False)
    T_Jaipur_Udaipur = models.BooleanField(default=False)
    T_Kanyakumari = models.BooleanField(default=False)
    T_Ajanta_Ellora = models.BooleanField(default=False)
    T_Mysore = models.BooleanField(default=False)
    T_Rishikesh = models.BooleanField(default=False)
    T_Gujarat = models.BooleanField(default=False)
    T_Adman_Islands = models.BooleanField(default=False)
    T_Meghalaya = models.BooleanField(default=False)
    T_Almora = models.BooleanField(default=False)
    T_Wagah_Border = models.BooleanField(default=False)
    T_Arunachal_Pradesh = models.BooleanField(default=False)
    
    




    def __unicode__(self):
    	return self.First_Name


    
class student(models.Model):
    user=models.OneToOneField(User)
    online=models.BooleanField(default=False)

    n_notif=models.IntegerField(default=0)
    n_msg=models.IntegerField(default=0)
    msg_list = ListField(default=[])
    n_frqst=models.IntegerField(default=0)
    logout = models.DateTimeField(auto_now_add=False,auto_now=False,null=True)
    
    status = models.CharField(max_length=100,null=True)
    first_name=models.CharField(max_length=100,null=True)
    last_name=models.CharField(max_length=100,null=True)
    date = models.IntegerField(default=0)
    month = models.IntegerField(default=0)
    year = models.IntegerField(default=0)
    male = models.BooleanField(default=False)
    female = models.BooleanField(default=False)
    
    post = ListField(default=[])
    work = models.TextField(default="")
    work_as = models.TextField(default="")
    school = models.TextField(default="")
    college = models.TextField(default="")
    party = models.TextField(default="")
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=15, validators=[phone_regex], blank=True) # validators should be a list
    
    current_place = models.TextField(default="")
    current_intrest = models.TextField(default="")
    hometown = models.TextField(default="")
    prof_pic = ListField(default=[])
    cover_pic = ListField(default=[])
    cov_fix = ListField(default=[0])
    priv_prof = models.IntegerField(default=1)
    priv_mess = models.IntegerField(default=1)
    priv_frnd = models.IntegerField(default=1)
    priv_email = models.IntegerField(default=1)
    priv_goups = models.IntegerField(default=1)
    priv_blokd = models.IntegerField(default=1)
    interest = ListField(default=[0])
    travels = ListField(default=[0])

    #post = ArrayField(models.CharField(max_length=100),blank=True)
    # date = models.DateField(_("Date"), default=datetime.date.today)

    
    
    fmsg = models.TextField(default="")
    fnotify = models.TextField(default="")
    fhitcomnotif= ListField(default=[])
    fcomnotif = ListField(default=[])
    f_list = ListField(default=[])
    add_f_list = ListField(default=[])
    accept_f_list = ListField(default=[])
    cov_r =models.IntegerField(default=0)
    cov_g =models.IntegerField(default=86)
    cov_b =models.IntegerField(default=134)
    groups = ListField(default=[])
    shared_posts = ListField(default=[])
    def __unicode__(self):
        return self.first_name


class posts(models.Model):
    identity = models.IntegerField(default=0)
    profile_picture= models.BooleanField(default=False)
    cover_picture= models.BooleanField(default=False)


    photos = models.IntegerField(default=0)
    hits = models.IntegerField(default=0)
    hitters = ListField(default=[])
    comnt = ListField(default=[])
    comments = models.IntegerField(default=0)
    say = models.TextField(default="")
    category = models.TextField(default="")
    anonymous = models.BooleanField(default=False)
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)

    def __unicode__(self):
        return self.photos

class postcomment(models.Model):
    c_identity = models.IntegerField(default=0)
    c_intity = models.IntegerField(default=0)

    c_photos = models.IntegerField(default=0,null=True)
    c_say = models.TextField(default="")

    def __unicode__(self):
        return self.c_photos

class message(models.Model):
    m_id = models.IntegerField(default=0)
    m_friend = models.IntegerField(default=0)
    m_message = models.TextField(default="")
    m_admin = models.BooleanField(default=False)
    def __unicode__(self):
        return self.m_id

class create_group(models.Model):
    cg_name =models.CharField(max_length=70)
    cg_lat = models.DecimalField(max_digits=8,decimal_places=5)
    cg_long = models.DecimalField(max_digits=8,decimal_places=5)
    cg_memb_id = ListField(default=[])

    def __unicode__(self):
        return self.cg_name

class intrests(models.Model):
    intrest_name = models.CharField(max_length=70)
    intrest_posts = ListField(default=[])
    intrest_memb_id = ListField(default=[])
    intrest_category = models.CharField(max_length=70)
    intrest_catelog = models.CharField(max_length=70)
    def __unicode__(self):
        return self.intrest_name


class create_city(models.Model):
    city_name =models.CharField(max_length=70)
    city_lat = models.DecimalField(max_digits=8,decimal_places=5)
    city_long = models.DecimalField(max_digits=8,decimal_places=5)
    city_memb_id = ListField(default=[])
    city_descript = models.TextField(default="")
    city_posts = ListField(default=[])

    def __unicode__(self):
        return self.city_name

class create_subjects(models.Model):
    subjects_name =models.CharField(max_length=70)
    subjects_memb_id = ListField(default=[])
    subjects_descript = models.TextField(default="")
    subjects_posts = ListField(default=[])

    def __unicode__(self):
        return self.subjects_name

class player(models.Model):
    p_id = models.IntegerField(default=0)
    p_votes = models.IntegerField(default=0)
    p_rating = models.IntegerField(default=0)
    p_team = models.IntegerField(default=0)
    p_game = models.CharField(max_length=70)

    def __unicode__(self):
        return self.p_id

class team(models.Model):
    team_name = models.CharField(max_length=100)
    team_capt = models.IntegerField(default=0)
    team_category = models.CharField(max_length=70)
    team_memb = ListField(default=[])
    team_matches = models.IntegerField(default=0)
    team_wins = models.IntegerField(default=0)
    team_lose =models.IntegerField(default=0)
    team_logo = models.TextField(default="")
    team_group = models.IntegerField(default=0)

    game_name = models.TextField(default="")

    game_server = models.TextField(default="")

    def __unicode__(self):
        return self.team_name


class trip(models.Model):
    trip_name = models.CharField(max_length=100)
    trip_from = models.DateTimeField(null=True,default=datetime.date.today)
    trip_to = models.DateField(default=datetime.date.today)
    trip_city = models.CharField(max_length=50)
    trip_mode = models.CharField(max_length=10)
    trip_memb =ListField(default=[])
    trip_n_membs =models.IntegerField(default=0)
    

    def __unicode__(self):
        return self.trip_name


class band(models.Model):
    band_name = models.CharField(max_length=100)
    band_descript = models.TextField(default="")
    band_V_piano = models.IntegerField(default=0)
    band_V_guitar = models.IntegerField(default=0)
    band_V_singer = models.IntegerField(default=0)
    band_V_violin = models.IntegerField(default=0)
    band_V_bass = models.IntegerField(default=0)
    band_V_drum = models.IntegerField(default=0)
    band_V_flute = models.IntegerField(default=0)
    band_V_saxophone = models.IntegerField(default=0)
    band_V_tabla = models.IntegerField(default=0)
    band_V_other = models.IntegerField(default=0)
    band_other = models.CharField(max_length=100)
    band_memb =ListField(default=[])
    band_memb_work =ListField(default=[])
    band_posts =ListField(default=[])
    band_hits = models.IntegerField(default=0)
    band_group = models.IntegerField(default=0)
    band_hitters =ListField(default=[])

    def __unicode__(self):
        return self.band_name


class groupchat(models.Model):
    gc_id = models.IntegerField(default=0)
    gc_group = models.IntegerField(default=0)
    gc_message = models.TextField(default="")
    admin = models.BooleanField(default=False)
    gc_interest = models.TextField(default=0)
    def __unicode__(self):
        return self.gc_id

class teamchat(models.Model):
    teamc_id = models.IntegerField(default=0)
    teamc_group = models.IntegerField(default=0)
    teamc_message = models.TextField(default="")
    admin = models.BooleanField(default=False)
    def __unicode__(self):
        return self.gc_id


class startup(models.Model):
    startup_name = models.CharField(max_length=100)
    startup_descript = models.TextField(default="")
    startup_V_ceo = models.IntegerField(default=0)
    startup_V_techop = models.IntegerField(default=0)
    startup_V_salesmark = models.IntegerField(default=0)
    startup_V_hr = models.IntegerField(default=0)
    startup_V_busdev = models.IntegerField(default=0)
    startup_V_custser = models.IntegerField(default=0)
    startup_V_salesman = models.IntegerField(default=0)
    startup_V_rnd = models.IntegerField(default=0)
    startup_V_adm = models.IntegerField(default=0)
    startup_V_other = models.IntegerField(default=0)
    startup_other = models.CharField(max_length=100)
    startup_memb =ListField(default=[])
    startup_memb_work =ListField(default=[])
    startup_posts =ListField(default=[])
    startup_hits = models.IntegerField(default=0)
    startup_group = models.IntegerField(default=0)
    startup_hitters =ListField(default=[])
    

    def __unicode__(self):
        return self.startup_name



class game(models.Model):
    game_name = models.CharField(max_length=100)
    game_capt = models.IntegerField(default=0)
    game_category = models.CharField(max_length=70)
    game_memb = ListField(default=[])
    game_limit = models.IntegerField(default=1)
    game_logo = models.TextField(default="")
    game_group = models.IntegerField(default=0)

    def __unicode__(self):
        return self.game_name


        
class create_cafe(models.Model):
    cafe_name =models.CharField(max_length=70)
    cafe_lat = models.DecimalField(max_digits=8,decimal_places=5)
    cafe_long = models.DecimalField(max_digits=8,decimal_places=5)
    cafe_reviewer_id = ListField(default=[])
    cafe_address = models.TextField(default="")
    cafe_posts = ListField(default=[])
    cafe_review = models.DecimalField(max_digits=3,decimal_places=2,null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=15, validators=[phone_regex], blank=True)

    def __unicode__(self):
        return self.cafe_name

class create_film(models.Model):
    film_name =models.CharField(max_length=70)
    film_reviewer_id = ListField(default=[])
    film_descript = models.TextField(default="")
    film_posts = ListField(default=[])
    film_memb= ListField(default=[])
    film_review = models.DecimalField(max_digits=3,decimal_places=2,null=True)
    
    def __unicode__(self):
        return self.film_name


class feedback(models.Model):
    feed = models.TextField(default="")
    
    def __unicode__(self):
        return self.id





class quick(models.Model):
    quick_name = models.TextField(default="")
    quick_from = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    quick_to = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    quick_memb =ListField(default=[])
    quick_group = models.IntegerField(default=0)
    
    

    def __unicode__(self):
        return self.quick_name

class pizza(models.Model):
    family_name = models.TextField(default="")
    hour_from = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    hour_to = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    family_memb =ListField(default=[])
    pizza_type=models.CharField(max_length=20,null=True)
    pizza_coupon = models.CharField(max_length=5,null=True)
    pizza_group = models.IntegerField(default=0)
    
    

    def __unicode__(self):
        return self.quick_name









