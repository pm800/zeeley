from __future__ import unicode_literals
from django.db import models
from time import time
from django.contrib.auth.models import User
#from django.contrib.postgres.fields import ArrayField
from django import forms
from django.db import models
import ast
import datetime
from django.core.validators import RegexValidator
from django.utils import timezone

from django.utils.encoding import python_2_unicode_compatible


# from gcm.models import AbstractDevice

# class MyDevice(AbstractDevice):
#     pass



class ListField(models.TextField):
    #__metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)


    def from_db_value(self, value, expression, connection, context):
        if value is None:
            return value
        return ast.literal_eval(value)


    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return unicode(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)

# Create your models here.
def get_upload_file_name(instance, filename):
    return "uploaded_files/%s_%s" % (str(time()).replace('.','_'),filename)

# class StringListField(models.Field):



#     __metaclass__=models.SubfieldBase
#     SPLIT_CHAR = u'\v'
#     def __init__(self,*args,**kwargs):
#         self.internal_type=kwargs.pop('internal_type','CharField')
#         super(StringListField,self).__init__(*args,**kwargs)

#     def to_python(self,value):
#         if isinstance(value,list):
#             return value
#         if value is None:
#             return []
#         return value.split(self.SPLIT_CHAR)

#     def get_internal_type(self):
#         return self.internal_type

#     def get_db_prep_lookup(self, lookup_type, value):
#         #SQL WHERE
#         raise NotImplementedError()

#     def get_db_prep_save(self,value):
#         return self.SPLIT_CHAR.join(value)

#     def formfield(self,**kwargs):
#         assert not kwargs, kwargs
#         return forms.MultipleChoiceField(choices=self.choices)

        
class regs(models.Model):
    First_Name = models.CharField(max_length=30,null=True)
    Username = models.CharField(max_length=30)
    Last_Name = models.CharField(max_length=30,null=True)
    City = models.CharField(max_length=30,null=True)
    Date = models.IntegerField(default=0)
    Month = models.IntegerField(default=0)
    Year = models.IntegerField(default=0)
     
    email = models.EmailField()
    password = models.CharField(max_length=30)
    confirm_password = models.CharField(max_length=30)
    male = models.BooleanField(default=False)
    female = models.BooleanField(default=False)
    other = models.BooleanField(default=False)
    #t=models.FileField(upload_to=get_upload_file_name,blank=True)Sport = models






    
    S_Cricket = models.BooleanField(default=False)
    S_Badminton = models.BooleanField(default=False)
    S_Volleyball = models.BooleanField(default=False)
    S_Football = models.BooleanField(default=False)
    S_Chess = models.BooleanField(default=False)
    S_Hockey = models.BooleanField(default=False)
    S_Table_Tennis = models.BooleanField(default=False)
    S_Lawn_Tennis = models.BooleanField(default=False)
    S_Kabaddi = models.BooleanField(default=False)
    S_Kho_Kho = models.BooleanField(default=False)







    M_Guitar = models.BooleanField(default=False)
    M_Sitar = models.BooleanField(default=False)
    M_Drum = models.BooleanField(default=False)
    M_Tabla = models.BooleanField(default=False)
    M_Flute = models.BooleanField(default=False)
    M_Violin = models.BooleanField(default=False)
    M_Piano = models.BooleanField(default=False)
    M_Regional = models.BooleanField(default=False)
    M_Organ_Pad = models.BooleanField(default=False)
    M_Western = models.BooleanField(default=False)
    M_Classical = models.BooleanField(default=False)
    M_Vocals = models.BooleanField(default=False)
    M_Harmonium = models.BooleanField(default=False)



    T_Manali = models.BooleanField(default=False)
    T_Kullu = models.BooleanField(default=False)
    T_Switzerland = models.BooleanField(default=False)
    T_Kashmir = models.BooleanField(default=False)
    T_Shimla = models.BooleanField(default=False)
    T_London = models.BooleanField(default=False)
    T_Mumbai = models.BooleanField(default=False)
    T_Kolkata = models.BooleanField(default=False)
    T_Chennai = models.BooleanField(default=False)
    T_Delhi = models.BooleanField(default=False)
    T_Ooty = models.BooleanField(default=False)
    T_Goa = models.BooleanField(default=False)
    T_NewYork = models.BooleanField(default=False)
    T_Malaysia = models.BooleanField(default=False)
    T_Darjeeling = models.BooleanField(default=False)
    T_Nainital = models.BooleanField(default=False)
    T_Agra = models.BooleanField(default=False)
    T_Jaipur_Udaipur = models.BooleanField(default=False)
    T_Kanyakumari = models.BooleanField(default=False)
    T_Ajanta_Ellora = models.BooleanField(default=False)
    T_Mysore = models.BooleanField(default=False)
    T_Rishikesh = models.BooleanField(default=False)
    T_Gujarat = models.BooleanField(default=False)
    T_Adman_Islands = models.BooleanField(default=False)
    T_Meghalaya = models.BooleanField(default=False)
    T_Almora = models.BooleanField(default=False)
    T_Wagah_Border = models.BooleanField(default=False)
    T_Arunachal_Pradesh = models.BooleanField(default=False)
    
    




    def __unicode__(self):
        return self.First_Name




        

    
class student(models.Model):


    #student_id = models.IntegerField(default=id)
    ##
    #date_created = models.DateTimeField('date_created')

    ##

    user=models.OneToOneField(User)
    online=models.BooleanField(default=False)

    n_notif=models.IntegerField(default=0)
    n_msg=models.IntegerField(default=0)
    msg_list = ListField(default=[]) # list of ids of pesons who sent message  (only one id for a person)
    #msg_list_2 = ManyToManyField(student, related_name = '')
    n_frqst=models.IntegerField(default=0)
    logout = models.DateTimeField(auto_now_add=False,auto_now=False,null=True) # last datetime when user is online
    
    status = models.CharField(max_length=100,null=True)
    first_name=models.CharField(max_length=100,null=True)
    last_name=models.CharField(max_length=100,null=True)
    date = models.IntegerField(default=0)
    month = models.IntegerField(default=0)
    year = models.IntegerField(default=0)
    male = models.BooleanField(default=False)
    female = models.BooleanField(default=False)
    
    latitude = models.DecimalField(max_digits=8,decimal_places=5,default=25.26276)
    longitude = models.DecimalField(max_digits=8,decimal_places=5,default=82.99078)

    post = ListField(default=[])# list of (postnumber)
    #post_2 = ManyToManyField(student, related_name = '')
    work = models.TextField(default="")
    work_as = models.TextField(default="")
    school = models.TextField(default="")
    college = models.TextField(default="")
    party = models.TextField(default="")
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=15, validators=[phone_regex], blank=True) # validators should be a list
    
    current_place = models.TextField(default="")
    current_intrest = models.TextField(default="")
    hometown = models.TextField(default="")
    prof_pic = ListField(default=[])# list of (prof_pic number) # /file_p8_25.jpg
    cover_pic = ListField(default=[])# list of (cover_pic number) # /file_c8.26.jpg
    cov_fix = ListField(default=[0])
    #prof_pic_2 = ManyToManyField(student, related_name = '')
    #cover_pic_2 = ManyToManyField(student, related_name = '')
    #cov_fix_2 = ManyToManyField(student, related_name = '')


    priv_prof = models.IntegerField(default=1)
    priv_mess = models.IntegerField(default=1)
    priv_frnd = models.IntegerField(default=1)
    priv_email = models.IntegerField(default=1)
    priv_goups = models.IntegerField(default=1)
    priv_blokd = models.IntegerField(default=1)


    interest = ListField(default=[0])# list of ids of inetrest objects
    travels = ListField(default=[0])
    #interest_2 = ManyToManyField(student, related_name = '')
    #travels_2 = ManyToManyField(student, related_name = '')

    #post = ArrayField(models.CharField(max_length=100),blank=True)
    # date = models.DateField(_("Date"), default=datetime.date.today)

    tagword = models.CharField(max_length=100,null=True,default="")

    priv_map = models.IntegerField(default=0)
    priv_dist = models.IntegerField(default=0)


    block_prof = ListField(default=[])
    block_location = ListField(default=[])
    block_msg = ListField(default=[])
    block_invite = ListField(default=[])
    #block_prof_2 = ManyToManyField(student, related_name = '')
    #block_location_2 = ManyToManyField(student, related_name = '')
    #block_msg_2 = ManyToManyField(student, related_name = '')
    #block_invite_2 = ManyToManyField(student, related_name = '')

    priv_string = models.CharField(max_length=10,null=True)
    
    fmsg = models.TextField(default="")
    fnotify = models.TextField(default="")
    fhitcomnotif= ListField(default=[])
    fcomnotif = ListField(default=[])
    f_list = ListField(default=[])#list of leaders ids
    add_f_list = ListField(default=[])#???
    accept_f_list = ListField(default=[])# list of follower's ids
    #fhitcomnotif_2 = ManyToManyField(student, related_name = '')
    #fcomnotif_2 = ManyToManyField(student, related_name = '')
    #f_list_2 = ManyToManyField(student, related_name = '')
    #add_f_list_2 = ManyToManyField(student, related_name = '')
    #accept_f_list_2 = ManyToManyField(student, related_name = '')
    cov_r =models.IntegerField(default=0)#???
    cov_g =models.IntegerField(default=86)
    cov_b =models.IntegerField(default=134)
    groups = ListField(default=[]) # list of ids of create_group objects in which user is present (not sure) or gropus created by user #???
    team_to_chat = ListField(default=[])# list of ids of teams
    shared_posts = ListField(default=[])# list of ids of posts shared by user.
   # groups_2 = ManyToManyField(student, related_name = '')
    #team_to_chat_2 = ManyToManyField(student, related_name = '')
   # shared_posts_2 = ManyToManyField(student, related_name = '')

    def __unicode__(self):
        return self.first_name


class posts(models.Model):
    identity = models.IntegerField(default=0) #crator's id
    profile_picture= models.BooleanField(default=False)
    cover_picture= models.BooleanField(default=False)


    photos = models.IntegerField(default=0) # photos = photo number 
    hits = models.IntegerField(default=0)
    hitters = ListField(default=[])
    comnt = ListField(default=[])#list of ids of commentors.[2,45,2,66,2]
   # hitters_2 = ManyToManyField(student, related_name = '')
   # comnt_2 = ManyToManyField(student, related_name = '')
    comments = models.IntegerField(default=0) # total no. of comments
    say = models.TextField(default="")
    category = models.TextField(default="")
    anonymous = models.BooleanField(default=False)
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)
    creator = models.ForeignKey(student,default=8)
    
    def __unicode__(self):
        return str(self.photos) #+++++
        #return "posts" 

class postcomment(models.Model): #done
    c_identity = models.IntegerField(default=0)#id of  comment creator.
    c_intity = models.IntegerField(default=0) #id of post creator.

    c_photos = models.IntegerField(default=0,null=True)# post id on which commenting
    c_say = models.TextField(default="",null=True)#c_say= comment.
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)
    def __unicode__(self):
        return str(self.c_photos) #+++
        #return 'postcomment'

class message(models.Model):
    m_id = models.IntegerField(default=0) # messenger's id
    m_friend = models.IntegerField(default=0) # message reciever's id
    m_message = models.TextField(default="") # message
    m_admin = models.BooleanField(default=False)#???
    m_seen = models.BooleanField(default=False)#lll
    m_photo = models.BooleanField(default=False)#if message is a photo
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)
    def __unicode__(self):
        return str(self.m_id) #++++++++++++++++++++++++++++++++++++++++++++++++++++
        #return 'message' 

class create_group(models.Model):
    cg_name =models.CharField(max_length=70)
    cg_lat = models.DecimalField(max_digits=8,decimal_places=5)
    cg_long = models.DecimalField(max_digits=8,decimal_places=5)
    cg_memb_id = ListField(default=[])
   # cg_memb_id_2 = ManyToManyField(student, related_name = '')

    def __unicode__(self):
        return self.cg_name

@python_2_unicode_compatible
class intrests(models.Model):
    intrest_name = models.CharField(max_length=70)
    intrest_posts = ListField(default=[])# list of ids of posts #guess# on this inerest.
  #  intrest_posts_2 = ManyToManyField(student, related_name = '')
    intrest_memb_id = ListField(default=[])# list of ids(unique) of students who have this iterest in present or past  
   # intrest_memb_id_2 = ManyToManyField(student, related_name = '')
    intrest_category = models.CharField(max_length=70)
    intrest_catelog = models.CharField(max_length=70)
    tagword = models.CharField(max_length=100,null=True,default="")
    def __unicode__(self):
        return self.intrest_name

    def __str__(self):
        return self.intrest_name


class create_city(models.Model):
    city_name =models.CharField(max_length=70)
    city_lat = models.DecimalField(max_digits=8,decimal_places=5)
    city_long = models.DecimalField(max_digits=8,decimal_places=5)
    city_memb_id = ListField(default=[])
   # city_memb_id_2 = ManyToManyField(student, related_name = '')
    city_descript = models.TextField(default="")
    city_posts = ListField(default=[])
   # city_posts_2 = ManyToManyField(student, related_name = '')
    tagword = models.CharField(max_length=100,null=True,default="")
    def __unicode__(self):
        return self.city_name

class create_subjects(models.Model):
    subjects_name =models.CharField(max_length=70)
    subjects_memb_id = ListField(default=[])
   # subjects_memb_id_2 = ManyToManyField(student, related_name = '')
    subjects_descript = models.TextField(default="")
    subjects_posts = ListField(default=[])
   # subjects_posts_2 = ManyToManyField(student, related_name = '')
    tagword = models.CharField(max_length=100,null=True,default="")
    def __unicode__(self):
        return self.subjects_name

class player(models.Model):
    p_id = models.IntegerField(default=0)# student's id
    p_votes = models.IntegerField(default=0)
    p_rating = models.IntegerField(default=0)
    p_team = models.IntegerField(default=0)# team id
    p_game = models.CharField(max_length=70)# basically a interest_name

    def __unicode__(self):
        return str(self.p_id)#+++
        #return 'player'
class team(models.Model):
    team_name = models.CharField(max_length=100)
    team_capt = models.IntegerField(default=0)# team creator
    team_category = models.CharField(max_length=70)# is basically a intrest_name of a intrest
    team_memb = ListField(default=[])
    team_invites = ListField(default=[])# list of ids of invited students 
   # team_memb_2 = ManyToManyField(student, related_name = '')
  #  team_invites_2 = ManyToManyField(student, related_name = '')
    team_matches = models.IntegerField(default=0)
    team_wins = models.IntegerField(default=0)
    team_lose =models.IntegerField(default=0)
    team_logo = models.TextField(default="")
    team_group = models.IntegerField(default=0)# id of create_group object #lll next realtion 

    game_name = models.TextField(default="")

    game_server = models.TextField(default="")

    def __unicode__(self):
        return self.team_name


class trip(models.Model):
    trip_name = models.CharField(max_length=100,null=True)
    trip_from = models.DateTimeField(null=True,default=datetime.datetime.today)
    trip_to = models.DateField(default=datetime.date.today)
    trip_city = models.CharField(max_length=50)
    trip_city_from = models.CharField(max_length=50)
    trip_mode = models.CharField(max_length=10,null=True)
    trip_memb =ListField(default=[])# list of ids of trip creator and #not sure# other members
    trip_invites =ListField(default=[])
  #  trip_memb_2 = ManyToManyField(student, related_name = '')
    #trip_invites_2 = ManyToManyField(student, related_name = '')
    trip_n_membs =models.IntegerField(default=0)# no. of menbers in trip
    trip_admin =models.IntegerField(default=6)
    
    from_latitude = models.DecimalField(max_digits=11,decimal_places=8,default=25.26370359)#??? default is of 
    from_longitude = models.DecimalField(max_digits=11,decimal_places=8,default=82.98598559)
    
    to_latitude = models.DecimalField(max_digits=11,decimal_places=8,default=25.26370359)
    to_longitude = models.DecimalField(max_digits=11,decimal_places=8,default=82.98598559)
    booking = models.BooleanField(default=False)
    remark = models.TextField(default="")
    

    def __unicode__(self):
        return self.trip_name


class band(models.Model):
    band_name = models.CharField(max_length=100)
    band_descript = models.TextField(default="")
    band_V_piano = models.IntegerField(default=0)
    band_V_guitar = models.IntegerField(default=0)
    band_V_singer = models.IntegerField(default=0)
    band_V_violin = models.IntegerField(default=0)
    band_V_bass = models.IntegerField(default=0)
    band_V_drum = models.IntegerField(default=0)
    band_V_flute = models.IntegerField(default=0)
    band_V_saxophone = models.IntegerField(default=0)
    band_V_tabla = models.IntegerField(default=0)
    band_V_other = models.IntegerField(default=0)
    band_other = models.CharField(max_length=100)
    band_memb =ListField(default=[])#list of ids of student
    band_memb_work =ListField(default=[])# list of intgers(denoting his work)
    band_posts =ListField(default=[])
   # band_memb_2 = ManyToManyField(student, related_name = '')
   # band_memb_work_2 = ManyToManyField(student, related_name = '')
   # band_posts_2 = ManyToManyField(student, related_name = '')
    band_hits = models.IntegerField(default=0)
    band_group = models.IntegerField(default=0)
    band_hitters =ListField(default=[])
    #band_hitters_2 = ManyToManyField(student, related_name = '')
    tagword = models.CharField(max_length=100,null=True,default="")
    def __unicode__(self):
        return self.band_name


class groupchat(models.Model):
    gc_id = models.IntegerField(default=0)# student id
    gc_group = models.IntegerField(default=0)# create_group id
    gc_message = models.TextField(default="")
    admin = models.BooleanField(default=False)
    gc_interest = models.TextField(default=0)# interest name
    def __unicode__(self):
        return str(self.gc_id) #+++
        #return 'groupchat'

class teamchat(models.Model):
    teamc_id = models.IntegerField(default=0)# student id
    teamc_group = models.IntegerField(default=0)# team id
    teamc_message = models.TextField(default="")
    admin = models.BooleanField(default=False)
    teamc_photo = models.BooleanField(default=False)
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)
    def __unicode__(self):
        return str(self.gc_id)#+++ need


class startup(models.Model):
    startup_name = models.CharField(max_length=100)
    startup_descript = models.TextField(default="")
    startup_V_ceo = models.IntegerField(default=0)
    startup_V_techop = models.IntegerField(default=0)
    startup_V_salesmark = models.IntegerField(default=0)
    startup_V_hr = models.IntegerField(default=0)
    startup_V_busdev = models.IntegerField(default=0)
    startup_V_custser = models.IntegerField(default=0)
    startup_V_salesman = models.IntegerField(default=0)
    startup_V_rnd = models.IntegerField(default=0)
    startup_V_adm = models.IntegerField(default=0)
    startup_V_other = models.IntegerField(default=0)
    startup_other = models.CharField(max_length=100)
    startup_memb =ListField(default=[])# list of student ids
    startup_memb_work =ListField(default=[])# list of integers denoting his work
    startup_posts =ListField(default=[])
   # startup_memb_2 = ManyToManyField(student, related_name = '')
   # startup_memb_work_2 = ManyToManyField(student, related_name = '')
   # startup_posts_2 = ManyToManyField(student, related_name = '')
    startup_hits = models.IntegerField(default=0)
    startup_group = models.IntegerField(default=0)
    startup_hitters =ListField(default=[])
   # startup_hitters_2 = ManyToManyField(student, related_name = '')
    tagword = models.CharField(max_length=100,null=True,default="")

    def __unicode__(self):
        return self.startup_name



class game(models.Model):
    game_name = models.CharField(max_length=100)
    game_capt = models.IntegerField(default=0)
    game_category = models.CharField(max_length=70)
    game_memb = ListField(default=[])
  #  game_memb_2 = ManyToManyField(student, related_name = '')
    game_limit = models.IntegerField(default=1)
    game_logo = models.TextField(default="")
    game_group = models.IntegerField(default=0)

    def __unicode__(self):
        return self.game_name


        
class create_cafe(models.Model):
    cafe_name =models.CharField(max_length=70)
    cafe_lat = models.DecimalField(max_digits=8,decimal_places=5)
    cafe_long = models.DecimalField(max_digits=8,decimal_places=5)
    cafe_reviewer_id = ListField(default=[])
   # cafe_reviewer_id_2 = ManyToManyField(student, related_name = '')
    cafe_address = models.TextField(default="")
    cafe_posts = ListField(default=[])
  #  cafe_posts_2 = ManyToManyField(student, related_name = '')
    cafe_review = models.DecimalField(max_digits=3,decimal_places=2,null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=15, validators=[phone_regex], blank=True)

    tagword = models.CharField(max_length=100,null=True,default="")
    
    def __unicode__(self):
        return self.cafe_name

class create_film(models.Model):
    film_name =models.CharField(max_length=70)
    film_reviewer_id = ListField(default=[])
   # film_reviewer_id_2 = ManyToManyField(student, related_name = '')
    film_descript = models.TextField(default="")
    film_posts = ListField(default=[])# ids of posts
    film_memb= ListField(default=[])
   # film_posts_2 = ManyToManyField(student, related_name = '')
  #  film_memb_2 = ManyToManyField(student, related_name = '')
    film_review = models.DecimalField(max_digits=3,decimal_places=2,null=True)
    tagword = models.CharField(max_length=100,null=True,default="")
    
    def __unicode__(self):
        return self.film_name


class feedback(models.Model):
    feed = models.TextField(default="")
    
    def __unicode__(self):
        return str(self.id)#+++need





class quick(models.Model):
    quick_name = models.TextField(default="")
    quick_from = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    quick_to = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    quick_memb =ListField(default=[])
  #  quick_memb_2 = ManyToManyField(student, related_name = '')
    quick_group = models.IntegerField(default=0)
    
    

    def __unicode__(self):
        return self.quick_name

class pizza(models.Model):
    family_name = models.TextField(default="")
    hour_from = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    hour_to = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    family_memb =ListField(default=[])
  #  family_memb_2 = ManyToManyField(student, related_name = '')
    pizza_type=models.CharField(max_length=20,null=True)
    pizza_coupon = models.CharField(max_length=5,null=True)
    pizza_group = models.IntegerField(default=0)# create_group object's id
    
    

    def __unicode__(self):
        return self.family_name


class cab_share(models.Model):
    
    from_latitude = models.DecimalField(max_digits=10,decimal_places=8,default=25.26370359)
    from_longitude = models.DecimalField(max_digits=10,decimal_places=8,default=82.98598559)
    place_from = models.CharField(max_length=100,null=True)
    to_latitude = models.DecimalField(max_digits=10,decimal_places=8,default=25.26370359)
    to_longitude = models.DecimalField(max_digits=10,decimal_places=8,default=82.98598559)
    place_to = models.CharField(max_length=100,null=True)
    depart_time = models.DateTimeField(auto_now_add=False,auto_now=False,null=True,default=datetime.time())
    cab_memb =ListField(default=[])
   #cab_memb_2 = ManyToManyField(student, related_name = '')
    cab_coupon = models.BooleanField(default=0)
    cab_limit = models.IntegerField(default=1)
    
    
    
    
    

    def __unicode__(self):
        return str(self.id) #+++need



@python_2_unicode_compatible
class Vote(models.Model):
    user = models.OneToOneField(User, related_name='votes')
    interest = models.OneToOneField(intrests)
    score = models.FloatField()
    def __unicode__(self):
        return str(self.score) #+++ need

    def __str__(self):#???
        return self.self.score




class user_device(models.Model):
    """docstring for user_device"""
    dev_id = models.OneToOneField(student)
    reg_key = models.TextField(default="")
    
    def __unicode__(self):
        return str(self.reg_key) #+++ need



class notification(models.Model):
    """docstring for user_device"""
    notif_from = models.IntegerField(default=0)
    notif_to = models.IntegerField(default=0)
    notif = models.TextField(default="")
    purpose = models.CharField(max_length=20,null=True)
    purpose_id = models.IntegerField(default=0) #postcomment id
    post_time = models.DateTimeField(auto_now=False,auto_now_add=True,blank=True)#auto_now : for each time ojcet saved. #add: for first time only when object is created
    
    def __unicode__(self):
        return str(self.id) #+++
        #return 'n'
