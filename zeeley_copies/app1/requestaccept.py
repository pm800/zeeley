
def PCGame_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            t_share.trip_invites.append(i.id)
            t_share.save()
            

            

            if i.id != p.id:
                p.n_notif=p.n_notif+1
                p.save()
                notif_msg = str(i.first_name.split(" ")[0]) + " has requsted to join your PCGame trip. Please contact him soon."
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="PCGame",purpose_id=t_share.id)
                new_notif.save()

                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley PCGame Requested", "text": notif_msg  },  "to" : str(ud.reg_key),
                        "data":{"team_id":t_share.id,"new_memb":i.id}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"
            

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def PCGame_accept(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            memb_id = request.GET["memb_id"]
            p=student.objects.filter(id=memb_id).first()
            t_share = trip.objects.filter(id=trip_id).first()
            if t_share.team_memb[0] == i.id:
                my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=p.id,trip_city=t_share.trip_city).first()
                myteam = team.objects.filter(team_memb__contains=p.id,team_category="PCGame")
                t_share.trip_invites.remove(p.id)
                t_share.trip_memb.append(p.id)
                t_share.save()
                

                t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="PCGame",team_group=t_share.id)
                if t:
                    m=t.first()
                    
                else:
                    t_n=i.first_name+"'s' PCGame "+"Troop"
                    m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="PCGame",team_group=t_share.id)
                    m.team_memb.append(t_share.trip_memb[0])
                    m.save()
                m.team_memb.append(p.id)
                m.save()

                if i.id != p.id:
                    p.n_notif=p.n_notif+1
                    p.save()
                    notif_msg = str(i.first_name.split(" ")[0]) + " has acceptd your PCGame request. Please contact him soon."
                    new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="PCGame",purpose_id=t_share.id)
                    new_notif.save()

                    if user_device.objects.filter(dev_id=p.id):
                        ud = user_device.objects.filter(dev_id=p.id).first() 
                        url='https://fcm.googleapis.com/fcm/send'
                        data ={ "notification": {"title": "Zeeley PCGame Accepted", "text": notif_msg  },  "to" : str(ud.reg_key),
                            "data":{"trip_id":t_share.id,"team_name":m.team_name,"team_id":m.id}}
                        header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                        r = requests.post(url, data=json.dumps(data), headers=header)
                        state = "Done"
                if my_trip:
                    my_trip.trip_memb.remove(p.id)
                    my_trip.save()
                    if not my_trip.trip_memb:
                        my_trip.delete()
                if myteam:
                    myteam.first().team_memb.remove(p.id)
                    myteam.first().save()
                    if not myteam.first().team_memb:
                        myteam.first().delete()
                
                

                z= "sent"

            else:
                z="You don't have permission to accept the request."


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')