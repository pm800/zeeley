"<div style='max-height:90px;overflow:hidden;border-radius:8px;float:right;'><img src='"+data.z+"' style='width:90px;'>"
def servicepage(request):
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        play=''
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="pg":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                    

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            
            if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
                play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
                tm=team.objects.filter(id=play).first()
                if tm:
                    if tm.team_capt==i.id:
                        capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"serve.html",context)

        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="pizza":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    
                    if pizza.objects.filter(pizza_group=t_g):
                        piz = pizza.objects.filter(pizza_group=int(t_g))
                        z=""
                        print("got")
                        for n in piz:
                            print("got")
                            z="<tr onclick='document.location=\"../view_pizza/?q="+str(n.id)+"\"' style='color:black;background-color: rgba(255,255,255, 0.6);'><b><td style='font-size:17px;width:20%;padding-left:25px;'>"+str(n.family_name)+"</td><td style='width:10%;font-size:17px'>"+str(n.hour_from)+"</td><td style='width:10%;font-size:17px'>"+str(n.hour_to)+"</td><td style='width:15%;font-size:17px'>"+str(n.pizza_coupon)+"</td><td style='width:15%;font-size:17px'>"+str(n.pizza_type)+"</td><td style='width:20%;font-size:17px;padding-right:10px;'><button type='submit'>Join Team</button></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post
            # if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
            #     play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
            #     if play:
            #         tm=team.objects.filter(id=play).first()
            #         if tm.team_capt==i.id:
            #             capt=c

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            form = quick_form(request.POST or None)
            quick=pizza.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,
                
                "quick":quick,
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                "form" : form
            }

            return render(request,"serve.html",context)   
        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="cafe":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name
            tri =trip.objects.filter(trip_city='Cafe')
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post
            # if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
            #     play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
            #     if play:
            #         tm=team.objects.filter(id=play).first()
            #         if tm.team_capt==i.id:
            #             capt=c

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            form = quick_form(request.POST or None)
            cafe=create_cafe.objects.all()
            # if trip.objects.filter(trip_city='cafe'):
            #     trip = trip.objects.filter(trip_city='cafe')
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "trip":tri,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,
                "cafe" : cafe,
                
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                "form" : form
            }

            return render(request,"serve.html",context)

        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="quick":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post
            if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
                play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
                if play:
                    tm=team.objects.filter(id=play).first()
                    if tm.team_capt==i.id:
                        capt=c

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            form = quick_form(request.POST or None)
            quicky=quick.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,
                "quick" : quik,
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                "form" : form
            }

            return render(request,"serve.html",context)
        
        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="group":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    # v = posts.objects.filter(photos=h).first()
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST['typed']
                    print (t_g)
                    if game.objects.filter(game_category=i.current_intrest,game_group=t_g).exclude(game_capt=i.id):
                        teams=game.objects.filter(game_category=i.current_intrest,game_group=t_g).exclude(game_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_game/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_game/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.game_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.game_name)+"</td><td style='width:35%;'>"+str(len(n.game_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
            if play:
                tm=game.objects.filter(id=play).first()
                if tm:
                    if tm.game_capt==i.id:
                        capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"serve.html",context)

        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="startup":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            bands=startup.objects.filter(startup_group__in=i.groups)

            

            
            if request.method=='POST':


                if request.POST.get('stup_name'): 
                    stup_name=request.POST["stup_name"]
                    stup_des=request.POST["stup_des"]
                    stup_group=request.POST["team_group"]
                    b=startup(startup_name=stup_name,startup_descript=stup_des,startup_group=stup_group)
                    b.startup_memb.append(i.id)
                    b.save()
                    design=request.POST.getlist("design")
                    print design
                    for ins in design:
                        k = str(ins)
                        num=request.POST[k]
                        print num
                        if k=="ceo":
                            b.startup_V_ceo=num
                            b.save()
                        if k=="techop":
                            b.startup_V_techop=num
                            b.save()
                        if k=="sales":
                            b.startup_V_salesmark =num
                            b.save()
                        if k=="hr":
                            b.startup_V_hr =num
                            b.save()
                        if k=="busdev":
                            b.startup_V_busdev=num
                            b.save()
                        if k=="custser":
                            b.startup_V_custser=num
                            b.save()
                        if k=="salesman":
                            b.startup_V_salesman=num
                            b.save()
                        if k=="rnd":
                            b.startup_V_rnd=num
                            b.save()
                        if k=="adm":
                            b.startup_V_adm=num
                            b.save()
                        if k=="other":
                            b.startup_V_other=num
                            b.startup_other = request.POST["stup_other"]
                            b.save()
                    

                    for count, x in enumerate(request.FILES.getlist("files")):
                        def process(f):
                            with open('/home/raftar/ttl/babaS/media_in_env/media_root/file_startup_' +str(b.id)+'.jpg', 'wb+') as destination:
                                for chunk in f.chunks():
                                    destination.write(chunk)

                        process(x)

                        infile='/home/raftar/ttl/babaS/media_in_env/media_root/file_startup_' +str(b.id)+'.jpg'
                        thumb(infile)

                    state="Your startup is set... Now change the world with your idea..."
                    messages.success(request,state)
                    return HttpResponseRedirect("/homestartup/")

                    




                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            page=intrests.objects.filter(intrest_name=i.current_intrest).first()
            if page:
                pc=page.intrest_category
            else:
                pc="pg"

            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "bands" : bands,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" :pc,
                "gr" : gr,
                #"form" : form
            }
            return render(request,"serve.html",context)


        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="band":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            bands=band.objects.filter(band_group__in=i.groups)

            

            
            if request.method=='POST':


                if request.POST.get('b_name'): 
                    b_name=request.POST["b_name"]
                    b_des=request.POST["b_des"]
                    b_group = request.POST["team_group"]
                    b=band(band_name=b_name,band_descript=b_des,band_group=b_group)

                    b.band_memb.append(i.id)
                    b.save()
                    instrument=request.POST.getlist("intrument")
                    print instrument
                    for ins in instrument:
                        k = str(ins)
                        num=request.POST[k]
                        print num
                        if k=="guitar":
                            b.band_V_guitar=num
                            b.save()

                        if k=="piano":
                            b.band_V_piano=num
                            b.save()
                        if k=="singer":
                            b.band_V_singer=num
                            b.save()
                        if k=="saxophone":
                            b.band_V_saxophone=num
                            b.save()
                        if k=="violin":
                            b.band_V_violin=num
                            b.save()
                        if k=="bass":
                            b.band_V_bass=num
                            b.save()
                        if k=="drum":
                            b.band_V_drum=num
                            b.save()
                        if k=="flute":
                            b.band_V_flute=num
                            b.save()
                        if k=="tabla":
                            b.band_V_tabla=num
                            b.save()
                        if k=="other":
                            b.band_V_other=num
                            b.band_other = request.POST["b_other"]
                            b.save()

                    

                    for count, x in enumerate(request.FILES.getlist("files")):
                        def process(f):
                            with open('/home/raftar/ttl/babaS/media_in_env/media_root/file_band_' +str(b.id)+'.jpg', 'wb+') as destination:
                                for chunk in f.chunks():
                                    destination.write(chunk)

                        process(x)

                        infile='/home/raftar/ttl/babaS/media_in_env/media_root/file_band_' +str(b.id)+'.jpg'
                        thumb(infile)




                    state="Your band has been created... Now rock the world with your talent..."
                    messages.success(request,state)
                    return HttpResponseRedirect("/homeband/")
                    




                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            page=intrests.objects.filter(intrest_name=i.current_intrest).first()
            if page:
                pc=page.intrest_category
            else:
                pc="pg"
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "bands" : bands,

                "g" : g.intrest_memb_id,
                
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" :pc,
                "gr" : gr,
                #"form" : form
            }
            return render(request,"serve.html",context)   



        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="movie":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            films = create_film.objects.all()
            tri =trip.objects.filter(trip_city='Film')
            a = all_memb(request)

            t_ch=""

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

                if request.POST["t_chosen"]:
                    t_c=request.POST["t_chosen"]
                    t_ch=trip.objects.filter(id=t_c).first()
                    return HttpResponseRedirect("/view_watcher/?q="+str(t_c))

                    

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "films":films,
                "page" : g.intrest_category,
                
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                
                "gr" : gr,
                "trip": tri,
                "t_ch":t_ch,
                
                #"form" : form
            }

            return render(request,"serve.html",context)


        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="tour":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            teams=team.objects.all()
            cities = create_city.objects.all()
            tri =trip.objects.all().exclude(trip_city='Film').exclude(trip_city='Cafe')
            a = all_memb(request)

            t_ch=""

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

                if request.POST["t_chosen"]:
                    t_c=request.POST["t_chosen"]
                    t_ch=trip.objects.filter(id=t_c).first()
                    return HttpResponseRedirect("/viewtraveller/?q="+str(t_c))

                    

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "page" : g.intrest_category,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "cities":cities,
                "gr" : gr,
                "trip": tri,
                "t_ch":t_ch,
                
                #"form" : form
            }

            return render(request,"serve.html",context)


        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="double":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost =posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            teams=team.objects.filter(team_category=i.current_intrest)
            if team.objects.filter(team_category=i.current_intrest,team_memb__contains=i.id).first():
                play = team.objects.filter(team_category=i.current_intrest,team_memb__contains=i.id).first()

            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if student.objects.filter(current_intrest=i.current_intrest,groups__contains=t_g).exclude(id=i.id):
                        stud=student.objects.filter(current_intrest=i.current_intrest,groups__contains=t_g).exclude(id=i.id)
                        z=""
                        for s in stud:
                            z="<tr style='padding-left:30px;width:100%;'><td width='15%' align='right'><div id='timg' ><img src='/media/file_p"+str(s.id)+"_"+str(s.prof_pic[-1])+".thumbnail'  style='width:100%;'></div></td><td width='70%' style='padding-left:15px;'><div style='font-:14px;'>"+str(s.first_name)+" "+str(s.last_name)+" </div><div id='n' style='font-size:12px;float:left;opacity:0.5;'> "+str(s.current_intrest)+" </div></div></td><td width='5%'><input type='checkbox' name='invited' value='"+str(s.id)+"'>"+z

                    else:
                        teams=''
                        z="<center><b> No Players Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')

                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "play" : play,
                "page" : g.intrest_category,
                
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                
                "gr" : gr,
                #"form" : form
            }
            return render(request,"serve.html",context)


        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="lan":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                    

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_gamers/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_gamers/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %} style='color:white;background-color: #0078d7;'><b><td style='font-size:14px;width:40%;padding-left: 30px;'>"+str(n.team_name)+"</td><td style='width:25%;font-size:14px'>"+str(n.game_server)+"</td><td style='width:20%;font-size:14px'>"+str(n.game_name)+"</td><td style='width:15%;font-size:14px'><button type='submit'>Join Team</button></td></b></tr>"+z

                            #<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_gamers/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_gamers/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %} style='color:white;background-color: #0078d7;'><b><td style='font-size:14px;width:40%;padding-left: 30px;'>"+str(n.team_name)+"</td><td style='width:25%;font-size:14px'>"+str(n.game_server)+"</td><td style='width:20%;font-size:14px'>"+str(n.game_name)+"</td><td style='width:15%;font-size:14px'><button type='submit'>Join Team</button></td></b></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            
            if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
                play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
                tm=team.objects.filter(id=play).first()
                if tm:
                    if tm.team_capt==i.id:
                        capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"serve.html",context)
        

        elif intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="chat":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                    

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            
            if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
                play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
                tm=team.objects.filter(id=play).first()
                if tm:
                    if tm.team_capt==i.id:
                        capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]

            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"homechat.html",context)


        else:
            state="Make any Intrest from the suggested list!!!"
            messages.error(request,state)
            return render(request,"serve.html",{})



    else:
        return HttpResponse("Login first!!!")


def edit(request):
    form = regs_form(request.POST or None)
    head = 'Edit Your Profile'
    title="Welcome"
    active = True
    u = regs.objects.filter(id=2).first()
    if request.method=='POST':
        post = request.POST
        
        u.First_Name = post["First_Name"]

        u.Last_Name = post["Last_Name"]
        u.City = post["City"]

        u.save(update_fields=['First_Name','Last_Name','City'])


    title = u.First_Name+" "+u.Last_Name

    # q = regs.objects.get(id=0)
    # # q.First_Name = i.First_Name
    # q.delete()
    
    #i=form.save(commit=False)
    # # m=i.save()
    # # regs.objects.all()
    # n=i.id
    # fn =i.First_Name
    # ln =i.Last_Name
    # ci =i.City
    # i.id=n
    # i.First_Name=fn
    # i.Last_Name=ln
    # i.City=ci



    # if form.is_valid():
    #     i.First_Name = form.cleaned_data.get("First_Name")
    #     i.Last_Name = form.cleaned_data.get("Last_Name")
    #     i.City = form.cleaned_data.get("City")

    # print i.First_Name

    # #i = form(First_Name="Nitin", Last_Name = "Vishwari", City ="Varanasi")
    # # first_name = form.cleaned_data.get("First_Name")
    # # if not first_name:
    # #     first_name = "User"
    #i.save()
    
    # i.First_Name = first_name
    
    
    # first_name=i.First_Name
    # if first_name:
    

    # q=regs.objects.get(id=1)
    # q.First_Name=title
    # q.save()

    context = {
        "title" : title,
        "counter" : counter,
        "form" : form,
        "head" : head
    }


    # if form.is_valid(): 
    #     instance = form.save(commit=False)
    # # if not instance._name:
    # #     instance.full_name = "mohan"
    #     instance.save()
    #     context ={
    #         "title" : "Thank You!!!"
    #     }
    return render(request,"edit_prof.html",context)






def homelan(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        play=''
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="lan":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''
            play=""



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                    

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_gamers/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_gamers/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %} style='color:white;background-color: #0078d7;'><b><td style='font-size:14px;width:40%;padding-left: 30px;'>"+str(n.team_name)+"</td><td style='width:25%;font-size:14px'>"+str(n.game_server)+"</td><td style='width:20%;font-size:14px'>"+str(n.game_name)+"</td><td style='width:15%;font-size:14px'><button type='submit'>Join Team</button></td></b></tr>"+z

                            #<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_gamers/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_gamers/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %} style='color:white;background-color: #0078d7;'><b><td style='font-size:14px;width:40%;padding-left: 30px;'>"+str(n.team_name)+"</td><td style='width:25%;font-size:14px'>"+str(n.game_server)+"</td><td style='width:20%;font-size:14px'>"+str(n.game_name)+"</td><td style='width:15%;font-size:14px'><button type='submit'>Join Team</button></td></b></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            
            tm=team.objects.filter(team_memb__contains=i.id,team_category=i.current_intrest).first()
            if tm:
                play = tm.id
                if tm.team_capt==i.id:
                    capt=i.id
            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"homelan.html",context)
            

        else:
            state="Make gaming as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homelan.html",{})
            


    else:
        return HttpResponse("Login first!!!")


def homechat(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        play=''
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="chat":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                    

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            
            if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
                play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
                tm=team.objects.filter(id=play).first()
                if tm:
                    if tm.team_capt==i.id:
                        capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]

            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"homechat.html",context)
            

        else:
            state="Make chatting as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homechat.html",{})
            


    else:
        return HttpResponse("Login first!!!")   


def Homedouble(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        play = ""
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="double":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost =posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            teams=team.objects.filter(team_category=i.current_intrest)
            if team.objects.filter(team_category=i.current_intrest,team_memb__contains=i.id).first():
                play = team.objects.filter(team_category=i.current_intrest,team_memb__contains=i.id).first()

            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if student.objects.filter(current_intrest=i.current_intrest,groups__contains=t_g).exclude(id=i.id):
                        stud=student.objects.filter(current_intrest=i.current_intrest,groups__contains=t_g).exclude(id=i.id)
                        z=""
                        for s in stud:
                            z="<tr style='padding-left:30px;width:100%;'><td width='15%' align='right'><div id='timg' ><img src='/media/file_p"+str(s.id)+"_"+str(s.prof_pic[-1])+".thumbnail'  style='width:100%;'></div></td><td width='70%' style='padding-left:15px;'><div style='font-:14px;'>"+str(s.first_name)+" "+str(s.last_name)+" </div><div id='n' style='font-size:12px;float:left;opacity:0.5;'> "+str(s.current_intrest)+" </div></div></td><td width='5%'><input type='checkbox' name='invited' value='"+str(s.id)+"'>"+z

                    else:
                        teams=''
                        z="<center><b> No Players Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')

                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "play" : play,
 
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                
                "gr" : gr,
                #"form" : form
            }
            return render(request,"homedouble.html",context)

        else:
            return HttpResponse("Make a single or double player game as your Intrest!!!")



    else:
        return HttpResponse("Login first!!!")




    
    

def Hometour(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="tour":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            teams=team.objects.all()
            cities = create_city.objects.all()
            tri =trip.objects.all().exclude(trip_city='Film').exclude(trip_city='Cafe')
            a = all_memb(request)

            t_ch=""

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

                if request.POST["t_chosen"]:
                    t_c=request.POST["t_chosen"]
                    t_ch=trip.objects.filter(id=t_c).first()
                    return HttpResponseRedirect("/viewtraveller/?q="+str(t_c))

            for tn in tri:
                if timezone.now().date() > tn.trip_to :
                    tn.delete()
                
            tm=trip.objects.filter(trip_memb__contains=i.id).exclude(trip_city="Cafe").exclude(trip_city="Film")
                  

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "tm" : tm,
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "cities":cities,
                "gr" : gr,
                "trip": tri,
                "t_ch":t_ch,
                
                #"form" : form
            }

            return render(request,"hometour.html",context)

        else:
            return HttpResponse("Make Travelling as ur Intrest!!!")

    else:
        return HttpResponse("Login first!!!")



def Homemovie(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        play = ""
        tm = ""
        capt = ""
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="movie":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            films = create_film.objects.all()
            tri =trip.objects.filter(trip_city='Film')
            a = all_memb(request)

            t_ch=""

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

                if request.POST["t_chosen"]:
                    t_c=request.POST["t_chosen"]
                    t_ch=trip.objects.filter(id=t_c).first()
                    return HttpResponseRedirect("/view_watcher/?q="+str(t_c))

            for tn in tri:
                if timezone.now().date() > tn.trip_to :
                    tn.delete()
                
            tm=trip.objects.filter(trip_memb__contains=i.id, trip_city="Film").first()
            if tm:
                play = tm.id 
                if tm.trip_memb[0]==i.id:
                    capt=i.id

            print play

            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "films":films,
                "play" :play,
                "tm": tm,
                "g" : g.intrest_memb_id,
                "capt":capt,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                
                "gr" : gr,
                "trip": tri,
                "t_ch":t_ch,
                
                #"form" : form
            }

            return render(request,"homemovie.html",context)

        else:
            return HttpResponse("Make Film as ur Intrest!!!")

    else:
        return HttpResponse("Login first!!!")


def Homeband(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="band":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            bands=band.objects.filter(band_group__in=i.groups)

            

            
            if request.method=='POST':


                if request.POST.get('b_name'): 
                    b_name=request.POST["b_name"]
                    b_des=request.POST["b_des"]
                    b_group = request.POST["team_group"]
                    b=band(band_name=b_name,band_descript=b_des,band_group=b_group)

                    b.band_memb.append(i.id)
                    b.save()
                    instrument=request.POST.getlist("intrument")
                    print instrument
                    for ins in instrument:
                        k = str(ins)
                        num=request.POST[k]
                        print num
                        if k=="guitar":
                            b.band_V_guitar=num
                            b.save()

                        if k=="piano":
                            b.band_V_piano=num
                            b.save()
                        if k=="singer":
                            b.band_V_singer=num
                            b.save()
                        if k=="saxophone":
                            b.band_V_saxophone=num
                            b.save()
                        if k=="violin":
                            b.band_V_violin=num
                            b.save()
                        if k=="bass":
                            b.band_V_bass=num
                            b.save()
                        if k=="drum":
                            b.band_V_drum=num
                            b.save()
                        if k=="flute":
                            b.band_V_flute=num
                            b.save()
                        if k=="tabla":
                            b.band_V_tabla=num
                            b.save()
                        if k=="other":
                            b.band_V_other=num
                            b.band_other = request.POST["b_other"]
                            b.save()

                    

                    for count, x in enumerate(request.FILES.getlist("files")):
                        def process(f):
                            with open('/home/raftar/ttl/babaS/media_in_env/media_root/file_band_' +str(b.id)+'.jpg', 'wb+') as destination:
                                for chunk in f.chunks():
                                    destination.write(chunk)

                        process(x)

                        infile='/home/raftar/ttl/babaS/media_in_env/media_root/file_band_' +str(b.id)+'.jpg'
                        thumb(infile)




                    state="Your band has been created... Now rock the world with your talent..."
                    messages.success(request,state)
                    return HttpResponseRedirect("/homeband/")
                    




                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            page=intrests.objects.filter(intrest_name=i.current_intrest).first()
            if page:
                pc=page.intrest_category
            else:
                pc="pg"
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "bands" : bands,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" :pc,
                "gr" : gr,
                #"form" : form
            }
            return render(request,"homeband.html",context)

        else:
            return HttpResponse("Make a Band as your Intrest!!!")



    else:
        return HttpResponse("Login first!!!")

def Homestartup(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="startup":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            bands=startup.objects.filter(startup_group__in=i.groups)

            

            
            if request.method=='POST':


                if request.POST.get('stup_name'): 
                    stup_name=request.POST["stup_name"]
                    stup_des=request.POST["stup_des"]
                    stup_group=request.POST["team_group"]
                    b=startup(startup_name=stup_name,startup_descript=stup_des,startup_group=stup_group)
                    b.startup_memb.append(i.id)
                    b.save()
                    design=request.POST.getlist("design")
                    print design
                    for ins in design:
                        k = str(ins)
                        num=request.POST[k]
                        print num
                        if k=="ceo":
                            b.startup_V_ceo=num
                            b.save()
                        if k=="techop":
                            b.startup_V_techop=num
                            b.save()
                        if k=="sales":
                            b.startup_V_salesmark =num
                            b.save()
                        if k=="hr":
                            b.startup_V_hr =num
                            b.save()
                        if k=="busdev":
                            b.startup_V_busdev=num
                            b.save()
                        if k=="custser":
                            b.startup_V_custser=num
                            b.save()
                        if k=="salesman":
                            b.startup_V_salesman=num
                            b.save()
                        if k=="rnd":
                            b.startup_V_rnd=num
                            b.save()
                        if k=="adm":
                            b.startup_V_adm=num
                            b.save()
                        if k=="other":
                            b.startup_V_other=num
                            b.startup_other = request.POST["stup_other"]
                            b.save()
                    

                    for count, x in enumerate(request.FILES.getlist("files")):
                        def process(f):
                            with open('/home/raftar/ttl/babaS/media_in_env/media_root/file_startup_' +str(b.id)+'.jpg', 'wb+') as destination:
                                for chunk in f.chunks():
                                    destination.write(chunk)

                        process(x)

                        infile='/home/raftar/ttl/babaS/media_in_env/media_root/file_startup_' +str(b.id)+'.jpg'
                        thumb(infile)

                    state="Your startup is set... Now change the world with your idea..."
                    messages.success(request,state)
                    return HttpResponseRedirect("/homestartup/")

                    




                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            page=intrests.objects.filter(intrest_name=i.current_intrest).first()
            if page:
                pc=page.intrest_category
            else:
                pc="pg"

            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "bands" : bands,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" :pc,
                "gr" : gr,
                #"form" : form
            }
            return render(request,"homestartup.html",context)

        else:
            return HttpResponse("Make a Startup as your Intrest!!!")



    else:
        return HttpResponse("Login first!!!")

def homegroup(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""

        play=''
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="group":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    # v = posts.objects.filter(photos=h).first()
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST['typed']
                    print (t_g)
                    if game.objects.filter(game_category=i.current_intrest,game_group=t_g).exclude(game_capt=i.id):
                        teams=game.objects.filter(game_category=i.current_intrest,game_group=t_g).exclude(game_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_game/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_game/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.game_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.game_name)+"</td><td style='width:35%;'>"+str(len(n.game_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
            if play:
                tm=game.objects.filter(id=play).first()
                if tm:
                    if tm.game_capt==i.id:
                        capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"homegroup.html",context)
            

        else:
            state="Make any group game as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homegroup.html",{})
            


    else:
        return HttpResponse("Login first!!!")



def homequick(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        play=""
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="quick":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''
            quicky=quick.objects.all() 



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            for tn in quicky:
                if timezone.now() > tn.quick_to:
                    tn.delete()
                
            tm=quick.objects.filter(quick_memb__contains=i.id).first()
            if tm:
                play = tm.id 
                if tm.quick_memb[0]==i.id:
                    capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            form = quick_form(request.POST or None)
            quicky=quick.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,
                "quick" : quicky,
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                "form" : form
            }

            return render(request,"homequick.html",context)
            

        else:
            state="Make quick event as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homequick.html",{})
            


    else:
        return HttpResponse("Login first!!!")


def homecafe(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        play=""
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="cafe":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name
            tri =trip.objects.filter(trip_city='Cafe')
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

                if request.POST["t_chosen"]:
                    t_c=request.POST["t_chosen"]
                    if trip.objects.filter(id=t_c).first():
                        return HttpResponseRedirect("/view_fooder/?q="+str(t_c))
                    else:
                        state="No such Restaurant found!!!"
                        messages.error(request,state)
                        return HttpResponseRedirect("/home/")
                    
            for tn in tri:
                if timezone.now().date() > tn.trip_to :
                    tn.delete()
                
            tm=trip.objects.filter(trip_memb__contains=i.id, trip_city="Cafe").first()
            if tm:
                play = tm.id 
                if tm.trip_memb[0]==i.id:
                    capt=i.id


            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post
            # if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
            #     play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
            #     if play:
            #         tm=team.objects.filter(id=play).first()
            #         if tm.team_capt==i.id:
            #             capt=c

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            form = quick_form(request.POST or None)
            cafe=create_cafe.objects.all()
            # if trip.objects.filter(trip_city='cafe'):
            #     trip = trip.objects.filter(trip_city='cafe')
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "trip":tri,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,
                "cafe" : cafe,
                
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                "form" : form
            }

            return render(request,"homecafe.html",context)
            

        else:
            state="Make Restaurant as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homecafe.html",{})
            


    else:
        return HttpResponse("Login first!!!")

def homepizza(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        play=""
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="pizza":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[10:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    
                    if pizza.objects.filter(pizza_group=t_g):
                        piz = pizza.objects.filter(pizza_group=int(t_g))
                        z=""
                        for n in piz:
                            
                            
                        
                            z="<tr onclick='document.location=\"../view_pizza/?q="+str(n.id)+"\"' style='color:black;background-color: rgba(255,255,255, 0.6);'><b><td style='font-size:17px;width:20%;padding-left:25px;'>"+str(n.family_name)+"</td><td style='width:10%;font-size:17px'>"+n.hour_from.ctime()+"</td><td style='width:10%;font-size:17px'>"+str(n.hour_to)+"</td><td style='width:15%;font-size:17px'>"+str(n.pizza_coupon)+"</td><td style='width:15%;font-size:17px'>"+str(n.pizza_type)+"</td><td style='width:20%;font-size:17px;padding-right:10px;'><button type='submit'>Join Team</button></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            quick=pizza.objects.all()
            
            for tn in quick:
                if timezone.now() > tn.hour_to:
                    tn.delete()



            tm=pizza.objects.filter(family_memb__contains=i.id).first()
            if tm:
                play = tm.id 

                if tm.family_memb[0]==i.id:
                    capt=i.id
            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:
                if k.logout:
                    last_visit_time = k.logout.tzinfo
                    time=(timezone.now()-k.logout).seconds
                    tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            form = quick_form(request.POST or None)
            quick=pizza.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,
                
                "quick":quick,
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                "form" : form
            }

            return render(request,"homepizza.html",context)
            

        else:
            state="Make Pizza as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homepizza.html",{})
            


    else:
        return HttpResponse("Login first!!!")
