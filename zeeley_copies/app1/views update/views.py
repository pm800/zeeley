
from django.shortcuts import render, render_to_response
from .forms import regs_form
from .forms import student_form,trip_form,quick_form
from .models import student,posts,postcomment,message,create_group,create_city,intrests,create_subjects,team,player,trip,band,groupchat,startup,teamchat,game,quick,create_cafe,create_film,pizza,feedback,cab_share,user_device,notification
from .models import regs
from django.conf import settings
from django.http import HttpResponse
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q
from django.core.context_processors import csrf
import operator
import os,sys,pytz
from PIL import Image
from datetime import datetime
from django.utils import timezone
from django.core import serializers
from django.core.mail import send_mail
from math import radians, cos, sin, asin, sqrt
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import EmailMessage

from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

#from PIL import ThePIL

from twilio.rest import TwilioRestClient
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from time import strftime
import urllib
import requests
import sqlite3
import smtplib
import subprocess
# Create your views here.


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))



def sendmessage(message):
    subprocess.Popen(['notify-send', message])
    return

def pqrs(request):
    #share location
    return render(request,"asdf.html",{})
    

def prof(request):
    
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        # i = my profile object
        
        c = i.id
	
        allpost = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).exclude(anonymous=True).order_by("-id")
        f=allpost[0:9]
        other = allpost[9:]
        
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=='POST':
            
            if request.POST.get('searchbox3'):
                ci = request.POST.get('searchbox3')
                # v = posts.objects.filter(photos=h).first()
                # i.current_intrest = ci
                # i.save()
                # j=intrests.objects.filter(intrest_name=ci).first()
                # if not c in j.intrest_memb_id:
                #     j.intrest_memb_id.append(c)
                #     j.save()


                # if j.intrest_category=="pg" or j.intrest_category=="group"  :
                #     if not player.objects.filter(p_id=c,p_game=ci):
                #         p=player(p_id=c,p_game=ci)
                #         p.save()

                find=intrests.objects.filter(intrest_name=ci).first()
                if find:
                    i.current_intrest = ci
                    i.interest.append(find.id)
                    i.save()
                    

                    if not c in find.intrest_memb_id:
                        find.intrest_memb_id.append(c)
                        find.save()

                    if find.intrest_category=="pg" or find.intrest_category=="group" :
                        if not player.objects.filter(p_id=c,p_game=ci):
                            p=player(p_id=c,p_game=ci)
                            p.save()

                    state="Now Your current interest is "+str(find.intrest_name)
                    messages.success(request,state)


                    return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                else:
                    state="No such interest found !!!"
                    messages.error(request,state)
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


            if request.POST.get('work'):
                work = request.POST.get('work')
                # v = posts.objects.filter(photos=h).first()
                i.work = work
                i.save()
            if request.POST.get('hometown'):
                hometown = request.POST.get('hometown')
                # v = posts.objects.filter(photos=h).first()
                i.hometown = hometown
                i.save()
            if request.POST.get('currentplace'):
                currentplace = request.POST.get('currentplace')
                # v = posts.objects.filter(photos=h).first()
                i.current_place = currentplace
                i.save()

        
        if i.male:
            l="his"

        else:
            l="her"

        active = True
        t = i.post
        
        
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        if len(i.cover_pic)>0:
            j=i.cover_pic[len(i.cover_pic)-1]
        else:
            j=0
        if len(i.prof_pic)>0:
            s=i.prof_pic[len(i.prof_pic)-1]
        else:
            s=0

        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        myteam = team.objects.filter(team_memb__contains=i.id)
        dist = 15000
        m = intrests.objects.filter(intrest_name=i.current_intrest).first()
        zipped = localite(request,i.current_intrest,dist)
        thisteam = team.objects.filter(team_category=m.intrest_name)
        ttmemb = []
        for la in thisteam:
            ttmemb = ttmemb+la.team_memb
        ttmemb = list(set(ttmemb))

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)

        a = student.objects.filter(id__in=java)


        
        context = {
            "title" : title,
            "counter" : counter,
            
            "count" :i.id,
            "t" : t,
            "l" : l,
            "f" : f,
            "other" : other,
            "i" : i,
            "e" : java,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "s" : s,
            "m" : m,
            "zip":zipped,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "page" : pc,
            "myteam":myteam,
            "mythisteam":myteam.filter(team_category=i.current_intrest).first(),
            "ttmemb":ttmemb,
            #"form" : form
        }


        return render(request,'profile1.html',context)



        #response = render_to_response("profile1.html",context)


        # visits = int(request.COOKIES.get('visits', '0'))

        # # Does the cookie last_visit exist?
        # if 'last_visit' in request.COOKIES:
        #     # Yes it does! Get the cookie's value.
        #     last_visit = request.COOKIES['last_visit']
        #     # Cast the value to a Python date/time object.
        #     last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        #     # If it's been more than a day since the last visit...
        #     if (datetime.now() - last_visit_time).seconds > 10:
        #         # ...reassign the value of the cookie to +1 of what it was before...
        #         response.set_cookie('visits', visits+1)
        #         print visits
        #         # ...and update the last visit cookie, too.
        #         response.set_cookie('last_visit', datetime.now())
        # else:
        #     # Cookie last_visit doesn't exist, so create it to the current date/time.
        #     response.set_cookie('last_visit', datetime.now())
        #     print "set_cookie"








        #return response





    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def Upload(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    k=len(i.post)+len(i.prof_pic)+len(i.cover_pic)
    p="/root/ttl/babaS/media_in_env/media_root/file_p" + str(c)+".jpg"

    #os.rename(p,p+".k"+str(k+1))
    i.prof_pic.append(k+1)
    i.save()
    m=posts(identity=c,photos=k+1,hits=0,comments=0,profile_picture=True,creator=i)
    m.save()
    ulx = float(request.POST["ulx"])
    uly = float(request.POST["uly"])
    brx = float(request.POST["brx"])
    bry = float(request.POST["bry"])
    img = request.POST["img"]
    raw_image = '/root/ttl/babaS/media_in_env/media_root/'+img
    original = Image.open(raw_image)
    w,h = original.size
    print (ulx,uly,brx,bry,w*ulx,w*brx,h*uly,h*bry,int(w*ulx),int(w*brx),int(h*uly),int(h*bry))
    cropped_image = original.crop((int(w*ulx),int(h*uly),int(w*brx),int(h*bry)))
    # for x in cropped_image:
    #     def process(f):
    #         with open('/root/ttl/babaS/media_in_env/media_root/file_p' + str(c)+'_'+str(k+1)+".jpg", 'wb+') as destination:
    #             with open('/root/ttl/babaS/media_in_env/media_root/file_' + str(c)+'.jpg', 'wb+') as destination1:
    #                 for chunk in f.chunks():
    #                     destination.write(chunk)
    #                     destination1.write(chunk)

    #     process(x)
    infile1 = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(c)+'_'+str(k+1)+'.jpg'
    infile = '/root/ttl/babaS/media_in_env/media_root/file_' + str(c)+'.jpg'
    cropped_image.save(infile)
    cropped_image.save(infile1)

    photo_resize(infile,800,800)

    outfile = os.path.splitext(infile)[0] + ".thumbnail"
    if infile != outfile:
        try:
            im = Image.open(infile)
            im.thumbnail((128,128),Image.ANTIALIAS)
            im.save(outfile, "JPEG")
        except IOError:
            print "cannot create thumbnail for", infile

    

    outfile1 = os.path.splitext(infile1)[0] + ".thumbnail"
    if infile1 != outfile1:
        try:
            im = Image.open(infile1)
            im.thumbnail((128,128),Image.ANTIALIAS)
            im.save(outfile1, "JPEG")
        except IOError:
            print "cannot create thumbnail for", infile1
    
    if len(set(i.prof_pic))==2:
        os.remove('/root/ttl/babaS/media_in_env/media_root/file_p' + str(c)+'_0'+".jpg")
        os.remove('/root/ttl/babaS/media_in_env/media_root/file_p' + str(c)+'_0'+".thumbnail")


    ctx= {'z':"Success"}
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Upload_cover(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    l=len(i.post)+len(i.prof_pic)+len(i.cover_pic)
    #p="/root/ttl/babaS/media_in_env/media_root/file_c" + str(c)+'.jpg'
    #os.rename(p+".0",p+"."+str(l+1))
    i.cover_pic.append(l+1)
    i.save()
    m=posts(identity=c,photos=l+1,hits=0,comments=0,cover_picture=True,creator=i)
    m.save()
    for count, x in enumerate(request.FILES.getlist("files")):
        def process(f):
            with open('/root/ttl/babaS/media_in_env/media_root/file_c' + str(c)+'_'+str(l+1)+'.jpg', 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

        process(x)

        infile = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(c) +'_'+str(l+1) +'.jpg'
        photo_resize(infile,1300,1300)

        outfile = os.path.splitext(infile)[0] + ".thumbnail"


        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail((400,400),Image.ANTIALIAS)
                im.save(outfile, "JPEG")
            except IOError:
                print "cannot create thumbnail for", infile
        
        do=1
    if len(set(i.cover_pic))==2:
        os.remove('/root/ttl/babaS/media_in_env/media_root/file_c' + str(c)+'_0'+".jpg")
        os.remove('/root/ttl/babaS/media_in_env/media_root/file_c' + str(c)+'_0'+".thumbnail")

    return HttpResponseRedirect('/covfix/')


def Upload_post(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    t = student.objects.get(id=c)
    p=len(t.post)+len(t.prof_pic)+len(t.cover_pic)
    t.post.append(p+1)
    t.save()
    cat=""
    if request.method=='POST':
        if request.POST.get("category"):
            cat = request.POST["category"]
        
        if request.POST["say"]:
            say= request.POST["say"]
            if cat:
                say=say+" #"+cat+" "
           

            if request.FILES.getlist("files"):

                m=posts(identity=c,photos=p+1,hits=0,comments=0,say=say,category=cat,creator=i)
                m.save()
            else:
                m=posts(identity=c,photos=-p-1,hits=0,comments=0,say=say,category=cat,creator=i)
                m.save()

            if "#" in say:
                said=say.split("#")

                for i in said[1:]:
                    saying=i.split(" ")[0]
                    if intrests.objects.filter(Q(tagword__iexact=saying)|Q(intrest_name__iexact=cat)):
                        link_intrst=intrests.objects.filter(Q(tagword__iexact=saying)|Q(intrest_name__iexact=cat)).first()
                        link_intrst.intrest_posts.append(m.id)
                        link_intrst.save()

                    if create_city.objects.filter(Q(tagword__iexact=saying)|Q(city_name__iexact=cat)):
                        link_city=create_city.objects.filter(Q(tagword__iexact=saying)|Q(city_name__iexact=cat)).first()
                        link_city.city_posts.append(m.id)
                        link_city.save()

                    if create_subjects.objects.filter(Q(tagword__iexact=saying)|Q(subjects_name__iexact=cat)):
                        link_subjects=create_subjects.objects.filter(Q(subjects_name__iexact=saying[0])|Q(subjects_name__iexact=cat)).first()
                        link_subjects.subjects_posts.append(m.id)
                        link_subjects.save()

                    if band.objects.filter(Q(tagword__iexact=saying)|Q(band_name__iexact=cat)):
                        link_band=band.objects.filter(Q(band_name__istartswith=saying[0])|Q(band_name__iexact=cat)).first()
                        link_band.band_posts.append(m.id)
                        link_band.save()

                    if startup.objects.filter(Q(tagword__iexact=saying)|Q(startup_name__iexact=cat)):
                        link_startup=startup.objects.filter(Q(startup_name__istartswith=saying[0])|Q(startup_name__iexact=cat)).first()
                        link_startup.startup_posts.append(m.id)
                        link_startup.save()

                    if create_cafe.objects.filter(Q(tagword__iexact=saying)|Q(cafe_name__iexact=cat)):
                        link_cafe=create_cafe.objects.filter(Q(cafe_name__istartswith=saying[0])|Q(cafe_name__iexact=cat)).first()
                        link_cafe.cafe_posts.append(m.id)
                        link_cafe.save()

                    if create_film.objects.filter(Q(tagword__iexact=saying)|Q(film_name__iexact=cat)):
                        link_film=create_film.objects.filter(Q(film_name__istartswith=saying[0])|Q(film_name__iexact=cat)).first()
                        link_film.film_posts.append(m.id)
                        link_film.save()

            if request.POST.get("onoffswitch"):
                if request.POST["onoffswitch"]=="on":
                    print request.POST["onoffswitch"]
                    m.anonymous=True
                    m.save()
                else:
                    m.anonymous=False
                    m.save()

            
        elif request.FILES.getlist("files"):
            m=posts(identity=c,photos=p+1,hits=0,comments=0,category=cat,creator=i)
            m.save()
            if cat:
                if intrests.objects.filter(intrest_name__iexact=cat):
                    link_intrst=intrests.objects.filter(intrest_name__iexact=cat).first()
                    link_intrst.intrest_posts.append(m.id)
                    link_intrst.save()

                if create_city.objects.filter(city_name__iexact=cat):
                    link_city=create_city.objects.filter(city_name__iexact=cat).first()
                    link_city.city_posts.append(m.id)
                    link_city.save()

                if create_subjects.objects.filter(subjects_name__iexact=cat):
                    link_subjects=create_subjects.objects.filter(subjects_name__iexact=cat).first()
                    link_subjects.subjects_posts.append(m.id)
                    link_subjects.save()

                if band.objects.filter(band_name__iexact=cat):
                    link_band=band.objects.filter(band_name__iexact=cat).first()
                    link_band.band_posts.append(m.id)
                    link_band.save()

                if startup.objects.filter(startup_name__iexact=cat):
                    link_startup=startup.objects.filter(startup_name__istartswith=cat).first()
                    link_startup.startup_posts.append(m.id)
                    link_startup.save()

                if create_cafe.objects.filter(cafe_name__iexact=cat):
                    link_cafe=create_cafe.objects.filter(cafe_name__istartswith=cat).first()
                    link_cafe.cafe_posts.append(m.id)
                    link_cafe.save()

                if create_film.objects.filter(film_name__istartswith=cat):
                    link_film=create_film.objects.filter(film_name__istartswith=cat).first()
                    link_film.film_posts.append(m.id)
                    link_film.save()

            if request.POST.get("onoffswitch"):
                if request.POST["onoffswitch"]=="on":
                    print request.POST["onoffswitch"]
                    m.anonymous=True
                    m.save()

            if request.POST.get("onoffswitch"):
                if request.POST["onoffswitch"]=="on":
                    print request.POST["onoffswitch"]
                    m.anonymous=True
                    m.save()

                else:
                    m.anonymous=False
                    m.save()

        else:
            state="No post to upload !!!"
            messages.success(request,state)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

            # return HttpResponse("No post to upload")

    
    

    for count, x in enumerate(request.FILES.getlist("files")):
        def process(f):
            with open('/root/ttl/babaS/media_in_env/media_root/file_' + str(c)+ '_'+ str(p+1)+'.jpg', 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

        process(x)


        infile = '/root/ttl/babaS/media_in_env/media_root/file_' + str(c) +'_'+str(p+1) +'.jpg'

        photo_resize(infile,800,800)
        
        outfile = os.path.splitext(infile)[0] + ".thumbnail"
        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail((128,128),Image.ANTIALIAS)
                im.save(outfile, "JPEG")
            except IOError:
                print "cannot create thumbnail for", infile


    return HttpResponseRedirect('/prof/')

    


def register_user(request):

    form = student_form(request.POST or None)
    title="Welcome"
    context = {
        "form" : form,
        "title" : title
    }


    if request.user.is_authenticated():
        if not request.user.is_superuser:
            i = student.objects.filter(user=request.user).first()
            return HttpResponseRedirect('/open_intrest/?q='+i.current_intrest)
        # return render(request,'.html',context)
    #     return HttpResponse("user.is_authenticated!!!")
        # else:
        #     return HttpResponseRedirect('/prof/')
    if request.POST:
        
        password=request.POST['password']
        # password1=request.POST['confirm_password']
        email=request.POST['email']
        
        first_name=request.POST['first_name']
        if request.POST['last_name']:
            last_name=request.POST['last_name']
        else:
            last_name = ""


        

        # date=request.POST.get('date')
        # month=request.POST.get('month')
        # year=request.POST.get('year')

        male = request.POST.get('male')

        # female = request.POST.get('male')
        # other = request.POST.get('m')
        

        USER=User.objects.filter(username=email)

        if USER:
            messages.error(request,"User already exist")
            #redirect here
            return HttpResponseRedirect("/")
    
        else:
            User.objects.create_user(username=email,password=password,email=email)
            Us=User.objects.get(username=email)
            ####
            
            # Us.last_name = str(datetime.now())
            # Us.save()

            # a=student.objects.all()
            # p=[]
            # for c in a:
            #     p.append(c.id)
                
            s=student(user=Us,status=password,first_name=first_name,last_name=last_name,post=[])
            s.save()
            s.prof_pic.append("0")
            s.cover_pic.append("0")
            # find = []
            # if request.POST["interest"]:
            #     interest = request.POST["interest"]
            #     find = intrests.objects.filter(intrest_name__exact=interest).first()
            # else:
            #     find = intrests.objects.filter(intrest_name__exact="Chat").first()
            # if not find:
            find = intrests.objects.filter(intrest_name__exact="Chat").first()
            change_interest(request,s,find)

            s.tagword = email

            

            # infile1 = '/root/ttl/babaS/media_in_env/media_root/defaultcover.jpg'
            # outfile1 = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

            
            # if infile1 != outfile1:
            #     try:
            #         im = Image.open(infile1)
            #         im.thumbnail((1600,900),Image.ANTIALIAS)
            #         im.save(outfile1, "JPEG")
            #     except IOError:
            #         print "cannot create thumbnail for", infile1

            
            

            # for c in a:
            #     c.add_f_list.append(s.id)
            #     c.save()
            i = student.objects.filter(id=320).first()

            make_frnd(request,s,i)
            

            if male == "male":
                s.male = True
                s.save()
            if male=="female":
                s.female = True
                s.save()
            

            if s.male:
                infile = '/root/ttl/babaS/media_in_env/media_root/default.jpg'
                outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
                if infile != outfile:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

                if infile != outfile2:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile2, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

            else:
                infile = '/root/ttl/babaS/media_in_env/media_root/defaultfemale.jpg'
                outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
                
                if infile != outfile:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

                if infile != outfile2:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile2, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

            thumb(outfile)
            # thumb(outfile1)
            thumb(outfile2)

            # res = send_mail("Hallo "+s.first_name+", Welcome to Zeeley","Open this link to activate your account:: www.zeeley.com/mail_confirm/?q="+str(s.id)+"@."+Us.last_name,"zeeleynoreply@gmail.com",[Us.username])


            user = authenticate(username=email, password=password)
            login(request, user)


            # Us.is_active=False
            # Us.save()


            return HttpResponseRedirect("/more_register/")



        
               

    return render(request,"home.html",context)


def login_user(request):
    state = ""
    username = password = ''
    if request.user.is_authenticated():
        #state="Already logged in, "+request.user.username
        if request.user.is_superuser:
            return HttpResponseRedirect('/admin')
        # profile = Profile.objects.get(user=request.user)
        # if(profile.level==1):
        #     return HttpResponseRedirect(reverse('programme_list', args=(str(dept.objects.filter(head=profile)[0].dept_code),)))
        return HttpResponseRedirect('/open_intrest/?q='+i.current_intrest)
    if request.POST:
        username = request.POST['usernam']
        password = request.POST['pass']

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_superuser:
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                i.online=True
                i.save()

                return HttpResponseRedirect('/admin')
            elif user.is_active:
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                i.online=True
                i.save()
                # profile = Profile.objects.get(user=request.user)
                # if profile.level==1:
                #     return HttpResponseRedirect(reverse('programme_list', args=(str(dept.objects.filter(head=profile)[0].dept_code),)))
                # elif profile.level==8:
                #     return HttpResponseRedirect('/Faculty/Dashboard')
                return HttpResponseRedirect('/open_intrest/?q='+i.current_intrest)
            else:
                state = "Your account is not active, please check your mail or contact the site admin."
                messages.error(request,state)
                return HttpResponseRedirect("/")
        else:
            state = "Your username and/or password were incorrect."
            messages.error(request,state)
            return HttpResponseRedirect("/")


    return render(request, 'login.html',{})


def logout_user(request):
    form = student_form(request.POST or None)
    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        i.online=False
        i.logout=timezone.now()
        i.save()
        logout(request)
        # state="Thank You for Stopping by !!!"
        # messages.success(request,state)
        return HttpResponseRedirect("/feedback/")
    else:
        state="Oops ! You are already logged out ! Try logging in again"
        messages.error(request,state)
    return render(request, 'home.html',{"form":form})



def Hit(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    k=""

    if request.method=='POST':
        h = str(request.POST.get('slug'))
        j = str(request.POST.get('sluge'))


        v = posts.objects.filter(identity=j,photos=h).first()
        
        
        if i.id in v.hitters:
            v.hits = v.hits-1
            i=v.hitters.index(c)
            del v.hitters[i]
            k="Hit"
            
            
            
            
        else:
            v.hits = v.hits+1
            v.hitters.append(c)
            k="Unhit"
            # if j!=str(c):
            #     p=student.objects.filter(id=j).first()
            #     # p.n_notif=p.n_notif+1

            #     #sendmessage(i.first_name+ " liked your post!!!")
            #     if v.profile_picture:
            #         p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><a href='../"+str(c)+"' > <b style='color:black;'>"+str(i.first_name)+"</b></a> hitted your profile picture.</td><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(v.identity)+"_"+str(v.photos)+".thumbnail' style='width:50px;height:50px;' onclick='port(this)' ></td></tr><br>"+p.fnotify
            #     elif v.cover_picture:
            #         p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><a href='../"+str(c)+"' > <b style='color:black;'>"+str(i.first_name)+"</b></a> hitted your cover photo.</td><td width='15%' style='padding-left:15px;'></td></tr><br>"+p.fnotify

            #     elif v.photos<0:
            #         p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><a href='../"+str(c)+"' > <b style='color:black;'>"+str(i.first_name)+"</b></a> hitted your post.</td><td style='width:15%;'></td></tr><br>"+p.fnotify

            #     else:
            #         p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><a href='../"+str(c)+"' > <b style='color:black;'>"+str(i.first_name)+"</b></a> hitted your post.</td><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(v.identity)+"_"+str(v.photos)+".thumbnail' style='width:50px;height:50px;' onclick='port(this)' ></td></tr><br>"+p.fnotify
            #     p.save()
                
        v.save()
        
        z=str(v.hits)
        ctx= {'z':z,'k':k,}
    
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Hitband(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    k=""

    if request.method=='POST':
        h = str(request.POST.get('slug'))
        j = str(request.POST.get('sluge'))

        if band.objects.filter(band_name=h,id=int(j)).first():
            v=band.objects.filter(band_name=h,id=int(j)).first()
            if i.id in v.band_hitters:
                
                l=v.band_hitters.index(i.id)
                del v.band_hitters[l]
                k="Hit"

            else:
                
                v.band_hitters.append(i.id)
                k="Unhit"
                if not i.id==v.band_memb[0]:
                    p=student.objects.filter(id=v.band_memb[0]).first()
                    p.n_notif=p.n_notif+1

                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+"</b></a> hitted your band "+str(v.band_name)+".</td><td width='15%' style='padding-left:15px;'><img src='/media/file_band_"+str(v.id)+".thumbnail' style='width:50px;height:50px;' onclick='port(this)' ></td></tr><br>"+p.fnotify
                    p.save()
                
                
            v.save()
            z=str(len(v.band_hitters))
            ctx= {'z':z,'k':k,}
        
            return HttpResponse(json.dumps(ctx), content_type='application/json')


        if startup.objects.filter(startup_name=h,id=int(j)).first():
            v=startup.objects.filter(startup_name=h,id=int(j)).first()
            if i.id in v.startup_hitters:
                
                l=v.startup_hitters.index(i.id)
                del v.startup_hitters[l]
                k="Hit"

            else:
                
                v.startup_hitters.append(c)
                k="Unhit"
                if not i.id==v.startup_memb[0]:
                    p=student.objects.filter(id=v.startup_memb[0]).first()
                    p.n_notif=p.n_notif+1

                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+"</b></a> hitted your startup "+str(v.startup_name)+".</td><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(v.id)+".thumbnail' style='width:50px;height:50px;' onclick='port(this)' ></td></tr><br>"+p.fnotify
                    p.save()
            
              
                       
            v.save()
        
            z=str(len(v.startup_hitters))
            ctx= {'z':z,'k':k,}
        
            return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        return HttpResponse("No such Group found!!!" )

def comment(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    n=0
    if request.method=='POST':
        h = request.POST.get('coment')
        k = request.POST.get('slug')
        j = request.POST.get('sluge')
        if h:
            s = posts.objects.filter(identity=j,photos=k).first()
            m = postcomment(c_identity=i.id,c_photos=s.id,c_say=h,c_intity=j)
            m.save()
            s.comments=s.comments+1
            s.comnt.append(c)
            s.save()
            n=s.comments
            l = student.objects.filter(id=m.c_identity).first()
            z="/media/file_"+str(l.id)+".thumbnail"+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
            if j!=str(c):
                p=student.objects.filter(id=j).first()
                
                if s.profile_picture:
                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(c)+"_"+str(i.prof_pic[-1])+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>profile picture.</a> : "+str(h[:15])+"...</td><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(s.identity)+"_"+str(s.photos)+".thumbnail' style='width:50px;height:50px;' onclick='port(this)' ></td></tr><br>"+p.fnotify
                    purp = "comment#"+"file_p"+str(s.identity)+"_"+str(s.photos)


                elif s.photos <= 0 :
                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(c)+"_"+str(i.prof_pic[-1])+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><td width='15%' style='padding-left:15px;'></td></tr><br>"+p.fnotify
                    purp = "comment#_"+s.say[:50]
                elif s.cover_picture:
                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(c)+"_"+str(i.prof_pic[-1])+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your cover picture. : "+str(h[:15])+"...</td><td width='15%' style='padding-left:15px;'></td></tr><br>"+p.fnotify
                    purp = "comment#"+"file_c"+str(s.identity)+"_"+str(s.photos)


                else:
                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(c)+"_"+str(i.prof_pic[-1])+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='60%' style='padding-right:18px;'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(s.identity)+"_"+str(s.photos)+".thumbnail' style='width:50px;height:50px;' onclick='port(this)' ></td></tr><br>"+p.fnotify
                    
                    purp = "comment#"+"file_"+str(s.identity)+"_"+str(s.photos)

                p.save()
                if i.id != p.id:
                    p.n_notif=p.n_notif+1
                    p.save()
                    notif_msg = i.first_name.split(" ")[0] +" commented on your post"
                    
                    new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose=purp,purpose_id=m.id)
                    new_notif.save()

    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z,'n':n}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def opine(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    x=''
    if request.method=='POST':
        j = request.POST.get('sluge')
        k = request.POST.get('slug')
        s = posts.objects.filter(identity=j,photos=k).first()
        m = postcomment.objects.filter(c_intity=j,c_photos=s.id)
        if m:
            for c in m:
                l = student.objects.filter(id=c.c_identity).first()
                z=z+"/media/file_"+str(l.id)+".thumbnail"+"@#$"+l.first_name+" "+l.last_name+"@#$"+str(c.post_time)+"@#$"+c.c_say+"@#$"+str(c.id)+"@#$@#$"
                x=j
        else :
            z="NO"


    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z,"y":i.id,"x":x}
    # ctx = 'yes'
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def pr(request,prof_id=5):

    if request.user.is_authenticated() :
    
        b= student.objects.filter(user=request.user).first()
        
        if student.objects.filter(id=prof_id):
        
            i=student.objects.filter(id=prof_id).first()
            name = i.first_name+" "+i.last_name
            if not i:
                return HttpResponse("No such Url exists!!!")
            myteam = team.objects.filter(team_memb__contains=b.id)

            if i.id==b.id:
                return HttpResponseRedirect("/prof/")

            counter=""
            e=student.objects.filter(id__in=b.f_list)
            gr = create_group.objects.filter(cg_memb_id__contains=i.id)

            grm=[]
            for k in gr:
                print k.cg_memb_id
                grm=grm+[x for x in k.cg_memb_id if x not in grm]
            grm = grm+[x for x in i.f_list if x not in grm]

            print grm

            print i
            
            if i.priv_prof==1:
                
                
                

                a=student.objects.filter(id__in=i.f_list)
                c = i.id
                allpost = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).exclude(anonymous=True).order_by("-id")
                f=allpost[0:9]
                other = allpost[9:]
                counter = "p"+str(b.id)+"_"+str(b.prof_pic[-1])


                bomb = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
                title = b.first_name+" "+b.last_name
                if i.male:
                    l="his"

                else:
                    l="her"
                
                
                t = i.post

                if len(i.cover_pic)>0:
                    j=i.cover_pic[len(i.cover_pic)-1]
                else:
                    j=0

                page=intrests.objects.filter(intrest_name=b.current_intrest).first()
                if page:
                    pc=page.intrest_category
                else:
                    pc="pg"
                dist = 15000
                m = intrests.objects.filter(intrest_name=b.current_intrest).first()
                zipped = localite(request,b.current_intrest,dist)
                thisteam = team.objects.filter(team_category=m.intrest_name)
                ttmemb = []
                for la in thisteam:
                    ttmemb = ttmemb+la.team_memb
                ttmemb = list(set(ttmemb))
                

                java=b.f_list
                mess = message.objects.filter(m_id=b.id).values("m_friend").distinct()
                    
                for k in mess:
                    if k["m_friend"] not in java:
                        java.append(k["m_friend"])

                for ka in b.msg_list:
                    if ka not in java:
                        java.append(ka)

                a=student.objects.filter(id__in=java)


                context = {
                    "title" : title,
                    "counter" : counter,
                    "name" :name,
                    "count" : b.id,
                    "bomb" : bomb,
                    "grm" : grm,
                    "t" : t,
                    "l" : l,
                    "f" : f,
                    "other" : other,
                    "i" : i,
                    "j" : j,
                    "af" : a[0:9],
                    "a" : e,
                    "e" : java,
                    "b" : b,
                    "page" : pc,
                    "cov_r":i.cov_r,
                    "cov_g":i.cov_g,
                    "cov_b":i.cov_b,
                    "m" :m,
                    "zip": zipped,
                    "ttmemb":ttmemb,
                    "mythisteam":thisteam.filter(team_memb__contains=b.id).first(),
                    "myteam":myteam,
                    #"form" : form
                }
                return render(request,"profile10.html",context)

            if i.priv_prof==2:
                if i.id in b.f_list:
                    a=student.objects.filter(id__in=i.f_list)
                    
                    c = i.id
                    allpost = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).order_by("-id")
                    f=allpost[0:9]
                    other = allpost[9:]
                    count = str(b.id)
                    title = b.first_name+" "+b.last_name
                    counter = "p"+str(b.id)+"_"+str(b.prof_pic[-1])
                    bomb = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
                    if i.male:
                        l="his"

                    else:
                        l="her"

                    
                    t = i.post

                    if len(i.cover_pic)>0:
                        j=i.cover_pic[len(i.cover_pic)-1]
                    else:
                        j=0

                    page=intrests.objects.filter(intrest_name=b.current_intrest).first()
                    if page:
                        pc=page.intrest_category
                    else:
                        pc="pg"
                    dist = 15000
                    m = intrests.objects.filter(intrest_name=b.current_intrest).first()
                    zipped = localite(request,b.current_intrest,dist)
                    thisteam = team.objects.filter(team_category=m.intrest_name)
                    ttmemb = []
                    for la in thisteam:
                        ttmemb = ttmemb+la.team_memb
                    ttmemb = list(set(ttmemb))
                    

                    java=b.f_list
                    mess = message.objects.filter(m_id=b.id).values("m_friend").distinct()
                        
                    for k in mess:
                        if k["m_friend"] not in java:
                            java.append(k["m_friend"])

                    for ka in b.msg_list:
                        if ka not in java:
                            java.append(ka)

                    a=student.objects.filter(id__in=java)

                    context = {
                        "title" : title,
                        "counter" : counter,
                        "name" :name,
                        "count" : i.id,
                        "t" : t,
                        "grm" : grm,
                        "l" : l,
                        "b" : b,
                        "f" : f,
                        "other" : other,
                        "i" : i,
                        "j" : j,
                        "af" : a,
                        "bomb":bomb,
                        "a" : e,
                        "e" : java,
                        "page" : pc,
                        "cov_r":i.cov_r,
                        "cov_g":i.cov_g,
                        "cov_b":i.cov_b,
                        "m" :m,
                        "zip": zipped,
                        "ttmemb":ttmemb,
                        "mythisteam":thisteam.filter(team_memb__contains=b.id).first(),
                        "myteam":myteam,
                        #"form" : form
                    }
                    return render(request,"profile10.html",context)
                else:
                    state="Sorry you have no permission to access this profile!!!"
                    messages.error(request,state)
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                    
            if i.priv_prof == 3:
                
                if b.id in grm:
                    a=student.objects.filter(id__in=i.f_list)
                    
                    c = i.id
                    allpost = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).order_by("-id")
                    f=allpost[0:9]
                    other = allpost[9:]
                    count = str(b.id)
                    title = b.first_name+" "+b.last_name
                    counter = "p"+str(b.id)+"_"+str(b.prof_pic[-1])
                    bomb = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
                    if i.male:
                        l="his"

                    else:
                        l="her"

                    
                    t = i.post

                    if len(i.cover_pic)>0:
                        j=i.cover_pic[len(i.cover_pic)-1]
                    else:
                        j=0

                    page=intrests.objects.filter(intrest_name=b.current_intrest).first()
                    if page:
                        pc=page.intrest_category
                    else:
                        pc="pg"
                    dist = 15000
                    m = intrests.objects.filter(intrest_name=b.current_intrest).first()
                    zipped = localite(request,b.current_intrest,dist)
                    thisteam = team.objects.filter(team_category=m.intrest_name)
                    ttmemb = []
                    for la in thisteam:
                        ttmemb = ttmemb+la.team_memb
                    ttmemb = list(set(ttmemb))


                    java=b.f_list
                    mess = message.objects.filter(m_id=b.id).values("m_friend").distinct()
                        
                    for k in mess:
                        if k["m_friend"] not in java:
                            java.append(k["m_friend"])

                    for ka in b.msg_list:
                        if ka not in java:
                            java.append(ka)

                    a=student.objects.filter(id__in=java)
                    
                    context = {
                        "title" : title,
                        "counter" : counter,
                        "name" :name,
                        "count" : i.id,
                        "grm" : grm,
                        "b" : b,
                        "t" : t,
                        "l" : l,
                        "f" : f,
                        "other" : other,
                        "bomb":bomb,
                        "i" : i,
                        "j" : j,
                        "af" : a,
                        "a" : e,
                        "e" : java,
                        "page" : pc,
                        "cov_r":i.cov_r,
                        "cov_g":i.cov_g,
                        "cov_b":i.cov_b,
                        "m" :m,
                        "zip": zipped,
                        "ttmemb":ttmemb,
                        "mythisteam":thisteam.filter(team_memb__contains=b.id).first(),
                        "myteam":myteam,
                        #"form" : form
                    }
                    return render(request,"profile10.html",context)

                else:
                    state="Sorry you have no permission to access this profile!!!"
                    messages.error(request,state)
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            state="Oops... No such User found!!!"
            messages.error(request,state)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def all_pr(request):
    a=all_memb(request)
    i=student.objects.filter(user=request.user).first()
    c = i.id
    f = posts.objects.filter(identity=c)
    count = "p"+str(c)+"_"+str(i.prof_pic[-1])
    counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
    title = i.first_name
    t = i.post
    context = {
        "title" : title,
        "counter" : counter,
        "count" : i.id,
        "t" : t,
        #"l" : l,
        "i" : i,
        "a" : a,
        "b" : i.add_f_list,
        "d" : i.accept_f_list,
        "e" : i.f_list,
        #"form" : form
    }



    return render(request,"profile9.html",context)

def send_request(request,req_id=1):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        j=student.objects.filter(id=req_id).first()
        state = ""
        if j.id not in i.f_list:
            i.f_list.append(j.id)
            j.accept_f_list.append(i.id)
            j.save()
            
            i.save()
            

            state="UnFollow"
            ctx= {'z':state}
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        if j.id in i.f_list:
            i.f_list.remove(j.id)
            if i.id in j.accept_f_list:
                j.accept_f_list.remove(i.id)
                j.save()
            
            i.save()
            

            state="Follow"
            ctx= {'z':state}
            return HttpResponse(json.dumps(ctx), content_type='application/json')

        # messages.success(request,state)
        # return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

# def accept_request(request,req_id=1):
#     if request.user.is_authenticated():
#         i=student.objects.filter(user=request.user).first()
#         j=student.objects.filter(id=req_id).first()
#         if j.id in i.accept_f_list:
#             j.f_list.append(i.id)
#             i.f_list.append(j.id)
#             # l=j.sent_f_list.index(i.id)
#             # del j.sent_f_list[l]
#             k=i.accept_f_list.index(j.id)
#             del i.accept_f_list[k]
#             i.save()
#             j.save()

#         return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
#     else:
#         state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

# def del_request(request,req_id=1):
#     if request.user.is_authenticated():
#         i=student.objects.filter(user=request.user).first()
#         j=student.objects.filter(id=req_id).first()
#         if i.id in j.accept_f_list:
#             k=j.accept_f_list.index(i.id)
#             del j.accept_f_list[k]
#             j.add_f_list.append(i.id)
#             j.save()
#             i.add_f_list.append(j.id)
#             i.save()
            

#         return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#     else:
#         state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def interests(request):
    if request.user.is_authenticated():
        a=all_memb(request)
        i=student.objects.filter(user=request.user).first()
        c = i.id
        
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        t = i.post
        j = intrests.objects.all()

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)



        il=len(i.interest)
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "t" : t,
            #"l" : l,
            "j" : j,
            
            "i" : i,
            "a" : a,
            # "b" : i.add_f_list,
            # "d" : i.accept_f_list,
            "e" : java,
            "il":il,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            #"form" : form
        }



        return render(request,"interest.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def photo(request):
    if request.user.is_authenticated():
        q = request.GET.get('q','')
        b= student.objects.filter(user=request.user).first()
        i=student.objects.filter(id=q).first()
        c = b.id
        f=posts.objects.filter(identity=i.id)
        counter = "p"+str(c)+"_"+str(b.prof_pic[-1])
        title = b.first_name+" "+b.last_name
        
        t = i.post
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        myteam = team.objects.filter(team_memb__contains=b.id)

        java=b.f_list
        mess = message.objects.filter(m_id=b.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in b.msg_list:
            if ka not in java:
                java.append(ka)

        a=student.objects.filter(id__in=java)


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "b": b,
            "t" : t,
            #"l" : l,
            "f" : f,
            "i" : i,
            "a" : a,
            "page" : pc,
            "e" : java,
            
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "myteam":myteam,
            #"form" : form
        }




        return render(request,"photos.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def frn(request):
    if request.user.is_authenticated():
        q = request.GET.get('q','')
        
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if str(i.id) == q:
            h=student.objects.filter(id__in=i.f_list)
            if len(h)>6:
                h=h[(len(h)-6):len(h)]

            b=student.objects.all().exclude(id__in=i.f_list)
            if len(b)>6:
                b=b[(len(b)-6):len(b)]
            d=student.objects.filter(id__in=i.accept_f_list)
            if len(d)>6:
                d=d[(len(d)-6):len(d)]
            #f = posts.objects.filter(identity=c)
            
            t = i.post
            myteam = team.objects.filter(team_memb__contains=i.id)

            java=i.f_list
            mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
                
            for k in mess:
                if k["m_friend"] not in java:
                    java.append(k["m_friend"])

            for ka in i.msg_list:
                if ka not in java:
                    java.append(ka)

            a=student.objects.filter(id__in=java)

            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                #"l" : l,
                
                "i" : i,
                "a" : a,
                "b" : b,
                "d" : d,
                "e" : java,
                "h" : h,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" : pc,
                "myteam":myteam,
                "my_id":i.id,
                #"form" : form
            }


            return render(request,"freinds.html",context)
        else:
            s=student.objects.filter(id=int(q)).first()
            h=student.objects.filter(id__in=s.f_list)
            title = s.first_name
            if len(h)>6:
                h=h[(len(h)-6):len(h)]
            
            #f = posts.objects.filter(identity=c)
            
            java=i.f_list
            mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
                
            for k in mess:
                if k["m_friend"] not in java:
                    java.append(k["m_friend"])
            
            for ka in s.msg_list:
                if ka not in java:
                    java.append(ka)

            a=student.objects.filter(id__in=java)
            context = {
                "title" : title,
                "counter" : counter,
                "count" : s.id,
                "i" : s,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" : pc,
                "a" : a,
                "e" : java,
                "my_id":i.id,
                
                
                
                
                
                "h" : h,
                
                #"form" : form
            }


            return render(request,"freinds.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



# def Msg(request):
#     i=student.objects.filter(user=request.user).first()
#     c=i.id
#     z=""

#     if request.method=='POST':
#         z = request.POST.get('slug')
#         k = str(z)
#         j = request.POST.get('sluge')
#         # if "https://" in k:
#         #     l= k.split("https://")
#         #     s= l[1].split(" ")
#         #     s[0] = "<a href='"+s[0]+"'>"+s[0]+"</a>"
#         #     l[1]=s.join(" ")
#         #     k=l.join("https://")

#         m = message(m_id=c,m_friend=int(j),m_message=k)
#         m.save()
#         p=student.objects.filter(id=int(j)).first()
#         print (p.id)
#         if not i.id in p.msg_list:
#             p.msg_list.append(i.id)
#             p.n_msg=p.n_msg+1
            
            
#         else:
#             p.msg_list.remove(i.id)
#             p.msg_list.append(i.id)
#             p.n_msg=p.n_msg+1


#         p.fmsg=""

#         for x in p.msg_list:
            
#             y=student.objects.filter(id=int(x)).first()
            
#             msgs=message.objects.filter(m_id=y.id,m_friend=int(j)).last()
#             if msgs:
#                 p.fmsg="<tr height='50px' class='openchat' value="+str(y.id)+"><td width='10%' style='padding-left:8px;'><img src='/media/file_"+str(y.id)+".thumbnail"+"' style='width:35px;height:35px;border-radius:10px;'></td><td width='85%' style='padding-left:8px;'>"+"<b style='color:black;'>"+str(y.first_name)+"</b>:"+str(msgs.m_message)+"</td></tr>"+p.fmsg
#             #"<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><br>"
#             print(p.id)
#         p.save()
        
#         # l = student.objects.filter(id=m.c_identity).first()
#         # z="/media/file_"+str(l.id)+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
#         msgs=message.objects.filter(m_id=i.id,m_friend=p.id).last()
#         z="<div class='sender' style='clear:both;'>"+msgs.m_message+"</div>"

#     # v=postcomment.objects.filter(c_identity=c,c_photos=k)
#     # z="yes"
#     ctx= {'z':z}
#     #ctx = z
#     return HttpResponse(json.dumps(ctx), content_type='application/json')


def Msg(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    if request.method=='POST':
        z = request.POST.get('slug')
        k = str(z)
        j = request.POST.get('sluge')
        # if "https://" in k:
        #     l= k.split("https://")
        #     s= l[1].split(" ")
        #     s[0] = "<a href='"+s[0]+"'>"+s[0]+"</a>"
        #     l[1]=s.join(" ")
        #     k=l.join("https://")
        m = message(m_id=c,m_friend=j,m_message=k)
        m.save()
        p=student.objects.filter(id=j).first()
        if not i.id in p.msg_list:
            p.msg_list.append(i.id)
            p.n_msg=p.n_msg+1
            
        else:
            p.msg_list.remove(i.id)
            p.msg_list.append(i.id)
            p.n_msg=p.n_msg+1
        p.fmsg=""
        for x in p.msg_list:
            if message.objects.filter(m_id=x,m_friend=j):
                msgs=message.objects.filter(m_id=x,m_friend=j).last()
                y=student.objects.filter(id=x).first()
                p.fmsg="<tr height='50px' class='openchat' value="+str(y.id)+"><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(y.id)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'>"+"<b style='color:black;'>"+str(y.first_name)+"</b> <br><span style='font-size:12px;'> "+str(msgs.m_message)+"</span></td></tr>"+p.fmsg
                #"<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><br>"
            
        p.save()
        
        # l = student.objects.filter(id=m.c_identity).first()
        # z="/media/file_"+str(l.id)+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
        z="<div class='sender' style='clear:both;'>"+m.m_message+"</div>"
    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')


# def Msglist(request):
#     i=student.objects.filter(user=request.user).first()
#     c=i.id
#     z=""

#     if request.method=='POST':
#         j = request.POST.get('sluge')
#         m = message.objects.filter(Q(m_id=c,m_friend=j) | Q(m_id=j,m_friend=c))
#         # n = message.objects.filter(m_id=j,m_friend=c)
#         n = student.objects.filter(id=j).first()
#         naam = str(n.first_name)+" "+str(n.last_name)
#         # z="/media/file_"+str(l.id)+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
#         data = serializers.serialize('json', m, fields=('id','m_id','m_friend','m_message','m_admin','post_time'))
#     ctx= {'data':data,'naam':naam,'my_id':i.id}
#     return HttpResponse(json.dumps(ctx), content_type='application/json')

def Msglist(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    if request.method=='POST':
        j = request.POST.get('sluge')
        m = message.objects.filter(Q(m_id=c,m_friend=j) | Q(m_id=j,m_friend=c))
        d = len(m)
        if d>15:
            e = d-15
            m = m[e:d]
        else:
            m=m[:d]    
        # n = message.objects.filter(m_id=j,m_friend=c)
        n = student.objects.filter(id=j).first()
        naam = str(n.first_name)+" "+str(n.last_name)
        
        # z="/media/file_"+str(l.id)+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
        date=""
        for l in m:
            if not l.m_admin:
                tz = pytz.timezone('Asia/Kolkata')
                your_now = l.post_time.astimezone(tz)

                if str(your_now).split(" ")[0]!= date:
                    z=z+"<center><div class='admin'><span style='font-size:14px;'>"+your_now.strftime("%a, %d %b %Y")+"</span></div></center>"
                    date = str(your_now).split(" ")[0]

                if l.m_photo:
                    if l.m_id==i.id:
                        z=z+"<div style='border-radius:8px;float:right;clear:both;'><a href='"+l.m_message.split(".thumbnail")[0]+".jpg' target='_blank'><img src='"+l.m_message+"' style='width:90px;'></a><br><span style='float:right;font-size:9px;'>"+your_now.strftime("%I:%M %p")+"</span></div>"
                    else:
                        z=z+"<div style='border-radius:8px;float:left;clear:both;'><a href='"+l.m_message.split(".thumbnail")[0]+".jpg' target='_blank'><img src='"+l.m_message+"' style='width:90px;'></a><br><span style='float:right;font-size:9px;'>"+your_now.strftime("%I:%M %p")+"</span></div>"

                else:
                    if l.m_id==i.id:
                        z=z+"<div class='sender' style='clear:both;'>"+l.m_message+"<br><span style='float:right;font-size:9px;'>"+your_now.strftime("%I:%M %p")+"</span></div>"
                    else:
                        z=z+"<br><img src='/media/file_"+str(l.m_id)+".thumbnail' class='imgchat' style='float:left;clear:both;'>"+"<div class='receiver' style='clear:both;float:left;'>"+l.m_message+"<br><span style='float:right;font-size:9px;'>"+your_now.strftime("%I:%M %p")+"</span></div>"
        z=z+"<br><br><br>"    
            # else:
            #     z=z+"<center><div class='admin'>"+l.m_message+"<br><span style='float:right;font-size:10px;'>"+l.post_time.strftime("%d %b, %I:%M %p")+"</span></div></center>"
        # for k in n:
        #     z=z+"<img src='/media/file_"+str(k.m_id)+"' style='width:38px;height:38px;border-radius:8px;'>"+"  "+k.m_message+"<br>"
    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z,'naam':naam}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')
        
        # for l in m:
        #     if not l.m_admin:
        #         if l.m_id==i.id:
        #             z=z+"<div class='sender' style='clear:both;'>"+l.m_message+"</div>"
        #         else:
        #             z=z+"<br><img src='/media/file_"+str(l.m_id)+".thumbnail' class='imgchat' style='float:left;clear:both;'>"+"<div class='receiver' style='clear:both;float:left;'>"+l.m_message+"</div>"
        #     else:
        #         z=z+"<center><div class='admin'>"+l.m_message+"</div></center>"


        # for k in n:
        #     z=z+"<img src='/media/file_"+str(k.m_id)+"' style='width:38px;height:38px;border-radius:8px;'>"+"  "+k.m_message+"<br>"

    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    # ctx= {'z':z,'naam':naam}
    #ctx = z
    # return HttpResponse(json.dumps(ctx), content_type='application/json')

def Frqst(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    # i.n_frqst=0
    k = i.accept_f_list
    if len(k):
        for l in k:
            j=student.objects.filter(id=l).first()
            z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a friend request.</td></tr>"

            #<tr><td width="55px">
            # <img src="{%static 'image/pro.jpg'%}" style="width:38px;height:38px;border-radius:8px;"></td><td>
            # Mark Zuckerburg sent you a friend request
            # <br>
            # Accept!!</td>
    else:
        z="No request.."
        
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Fmsg(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    if i.fmsg:
        i.n_msg=0
        z = i.fmsg
    # if len(k):
    #     for l in k:
    #         j=student.objects.filter(id=l).first()
    #         z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+"' style='width:38px;height:38px;border-radius:8px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a message.</td></tr>"

            #<tr><td width="55px">
            # <img src="{%static 'image/pro.jpg'%}" style="width:38px;height:38px;border-radius:8px;"></td><td>
            # Mark Zuckerburg sent you a friend request
            # <br>
            # Accept!!</td>
    else:
        z="No recent messages"
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Chatmsg(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    if i.n_msg:
        i.n_msg=0
        z="Done"
    if i.n_frqst:
        i.n_frqst=0
        
    
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')




# def Fnotif(request):



    # i=student.objects.filter(user=request.user).first()
    # c=i.id
    # z=""
    # k="<tr><td width='15%' style='padding-left:15px;'>"
    # if i.fnotify:
    #     i.n_notif=0
    #     z = i.fnotify
        
    #     s=i.fnotify.split(k)
    #     if len(s)>6:
    #         i.fnotify=""
    #         for n in s[1:5]:
    #             i.fnotify=i.fnotify+k+n
    # # if len(k):
    # #     for l in k:
    # #         j=student.objects.filter(id=l).first()
    # #         z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+"' style='width:38px;height:38px;border-radius:8px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a message.</td></tr>"

    #         #<tr><td width="55px">
    #         # <img src="{%static 'image/pro.jpg'%}" style="width:38px;height:38px;border-radius:8px;"></td><td>
    #         # Mark Zuckerburg sent you a friend request
    #         # <br>
    #         # Accept!!</td>
    # else:
    #     z="No recent notifications"
    # i.save()
    # ctx= {'z':z}
    # #ctx = z
    # return HttpResponse(json.dumps(ctx), content_type='application/json')



def homepg(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        play=''
        
        fl =i.f_list
        mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
        count = c
        for c in fl:
            mylist.append(Q(identity=c))
        # allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
        allpost = posts.objects.all().exclude(anonymous=True).order_by("-id")
        f=allpost[0:9]
        other = allpost[9:]
        
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name

        



        

        
        if request.method=='POST':
            
            if request.POST.get('searchbox3'):
                ci = request.POST.get('searchbox3')
                
                find=intrests.objects.filter(intrest_name=ci).first()
                if find:    

                    change_interest(request,i,find)    
                    return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                else:
                    state="No such interest found !!!"
                    messages.error(request,state)
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                

            

        
        if i.male:
            l="his"

        else :
            l="her"

        active = True
        t = i.post

        
        
        e=i.f_list

        b = student.objects.filter(id__in=e)
        tim=[]
        for k in b:
            if k.logout:
                last_visit_time = k.logout.tzinfo
                time=(timezone.now()-k.logout).seconds
                tim.append(time)

        
        myteam = team.objects.filter(team_memb__contains=i.id)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        a = all_memb(request)
        #k = i.shared_posts

        dist = 15000
        m = intrests.objects.filter(intrest_name=i.current_intrest).first()
        zipped = localite(request,i.current_intrest,dist)

        thisteam = team.objects.filter(team_category=m.intrest_name)
        ttmemb = []
        for la in thisteam:
            ttmemb = ttmemb+la.team_memb
        ttmemb = list(set(ttmemb))

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)





        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            
            
            "l" : l,
            "f" : f,
            "other" : other,
            "i" : i,
            "e" : java,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "myteam": myteam,
            "list":mylist,
            "m" : m,
            "zip" : zipped,
            "ttmemb":ttmemb,
            "mythisteam":thisteam.filter(team_memb__contains=i.id).first(),

            
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "capt" : capt,
            
            #"form" : form
        }

        return render(request,"flippost.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




    


def database(request):
    a=posts.objects.all()
    for c in a:
        c.comnt=[]
        c.comments=0
        z=postcomment.objects.filter(c_photos=c.id)
        for k in z:

            c.comnt.append(k.c_identity)
        c.comments=len(c.comnt)
        c.save()

    return HttpResponse("database Updated")


def group(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=='POST':
            group_name=request.POST['group_name']
            group_lat = request.POST['group_lat']
            group_long=request.POST['group_long']
            m=create_group(cg_name=group_name,cg_lat=group_lat,cg_long=group_long,cg_memb_id=[c])
            m.save()
            p=m.id
            i.groups.append(p)
            i.save()
        
            for count, x in enumerate(request.FILES.getlist("files")):
                def process(f):
                    with open('/root/ttl/babaS/media_in_env/media_root/file_g' + str(c)+ '_'+ str(p)+".jpg", 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)

                process(x)

                infile='/root/ttl/babaS/media_in_env/media_root/file_g' + str(c)+ '_'+ str(p)+".jpg"
                outfile = os.path.splitext(infile)[0] + ".thumbnail"
                if infile != outfile:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((256,256),Image.ANTIALIAS)
                        im.save(outfile, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

                

            state="Your group has been created..."
            messages.success(request,state)
            
            return HttpResponseRedirect("/prof/")

        if i.male:
            l="his"

        if i.female:
            l="her"

        active = True
        t = i.post
        
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)





        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "t" : t,
            "l" : l,
            
            "i" : i,
            "e" : java,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
        return render(request,"group.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")
    



def Rgba(request):
    i=student.objects.filter(user=request.user).first()
    if request.method=='POST':
        red=request.POST['r']
        green = request.POST['g']
        blue=request.POST['b']
        i.cov_r=red
        i.cov_g=green
        i.cov_b=blue
        i.save()

    ctx="("+str(i.cov_r)+","+str(i.cov_g)+","+str(i.cov_b)+")"
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Joingroup(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        g=create_group.objects.all().exclude(id__in=i.groups).order_by('-id')

        b=[]
        for j in g:
            if j.cg_long:
                
                dist = haversine(request,j.cg_long , j.cg_lat , i.longitude  , i.latitude)
                
                if dist < 10000:
                    b.append(j)

        if request.method=='POST':
            gr=request.POST.getlist("sel_group")
            print gr
            for k in gr:
                i.groups.append(int(float(k)))
                l=create_group.objects.filter(id=int(float(k))).first()
                l.cg_memb_id.append(c)
                l.save()


            
            i.save()
            state="You have joined the selected groups..."
            messages.success(request,state)
            
            if i.current_intrest!="Chat":
                return HttpResponseRedirect("/prof/")
            else:
                return HttpResponseRedirect("/interest/")


        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context={
            "i":i,
            "groups":i.groups,
            "p" : p,
            "q" : q,
            "r" : r,
            "g" :b,
            "title" : title,
            "counter" : counter,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }

        return render(request,"nogrp.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Share(request):

    i=student.objects.filter(user=request.user).first()
    c=i.id
    z="NOT DONE"

    if request.method=='POST':
        h = str(request.POST.get('slug'))
        j = str(request.POST.get('sluge'))
        p = posts.objects.filter(identity=j,photos=h).first()
        i.shared_posts.append(p.id)
        i.save()
        z="DONE"


    ctx={"z":z}

    return HttpResponse(json.dumps(ctx), content_type='application/json')




def Addfriend(request):
    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        fs=student.objects.filter(id__in=i.add_f_list)
        if request.method=='POST':
            gr=request.POST.getlist("sel_group")
            for k in gr:
                j=student.objects.filter(id=k).first()
                j.accept_f_list.append(i.id)
                #i.sent_f_list.append(j.id)
                l=j.add_f_list.index(i.id)
                del j.add_f_list[l]
                k=i.add_f_list.index(j.id)
                del i.add_f_list[k]
                # j.n_frqst=j.n_frqst+1
                i.save()
                j.save()

            state="Your requests have been sent..."
            messages.success(request,state)
            
            
            return HttpResponseRedirect("/prof/")




        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context={
            "p" : p,
            "q" : q,
            "r" : r,
            "f" : fs,
        }

        return render(request,"nofrnd.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Create_city(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        # f = posts.objects.filter(identity=c)
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=='POST':
            city_name=request.POST['city_name']
            city_lat = request.POST['city_lat']
            city_long=request.POST['city_long']
            city_des = request.POST['city_descript']
            m=create_city(city_name=city_name,city_lat=city_lat,city_long=city_long,city_descript=city_des)
            m.save()
            p=m.id
            
        
        for count, x in enumerate(request.FILES.getlist("files")):
            def process(f):
                with open('/root/ttl/babaS/media_in_env/media_root/file_city' + '_'+ str(p)+".jpg", 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)

            process(x)

            infile='/root/ttl/babaS/media_in_env/media_root/file_city' + '_'+ str(p)+".jpg"
            thumb(infile)


        

        active = True
        
        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)



        
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            
            
            "i" : i,
            "e" : java,
            
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
        return render(request,"city.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")
    
def Create_cafe(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        #f = posts.objects.filter(identity=c)
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=='POST':
            cafe_name=request.POST['c_name']
            cafe_lat = request.POST['c_lat']
            cafe_long=request.POST['c_long']
            cafe_des = request.POST['c_descript']
            cafe_cont = request.POST['contact']
            m=create_cafe(cafe_name=cafe_name,cafe_lat=cafe_lat,cafe_long=cafe_long,cafe_address=cafe_des,phone_number=cafe_cont)
            m.save()
            p=m.id
            state="The cafe "+str(m.cafe_name)+" has been created. Enjoy it !!!"
            messages.success(request,state)
            return HttpResponseRedirect('/cafe/?q='+str(m.cafe_name))
        # for count, x in enumerate(request.FILES.getlist("files")):
        #     def process(f):
        #         with open('/root/ttl/babaS/media_in_env/media_root/file_cafe' + '_'+ str(p)+".jpg", 'wb+') as destination:
        #             for chunk in f.chunks():
        #                 destination.write(chunk)

        #     process(x)

        #     infile='/root/ttl/babaS/media_in_env/media_root/file_cafe' + '_'+ str(p)+".jpg"
        #     thumb(infile)


        

        active = True
        
        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)



        
        
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            
            
            "i" : i,
            "e" : java,
            
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
        return render(request,"createrest.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Create_film(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        #f = posts.objects.filter(identity=c)
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=='POST':
            film_name=request.POST['c_name']
            
            film_des = request.POST['c_descript']
            
            m=create_film(film_name=film_name,film_descript=film_des)
            m.save()
            p=m.id
            
        
        for count, x in enumerate(request.FILES.getlist("files")):
            def process(f):
                with open('/root/ttl/babaS/media_in_env/media_root/file_film' + '_'+ str(p)+".jpg", 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)

            process(x)

            infile='/root/ttl/babaS/media_in_env/media_root/file_film' + '_'+ str(p)+".jpg"
            thumb(infile)


        

        active = True
        
        
        
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            
            
            "i" : i,
            "e" : i.f_list,
            
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
        return render(request,"createmovie.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def Film(request):
    if request.user.is_authenticated():
        film_name = request.GET.get('q','')
        j = create_film.objects.filter(film_name__iexact=film_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id
        allpost = posts.objects.filter(id__in=j.film_posts).order_by("-id")
        f=allpost[0:9]
        other = allpost[9:]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)



        # form = trip_form(request.POST or None)
        if request.method=="POST":

            #film_name = request.POST["filmname"]
            # t_from = request.POST["film_from"]
            # t_to = request.POST["film_to"]
            if request.POST.get('rate'):
                rate = request.POST['rate']
                r = ((j.film_review*float(len(j.film_reviewer_id)))+float(rate))/(float(len(j.film_reviewer_id))+1.00)
                j.film_review=r
                j.film_reviewer_id.append(i)
                j.save()
            else:

                t_mode = request.POST["theatre"]
                #q_from = request.POST ["timestamp"]
                month = request.POST["month"]
                day = request.POST["day"]
                year = request.POST["year"]
                # q_from=change_date_formt(q_from)
                t_from = year+'-'+month+'-'+day
                t_to = year+'-'+month+'-'+day
                t_memb = request.POST["memb"]
                t=trip(trip_name=j.film_name,trip_from=t_from,trip_to=t_to,trip_mode=t_mode,trip_n_membs=t_memb,trip_city="Film")
                t.trip_memb.append(i.id)
                t.save()
                j.film_memb.append(i.id)
                j.save()
                state="Your trip for "+str(j.film_name).upper()+" has been created. Enjoy it !!!"
                messages.success(request,state)


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            "other" : other,

            "page": pc,
            
        }

        return render(request,"Film.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def City(request):
    if request.user.is_authenticated():
        city_name = request.GET.get('q','')
        j = create_city.objects.filter(city_name__iexact=city_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id
        allpost = posts.objects.filter(id__in=j.city_posts).order_by("-id")
        f=allpost[0:9]
        other = allpost[9:]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)





        form = trip_form(request.POST or None)
        if request.method=="POST":
            t_name = request.POST["tourname"]
            t_from = request.POST["trip_from"]
            t_to = request.POST["trip_to"]
            t_mode = request.POST["mode"]
            t_memb = request.POST["memb"]
            t=trip(trip_name=t_name,trip_from=t_from,trip_to=t_to,trip_mode=t_mode,trip_n_membs=t_memb,trip_city=j.city_name)
            t.trip_memb.append(i.id)
            t.save()
            j.city_memb_id.append(i.id)
            j.save()
            state="Your trip has been created.."
            messages.success(request,state)


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            "other" : other,
            "page": pc,
            "form":form,
        }

        return render(request,"place.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def create_new_intrest(request):
    if request.user.is_authenticated():
        in_names=request.GET.get('q','')
        in_name=in_names.split(".")
        if in_name:
            m = intrests(intrest_name=in_name[0],intrest_category=in_name[1],intrest_catelog=in_name[2])
            m.save()
            return HttpResponse("DONE")
        else:
            return HttpResponse("Nothing to Create Intrest")
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Open_intrest(request):
    if request.user.is_authenticated():
        in_name=request.GET.get('q','')
        m = intrests.objects.filter(intrest_name__iexact=in_name).first()

        if m.intrest_name == "Band":
            return HttpResponseRedirect("/bands/")
        if m.intrest_name == "Restaurant":
            return HttpResponseRedirect("/rest_share/?restaurant=")
        if m.intrest_name == "Film":
            return HttpResponseRedirect("/movie_share/?film=")
        if m.intrest_name == "Entrepreneurship":
            return HttpResponseRedirect("/startups/")

        if m.intrest_name == "Travelling" :
            return HttpResponseRedirect("/cotravelling/?place_from")
        if m.intrest_name == "Cab_Sharing" :
            return HttpResponseRedirect("/cab_sharing/?place_from")
        if m.intrest_name == "Gaming" :
            return HttpResponseRedirect("/pcgame_share/?game=")
        if m.intrest_name == "Quicky" :
            return HttpResponseRedirect("/quicky_share/?quicky=")



        i=student.objects.filter(user=request.user).first()
        c = i.id
        allpost = posts.objects.filter(id__in=m.intrest_posts).order_by("-id").exclude(anonymous=True)
        f=allpost[0:9]
        other = allpost[9:]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        k=str(m.intrest_name)+".html"
        l=student.objects.filter(id__in=m.intrest_memb_id)
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        teams = []
        zipped_team = []
        grp =[]
        if m.intrest_catelog in ["Sports","Gaming"]:
            teamz = team.objects.filter(team_category=m.intrest_name,team_group__in=i.groups)
            for t in teamz:
                group = create_group.objects.filter(id=t.team_group).first()
                grp.append(group.cg_name)
            zipped_team = zip(teamz,grp)  
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        myteam = team.objects.filter(team_memb__contains=i.id)
        mythisteam = myteam.filter(team_category=m.intrest_name).first()

        thisteam = team.objects.filter(team_category=m.intrest_name)
        ttmemb = []
        for la in thisteam:
            ttmemb = ttmemb+la.team_memb
        ttmemb = list(set(ttmemb))

        dist = 15000
        if m.intrest_name == "Dating":
            zipped = localite_date(request,in_name,dist)
        else:
            zipped = localite(request,in_name,dist)
        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)




        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "m" : m,
            "f" : f,
            "other" : other,
            "zip" : zipped,
            "zip_team" :zipped_team,
            "l" : l,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "page" : pc,
            "myteam":myteam,
            "mythisteam":mythisteam,
            "ttmemb" : ttmemb,

        }

        return render(request,"Cricket.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Search(request):
    if request.user.is_authenticated():
        if request.method=='POST':
            k=request.POST.get('searchbox1')
            print k
            if student.objects.filter(first_name__iexact=k):
                m=student.objects.get(first_name__iexact=k)
                return HttpResponseRedirect("/"+str(m.id)+"/")
            
            elif intrests.objects.filter(intrest_name__iexact=k):
                n=intrests.objects.get(intrest_name__iexact=k)
                return HttpResponseRedirect("/open_intrest/?q="+str(n.intrest_name))
            
            elif create_city.objects.filter(city_name__iexact=k):
                o=create_city.objects.get(city_name__iexact=k)
                return HttpResponseRedirect("/city/?q="+str(o.city_name))

            elif create_subjects.objects.filter(subjects_name__iexact=k):
                o=create_subjects.objects.get(subjects_name__iexact=k)
                return HttpResponseRedirect("/subject/?q="+str(o.subjects_name))
            else:
                return HttpResponse("No match found...")
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

        

def Searching(request):
    if request.user.is_authenticated:
        z=""
        s=""
        srcnt=0
        if request.method=='POST':
            k=request.POST.get('typed')
            print k

            if student.objects.filter(first_name__istartswith=k):
                m=student.objects.filter(first_name__istartswith=k)
                s=m.first().first_name
                for c in m:
                    srcnt=srcnt+1
                    z=z+"<a href='/"+str(c.id)+"'><div><tr id='srch"+str(srcnt)+"' class='srtr' onclick='document.location=\"/"+str(c.id)+"\"' style=''><td style='width:16%;overflow:hidden;padding-left:8px;'><img src='/media/file_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.first_name)+" "+str(c.last_name)+"</b></h4>"+str(c.current_intrest)+", "+str(c.current_place)+"<br>From: "+str(c.hometown)+"</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div></a>"


            if intrests.objects.filter(intrest_name__istartswith=k):
                n=intrests.objects.filter(intrest_name__istartswith=k)
                if not s:
                    s=n.first().intrest_name
                for c in n:
                    srcnt=srcnt+1
                    z=z+"<a href='/open_intrest/?q="+str(c.intrest_name)+"'><div><tr id='srch"+str(srcnt)+"' onclick='document.location=\"/open_intrest/?q="+str(c.intrest_name)+"\"' style='border-bottom: 3px solid #EFF3F9;border-left: 4px solid #EFF3F9;border-right: 4px solid #EFF3F9;background-color: #D5DAE2;padding-top: 3px;padding-bottom: 3px;'><td style='width:16%;overflow:hidden;padding:8px;'><img src='/media/intrest/"+str(c.intrest_name)+".thumbnail' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.intrest_name)+"</b></h4>Interest<br>"+str(len(c.intrest_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div></a>"


        
                

            if create_city.objects.filter(city_name__istartswith=k):
                o=create_city.objects.filter(city_name__istartswith=k)
                if not s:
                    s=o.first().city_name
                for c in o:
                    srcnt=srcnt+1
                    z=z+"<a href='/city/?q="+str(c.city_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/city/?q="+str(c.city_name)+"\"' style='border-bottom: 3px solid #EFF3F9;border-left: 4px solid #EFF3F9;border-right: 4px solid #EFF3F9;background-color: #D5DAE2;padding-top: 3px;padding-bottom: 3px;'><td style='width:16%;overflow:hidden;padding:8px;'><img src='/media/file_city_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.city_name)+"</b></h4>Intrest<br>"+str(len(c.city_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div></a>"

            if create_subjects.objects.filter(subjects_name__istartswith=k):
                o=create_subjects.objects.filter(subjects_name__istartswith=k)
                if not s:
                    s=o.first().subjects_name
                for c in o:
                    srcnt=srcnt+1
                    z=z+"<a href='/subject/?q="+str(c.subjects_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/subject/?q="+str(c.subjects_name)+"\"' style='border-bottom: 3px solid #EFF3F9;border-left: 4px solid #EFF3F9;border-right: 4px solid #EFF3F9;background-color: #D5DAE2;padding-top: 3px;padding-bottom: 3px;'><td style='width:16%;overflow:hidden;padding:8px;'><img src='/media/file_sub_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.subjects_name)+"</b></h4>Study<br>"+str(len(c.subjects_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div></a>"

            if startup.objects.filter(startup_name__istartswith=k):
                o=startup.objects.filter(startup_name__istartswith=k)
                if not s:
                    s=o.first().startup_name
                for c in o:
                    srcnt=srcnt+1
                    z=z+"<a href='/entrepreneurship/?q="+str(c.startup_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/entrepreneurship/?q="+str(c.startup_name)+"\"' style='border-bottom: 3px solid #EFF3F9;border-left: 4px solid #EFF3F9;border-right: 4px solid #EFF3F9;background-color: #D5DAE2;padding-top: 3px;padding-bottom: 3px;'><td style='width:16%;overflow:hidden;padding:8px;'><img src='/media/file_startup_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.startup_name)+"</b></h4>Startup<br>"+str(len(c.startup_memb))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div></a>"

            if band.objects.filter(band_name__istartswith=k):
                o=band.objects.filter(band_name__istartswith=k)
                if not s:
                    s=o.first().band_name
                for c in o:
                    srcnt=srcnt+1
                    z=z+"<a href='/band/?q="+str(c.band_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/band/?q="+str(c.band_name)+"\"' style='border-bottom: 3px solid #EFF3F9;border-left: 4px solid #EFF3F9;border-right: 4px solid #EFF3F9;background-color: #D5DAE2;padding-top: 3px;padding-bottom: 3px;'><td style='width:16%;overflow:hidden;padding:8px;'><img src='/media/file_band_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.band_name)+"</b></h4>Band<br>"+str(len(c.band_memb))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div></a>"

            if create_cafe.objects.filter(cafe_name__istartswith=k):
                o=create_cafe.objects.filter(cafe_name__istartswith=k)
                if not s:
                    s=o.first().cafe_name
                for c in o:
                    srcnt=srcnt+1
                    z=z+"<a href='/cafe/?q="+str(c.cafe_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/cafe/?q="+str(c.cafe_name)+"\"' style='border-bottom: 3px solid #EFF3F9;border-left: 4px solid #EFF3F9;border-right: 4px solid #EFF3F9;background-color: #D5DAE2;padding-top: 3px;padding-bottom: 3px;'><td style='width:16%;overflow:hidden;padding:8px;'><img src='/media/cafe1.thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.cafe_name)+"</b></h4>Restaurant</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div></a>"
            
            if create_film.objects.filter(film_name__istartswith=k):
                o=create_film.objects.filter(film_name__istartswith=k)
                if not s:
                    s=o.first().film_name
                for c in o:
                    srcnt=srcnt+1
                    z=z+"<a href='/film/?q="+str(c.film_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/film/?q="+str(c.film_name)+"\"' style='border-bottom: 3px solid #EFF3F9;border-left: 4px solid #EFF3F9;border-right: 4px solid #EFF3F9;background-color: #D5DAE2;padding-top: 3px;padding-bottom: 3px;'><td style='width:16%;overflow:hidden;padding:8px;'><img src='/media/file_film_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.film_name)+"</b></h4>Film</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div></a>"



            
            
            
            ctx= {'z':z,'s':s,}
            
            return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Searching_Intrest(request):
    if request.user.is_authenticated():
        z=""
        s=""
        srcnt=0
        if request.method=='POST':
            k=request.POST.get('typed')
            print k

            if intrests.objects.filter(intrest_name__istartswith=k):
                n=intrests.objects.filter(intrest_name__istartswith=k)
                s=n.first().intrest_name
                
                for c in n:
                    srcnt+=1
                    z=z+"<div><tr id='srch"+str(srcnt)+"' onclick='document.location=\"/add_as_interest/?q="+str(c.intrest_name)+"\"' style='border-bottom: 3px solid #EFF3F9;border-left: 4px solid #EFF3F9;border-right: 4px solid #EFF3F9;background-color: #D5DAE2;padding-top: 3px;padding-bottom: 3px;'><td style='width:16%;overflow:hidden;padding:8px;'><img src='/media/intrest/"+str(c.intrest_name)+".thumbnail' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding-left:15px;'><h4 style='margin-bottom:2px;'><b>"+str(c.intrest_name)+"</b></h4>Intrest<br>"+str(len(c.intrest_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'></td></tr></div>"
        
       

        ctx= {'z':z,'s':s,}
        
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")
    
    


def Open(request):
    if  request.user.is_authenticated():
        name=request.GET.get('q','')
        if intrests.objects.filter(tagword__iexact=name):
            s=intrests.objects.filter(tagword__iexact=name).first()
            return HttpResponseRedirect("/open_intrest/?q="+s.intrest_name)
        elif create_city.objects.filter(tagword__iexact=name):
            s=create_city.objects.filter(tagword__iexact=name).first()
            return HttpResponseRedirect("/city/?q="+s.city_name)
        elif student.objects.filter(tagword__iexact=name):
            s=student.objects.filter(tagword__iexact=name).first()
            return HttpResponseRedirect("/"+str(s.id))
        elif band.objects.filter(tagword__iexact=name):
            s=band.objects.filter(tagword__iexact=name).first()
            return HttpResponseRedirect("/band/?q="+str(s.band_name))
        elif create_subjects.objects.filter(tagword__iexact=name):
            s=create_subjects.objects.filter(tagword__iexact=name).first()
            return HttpResponseRedirect("/subject/?q="+str(s.subject_name))
        elif startup.objects.filter(tagword__iexact=name):
            s= startup.objects.filter(tagword__iexact=name).first()
            return HttpResponseRedirect("/entrepreneurship/?q="+str(s.startup_name))
        

        else :
            return HttpResponse("No such page...")
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")





def add_subjects(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(identity=c)
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=='POST':
            subject_name = request.POST['subject_name']
            subject_des = request.POST['subject_descript']

            m=create_subjects(subjects_name=subject_name,subjects_descript=subject_des)
            m.save()
            p=m.id
            
        
            for count, x in enumerate(request.FILES.getlist("files")):
                def process(f):
                    with open('/root/ttl/babaS/media_in_env/media_root/file_sub' + '_'+ str(p)+".jpg", 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)

                process(x)   
                infile='/root/ttl/babaS/media_in_env/media_root/file_sub' + '_'+ str(p)+".jpg"
                thumb(infile)

            state="Subject "+str(m.subjects_name)+" has been created !!!"
            messages.success(request,state)
            return HttpResponseRedirect("/subject/?q="+str(m.subjects_name))

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)





        active = True
        t = i.post
        
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "t" : t,
            "f" : f,
            
            "i" : i,
            "e" : java,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")
    return render(request,"newsub.html",context)


def Subject(request):
    if request.user.is_authenticated():
        sub_name = request.GET.get('q','')
        j = create_subjects.objects.filter(subjects_name__iexact=sub_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id
        allpost = posts.objects.filter(id__in=j.subjects_posts).order_by("-id")
        f=allpost[0:9]
        other = allpost[9:]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)





        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            "other" : other,
            "page" :pc,
        }

        return render(request,"subject.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Cafe(request):
    if request.user.is_authenticated():
        cafe_name = request.GET.get('q','')
        j = create_cafe.objects.filter(cafe_name__iexact=cafe_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id
        allpost = posts.objects.filter(id__in=j.cafe_posts).order_by("-id")
        f=allpost[0:9]
        other = allpost[9:]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=="POST":

            #film_name = request.POST["filmname"]
            # t_from = request.POST["film_from"]
            # t_to = request.POST["film_to"]
            if request.POST.get('rate'):
                rate = request.POST['rate']
                r = ((j.film_review*float(len(j.film_reviewer_id)))+float(rate))/(float(len(j.film_reviewer_id))+1.00)
                j.film_review=r
                j.film_reviewer_id.append(i)
                j.save()
            else:

                t_mode = request.POST["meal"]
                #q_from = request.POST ["timestamp"]
                month = request.POST["month"]
                day = request.POST["day"]
                year = request.POST["year"]
                # q_from=change_date_formt(q_from)
                t_from = year+'-'+month+'-'+day
                t_to = year+'-'+month+'-'+day
                t_memb = request.POST["memb"]
                t=trip(trip_name=j.cafe_name,trip_from=t_from,trip_to=t_to,trip_mode=t_mode,trip_n_membs=t_memb,trip_city="Cafe")
                t.trip_memb.append(i.id)
                t.save()
                
                state="Your trip for "+str(j.cafe_name)+" has been created. Enjoy it !!!"
                messages.success(request,state)


        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"


        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)





        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            "other" : other,
            "page" :pc,
        }

        return render(request,"cafe.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

# def Cafe(request):
#     if request.user.is_authenticated():
#         # cafe_name = request.GET.get('q','')
#         # j = create_subjects.objects.filter(subjects_name__iexact=cafe_name).first()
#         i=student.objects.filter(user=request.user).first()
#         c = i.id
#         #f = posts.objects.filter(id__in=j.subjects_posts).order_by("-id")[0:9]
#         count = "p"+str(c)+"_"+str(i.prof_pic[-1])
#         counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
#         title = i.first_name
#         a = all_memb(request)
#         p=i.n_frqst
#         q=i.n_msg
#         r=i.n_notif
#         page=intrests.objects.filter(intrest_name=i.current_intrest).first()
#         if page:
#             pc=page.intrest_category
#         else:
#             pc="pg"
#         context = {
#             "title" : title,
#             "counter" : counter,
#             "count" : i.id,
#             "i" : i,
#             "a" : a,
#             "e" : i.f_list,
#             "p" : p,
#             "q" : q,
#             "r" : r,
#             # "j" : j,
#             # "f" : f,
            #"other" : other,
#             "page" :pc,
#         }

#         return render(request,"cafe.html",context)

#     else:
#         state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Create_team(request):
    if request.user.is_authenticated() :
        t_n=""
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            t_n=request.POST['team_name']
            t_l=request.POST['team_logo']
            t_g=request.POST['team_group']
            print (t_g)
            m = team(team_name=t_n,team_capt=c,team_logo=t_l,team_category=i.current_intrest,team_group=t_g)
            m.team_memb.append(c)
            m.save()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            play.p_team=m.id
            play.save()
            

            k=team.objects.filter(team_name=t_n).first()
            return HttpResponseRedirect("/make_team/?q="+str(k.id))


        else:
            return HttpResponse("No team created....")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Create_game_team(request):
    if request.user.is_authenticated() :
        t_n=""
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            t_n=request.POST['team_name']
            t_l=request.POST['team_logo']
            t_g=request.POST['team_group']
            g_n = request.POST['game']
            g_server = request.POST['server_address']
            print (t_g)
            za = team.objects.filter(team_category=i.current_intrest,team_capt=c)
            for z in za:
                z.delete()
            m = team(game_server=g_server,game_name=g_n,team_name=t_n,team_capt=c,team_logo=t_l,team_category=i.current_intrest,team_group=t_g)
            m.team_memb.append(c)
            m.save()
            
            

            k=team.objects.filter(team_name=t_n).first()
            return HttpResponseRedirect("/make_all_groups/?q="+str(i.current_intrest)+"."+str(k.id))


        else:
            return HttpResponse("No team created....")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Make_team(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if team.objects.filter(id=team_id):
            t=team.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            c = i.id
            b=[]
            
            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            gr=create_group.objects.filter(id=t.team_group).first()
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            memb = g.intrest_memb_id
            for p in gr.cg_memb_id:
                b.append(p)

            a = student.objects.filter(id__in=b).order_by("current_intrest").exclude(id=i.id)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"createteam.html",context)
        else:
            state="Make any team game as ur Intrest!!!"
            messages.error(request,state)
            return render(request,"createteam.html",{})

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Make_allgroups(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        b=[]
        z=""
        teamref = request.GET.get('q','')
        interest_name = teamref.split(".")[0]
        team_id = teamref.split(".")[1]

        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if intrests.objects.filter(intrest_name=interest_name):
            interest = intrests.objects.filter(intrest_name=interest_name).first()
            
            if interest.intrest_name=="Pizza":
                piz = pizza.objects.filter(id=team_id).first()

                if piz.family_memb[0]==i.id:
                    gr=create_group.objects.filter(id=piz.pizza_group).first()
                    for p in gr.cg_memb_id:
                        if p not in piz.family_memb:
                            b.append(p)
                            z="Pizza"
                else:
                    state="You are not the admin of the group!!!"
                    messages.error(request,state)
                    return render(request,"creatememb.html",{})

            if interest.intrest_name=="Gaming":
                piz = team.objects.filter(id=team_id).first()
                if piz.team_memb[0]==i.id:
                    gr=create_group.objects.filter(id=piz.team_group).first()
                    for p in gr.cg_memb_id:
                        if p not in piz.team_memb:
                            b.append(p)
                            z="Gaming"
                else:
                    state="You are not the admin of the group!!!"
                    messages.error(request,state)
                    return render(request,"creatememb.html",{})




        

            a = student.objects.filter(id__in=b)
            t=piz
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "z" : z,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"creatememb.html",context)
        else:
            state="No such Intrest found!!!"
            messages.error(request,state)
            return render(request,"creatememb.html",{})

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")
 

def Invitefriends(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        teamref = request.GET.get('q','')
        interest_name = teamref.split(".")[0]
        team_id = teamref.split(".")[1]

        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['teamid']
            if interest_name=="Pizza":

                m = pizza.objects.filter(id=l).first()
                for j in k:
                    p=student.objects.filter(id=j).first()
                    p.n_notif=p.n_notif+1


                    
                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/"+str(i.id)+"' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../view_pizza/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.family_name)+"</a></b> invited you for "+str(i.current_intrest)+" in their family.<br> <a href='../accept/?q=Invitepizzamember."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"."+str(m.id)+"' style='color:black;'> <button>Accept</button></a></td></tr><br>"+p.fnotify
                    p.save()
            if interest_name=="Gaming":
                m=team.objects.filter(id=l).first()
                for j in k:
                    p=student.objects.filter(id=j).first()
                    p.n_notif=p.n_notif+1


                    
                    p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/static/"+str(m.team_logo)+"' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../view_gamers/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.team_name)+"</a></b> invited you for "+str(i.current_intrest)+" in their team.<br> <a href='../accept/?q=Invitegamers."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"."+str(m.id)+"' style='color:black;'> <button>Accept</button></a></td></tr><br>"+p.fnotify
                    p.save()
            


        return HttpResponseRedirect("/home/")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")   

def Invite(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['teamid']
            m = team.objects.filter(id=l).first()
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1


                
                p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/static/"+str(m.team_logo)+"' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../view_team/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.team_name)+"</a></b> invited you for "+str(i.current_intrest)+" in their team.<br> <a href='../accept/?q=Invite."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a></td></tr><br>"+p.fnotify
                p.save()


        return HttpResponseRedirect("/home/")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def View_team(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if team.objects.filter(id=team_id):
            t=team.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            c = i.id
            if request.method=='POST':
                k=request.POST['player']
                if play.p_team:
                    
                    state="Leave pevious team first!!!"
                    messages.error(request,state)
                    return render(request,"visit.html",{})

                    
                else:
                    if not c in t.team_memb:
                        for p in t.team_memb:
                            s=student.objects.filter(id=p).first()
                            s.n_msg=s.n_msg+1
                            i.n_msg=i.n_msg+1
                            s.fmsg="<tr height='50px' class='openchat' value=><td><center><a href='../"+str(i.id)+"'>"+str(i.first_name)+"</a> joined your team "+str(t.team_name)+"</center></td></tr>"+s.fmsg
                            i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined the team "+str(t.team_name)+"</center></td></tr>"+i.fmsg
                            s.save()
                            i.save()
                            m = message(m_id=i.id,m_friend=p,m_admin=True,m_message="<a href='../"+str(i.id)+"'>"+str(i.first_name)+"</a> joined your team "+str(t.team_name)+". Have Fun together !!!")
                            m.save()
                        t.team_memb.append(c)
                        t.save()
                        print(t.id)
                        play.p_team=t.id
                        play.save()
                    return HttpResponseRedirect("/home/")


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            a = student.objects.filter(id__in=t.team_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"visit.html",context)
        else:
            return HttpResponse("No such team found!!!")


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def View_gamers(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if team.objects.filter(id=team_id):
            t=team.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            c = i.id
            if request.method=='POST':
                k=request.POST['player']
                if i.id in t.team_memb:
                    
                    state="Leave pevious team first!!!"
                    messages.error(request,state)
                    return render(request,"visit.html",{})

                    return render(request,"visit.html",{})
                else:
                    if not c in t.team_memb:
                        for p in t.team_memb:
                            s=student.objects.filter(id=p).first()
                            s.n_msg=s.n_msg+1
                            i.n_msg=i.n_msg+1
                            s.fmsg="<tr height='50px' class='openchat' value=><td><center><a href='../"+str(i.id)+"'>"+str(i.first_name)+"</a> joined your game "+str(t.team_name)+"</center></td></tr>"+s.fmsg
                            i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined the game "+str(t.team_name)+"</center></td></tr>"+i.fmsg
                            s.save()
                            i.save()
                            m = message(m_id=i.id,m_friend=p,m_admin=True,m_message="<a href='../"+str(i.id)+"'>"+str(i.first_name)+"</a> joined your game "+str(t.team_name)+". Have Fun together !!!")
                            m.save()


                        t.team_memb.append(c)
                        t.save()
                        
                    return HttpResponseRedirect("/home/")


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            a = student.objects.filter(id__in=t.team_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"visit.html",context)
        else:
            return HttpResponse("No such team found!!!")


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def view_pizza(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if pizza.objects.filter(id=team_id):
            t=pizza.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            #play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            c = i.id
            if request.method=='POST':
                k=request.POST['player']
                if i.id in t.family_memb:
                    
                    state="You are already in the family!!!"
                    messages.error(request,state)
                    return HttpResponseRedirect("/home/")
                else:
                    for p in t.family_memb:
                        s=student.objects.filter(id=p).first()
                        s.n_msg=s.n_msg+1
                        i.n_msg=i.n_msg+1
                        s.fmsg="<tr height='50px' class='openchat' value=><td><center><a href='../"+str(i.id)+"'>"+str(i.first_name)+"</a> joined Pizza family "+str(t.family_name)+"</center></td></tr>"+s.fmsg
                        i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined Pizza family "+str(t.family_name)+"</center></td></tr>"+i.fmsg
                        s.save()
                        i.save()
                        m = message(m_id=i.id,m_friend=p,m_admin=True,m_message="<a href='../"+str(i.id)+"'>"+str(i.first_name)+"</a> joined Pizza family "+str(t.family_name)+". Have Fun together !!!")
                        m.save()
                    
                    t.family_memb.append(c)
                    t.save()
                    
                    return HttpResponseRedirect("/home/")





            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            a = student.objects.filter(id__in=t.family_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"visit.html",context)
        else:
            return HttpResponse("No such team found!!!")


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")    




def Invitechallenger(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            l=request.POST.get('game')
            print k
            
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1
                p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > "+"<b style='color:black;'>"+str(i.first_name)+"</b> challenged you to play "+str(l)+" <a href='../accept/?q=Invitechallenger."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a> </td></tr><br>"+p.fnotify
                p.save()
                #"<tr><td width='15%' style='padding-left:15px;'><img src='/static/"+str(m.team_logo)+".jpg' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../view_team/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.team_name)+"</a></b> invited you to play in their team.<br> <a href='../accept/?q=Invite."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a><br></td>"


        return HttpResponseRedirect("/homedouble/")
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Invitepartner(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            l=request.POST.get('game')
            print k
            
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1
                p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'>"+"<b style='color:black;'>"+str(i.first_name)+"</b> invited you to play "+str(l)+" as a partner with him <a href='../accept/?q=Invitepartner."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"'> <button>Accept</button></a></td></tr> <br>"+p.fnotify
                #"<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><br>"
                p.save()


        return HttpResponseRedirect("/homedouble/")
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Leaveteam(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if i.current_intrest=="Gaming":
            t=team.objects.filter(team_memb__contains=i.id,team_category=i.current_intrest).first()
            t.team_memb.remove(i.id)
            t.save()
            if t.team_capt==c:
                t.delete()
            return HttpResponseRedirect("/homelan/")

        if i.current_intrest=="Pizza":
            t=pizza.objects.filter(family_memb__contains=i.id).first()
            if t.family_memb[0]==i.id:
                t.delete()
            else:
                t.family_memb.remove(i.id)
                t.save()
            
            return HttpResponseRedirect("/homepizza/")

        if i.current_intrest=="Film":
            t=trip.objects.filter(trip_memb__contains=i.id,trip_city="Film").first()
            if t.trip_memb[0]==i.id:
                t.delete()
            else:
                t.trip_memb.remove(i.id)
                t.save()
            
            return HttpResponseRedirect("/home/")

        if i.current_intrest=="Quicky":
            t=quick.objects.filter(quick_memb__contains=i.id).first()
            if t.quick_memb[0]==i.id:
                t.delete()
            else:
                t.quick_memb.remove(i.id)
                t.save()
            
            return HttpResponseRedirect("/home/")

        if i.current_intrest=="Restaurant":
            t=trip.objects.filter(trip_memb__contains=i.id,trip_city="Cafe").first()
            if t.trip_memb[0]==i.id:
                t.delete()
            else:
                t.trip_memb.remove(i.id)
                t.save()
            
            return HttpResponseRedirect("/home/")

        
        p=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
        t=team.objects.filter(id=p.p_team).first()
        t.team_memb.remove(i.id)
        t.save()
        m=t.id
        p.p_team=0
        p.save()
        if t.team_capt==c:
            k=player.objects.filter(p_team=m)
            for l in k:
                l.p_team=0
                l.save()
            t.delete()

        return HttpResponseRedirect("/homepg/")
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Leavedouble(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        
        t=team.objects.filter(team_category=i.current_intrest,team_memb__contains=i.id).first()
        t.delete()

        return HttpResponseRedirect("/home/")
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Viewtraveller(request):
    if request.user.is_authenticated() :
        trip_id = request.GET.get('q','')
        if trip.objects.filter(id=trip_id):
            t=trip.objects.filter(id=trip_id).first()
            i=student.objects.filter(user=request.user).first()
            
            c = i.id
            if request.method=='POST':
                k=request.POST['traveller']
                if k==str(i.id):
                    if i.id not in t.trip_memb:
                        for p in t.trip_memb:
                            s=student.objects.filter(id=p).first()
                            s.n_msg=s.n_msg+1
                            i.n_msg=i.n_msg+1
                            s.fmsg="<tr height='50px' class='openchat' value=><td><center><a href='/prof/"+str(i.id)+"'>"+str(i.first_name)+"</a> joined trip to"+str(t.trip_city)+"</center></td></tr>"+s.fmsg
                            i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined trip to"+str(t.trip_city)+"</center></td></tr>"+i.fmsg
                            s.save()
                            i.save()
                            m = message(m_id=i.id,m_friend=p,m_admin=True,m_message="<a href='/prof/"+str(i.id)+"'>"+str(i.first_name)+"</a> joined trip to"+str(t.trip_city)+". Have Fun together !!!")
                            m.save()
                        t.trip_memb.append(i.id)
                        t.save()
                        
                
                    else:
                        state="You are already in the group."
                        messages.error(request,state)
                return HttpResponseRedirect("/home/") 
                
                


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            
            a = student.objects.filter(id__in=t.trip_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                            
                "i" : i,
                
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"viewtraveller.html",context)
        else:
            return HttpResponse("No such trip found!!!")




    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Viewfooder(request):
    if request.user.is_authenticated() :
        trip_id = request.GET.get('q','')
        if trip.objects.filter(id=trip_id,trip_city="Cafe"):
            t=trip.objects.filter(id=trip_id).first()
            i=student.objects.filter(user=request.user).first()
            
            c = i.id
            if request.method=='POST':
                k=request.POST['traveller']
                if k==str(i.id):
                    if i.id not in t.trip_memb:
                        for p in t.trip_memb:
                            s=student.objects.filter(id=p).first()
                            s.n_msg=s.n_msg+1
                            i.n_msg=i.n_msg+1
                            s.fmsg="<tr height='50px' class='openchat' value=><td><center><a href='/"+str(i.id)+"'>"+str(i.first_name)+"</a> joined trip to"+str(t.trip_name)+"</center></td></tr>"+s.fmsg
                            i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined trip to"+str(t.trip_name)+"</center></td></tr>"+i.fmsg
                            s.save()
                            i.save()
                            m = message(m_id=i.id,m_friend=p,m_admin=True,m_message="<a href='/prof/"+str(i.id)+"'>"+str(i.first_name)+"</a> joined trip to"+str(t.trip_city)+". Have Fun together !!!")
                            m.save()
                        t.trip_memb.append(i.id)
                        t.save()
                        
                
                    else:
                        state="You are already in the group."
                        messages.error(request,state)
                return HttpResponseRedirect("/home/") 
                
                


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            
            a = student.objects.filter(id__in=t.trip_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                            
                "i" : i,
                
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"viewfooder.html",context)
        else:
            return HttpResponse("No such trip found!!!")




    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")
        



def Viewwatcher(request):
    if request.user.is_authenticated() :
        trip_id = request.GET.get('q','')
        if trip.objects.filter(id=trip_id):
            t=trip.objects.filter(id=trip_id).first()
            i=student.objects.filter(user=request.user).first()
            
            c = i.id
            if request.method=='POST':
                k=request.POST['traveller']
                if k==str(i.id):
                    if i.id not in t.trip_memb:
                        for p in t.trip_memb:
                            s=student.objects.filter(id=p).first()
                            s.n_msg=s.n_msg+1
                            i.n_msg=i.n_msg+1
                            s.fmsg="<tr height='50px' class='openchat' value=><td><center><a href='/prof/"+str(i.id)+"'>"+str(i.first_name)+"</a> joined you to watch "+str(t.trip_name)+"</center></td></tr>"+s.fmsg
                            i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined the crew to watch "+str(t.trip_name)+"</center></td></tr>"+i.fmsg
                            s.save()
                            i.save()
                            m = message(m_id=i.id,m_friend=p,m_admin=True,m_message="<a href='/prof/"+str(i.id)+"'>"+str(i.first_name)+"</a> joined your crew to watch "+str(t.trip_name)+". Have Fun together !!!")
                            m.save()
                        t.trip_memb.append(i.id)
                        t.save()
                        
                
                    else:
                        state="You are already in the group."
                        messages.error(request,state)
                return HttpResponseRedirect("/home/") 
                
                


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            
            a = student.objects.filter(id__in=t.trip_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                            
                "i" : i,
                
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"viewwatcher.html",context)
        else:
            return HttpResponse("No such trip found!!!")




    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Viewquickster(request):
    if request.user.is_authenticated() :
        trip_id = request.GET.get('q','')
        if quick.objects.filter(id=trip_id):
            t=quick.objects.filter(id=trip_id).first()
            i=student.objects.filter(user=request.user).first()
            
            c = i.id
            if request.method=='POST':
                k=request.POST['traveller']
                if k==str(i.id):
                    if i.id not in t.quick_memb:
                        for p in t.quick_memb:
                            s=student.objects.filter(id=p).first()
                            s.n_msg=s.n_msg+1
                            i.n_msg=i.n_msg+1
                            s.fmsg="<tr height='50px' class='openchat' value=><td><center><a href='../"+str(i.id)+"'>"+str(i.first_name)+"</a> joined the quick event for"+str(t.quick_name)+"</center></td></tr>"+s.fmsg
                            i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined the quick event for"+str(t.quick_name)+"</center></td></tr>"+i.fmsg
                            s.save()
                            i.save()
                            m = message(m_id=i.id,m_friend=p,m_admin=True,m_message="<a href='../"+str(i.id)+"'>"+str(i.first_name)+"</a> joined the quick event for"+str(t.quick_name)+". Have Fun together !!!")
                            m.save()
                        t.quick_memb.append(i.id)
                        t.save()
                        
                
                    else:
                        state="You are already in the group."
                        messages.error(request,state)
                return HttpResponseRedirect("/home/") 
                
                


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            
            a = student.objects.filter(id__in=t.quick_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                            
                "i" : i,
                
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"viewquickster.html",context)
        else:
            return HttpResponse("No such Quick event found!!!")


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def Accept(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        acpt = request.GET.get('q','')
        print acpt
        acept= acpt.split(".")
        find = intrests.objects.filter(intrest_name__iexact=acept[1]).first()
        change_interest(request,i,find)

        if acept[0]=="Invite":
            p=student.objects.filter(id=acept[2]).first()
            t=team.objects.filter(id=int(acept[4]))
            if t:
                
                t= t.first()
                if i.id not in t.team_memb and i.id in t.team_invites:
                    t.team_memb.append(i.id)
                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " has accepted your invite for "+str(t.team_category)+". Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose=t.team_category,purpose_id=t.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"team_id":c_share.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"
                if i.id in t.team_invites:
                    t.team_invites.remove(i.id)
                t.save()
                # p.n_msg=p.n_msg+1
                # i.n_msg=i.n_msg+1
                # p.fmsg="<tr height='50px' class='openchat' value=><td width='15%' style='padding-left:15px;'><td width='85%'><center>"+str(i.first_name)+" joined your group for "+str(acept[1])+".</center></td></tr>"+p.fmsg
                # i.fmsg="<tr height='50px' class='openchat' value=><td width='15%' style='padding-left:15px;'></td><td width='85%'><center> You joined "+str(p.first_name)+"'s group for "+str(acept[1])+"</center></td></tr>"+i.fmsg
                
                # p.save()
                # i.save()

                


                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="Your team is ready to challenge in "+str(acept[1])+". All the best!!!")
                m.save()
                state={"Group_id":t.id}
                return HttpResponse(json.dumps(state), content_type='application/json')
            else :
                state=str(p.first_name)+"'s group doesnot exist now!!! Ooops, You are late..."
                state={"Status":state}
                return HttpResponse(json.dumps(state), content_type='application/json')



        if acept[0]=="Invitepartner":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<tr height='50px' class='openchat' value=><td><center>"+str(i.first_name)+" accepted your request to play "+str(acept[1])+" as your partner</center></td></tr>"+p.fmsg
                i.fmsg="<tr height='50px' class='openchat' value=><td><center> You accepted "+str(p.first_name)+"'s request to play "+str(acept[1])+"as his partner</center></td></tr>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="You both are ready to play "+str(acept[1])+" together")
                m.save()

                d = team(team_name=str(p.first_name)+"&"+str(i.first_name),team_capt=p.id,team_logo="file_"+str(p.id)+".thumbnail&file_"+str(i.id)+".thumbnail",team_category=i.current_intrest)
                d.team_memb.append(p.id)
                d.team_memb.append(i.id)
                d.save()
                

                return HttpResponseRedirect("/homedouble/")

            else :
                state=str(p.first_name)+"has changed his interest."
                messages.error(request,state)
                return HttpResponseRedirect("/homedouble/")


        if acept[0]=="Invitechallenger":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<tr height='50px' class='openchat' value=><td><center>"+str(i.first_name)+" accepted your challenge to play "+str(acept[1])+"</center></td></tr>"+p.fmsg
                i.fmsg="<tr height='50px' class='openchat' value=><td><center> You accepted "+str(p.first_name)+"'s challenge to play "+str(acept[1])+"</center></td></tr>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="You both are ready to challenge each other in "+str(acept[1])+". All the best!!!")
                m.save()

                return HttpResponseRedirect("/homedouble/")
            else :
                state=str(p.first_name)+"has changed his interest."
                messages.error(request,state)
                return HttpResponseRedirect("/homedouble/")


        if acept[0]=="Challenge":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<tr height='50px' class='openchat' value=><td width='15%' style='padding-left:15px;'><td width='85%'><center>"+str(i.first_name)+" accepted your challenge to play "+str(acept[1])+"</center></td></tr>"+p.fmsg
                i.fmsg="<tr height='50px' class='openchat' value=><td width='15%' style='padding-left:15px;'></td><td width='85%'><center> You accepted "+str(p.first_name)+"'s challenge to play "+str(acept[1])+"</center></td></tr>"+i.fmsg
                #"<tr height='50px' class='openchat' value="+str(y.id)+"><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(y.id)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'>"+"<b style='color:black;'>"+str(y.first_name)+"</b>:"+str(msgs.m_message)+"</td></tr>"+p.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="You both are ready to challenge each other in "+str(acept[1])+". All the best!!!")
                m.save()

                return HttpResponseRedirect("/homepg/")
            else :
                state=str(p.first_name)+"'s team doesnot exist now!!! Ooops, You are late..."
                messages.error(request,state)
                return HttpResponseRedirect("/homepg/")
                
        

        if acept[0]=="Invitegamers":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<tr height='50px' class='openchat' value=><td><center>"+str(i.first_name)+" joined your team for "+str(acept[1])+".</center></td></tr>"+p.fmsg
                i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined "+str(p.first_name)+"'s team for "+str(acept[1])+"</center></td></tr>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="Your team is ready to challenge in "+str(acept[1])+". All the best!!!")
                m.save()
                
                t=team.objects.filter(id=acept[4]).first()
                t.team_memb.append(i.id)
                t.save()

                return HttpResponseRedirect("/home/")
            else :
                state=str(p.first_name)+"'s team doesnot exist now!!! Ooops, You are late..."
                messages.error(request,state)
                return HttpResponseRedirect("/home/") 


        if acept[0]=="Invitegamer":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<tr height='50px' class='openchat' value=><td><center>"+str(i.first_name)+" joined your team to play "+str(acept[1])+".</center></td></tr>"+p.fmsg
                i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined "+str(p.first_name)+"'s team to play "+str(acept[1])+"</center></td></tr>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="Your team is ready to challenge in "+str(acept[1])+". All the best!!!")
                m.save()
                pl=player.objects.filter(p_id=p.id,p_game=p.current_intrest).first()
                t=game.objects.filter(id=pl.p_team).first()
                t.game_memb.append(i.id)
                t.save()
                return HttpResponseRedirect("/home/")
            else :
                state=str(p.first_name)+"'s team doesnot exist now!!! Ooops, You are late..."
                messages.error(request,state)
                return HttpResponseRedirect("/home/")               
        


        if acept[0]=="Invitepizzamember":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<tr height='50px' class='openchat' value=><td><center>"+str(i.first_name)+" joined your"+str(acept[1])+" family.</center></td></tr>"+p.fmsg
                i.fmsg="<tr height='50px' class='openchat' value=><td><center> You joined "+str(p.first_name)+"'s  "+str(acept[1])+"family.</center></td></tr>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="You are ready to challenge each other in "+str(acept[1])+". All the best!!!")
                m.save()
                
                t=pizza.objects.filter(id=acep[4]).first()
                t.family_memb.append(i.id)
                t.save()

                return HttpResponseRedirect("/homegroup/")
            else :
                state=str(p.first_name)+"'s team doesnot exist now!!! Ooops, You are late..."
                messages.error(request,state)
                return HttpResponseRedirect("/homegroup/")
                
        


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Challenge(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['teamid']
            print l
            m = team.objects.filter(id=l).first()
            for j in k:
                n= team.objects.filter(id=j).first()
                p=student.objects.filter(id=n.team_capt).first()
                p.n_notif=p.n_notif+1
                p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='{"+"%"+"static \'"+str(m.team_logo)+"\'%}' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'>"+"<a href='../view_team/?q="+str(m.id)+"' <b style='color:black;'>"+str(m.team_name)+"</b></a> challenged you to play "+str(i.current_intrest)+". <a href='../accept/?q=Challenge."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"'> <button>Accept</button></a></td></tr> <br>"+p.fnotify
                p.save()
                #"<tr><td width='15%' style='padding-left:15px;'><img src='/static/"+str(m.team_logo)+".jpg' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../view_team/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.team_name)+"</a></b> invited you to play in their team.<br> <a href='../accept/?q=Invite."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a><br></td>"


        return HttpResponseRedirect("/homepg/")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Band(request):
    if request.user.is_authenticated():
        band_name = request.GET.get('q','')
        j = band.objects.filter(band_name__iexact=band_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id

        allpost = posts.objects.filter(id__in=j.band_posts).order_by("-id")
        f=allpost[0:9]
        other = allpost[9:]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        dictionary_band = {
        '0' : 'Singer',
        '1' : 'Guitar',
        '2': 'Piano/Keyboard',
        '3': 'Bass',
        '4': 'Violin',
        '5': 'Drum',
        '6': 'Flute',
        '7': 'Saxophone',
        '8': 'Tabla',

        }
        stud = student.objects.filter(id=j.band_memb[0]).first()
        creator = stud.first_name+" "+stud.last_name
        zipped = zip(j.band_memb,j.band_memb_work)
        dict1={}
        for m,n in zipped:
            stud = student.objects.filter(id=m).first()
            
            z=dictionary_band.get(str(n))
            dict0={stud.first_name+" "+stud.last_name : z}
            dict1.update(dict0)

        dict2 ={
            dictionary_band.get('0'):j.band_V_singer,
            dictionary_band.get('1'):j.band_V_guitar,
            dictionary_band.get('2') : j.band_V_piano,
            dictionary_band.get('3'): j.band_V_bass,
            dictionary_band.get('4') : j.band_V_violin,
            dictionary_band.get('5') :j.band_V_drum,
            dictionary_band.get('6') : j.band_V_flute,
            dictionary_band.get('7') : j.band_V_saxophone,
            dictionary_band.get('8') : j.band_V_tabla,
            j.band_other : j.band_V_other,


        }
        # form = trip_form(request.POST or None)
        # if request.method=="POST":
        #     t_name = request.POST["tourname"]
        #     t_from = request.POST["trip_from"]
        #     t_to = request.POST["trip_to"]
        #     t_mode = request.POST["mode"]
        #     t_memb = request.POST["memb"]
        #     t=trip(trip_name=t_name,trip_from=t_from,trip_to=t_to,trip_mode=t_mode,trip_n_membs=t_memb,trip_city=j.city_name)
        #     t.trip_memb.append(i.id)
        #     t.save()
        #     j.city_memb_id.append(i.id)
        #     j.save()
        #     return HttpResponse("Trip Created!!!")
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)





        context = {
            "title" : title,
            "counter" : counter,
            "creator" : creator,
            "count" : i.id,
            "i" : i,
            "dict1":dict1,
            "dict2":dict2,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            "other" : other,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "page" :pc,
            #"form":form,
        }

        return render(request,"Band.html",context)


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Join_band(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c = i.id
        l=""
        dictionary_band = {
        '0' : 'Singer',
        '1' : 'Guitar',
        '2': 'Piano/Keyboard',
        '3': 'Bass',
        '4': 'Violin',
        '5': 'Drum',
        '6': 'Flute',
        '7': 'Saxophone',
        '8': 'Tabla',

        }

        dictionary_startup = {
        '0' : 'CEO',
        '1' : 'Technical Operators',
        '2': 'Sales & Marketing Head',
        '3': 'HR',
        '4': 'Business Developmet',
        '5': 'Customer Service',
        '6': 'Salesman',
        '7': 'R&D',
        '8': 'Administration',

        }

        if request.method=='POST':
            kind =request.POST['kind']

            if kind == "band":
                ident=request.POST['identity']
                ins = request.POST['instrument']
                j = band.objects.filter(id=ident).first()
                for key,value in dictionary_band.items():
                    if value==ins:
                        l=key
                if ins.lower()=="guitar":
                    j.band_V_guitar-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()

                if ins.lower()=="piano":
                    j.band_V_piano-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
                if ins.lower()=="singer":
                    j.band_V_singer-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
                if ins.lower()=="saxophone":
                    j.band_V_saxophone-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
                if ins.lower()=="violin":
                    j.band_V_violin-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
                if ins.lower()=="bass":
                    j.band_V_bass-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
                if ins.lower()=="drum":
                    j.band_V_drum-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
                if ins.lower()=="flute":
                    j.band_V_flute-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
                if ins.lower()=="tabla":
                    j.band_V_tabla-=1
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
                if ins==j.band_other:
                    j.band_V_other-=1
                    
                    j.band_memb.append(i.id)
                    j.band_memb_work.append(l)
                    j.save()
            
                return HttpResponseRedirect ('/band/?q='+str(j.band_name))

            elif kind == "startup":
                ident=request.POST['identity']
                ins = request.POST['instrument']
                j = startup.objects.filter(id=ident).first()
                for key,value in dictionary_startup.items():
                    if value==ins:
                        l=key
                if ins.lower()=="ceo":
                    j.startup_V_ceo -=1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()

                if ins.lower()=="technical operators":
                    j.startup_V_techop -= 1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
                if ins.lower()=="sales & marketing head":
                    j.startup_V_salesmark-=1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
                if ins.lower()=="hr":
                    j.band_V_hr-=1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
                if ins.lower()=="business developmet":
                    j.startup_V_busdev-=1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
                if ins.lower()=="customer service":
                    j.startup_V_custser-=1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
                if ins.lower()=="salesman":
                    j.startup_V_salesman-=1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
                if ins.lower()=="r&d":
                    j.startup_V_rnd-=1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
                if ins.lower()=="administration":
                    j.startup_V_adm-=1
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
                if ins==j.startup_other:
                    j.startup_V_other-=1
                    
                    j.startup_memb.append(i.id)
                    j.startup_memb_work.append(l)
                    j.save()
            
                return HttpResponseRedirect ('/entrepreneurship/?q='+str(j.startup_name))
        

        


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def Startup(request):
    if request.user.is_authenticated():
        band_name = request.GET.get('q','')
        j = startup.objects.filter(startup_name__iexact=band_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id

        dictionary_startup = {
        '0' : 'CEO',
        '1' : 'Technical Operators',
        '2': 'Sales & Marketing Head',
        '3': 'HR',
        '4': 'Business Developmet',
        '5': 'Customer Service',
        '6': 'Salesman',
        '7': 'R&D',
        '8': 'Administration',

        }

        allpost = posts.objects.filter(id__in=j.startup_posts).order_by("-id")
        f=allpost[0:9]
        other = allpost[9:]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = all_memb(request)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        stud = student.objects.filter(id=j.startup_memb[0]).first()
        creator = stud.first_name+" "+stud.last_name
        zipped = zip(j.startup_memb,j.startup_memb_work)
        dict1={}
        for m,n in zipped:
            stud = student.objects.filter(id=m).first()
            
            z=dictionary_startup.get(str(n))
            dict0={stud.first_name+" "+stud.last_name : z}
            dict1.update(dict0)


        dict2 ={
            'CEO':j.startup_V_ceo,
            'Technical Operators':j.startup_V_techop,
            'Sales & Marketing Head' : j.startup_V_salesmark,
            'HR': j.startup_V_hr,
            'Business Developmet' : j.startup_V_busdev,
            'Customer Service' :j.startup_V_custser,
            'Salesman' : j.startup_V_salesman,
            'R&D' : j.startup_V_rnd,
            'Administration' : j.startup_V_adm,
            j.startup_other : j.startup_V_other,


        }
        # form = trip_form(request.POST or None)
        # if request.method=="POST":
        #     t_name = request.POST["tourname"]
        #     t_from = request.POST["trip_from"]
        #     t_to = request.POST["trip_to"]
        #     t_mode = request.POST["mode"]
        #     t_memb = request.POST["memb"]
        #     t=trip(trip_name=t_name,trip_from=t_from,trip_to=t_to,trip_mode=t_mode,trip_n_membs=t_memb,trip_city=j.city_name)
        #     t.trip_memb.append(i.id)
        #     t.save()
        #     j.city_memb_id.append(i.id)
        #     j.save()
        #     return HttpResponse("Trip Created!!!")
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)





        context = {
            "title" : title,
            "counter" : counter,
            "creator" : creator,
            "count" : i.id,
            "i" : i,
            "dict1" : dict1,
            "dict2" : dict2,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            "other" : other,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "page" :pc,
            #"form":form,
        }

        return render(request,"Entrepreneurship1.html",context)


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def thumb(infile):
    
        
    

    outfile = os.path.splitext(infile)[0] + ".thumbnail"
    if infile != outfile:
        try:
            im = Image.open(infile)
            im.thumbnail((128,128),Image.ANTIALIAS)
            im.save(outfile, "JPEG")
            print "kr daal"
        except IOError:
            print "cannot create thumbnail for", infile
    return ("DONE")


def thumbing(request):
    pr = intrests.objects.all()
    for p in pr:
        z=str(p.intrest_name)+".jpg"
        infile = os.path.join(BASE_DIR, 'media_in_env','media_root','intrest',z)
        #outfile = os.path.join(BASE_DIR, 'media_in_env','media_root',k)

        outfile = os.path.splitext(infile)[0] + ".thumbnail"
        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail((256,256),Image.ANTIALIAS)
                im.save(outfile, "JPEG")
                print "kr daal"
            except IOError:
                print "cannot create thumbnail for", infile

        

            
    return HttpResponse("DONE")





# def Groupmsg(request):
#     if request.user.is_authenticated():
#         i=student.objects.filter(user=request.user).first()
#         c=i.id
#         z=""

#         if request.method=='POST':
#             k = request.POST.get('sluge')
            
#             j = request.POST.get('slug')
            
#             grp = create_group.objects.filter(id=k).first()
#             gcht = groupchat(gc_id=i.id,gc_group=grp.id,gc_message=j,gc_interest=i.current_intrest)
#             gcht.save()
#             z="<div class='sender' style='clear:both;'>"+j+"</div>"
            
#         ctx= {'z':z}
#         return HttpResponse(json.dumps(ctx), content_type='application/json')

#     else:
#state = "Login First..."
        #messages.success(request,state)
        #return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def Groupmsg(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""
        if request.method=='POST':
            k = request.POST.get('sluge')
            
            j = request.POST.get('slug')
            
            grp = create_group.objects.filter(id=k).first()
            gcht = groupchat(gc_id=i.id,gc_group=grp.id,gc_message=j,gc_interest=i.current_intrest)
            gcht.save()
            z="<div class='sender' style='clear:both;'>"+j+"</div>"
            
        ctx= {'z':z}
        return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

# def Groupcht(request):
#     if request.user.is_authenticated():
#         i=student.objects.filter(user=request.user).first()
#         c=i.id
#         z=""

#         if request.method=='POST':
#             j = request.POST.get('sluge')
#             gr = create_group.objects.filter(id=j).first()
#             m = groupchat.objects.filter(gc_group=j,gc_interest=i.current_intrest)
#             print (gr.cg_name)
#             naam=str(gr.cg_name)
#             for l in m:
#                 if not l.admin:
#                     if l.gc_id==i.id:
#                         z=z+"<div class='sender' style='clear:both;'>"+l.gc_message+"</div>"
#                     else:
#                         z=z+"<br><img src='/media/file_"+str(l.gc_id)+".thumbnail' class='imgchat' style='float:left;clear:both;'>"+"<div class='receiver' style='clear:both;float:left;'>"+l.gc_message+"</div>"
#                 else:
#                     z=z+"<center class='admin'>"+l.gc_message+"</center>"


            
#         ctx= {'z':z,'naam':naam}
        
#         return HttpResponse(json.dumps(ctx), content_type='application/json')
#     else:
#         state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Groupcht(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""
        if request.method=='POST':
            j = request.POST.get('sluge')
            gr = create_group.objects.filter(id=j).first()
            m = groupchat.objects.filter(gc_group=j,gc_interest=i.current_intrest)
            print (gr.cg_name)
            naam=str(gr.cg_name)
            for l in m:
                if not l.admin:
                    if l.gc_id==i.id:
                        z=z+"<div class='sender' style='clear:both;'>"+l.gc_message+"</div>"
                    else:
                        z=z+"<br><img src='/media/file_"+str(l.gc_id)+".thumbnail' class='imgchat' style='float:left;clear:both;'>"+"<div class='receiver' style='clear:both;float:left;'>"+l.gc_message+"</div>"
                else:
                    z=z+"<center clss='admin'>"+l.gc_message+"</center>"
            
        ctx= {'z':z,'naam':naam}
        
        return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

# def Teammsg(request):
#     if request.user.is_authenticated():
#         i=student.objects.filter(user=request.user).first()
#         c=i.id
#         z=""

#         if request.method=='POST':
#             k = request.POST.get('sluge')
            
#             j = request.POST.get('slug')
            
#             t = team.objects.filter(id=k).first()
#             tcht = teamchat(teamc_id=i.id,teamc_group=t.id,teamc_message=j)
#             tcht.save()
#             print(tcht.teamc_message)
#             z="<div class='sender' style='clear:both;'>"+j+"</div>"
            
#         ctx= {'z':z}
#         return HttpResponse(json.dumps(ctx), content_type='application/json')

#     else:
#         state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Teammsg(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""
        if request.method=='GET':
            k = request.GET.get('sluge')
            
            j = request.GET.get('slug')
            
            t = team.objects.filter(id=k).first()
            tcht = teamchat(teamc_id=i.id,teamc_group=t.id,teamc_message=j)
            tcht.save()
            for k in t.team_memb:
                if k != i.id:
                    ks = student.objects.filter(id=k).first()
                    ks.n_frqst=ks.n_frqst+1
                    ks.team_to_chat.append(tcht.teamc_group)
                    ks.save()
            

            print(tcht.teamc_message)
            z="<div class='sender' style='clear:both;'>"+j+"</div>"
            
        ctx= {'z':z}
        return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


# def Teamcht(request):
#     if request.user.is_authenticated():
#         i=student.objects.filter(user=request.user).first()
#         c=i.id
#         z=""

#         if request.method=='POST':
#             j = request.POST.get('sluge')
#             t = team.objects.filter(id=int(j)).first()
#             m = teamchat.objects.filter(teamc_group=t.id)
            
#             naam=str(t.team_name)
#             for l in m:
#                 if not l.admin:
#                     if l.gc_id==i.id:
#                         z=z+"<div class='sender' style='clear:both;'>"+l.teamc_message+"</div>"
#                     else:
#                         z=z+"<br><img src='/media/file_"+str(l.teamc_id)+".thumbnail' class='imgchat' style='float:left;clear:both;'>"+"<div class='receiver' style='clear:both;float:left;'>"+l.gc_message+"</div>"
#                 else:
#                     z=z+"<center class='admin'>"+l.teamc_message+"</center>"


            
#         ctx= {'z':z,'naam':naam}
        
#         return HttpResponse(json.dumps(ctx), content_type='application/json')

#     else:
#         state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def Teamcht(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""
        if request.method=='GET':
            j = request.GET.get('sluge')
            t = team.objects.filter(id=int(j)).first()
            m = teamchat.objects.filter(teamc_group=t.id)
            date=''
            naam=str(t.team_name)
            for l in m:
                # if not l.admin:
                #     if l.teamc_id==i.id:
                #         z=z+"<div class='sender' style='clear:both;'>"+l.teamc_message+"</div>"
                #     else:
                #         z=z+"<br><img src='/media/file_"+str(l.teamc_id)+".thumbnail' class='imgchat' style='float:left;clear:both;'>"+"<div class='receiver' style='clear:both;float:left;'>"+l.teamc_message+"</div>"
                
            
                if not l.admin:
                    tz = pytz.timezone('Asia/Kolkata')
                    your_now = l.post_time.astimezone(tz)

                    if str(your_now).split(" ")[0]!= date:
                        z=z+"<center><div class='admin'><span style='font-size:14px;'>"+your_now.strftime("%a, %d %b %Y")+"</span></div></center>"
                        date = str(your_now).split(" ")[0]

                    if l.teamc_photo:
                        if l.teamc_id==i.id:
                            z=z+"<div style='border-radius:8px;float:right;clear:both;'><a href='"+l.teamc_message.split(".thumbnail")[0]+".jpg' target='_blank'><img src='"+l.teamc_message+"' style='width:90px;'></a><br><span style='float:right;font-size:9px;'>"+your_now.strftime("%I:%M %p")+"</span></div>"
                        else:
                            z=z+"<br><img src='/media/file_"+str(l.teamc_id)+".thumbnail' class='imgchat' style='float:left;clear:both;'>"+"<div style='border-radius:8px;float:left;clear:both;'><a href='"+l.teamc_message.split(".thumbnail")[0]+".jpg' target='_blank'><img src='"+l.teamc_message+"' style='width:90px;'></a><br><span style='float:right;font-size:9px;'>"+your_now.strftime("%I:%M %p")+"</span></div>"

                    else:
                        if l.teamc_id==i.id:
                            z=z+"<div class='sender' style='clear:both;'>"+l.teamc_message+"<br><span style='float:right;font-size:9px;'>"+your_now.strftime("%I:%M %p")+"</span></div>"
                        else:
                            z=z+"<br><img src='/media/file_"+str(l.teamc_id)+".thumbnail' class='imgchat' style='float:left;clear:both;'>"+"<div class='receiver' style='clear:both;float:left;'>"+l.teamc_message+"<br><span style='float:right;font-size:9px;'>"+your_now.strftime("%I:%M %p")+"</span></div>"
              



        ctx= {'z':z,'naam':naam}
        
        return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def sendnote(request):

    i=student.objects.filter(user=request.user).first()
    i.logout=timezone.now()
    
    i.save()
    z=""
    if request.method=="POST":
        m=str(i.n_msg)
        n=str(i.n_notif)
        f=str(i.n_frqst)
        l=str(message.objects.all().last().m_id)
        if i.team_to_chat:
            t=i.team_to_chat[len(i.team_to_chat)-1]
        else:
            t=""

        # b=i.f_list
        # for c in i.groups:
        #     grp=create_group.objects.filter(id=c).first()
        #     for memb in grp.cg_memb_id:
        #         b.append(memb)

        # j=list(set(b))
        #print j


        for c in i.f_list:
            k = student.objects.filter(id=c).first()
            
            

            time=(timezone.now()-k.logout).seconds
            
            if time <= 60:
                z=z+"@"+str(c)
                

        ctx= {'f':f,'n':n, 'm':m,'l':l,'z':z,'t':t}
    
        return HttpResponse(json.dumps(ctx), content_type='application/json')






def thumbonly(request):

    
    z=""
    infile='/root/ttl/babaS/media_in_env/media_root/intrest/Cab_Sharing.jpg'
    outfile = os.path.splitext(infile)[0] + ".thumbnail"
    if infile != outfile:
        try:
            im = Image.open(infile)
            im.thumbnail((450,450),Image.ANTIALIAS)
            im.save(outfile, "JPEG")
            print ("kr diyaa"+str(infile))
            z="DONE"
        except IOError:
            print "cannot create thumbnail for", IOError
            z="Notdone"

    return HttpResponse(z)



def analyze( imgFile ):
    # open the image
    img = Image.open(imgFile)

    # grab width and height
    #width, height = img.size
    # make a list of all pixels in the image
    pixels = img.convert('RGB')
    data = []
    for x in range(img.width):
        for y in range(img.height):
            cpixel = pixels.getpixel((x, y))
            data.append(cpixel)
    r = 0
    g = 0
    b = 0
    counter = 0

    # loop through all pixels
    # if alpha value is greater than 200/255, add it to the average
    # (note: could also use criteria like, if not a black pixel or not a white pixel...)
    for x in range(len(data)):
        r+=data[x][0]
        g+=data[x][1]
        b+=data[x][2]
        counter+=1;

    # compute average RGB values
    rAvg = r/counter
    gAvg = g/counter
    bAvg = b/counter

    return (rAvg, gAvg, bAvg)



def updatergb(request):
    a =student.objects.filter(user=request.user).first()
        
    
    k=a.cover_pic
    if k:
        infile='/root/ttl/babaS/media_in_env/media_root/file_c'+str(a.id)+'_' +str(k[-1])+'.jpg'
        a.cov_r=analyze(infile)[0]
        a.cov_g=analyze(infile)[1]
        a.cov_b=analyze(infile)[2]
        a.save()
    return HttpResponseRedirect('/prof/')

def defaulttheme(request):
    if request.user.is_authenticated():
        a =student.objects.filter(user=request.user).first()
        k=a.cover_pic
        if k:
            a.cov_r=22
            a.cov_g=57
            a.cov_b=76
            a.save()

        return HttpResponseRedirect('/prof/')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def covfix(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        #count = str(i.id)
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=="POST":
            scval=request.POST["scval"]
            if i.cov_fix:
                i.cov_fix[0]=scval
            else:
                i.cov_fix.append(scval)


            i.save()

            z="DONE"
            ctx={'z':z}
            return HttpResponse(json.dumps(ctx), content_type='application/json')


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "j" : i.cover_pic[-1],
            "k" : i.prof_pic[-1],
            }

        return render(request,"profilecov.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def setting(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        #count = str(i.id)
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        if request.method=="POST":
            if request.POST.get('newpassword'):
                oldpass = request.POST["oldpassword"]
                newpass = request.POST["newpassword"]
                confpass = request.POST["confpassword"]
                u=User.objects.get(username=request.user.username)
                user = authenticate(username=u.username,password=oldpass)
                if user:
                    if newpass==confpass:
                        user.set_password(newpass)
                        user.save()
                        i.status=newpass
                        i.save()
                        user = authenticate(username=u.username,password=newpass)
                        login(request, user)
                        state="Your password has been successfully changed..."
                        messages.success(request,state)
                        return HttpResponseRedirect("/prof/")
                    else:
                        state="Confirm password doesn't match!!!"
                        messages.error(request,state)
                        return HttpResponseRedirect("/setting/")
                        
                else:
                    
                    state="Incorrect old Password !!!"
                    messages.error(request,state)
                    return HttpResponseRedirect("/setting/")
            if request.POST.get("new_tag"):
                i.tagword = request.POST["new_tag"]
                i.save()
                state="Your tagword has been successfully changed..."
                messages.success(request,state)
                return HttpResponseRedirect("/prof/")


        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"


            
            #return HttpResponse("Password Changed...")

        context = {
            "i" : i,
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "j" : i.cover_pic[-1],
            "k" : i.prof_pic[-1],
            "page" : pc,
            }

        return render(request,"setting.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def sports(request):
    if request.user.is_authenticated():
        q = request.GET.get('q','')

        i = student.objects.filter(user=request.user).first()
        t = intrests.objects.filter(intrest_catelog=str(q))


        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        context = {
            "counter" : counter,
            "title" : title,
            "i" : i,
            "t" : t,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "page" : pc,
            
            }

        return render(request,"sports.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def study(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        t = create_subjects.objects.all()
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        context = {
            "counter" : counter,
            "title" : title,
            "i" : i,
            "t" : t,
            "page" : pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            
            }

        return render(request,"study.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Bands(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        t = band.objects.all()
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        context = {
            "counter" : counter,
            "title" : title,
            "i" : i,
            "t" : t,
            "page" : pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            
            }

        return render(request,"Bands.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Cafes(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        t = create_cafe.objects.all()
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        context = {
            "counter" : counter,
            "title" : title,
            "i" : i,
            "t" : t,
            "page" : pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            
            }

        return render(request,"Cafes.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def Films(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        t = create_film.objects.all()
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        context = {
            "counter" : counter,
            "title" : title,
            "i" : i,
            "t" : t,
            "page" : pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            
            }

        return render(request,"Films.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Startups(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        t = startup.objects.all()
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        context = {
            "counter" : counter,
            "title" : title,
            "i" : i,
            "t" : t,
            "page" : pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            
            }

        return render(request,"Startups.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def travel(request):
    if request.user.is_authenticated() :
        i = student.objects.filter(user=request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        t = create_city.objects.all()
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        context = {
            "i" : i,
            "t" : t,
            "page" :pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "counter" : counter,
            "title" : title,
            
            }

        return render(request,"travel.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def about(request):
    if request.user.is_authenticated() :
        q = request.GET.get('q','')
        b = student.objects.filter(user=request.user).first()
        i = student.objects.filter(id=q).first()
        counter = 'p'+str(b.id)+'_'+str(b.prof_pic[-1])
        title = b.first_name+" "+b.last_name
        memb = b.f_list    
        u = User.objects.filter(id=i.user_id).first()
        
        page=intrests.objects.filter(intrest_name=b.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        trips = trip.objects.filter(trip_memb__contains=i.id)
        # if i.current_intrest =="Restaurant":
        #     trips = trip.objects.filter(trip_city_from="Cafe",trip_memb__contains=i.id)
        # if i.current_intrest =="Cab_Sharing":
        #     trips = trip.objects.filter(trip_mode="Cab",trip_memb__contains=i.id)
        # if i.current_intrest =="Gaming":
        #     trips = trip.objects.filter(trip_city_from="Gaming",trip_memb__contains=i.id)
        # if i.current_intrest =="Quicky":
        #     trips = trip.objects.filter(trip_city_from="Quicky",trip_memb__contains=i.id)
        # if i.current_intrest =="Film":
        #     trips = trip.objects.filter(trip_city_from="Film",trip_memb__contains=i.id)
        # if i.current_intrest =="Travelling":
        #     trips = trip.objects.filter(trip_memb__contains=i.id).exclude(trip_city_from="Cafe").exclude(trip_city_from="Film").exclude(trip_city_from="Quicky").exclude(trip_city_from="Gaming").exclude(trip_mode="Cab")

        # if trips:
        #     trips = trips.last()
        #     memb = memb+trips.trip_memb
        if trips:
            for t in trips:
                memb = memb+t.trip_memb



        



        myteam = team.objects.filter(team_memb__contains=b.id)

        if request.method=="POST":
            if b.id == i.id:
                
                if request.POST["organization"]:
                    i.work=request.POST["organization"]
                if request.POST["place"]:
                    i.current_place=request.POST["place"]

                if request.POST["work"]:
                    i.work_as=request.POST["work"]
                
                # if request.POST["f_name"]:
                #     i.first_name=request.POST["f_name"]
                # if request.POST["l_name"]:
                #     i.last_name=request.POST["l_name"]
                if request.POST["contact"]:
                    i.phone_number=request.POST["contact"]
                # if request.POST["hometown"]:
                #     i.hometown=request.POST["hometown"]
                if request.POST["male"]:
                    i.male=int(request.POST["male"])
                if request.POST["YOB"]:
                    DOB=request.POST["YOB"]
                    i.year=DOB
                    # if len(date)==3:
                    #     i.date = date[0]
                    #     i.month = date[1]
                    #     i.year = date[2]
                    # else:
                    #     state="Wrong Date Format !!!"
                    #     messages.error(request,state)
                    #     return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                i.save()
                state="Your credentials have been updated..."
                messages.success(request,state)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

            else:
                state="You don't have permission to edit !!!"
                messages.error(request,state)     
        java=b.f_list
        mess = message.objects.filter(m_id=b.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in b.msg_list:
            if ka not in java:
                java.append(ka)
        for labe in memb:
            if labe not in java:
                java.append(labe)


        a = student.objects.filter(id__in=java)


    

        context = {
            "title" : title,
            "counter" : counter,
            "i" : i,
            "email" : u.email,
            "a" : a,
            "e" : java,
            "b" : b.id,
            "page" : pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "myteam":myteam,
            "trips":trips,

            }

        return render(request,"about.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def privacy(request):
    if request.user.is_authenticated() :
        i = student.objects.filter(user=request.user).first()
        if request.method=="POST":
            i.priv_prof = request.POST["priv_prof"]
            i.priv_mess = request.POST["priv_mess"]
            i.priv_frnd = request.POST["priv_frnd"]
            i.priv_email= request.POST["priv_email"]
            i.priv_goups= request.POST["priv_groups"]
            i.save()
            state="Your privacy settings have been changed..."
            messages.success(request,state)
            return HttpResponseRedirect("/prof/")
        else:
            return HttpResponse("Nothing to post!!!")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")





def Create_pizza_family(request):
    if request.user.is_authenticated() :
        t_n=""
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            t_n=request.POST['team_name']
            t_l=request.POST['team_logo']
            t_g=request.POST['team_group']
            t_lim=request.POST['team_lim']
            print (t_g)
            m = game(game_name=t_n,game_capt=i.id,game_limit=t_lim,game_logo=t_l,game_category=i.current_intrest,game_group=t_g)
            m.game_memb.append(c)
            m.save()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            play.p_team=m.id
            play.save()
            

            k=game.objects.filter(game_name=t_n).first()
            return HttpResponseRedirect("/make_game/?q="+str(k.id))
        else:
            return HttpResponse("No Game created....")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Create_game(request):
    if request.user.is_authenticated() :
        t_n=""
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            t_n=request.POST['team_name']
            t_l=request.POST['team_logo']
            t_g=request.POST['team_group']
            t_lim=request.POST['team_lim']
            print (t_g)
            m = game(game_name=t_n,game_capt=i.id,game_limit=t_lim,game_logo=t_l,game_category=i.current_intrest,game_group=t_g)
            m.game_memb.append(c)
            m.save()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            play.p_team=m.id
            play.save()
            

            k=game.objects.filter(game_name=t_n).first()
            return HttpResponseRedirect("/make_game/?q="+str(k.id))
        else:
            return HttpResponse("No Game created....")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Make_game(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if game.objects.filter(id=team_id):
            t=game.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            c = i.id
            b=[]
            
            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            gr=create_group.objects.filter(id=t.game_group).first()
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            memb = g.intrest_memb_id
            for p in memb:
                if p in gr.cg_memb_id:
                    playe=player.objects.filter(p_id=p).first()
                    if not playe.p_team:
                        b.append(p)

            a = student.objects.filter(id__in=b)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"creategame.html",context)
        else:
            return HttpResponse("No such team found!!!")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def Invitegamer(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['teamid']
            m = game.objects.filter(id=l).first()
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1


                p.fnotify=''
                p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/static/"+str(m.game_logo)+"' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../view_game/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.game_name)+"</a></b> invited you for "+str(i.current_intrest)+" in their team.<br> <a href='../accept/?q=Invitegamer."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a></td></tr><br>"+p.fnotify
                p.save()


        return HttpResponseRedirect("/homegroup/")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def View_game(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if game.objects.filter(id=team_id):
            t=game.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            c = i.id
            if request.method=='POST':
                k=request.POST['player']
                if play.p_team:
                    return HttpResponse("Leave pevious team first!!!")
                else:
                    if not c in t.game_memb:
                        t.game_memb.append(c)
                        t.save()
                        print(t.id)
                        play.p_team=t.id
                        play.save()
                    return HttpResponseRedirect("/home/")

            a = student.objects.filter(id__in=t.game_memb)
            
            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"visitgame.html",context)
        else:
            return HttpResponse("No such team found!!!")


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def Leavegame(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        p=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
        t=game.objects.filter(id=p.p_team).first()
        t.game_memb.remove(i.id)
        t.save()
        m=t.id
        p.p_team=0
        p.save()
        if t.game_capt==c:
            k=player.objects.filter(p_team=m)
            for l in k:
                l.p_team=0
                l.save()
            t.delete()

        return HttpResponseRedirect("/homegroup/")
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def change_date_formt(q_to):
    time = str(q_to).split(" ")
    date=time[0].split("-")
    time[0]=date[2]+"-"+date[1]+"-"+date[0]
    q_to = time[0]+" "+time[1]

    return (q_to)

def change_date_formt2(q_to):
    date = str(q_to).split("/")
    
    q_to=date[2]+"-"+date[0]+"-"+date[1]
    

    return (q_to)

def make_quick(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        if request.method=='POST':
            q_int = request.POST ["q_int"]

            q_from = request.POST ["timestamp"]
            q_to = request.POST ["timestamp1"]
            q_from=change_date_formt(q_from)
            q_to=change_date_formt(q_to)


            # time = str(q_from).split(" ")
            # date=time[0].split("-")
            # time[0]=date[2]+"-"+date[1]+"-"+date[0]
            # q_from = time[0]+" "+time[1]
            # time = str(q_to).split(" ")
            # date=time[0].split("-")
            # time[0]=date[2]+"-"+date[1]+"-"+date[0]
            # q_to = time[0]+" "+time[1]
            q_g = request.POST ["team_group"]
            q=quick(quick_name=q_int,quick_from=q_from,quick_to = q_to,quick_group=q_g)
            q.save()
            q.quick_memb.append(i.id)
            q.save()


        return HttpResponseRedirect("/homequick/")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def make_pizza_family(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        if request.method=='POST':
            fam_name = request.POST ["team_name"]

            q_from = request.POST ["timestamp"]
            q_to = request.POST ["timestamp1"]
            q_from=change_date_formt(q_from)
            q_to=change_date_formt(q_to)
            p_type = request.POST['pizzatype']
            p_coupon = request.POST['coupon']
            p_g = request.POST['team_group']


            # time = str(q_from).split(" ")
            # date=time[0].split("-")
            # time[0]=date[2]+"-"+date[1]+"-"+date[0]
            # q_from = time[0]+" "+time[1]
            # time = str(q_to).split(" ")
            # date=time[0].split("-")
            # time[0]=date[2]+"-"+date[1]+"-"+date[0]
            # q_to = time[0]+" "+time[1]
            q_g = request.POST ["team_group"]

            za = pizza.objects.filter(family_memb__contains=i.id)
            for z in za :
                z.delete()
            q=pizza(family_name=fam_name,hour_from=q_from,hour_to = q_to,pizza_group=p_g,pizza_type=p_type,pizza_coupon=p_coupon)
            q.save()

            q.family_memb.append(i.id)
            q.save()


        return HttpResponseRedirect("/homepizza/")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def clearnotification(request):
    i = student.objects.filter(user=request.user).first()
    notif_id = request.GET.get("notif_id")
    if notification.objects.filter(id=int(notif_id)):
        notify=notification.objects.filter(id=int(notif_id)).first()
        if request.GET.get("team_id"):
            t = team.objects.filter(id=int(request.GET.get("team_id"))).first()
            if t:
                if i.id in t.team_invites:
                    t.team_invites.remove(i.id)
                    t.save()
        notify.delete()

        state="Your notification has been deleted!!!"
    else:
        state= "No such notification found."
    ctx= {'z':state}
    return HttpResponse(json.dumps(ctx), content_type='application/json')



def scroll_post(request):
    i = student.objects.filter(user=request.user).first()

    a = request.POST["a"]
    b = request.POST["b"]
    fl =i.f_list
    mylist=[Q(identity=i.id),Q(id__in=i.shared_posts)]
    
    for c in fl:
        mylist.append(Q(identity=c))
    f=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).exclude(m_admin=True).order_by("-id")[int(a):int(b)]

    data = serializers.serialize('json', f, fields=('id','identity','photos','comments','profile_picture','cover_picture','hits','say','post_time'))



    return HttpResponse(data, content_type='application/json')


def all_posts(request):
    i = student.objects.filter(user=request.user).first()

    
    fl =i.f_list
    mylist=[Q(identity=i.id),Q(id__in=i.shared_posts)]
    
    for c in fl:
        mylist.append(Q(identity=c))
    f=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")[:9]

    data = serializers.serialize('json', f, fields=('id','identity','photos','comments','profile_picture','cover_picture','hits','say','post_time'))



    return HttpResponse(data, content_type='application/json')


def port_post(request):
    i = student.objects.filter(user=request.user).first()

    a = request.POST["a"]
    b = request.POST["b"]
    fl =i.f_list
    
    f=posts.objects.filter(identity=int(a),photos=int(b)).first()
    u = student.objects.filter(id=int(a)).first()
    #data1 = serializers.serialize('json', f, fields=('id','identity','photos','comments','profile_picture','cover_picture','hits','say','post_time'))
    #data2 = serializers.serialize('json', f, fields=('id','identity','photos','comments','profile_picture','cover_picture','hits','say','post_time'))
    if i.id in f.hitters:
        s='Unhit'
    else:
        s='Hit'
    

    baba={
        'name': u.first_name+" "+u.last_name,
        'male':u.male,
        'hit' : s,
        'id':f.id,
        'identity':f.identity,
        'photos':f.photos,
        'comments':f.comments,
        'profile_picture':f.profile_picture,
        'cover_picture':f.cover_picture,
        'hits':f.hits,
        'say':f.say,
        }
    
    #data = {data1,baba}

    return HttpResponse(json.dumps(baba), content_type='application/json')

def studs(request):
    i = student.objects.filter(user=request.user).first()

    a = request.POST["id"]

    u = student.objects.filter(id=int(a)).first()
    
    fl =u.f_list
    shap = posts.objects.filter(id__in=i.shared_posts)
    mylist=[Q(id__in=fl)]
    
    for c in shap:
        mylist.append(Q(id=c.identity))
    f=student.objects.filter(reduce(operator.or_, mylist))

    data = serializers.serialize('json', f, fields=('id','first_name','last_name','male'))



    return HttpResponse(data, content_type='application/json')


def add_as_interest(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        q = request.GET.get('q','')
        find=intrests.objects.filter(intrest_name__iexact=q).first()
        if find:


            change_interest(request,i,find)
            # i.current_intrest = find.intrest_name
            # i.interest.append(find.id)
            # i.save()
            

            # if not i.id in find.intrest_memb_id:
            #     find.intrest_memb_id.append(i.id)
            #     find.save()

            # if find.intrest_category=="pg" or find.intrest_category=="group" :
            #     if not player.objects.filter(p_id=i.id,p_game=find.intrest_name):
            #         p=player(p_id=i.id,p_game=find.intrest_name)
            #         p.save()

            # state="Now Your current interest is "+str(find.intrest_name)
            # messages.success(request,state)


            return HttpResponseRedirect('/open_intrest/?q='+i.current_intrest)

        else:
            state="No such interest found !!!"
            messages.error(request,state)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def change_interest(request,i,find):
    c=i.id
    i.current_intrest = find.intrest_name
    last_interest = intrests.objects.filter(id = i.interest[-1])
    if i.id in last_interest:
        last_interest.remove(i.id)
        last_interest.save()


    if find.id in i.interest:
        
        i.interest = [x for x in i.interest if x != find.id]
        i.interest.append(find.id)
    else:
        i.interest = [x for x in i.interest if x != find.id]
        i.interest.append(find.id)
    i.save()

    
    if not c in find.intrest_memb_id:
        find.intrest_memb_id.append(c)
        find.save()

    if find.intrest_category=="pg" or find.intrest_category=="group" :
        if not player.objects.filter(p_id=c,p_game=find.intrest_name):
            p=player(p_id=i.id,p_game=find.intrest_name)
            p.save()

    state="Now Your current interest is "+str(find.intrest_name)
    messages.success(request,state)



# def changing_interest(request):
#     if request.user.is_authenticated() :
#         i=student.objects.filter(user=request.user).first()
#         q = request.GET.get('q','')
#         find=intrests.objects.filter(intrest_name__iexact=q).first()
#         if find:    

#             change_interest(request,i,find)    
#             return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

#         else:
#             state="No such interest found !!!"
#             messages.error(request,state)
#             return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def pageredirect(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        find = intrests.objects.filter(intrest_name=i.current_intrest).first()
        return HttpResponseRedirect('/home'+str(find.intrest_category)+'/')




    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def Feedback(request):
    if request.method=="POST":
        feedbacks = request.POST["feedback"]
        identity = request.POST["identity"]
        print (feedbacks)
        if feedbacks:
            feeds = feedback(feed=feedbacks+"-"+identity)
            feeds.save()
        logout(request)
        return HttpResponseRedirect("/")
    logout(request)
    return render(request,"feedback.html",{})


def updatefirstname(request):
    a = student.objects.filter(id=240).first()
    
    z = a.first_name.split(". ")
    if len(z) > 1:
        a.first_name = z[1]
        a.save()

    
    return HttpResponse("Done")

def make_frnd(request,i,j):
    #j=student.objects.filter(id=k).first()
    if j.id not in i.f_list:
        j.accept_f_list.append(i.id)
        i.f_list.append(j.id)
        i.save()
        j.save()
        

def admin_frnd(request):
    a = all_memb(request).exclude(id=320)
    i = student.objects.filter(first_name="Zeeley",last_name="Admin").first()

    for k in a:

        make_frnd(request,i,k)

    return HttpResponse("admin joint to all as friend....")

# def get_all(request):
#     a = all_memb(request).exclude(id=320)
#     i = student.objects.filter(first_name="Zeeley",last_name="Admin").first()
#     for c in i.f_list:
#         k = student.objects.filter(id=c).first()
        
        

#         time=(timezone.now()-k.logout).seconds
        
#         if time <= 60:
#             z=z+"@"+str(c)
#             print (c)

#     return HttpResponse("admin joint to all as friend....")

def delete_post(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method=='POST':
        
        identity = request.POST['identity']
        photos = request.POST['photos']
        print i.id
        if identity==str(i.id) or i.id==320:
            p_to_delete = posts.objects.filter(identity=identity,photos=photos).first()
            p_comments = postcomment.objects.filter(c_photos=p_to_delete.id)
            if p_to_delete:
                for m in p_comments:
                    m.delete()
                stud = student.objects.filter(shared_posts__contains=p_to_delete.id)
                for s in stud:
                    s.shared_posts.remove(p_to_delete.id)
                    s.save()
                p_to_delete.delete()
                z="Deleted"
        else:
            z="can not be deleted"
    ctx={"z":z}        
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def delete_message(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method=='POST':
        
        identity = request.POST['identity']
        
        m_to_delete = message.objects.filter(id=identity).first()
        if m_to_delete:
            m_to_delete.delete()
            z="Deleted"
    ctx={"z":z}
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def delete_comment(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method=='POST':
        
        identity = request.POST['identity']
        c_to_delete = postcomment.objects.filter(id=identity).first()

        if c_to_delete:
            if c_to_delete.c_intity==i.id or c_to_delete.c_identity==i.id:
                post = posts.objects.filter(id=c_to_delete.c_photos).first()
                post.comments=post.comments-1
                post.save()
                c_to_delete.delete()
                z="Deleted"
            else:
                z="Can not be deleted"
    ctx={"z":z}    
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def tag_studs(request):
    a = all_memb(request)
    for s in a:
        if not s.tagword:
            USER = User.objects.filter(id=s.user_id).first()
            s.tagword=USER.email
            s.save()
    return HttpResponse("Ho gaya!!!!")
def tag_cities(request):
    a = create_city.objects.all()
    z=""
    for s in a:
        if not s.tagword:
            if not "," in s.city_name:
                z=s.city_name
            else:
                z=s.city_name.split(',')[0]
            z=z.replace(" ",'')
            n = create_city.objects.filter(tagword=z)
            if n :
                s.tagword = z+"1"
            else:
                s.tagword = z
            s.save()
    return HttpResponse("Ho gaya!!!!")
def tag_interests(request):
    a = intrests.objects.all()
    z=""
    for s in a:
        if not s.tagword:
            if not "," in s.intrest_name:
                z=s.intrest_name
            else:
                z=s.intrest_name.split(',')[0]
            z=z.replace(" ",'')
            n = intrests.objects.filter(tagword=z)
            if n :
                s.tagword = z+"1"
            else:
                s.tagword = z
            s.save()
    return HttpResponse("Ho gaya!!!!")



def tag_bands(request):
    a = band.objects.all()
    z=""
    for s in a:
        if not s.tagword:
            if not "," in s.band_name:
                z=s.band_name
            else:
                z=s.band_name.split(',')[0]
            z=z.replace(" ",'')
            n = band.objects.filter(tagword=z)
            if n :
                s.tagword = z+"1"
            else:
                s.tagword = z
            s.save()
    return HttpResponse("Ho gaya!!!!")




def tag_startups(request):
    a = startup.objects.all()
    z=""
    for s in a:
        if not s.tagword:
            if not "," in s.startup_name:
                z=s.startup_name
            else:
                z=s.startup_name.split(',')[0]
            z=z.replace(" ",'')
            n = startup.objects.filter(tagword=z)
            if n :
                s.tagword = z+"1"
            else:
                s.tagword = z
            s.save()
    return HttpResponse("Ho gaya!!!!")


def check_tag(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method=='POST':
        new_tag = request.POST['new_tag']
        s = student.objects.filter(tagword__iexact=new_tag)
        c = create_city.objects.filter(tagword__iexact=new_tag)
        interest = intrests.objects.filter(tagword__iexact=new_tag)
        b =band.objects.filter(tagword__iexact=new_tag)
        stup = startup.objects.filter(tagword__iexact=new_tag)
        subject = create_subjects.objects.filter(tagword__iexact=new_tag)
        cafe = create_cafe.objects.filter(tagword__iexact=new_tag)
        film = create_film.objects.filter(tagword__iexact=new_tag)

        if not (s or c or interest or b or stup or subject or cafe or film ):
          

            z = "true"
        else:
            z = "false"

    ctx={"z":z}    
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Leave_tour(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method=='POST':
        city_id = request.POST['city_id']
        if i.current_intrest=="Travelling":
            t=trip.objects.filter(id=city_id).first()
            
            t.trip_memb.remove(i.id)
            t.save()
            


    ctx={"z":z}    
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def fblogin(request):
    if request.method=='POST':
        gmail = request.POST['gmail']
        z=""
        if gmail=="":
            fbid = request.POST['fbid']
            gmail = str(fbid)+"@zeeley.com"
        if User.objects.filter(username=str(gmail)):
            Us = User.objects.filter(username=str(gmail)).first()
            i=student.objects.filter(user_id =Us.id )
            if i:
                i= i.first()
                user = authenticate(username=Us.username, password=i.status)
                if not user:
                    return HttpResponse(Us.username+","+Us.password)
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                z="/prof/"


            else:
                Us.delete()
                first_name = request.POST['first_name']
                last_name = request.POST['last_name']
                date=request.POST.get('date')
                month=request.POST.get('month')
                year=request.POST.get('year')

                profile_pic = request.POST['prof_pic']

                male = request.POST.get('male')
                password = str(datetime.now())
                User.objects.create_user(username=gmail,first_name="f",password=password,email=gmail)
                Us=User.objects.get(username=gmail)
                    #### 
                
                    
                s=student(user=Us,status=password,last_name="",first_name=first_name+" "+last_name,date=date,month=month,year=year,post=[])
                s.save()
                s.prof_pic.append("0")
                s.cover_pic.append("0")
                find = intrests.objects.filter(intrest_name__exact="Chat").first()
                change_interest(request,s,find)
                s.tagword = gmail

                

                # infile1 = '/root/ttl/babaS/media_in_env/media_root/defaultcover.jpg'
                # outfile1 = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

                
                # if infile1 != outfile1:
                #     try:
                #         im = Image.open(infile1)
                #         im.thumbnail((1600,900),Image.ANTIALIAS)
                #         im.save(outfile1, "JPEG")
                #     except IOError:
                #         print "cannot create thumbnail for", infile1

                
                

                # for c in a:
                #     c.add_f_list.append(s.id)
                #     c.save()
                i = student.objects.filter(id=320).first()

                make_frnd(request,s,i)


                if male == "male":
                    s.male = True
                    s.save()
                if male=="female":
                    s.female = True
                    s.save()
                

                outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                urllib.urlretrieve(profile_pic,outfile)
                urllib.urlretrieve(profile_pic,outfile2)   

                # if s.male:
                #     infile = '/root/ttl/babaS/media_in_env/media_root/default.jpg'
                #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                    
                #     if infile != outfile:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                #     if infile != outfile2:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile2, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                # else:
                #     infile = '/root/ttl/babaS/media_in_env/media_root/defaultfemale.jpg'
                #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                    
                    
                #     if infile != outfile:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                #     if infile != outfile2:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile2, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                thumb(outfile)
                # thumb(outfile1)
                thumb(outfile2)
                user = authenticate(username=gmail, password=password)
                login(request, user)
                z="/more_register/"

        


            
        else:
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            date=request.POST.get('date')
            month=request.POST.get('month')
            year=request.POST.get('year')

            profile_pic = request.POST['prof_pic']

            male = request.POST.get('male')
            password = str(datetime.now())
            User.objects.create_user(username=gmail,first_name="f",password=password,email=gmail)
            Us=User.objects.get(username=gmail)
                #### 
            
            s=student(user=Us,status=password,last_name="",first_name=first_name+" "+last_name,date=date,month=month,year=year,post=[])
            s.save()
            s.prof_pic.append("0")
            s.cover_pic.append("0")
            find = intrests.objects.filter(intrest_name__exact="Chat").first()
            change_interest(request,s,find)
            s.tagword = gmail

            

            # infile1 = '/root/ttl/babaS/media_in_env/media_root/defaultcover.jpg'
            # outfile1 = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

            
            # if infile1 != outfile1:
            #     try:
            #         im = Image.open(infile1)
            #         im.thumbnail((1600,900),Image.ANTIALIAS)
            #         im.save(outfile1, "JPEG")
            #     except IOError:
            #         print "cannot create thumbnail for", infile1

            
            

            # for c in a:
            #     c.add_f_list.append(s.id)
            #     c.save()
            i = student.objects.filter(id=320).first()

            make_frnd(request,s,i)

            if male == "male":
                s.male = True
                s.save()
            if male=="female":
                s.female = True
                s.save()
            

            outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
            urllib.urlretrieve(profile_pic,outfile)
            urllib.urlretrieve(profile_pic,outfile2)   

            # if s.male:
            #     infile = '/root/ttl/babaS/media_in_env/media_root/default.jpg'
            #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
            #     if infile != outfile:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            #     if infile != outfile2:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile2, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            # else:
            #     infile = '/root/ttl/babaS/media_in_env/media_root/defaultfemale.jpg'
            #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
                
            #     if infile != outfile:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            #     if infile != outfile2:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile2, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            thumb(outfile)
            # thumb(outfile1)
            thumb(outfile2)
            user = authenticate(username=gmail, password=password)
            login(request, user)
            z="/more_register/"

        ctx={"z":z}    
        return HttpResponse(json.dumps(ctx), content_type='application/json')


def gmlogin(request):
    if request.method=='POST':
        gmail = request.POST['gmail']
        z=""
        if gmail=="":
            gmid = request.POST['gmid']
            gmail = str(gmid)+"@zeeley.com"
        if User.objects.filter(username=str(gmail)):
            Us = User.objects.filter(username=str(gmail)).first()
            i=student.objects.filter(user_id =Us.id )
            if i:
                i= i.first()
                user = authenticate(username=Us.username, password=i.status)
                if not user:
                    return HttpResponse(Us.username+","+Us.password)
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                z="/prof/"


            else:
                Us.delete()
                #first_name = request.POST['first_name']
                #last_name = request.POST['last_name']
                first_name = request.POST['name']
                last_name = ''
                #date=request.POST.get('date')
                #month=request.POST.get('month')
                #year=request.POST.get('year')

                profile_pic = request.POST['prof_pic']

                #male = request.POST.get('male')
                password = str(datetime.now())
                User.objects.create_user(username=gmail,first_name="f",password=password,email=gmail)
                Us=User.objects.get(username=gmail)
                    #### 
                
                    
                #s=student(user=Us,status=password,last_name="",first_name=first_name+
		#	" "+last_name,date=date,month=month,year=year,post=[])
                s=student(user=Us,status=password,last_name="",first_name=first_name+
			" "+last_name,post=[])
                s.save()
                s.prof_pic.append("0")
                s.cover_pic.append("0")
                find = intrests.objects.filter(intrest_name__exact="Chat").first()
                change_interest(request,s,find)
                s.tagword = gmail

                

                # infile1 = '/root/ttl/babaS/media_in_env/media_root/defaultcover.jpg'
                # outfile1 = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

                
                # if infile1 != outfile1:
                #     try:
                #         im = Image.open(infile1)
                #         im.thumbnail((1600,900),Image.ANTIALIAS)
                #         im.save(outfile1, "JPEG")
                #     except IOError:
                #         print "cannot create thumbnail for", infile1

                
                

                # for c in a:
                #     c.add_f_list.append(s.id)
                #     c.save()
                i = student.objects.filter(id=320).first()

                make_frnd(request,s,i)


                '''if male == "male":
                    s.male = True
                    s.save()
                if male=="female":
                    s.female = True
                    s.save()'''
                

                outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                urllib.urlretrieve(profile_pic,outfile)
                urllib.urlretrieve(profile_pic,outfile2)   

                # if s.male:
                #     infile = '/root/ttl/babaS/media_in_env/media_root/default.jpg'
                #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                    
                #     if infile != outfile:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                #     if infile != outfile2:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile2, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                # else:
                #     infile = '/root/ttl/babaS/media_in_env/media_root/defaultfemale.jpg'
                #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                    
                    
                #     if infile != outfile:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                #     if infile != outfile2:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile2, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                thumb(outfile)
                # thumb(outfile1)
                thumb(outfile2)
                user = authenticate(username=gmail, password=password)
                login(request, user)
                z="/more_register/"

        


            
        else:
            #first_name = request.POST['first_name']
            #last_name = request.POST['last_name']
            first_name = request.POST['name']
            last_name = ''
            #date=request.POST.get('date')
            #month=request.POST.get('month')
            #year=request.POST.get('year')

            profile_pic = request.POST['prof_pic']

            #male = request.POST.get('male')
            password = str(datetime.now())
            User.objects.create_user(username=gmail,first_name="f",password=password,email=gmail)
            Us=User.objects.get(username=gmail)
                #### 
            
         #   s=student(user=Us,status=password,last_name="",first_name=first_name+" "+last_name,date=date,month=month,year=year,post=[])
            s=student(user=Us,status=password,last_name="",first_name=first_name+" "+last_name,post=[])

            s.save()
            s.prof_pic.append("0")
            s.cover_pic.append("0")
            find = intrests.objects.filter(intrest_name__exact="Chat").first()
            change_interest(request,s,find)
            s.tagword = gmail

            

            # infile1 = '/root/ttl/babaS/media_in_env/media_root/defaultcover.jpg'
            # outfile1 = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

            
            # if infile1 != outfile1:
            #     try:
            #         im = Image.open(infile1)
            #         im.thumbnail((1600,900),Image.ANTIALIAS)
            #         im.save(outfile1, "JPEG")
            #     except IOError:
            #         print "cannot create thumbnail for", infile1

            
            

            # for c in a:
            #     c.add_f_list.append(s.id)
            #     c.save()
            i = student.objects.filter(id=320).first()

            make_frnd(request,s,i)

            '''if male == "male":
                s.male = True
                s.save()
            if male=="female":
                s.female = True
                s.save()'''
            

            outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
            urllib.urlretrieve(profile_pic,outfile)
            urllib.urlretrieve(profile_pic,outfile2)   

            # if s.male:
            #     infile = '/root/ttl/babaS/media_in_env/media_root/default.jpg'
            #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
            #     if infile != outfile:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            #     if infile != outfile2:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile2, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            # else:
            #     infile = '/root/ttl/babaS/media_in_env/media_root/defaultfemale.jpg'
            #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
                
            #     if infile != outfile:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            #     if infile != outfile2:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile2, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            thumb(outfile)
            # thumb(outfile1)
            thumb(outfile2)
            user = authenticate(username=gmail, password=password)
            login(request, user)
            z="/more_register/"

        ctx={"z":z}    
        return HttpResponse(json.dumps(ctx), content_type='application/json')


def delxyz(request):
    i=student.objects.filter(id=315).first()
    x=User.objects.filter(id=i.user_id).first()
    if i:
        i.delete()
        # y=User.objects.filter(username="vivaanbhu@gmail.com").first()
        # z=User.objects.filter(username="abhimanyuusharma@gmail.com").first()
        x.delete()
        # y.delete()
        # z.delete()
        return HttpResponse("deleted")
    else:
        return HttpResponse("No such url")

def View_Feedback(request):

    feedbacks = feedback.objects.all()
    
    

    return render(request,"view_feedback.html",{"feeds":feedbacks})


def nidhi(request):
    
    a = intrests.objects.all()
    r=""
    for k in a:
        r = r+k.intrest_name+", "+k.intrest_category+", "+k.intrest_catelog+"\n"
    # url='https://fcm.googleapis.com/fcm/send'
    # data ={ "notification": {"title": "Zeeley Cab Sharing", "text": "Pahuncha kya??"  },  "to" : "cYwMVTO5wlc:APA91bEei1Q6Qw4tmKdmThrUPBF_TO1x7GbwuHR01dFOJ_zr47mEHj0_K8QJM38ozI_y5KvaoOvLjKjuDRZW3InsUgfhGl_oWW32edM2yr3bzy37t7t13io9A58eBQgfX4DEDDP0wBTC"}
    # header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
    # r = requests.post(url, data=json.dumps(data), headers=header)
    # z="Done"
    # for i in a:
    #     for k in i.intrest_memb_id:
    #         s = student.objects.filter(id = k).first()
    #         if s:
    #             if not s.interest[-1]==i.id:
    #                 i.intrest_memb_id.remove(s.id)
    #                 i.save()
    #                 z= z+str(i.id)+" wala intrest "+str(s.id)+" wali id hogyi \n @@@"
    


    # i = student.objects.filter(id=396).first()    
    # a = student.objects.all()
    # for s in a:
    #     if s.groups:
    #         s.groups=[]
    #         s.save()
    #         z="very good"
        
    # count=1
    # for s in a:
    #     if not s.status:
    #         u=User.objects.filter(id=s.user_id).first()
    #         s.status = u.password
    #         s.save()
    #         z=z+", "+str(count)+"Done"
    #         count=count+1

        # b=[]
        # for k in i.cg_memb_id:
        #     if not k in b:
        #         b.append(k)
        # i.cg_memb_id=b
        # i.save()
    # z=i.status
    return HttpResponse(r)

def mail_confirmation(request):
    q = request.GET.get('q','')
    if "@." in q: 
        r = q.split("@.")
        print q,r
        i = student.objects.filter(id=int(r[0])).first()
        Us = User.objects.filter(id=i.user_id).first()
        if Us.last_name==r[1]:
            Us.is_active=True
            Us.last_name=""
            Us.save()
            state="Hallo "+i.first_name+", Now Your account is active!!!"
            messages.success(request,state)
            return HttpResponseRedirect("/")
        else:
            return HttpResponse("Your account is not activated. Something wrong occured. Contact Zeeley admin.")

    else:
        return HttpResponse("Your account is not activated. Something wrong occured. Contact Zeeley admin.")




def haversine(request,lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371000 # Radius of earth in kilometers. Use 3956 for miles
    return c * r


def localite(request,interest,distance):
    
    i = student.objects.filter(user=request.user).first()
    
    s = student.objects.filter(current_intrest=interest).exclude(id=i.id).order_by("-logout")

    b=[]
    c=[]
    if i.latitude!=90:
        for j in s:
            if j.longitude:
                
                dist = haversine(request,j.longitude , j.latitude , i.longitude , i.latitude)
                
                if dist < distance:
                    b.append(j)
                    c.append(dist)
    zipped = zip(b,c)
    zipped = sorted(zipped, key = lambda t:t[1])
    return zipped
    
def localite_date(request,interest,distance):
    
    i = student.objects.filter(user=request.user).first()
    if i.male:
        s = student.objects.filter(current_intrest=interest).exclude(male=True).order_by("-logout")
    else:
        s = student.objects.filter(current_intrest=interest).exclude(male=Falsee).order_by("-logout")
    b=[]
    c=[]
    if i.latitude!=90:
        for j in s:
            if j.longitude:
                
                dist = haversine(request,j.longitude , j.latitude , i.longitude , i.latitude)
                
                if dist < distance:
                    b.append(j)
                    c.append(dist)
    zipped = zip(b,c)
    zipped = sorted(zipped, key = lambda t:t[1])
    return zipped

def now_online():
    a = all_memb(request)
    b=[]
    for k in a:
        if k.logout:
            last_visit_time = k.logout.tzinfo
            time=(timezone.now()-k.logout).seconds
            if time<60:
                b.append(k.id)

    return b


def Positionupdate(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="POST":
        lati = request.POST["lat"]
        longi =request.POST["long"]
        i.latitude = lati
        i.longitude = longi
        i.save()
        z="Updated"

        ctx={"z":z}    
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        return HttpResponse("Nothing to post!!!")


def change_date_formatt(q_to,time):
    date = str(q_to).split("/")
    
    t1=date[2]+"-"+date[0]+"-"+date[1]
    q_to = t1+" "+time

    return (q_to)


def Cab_sharing(request):
    
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        trips =[]
        membs = i.f_list
        all_trips = trip.objects.filter(trip_mode="Cab").order_by("trip_from")
        near_trips = []
        for tn in all_trips:
            
            if datetime.now(pytz.utc) > tn.trip_from:
                tn.delete()
            else:
                new = []
                new = [x for x in tn.trip_memb if x not in new]
                tn.trip_memb = new
                tn.save()
                membs = membs+tn.trip_memb
                if i.id not in tn.trip_memb:
                    # distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
                    # if distance <= 15000:
                    near_trips.append(tn)


        membs = list(set(membs))
        your_trips = trip.objects.filter(trip_mode="Cab",trip_memb__contains=i.id).order_by("trip_from")
        count = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        m = intrests.objects.filter(intrest_name__iexact="Cab_Sharing").first()
        dist = 15000
        zipped = localite(request,m.intrest_name,dist)



        if request.method=="GET":
            if request.GET["place_from"]:
            
                place_from = request.GET["place_from"]
                place_to = request.GET["place_to"]
                from_lat = request.GET["from_lat"]
                from_long = request.GET["from_long"]
                to_lat = request.GET["to_lat"]
                to_long = request.GET["to_long"]
                max_memb = request.GET["max_memb"]
                remark = request.GET["remark"]
                date = request.GET["date"]
                time = request.GET["time"]
                d = date.split("/")
                date_from = d[2]+"-"+d[0]+"-"+d[1]+" "+time+":00"
                # date_from = change_date_formt2(request.GET["date"])

                date_to = change_date_formt2(request.GET["date"])

                # coupon =request.GET["coupon"]

                shared = trip.objects.filter(trip_city_from=place_from,trip_city=place_to,trip_memb__contains =i.id,trip_from=date_from)
                for shr in shared:
                    if len(shr.trip_memb) ==1:
                        shr.delete()
                trip_name = i.first_name + "'s Cab_Sharing Troop"
                tour = trip(trip_admin=i.id,trip_memb=[i.id],trip_name =trip_name,trip_mode="Cab", trip_city_from=place_from,trip_city=place_to,from_latitude=from_lat,from_longitude=from_long,to_latitude=to_lat,to_longitude=to_long,trip_n_membs=max_memb, trip_from=date_from,trip_to=date_to,remark=remark)
                tour.save()
                
                # cabs = cab_share.objects.filter(place_from=place_from,place_to=pace_to,depart_time=date+" "+time)
                
                tour.save()

                z=""
                # list1 =[]
                # more1 = trip.objects.filter(trip_mode="Cab",trip_city_from=tour.trip_city_from,trip_city=tour.trip_city).exclude(trip_memb__contains=i.id)
                # if more1:
                #     for  k in more1:
                #         list1 =list1+ k.trip_memb

                #     z = '_'.join(str(e) for e in list(set(list1)))

                ctx={"z":z}
            

                return HttpResponse(json.dumps(ctx), content_type='application/json')

        if your_trips:
            trips = trip.objects.filter(trip_mode="Cab",trip_city_from=your_trips.last().trip_city_from,trip_city=your_trips.last().trip_city).exclude(trip_memb__contains=i.id).order_by("trip_from")
        
        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)

        for labe in membs:
            if labe not in java:
                java.append(labe)


        a = student.objects.filter(id__in=java)

        allpost = posts.objects.filter(id__in=m.intrest_posts).order_by("-id").exclude(anonymous=True)
        f=allpost[0:9]
        other = allpost[9:]


        myteam = team.objects.filter(team_memb__contains=i.id)
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "m" : m,
            "your_trips" :your_trips,
            "myteam":myteam,
            "f" : f,
            "other" : other,
            "zip" : zipped,
            # "zip_team" :zipped_team,
            "near_trips" : near_trips,
            "trips" : trips,
            # "l" : l,
            
        }
   
        return render(request,"Cab_Sharing.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




    # if request.user.is_authenticated():
    #     i = student.objects.filter(user=request.user).first()
    #     cabs =[]
    #     membs = i.f_list
    #     near_trips = []
    #     all_cabs = cab_share.objects.all()

    #     for tn in all_cabs:
    #         if timezone.now() > tn.depart_time:
    #             tn.delete()
    #         else:
    
                
    #             if i.id not in tn.cab_memb:
    #                 distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
    #                 if distance <= 15000:
    #                     near_trips.append(tn)

    #     membs = list(set(membs))
    #     your_cabs = cab_share.objects.filter(cab_memb__contains=i.id)
    #     count = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
    #     counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
    #     title = i.first_name
    #     a = student.objects.filter(id__in=membs)
    #     p=i.n_frqst
    #     q=i.n_msg
    #     r=i.n_notif
    #     m = intrests.objects.filter(intrest_name__iexact="Cab_sharing").first()
    #     dist = 500
    #     zipped = localite(request,m.intrest_name,dist)
    #     if request.method=="GET":
    #         if request.GET["place_from"]:
    #             place_from = request.GET["place_from"]
    #             place_to = request.GET["place_to"]
    #             from_lat = request.GET["from_lat"]
    #             from_long = request.GET["from_long"]
    #             to_lat = request.GET["to_lat"]
    #             to_long = request.GET["to_long"]
    #             max_memb = request.GET["max_memb"]
    #             date = request.GET["date"]
    #             time = request.GET["time"]
    #             d_time = change_date_formatt(date,time)
    #             # coupon =request.GET["coupon"]

    #             shared = cab_share.objects.filter(place_from=place_from,place_to=place_to,cab_memb__contains =i.id)
    #             for shr in shared:
    #                 if len(shr.cab_memb) ==1:
    #                     shr.delete()

    #             c_share = cab_share(place_from=place_from,place_to=place_to,from_latitude=from_lat,from_longitude=from_long,to_latitude=to_lat,to_longitude=to_long,cab_limit=max_memb, depart_time=d_time,cab_coupon=False)
    #             c_share.save()
                
    #             # cabs = cab_share.objects.filter(place_from=place_from,place_to=pace_to,depart_time=date+" "+time)
    #             c_share.cab_memb.append(i.id)
    #             c_share.save()
                
    #             z="Done"

    #             ctx={"z":z}
            

    #             return HttpResponse(json.dumps(ctx), content_type='application/json')

    #     if your_cabs:
    #         cabs = cab_share.objects.filter(place_from=your_cabs.last().place_from,place_to=your_cabs.last().place_to).exclude(cab_memb__contains=i.id)
    #     context = {
    #         "title" : title,
    #         "counter" : counter,
    #         "count" : i.id,
    #         "i" : i,
    #         "a" : a,
    #         "e" : i.f_list,
    #         "p" : p,
    #         "q" : q,
    #         "r" : r,
    #         "m" : m,
    #         "your_cabs" :your_cabs,
    #         # "f" : f,
    #         # "other" : other,
    #         "zip" : zipped,
    #         # "zip_team" :zipped_team,
    #         "near_trips":near_trips,
    #         "cabs" : cabs,
    #         # "l" : l,
            
    #     }
   
    #     return render(request,"Cab_Sharing.html",context)
    # else:
    #     state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


# def Cab_sharing(request):
#     if request.user.is_authenticated():
#         i = student.objects.filter(user=request.user).first()
#         cabs =[]
#         all_cabs = cab_share.objects.all()

#         for tn in all_cabs:
#             if timezone.now() > tn.depart_time:
#                 tn.delete()

#         your_cabs = cab_share.objects.filter(cab_memb__contains=i.id)
#         count = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
#         counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
#         title = i.first_name
#         a = all_memb(request)
#         p=i.n_frqst
#         q=i.n_msg
#         r=i.n_notif
#         m = intrests.objects.filter(intrest_name__iexact="Cab_sharing").first()
#         dist = 500
#         zipped = localite(request,m.intrest_name,dist)
#         if request.method=="POST":
            
#             place_from = request.POST["place_from"]
#             place_to = request.POST["place_to"]
#             from_lat = request.POST["from_lat"]
#             from_long = request.POST["from_long"]
#             to_lat = request.POST["to_lat"]
#             to_long = request.POST["to_long"]
#             max_memb = request.POST["max_memb"]
#             date = request.POST["date"]
#             time = request.POST["time"]
#             d_time = change_date_formatt(date,time)
#             # coupon =request.POST["coupon"]

#             shared = cab_share.objects.filter(place_from=place_from,place_to=place_to,cab_memb__contains =i.id)
#             for shr in shared:
#                 if len(shr.cab_memb) ==1:
#                     shr.delete()

#             c_share = cab_share(place_from=place_from,place_to=place_to,from_latitude=from_lat,from_longitude=from_long,to_latitude=to_lat,to_longitude=to_long,cab_limit=max_memb, depart_time=d_time,cab_coupon=False)
#             c_share.save()
            
#             # cabs = cab_share.objects.filter(place_from=place_from,place_to=pace_to,depart_time=date+" "+time)
#             c_share.cab_memb.append(i.id)
#             c_share.save()
#             cabs = cab_share.objects.filter(place_from=place_from,place_to=place_to).exclude(cab_memb__contains=i.id)


#         context = {
#             "title" : title,
#             "counter" : counter,
#             "count" : i.id,
#             "i" : i,
#             "a" : a,
#             "e" : i.f_list,
#             "p" : p,
#             "q" : q,
#             "r" : r,
#             "m" : m,
#             "your_cabs" :your_cabs,
#             # "f" : f,
#             # "other" : other,
#             "zip" : zipped,
#             # "zip_team" :zipped_team,
#             "cabs" : cabs,
#             # "l" : l,
            
#         }
   
#         return render(request,"Cab_Sharing.html",context)
#     else:
#         state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def all_memb(request):
    i = student.objects.filter(user = request.user).first()
    java=i.f_list
    mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
        
    for k in mess:
        if k["m_friend"] not in java:
            java.append(k["m_friend"])
    # group = create_group.objects.filter(id__in=i.groups)
    # m=i.f_list
    # m.append(i.id)
    java.append(i.id)
    # for g in group:
    #     m= m+g.cg_memb_id
    m=list(set(java))
    a = student.objects.filter(id__in = m)

    return a

def android_position(request):


    position_long = request.GET["longitude"]

    position_lat = request.GET["latitude"]

    interest = request.GET["interest"]
    print position_lat 
    b=[]
    
    i = student.objects.filter(user=request.user).first()

    if interest == "all":
        s = student.objects.all().exclude(id=i.id)

        
        for j in s:
            if j.longitude:
                
                dist = haversine(request,j.longitude , j.latitude , float(position_long)  , float(position_lat))
                
                if dist < 15000:
                    b.append(j)
    else:
        s = student.objects.filter(current_intrest=interest).exclude(id=i.id)
        find = intrests.objects.filter(intrest_name=interest).first()
        change_interest(request,i,find)

        
        for j in s:
            if j.longitude:
                
                dist = haversine(request,j.longitude , j.latitude , float(position_long)  , float(position_lat))
                
                if dist < 15000:
                    j.priv_string = priv_string(request,i,j)

                    b.append(j)
        
        
                

    data = serializers.serialize('json', b, fields=('id','first_name','last_name','current_intrest','latitude','longitude',"priv_string"))

    
    
            

    return HttpResponse(data, content_type='application/json')


def android_view_interest(request):


    position_long = request.GET["longitude"]

    position_lat = request.GET["latitude"]

    interest = request.GET["interest"]
    print position_lat 
    b=[]
    
    i = student.objects.filter(user=request.user).first()

    if interest == "all":
        s = student.objects.all().exclude(id=i.id)

        
        for j in s:
            if j.longitude:
                
                dist = haversine(request,j.longitude , j.latitude , float(position_long)  , float(position_lat))
                
                if dist < 15000:
                    b.append(j)
    else:
        s = student.objects.filter(current_intrest=interest).exclude(id=i.id)
        find = intrests.objects.filter(intrest_name=interest).first()
        # change_interest(request,i,find)

        
        for j in s:
            if j.longitude:
                
                dist = haversine(request,j.longitude , j.latitude , float(position_long)  , float(position_lat))
                
                if dist < 15000:
                    j.priv_string = priv_string(request,i,j)

                    b.append(j)
        
        
                

    data = serializers.serialize('json', b, fields=('id','first_name','last_name','current_intrest','latitude','longitude',"priv_string"))

    
    
            

    return HttpResponse(data, content_type='application/json')

def android_login(request):

    z="Not done yet"

    Csrf_token = unicode(csrf(request)['csrf_token'])
    
    

        
    password=request.GET['password']
    
    email=request.GET['email']
    print email
    if email and password:
        

        USER = authenticate(username=email, password=password)
        if USER:
            login(request, USER)
            i = student.objects.filter(user=request.user).first()
            z = i.first_name + " " + i.last_name
            i.logout=timezone.now()
            i.save()
            j=[]
            m = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
            for k in m:
                
                j.append(k["m_friend"])
            j.append(i.id)
        
            a = student.objects.filter(id__in=j).order_by("-logout")
            b=[]
            for j in a:
                
                j.priv_string = priv_string(request,i,j)

                b.append(j)

            
            
            data = serializers.serialize('json', b, fields=('id',"logout",'first_name','last_name','current_intrest','priv_string'))
            
            return HttpResponse(data, content_type='application/json')
        else:
            return  HttpResponse("User Does not exist!!!")
    # position = json.loads(request.body)
    # position_lat = latitude

    # position_long = request.POST["longitude"]

    # position_lat = request.POST["latitude"]
    # print position_lat 
    # if position_lat and position_long:
    #     z= "I got lat-"+str(position_lat)+" and long-"+str(position_long)
    
    ctx={"z":z}
            

    return  HttpResponse(json.dumps(ctx), content_type='application/json')
    
@csrf_exempt
def android_login2(request):

    z="Not done yet"

    # Csrf_token = unicode(csrf(request)['csrf_token'])
    
    
    if request.method=="POST":
        z = "Done"
        print "A agya"
        email=request.POST.get('email')
        print email
        
        password=request.POST['password']
        
        
        if email:
            z="Done"
            

            # USER = authenticate(username=email, password=password)
            # if USER:
            #     login(request, USER)
            #     i = student.objects.filter(user=request.user).first()
            #     z = i.first_name + " " + i.last_name
            #     j = i.f_list
            #     # j.append(i.id)
            #     a = student.objects.filter(id__in=j)
                
            #     data = serializers.serialize('json', a, fields=('id','first_name','last_name','current_intrest','latitude','longitude'))
                
            #     return HttpResponse(data, content_type='application/json')
            # else:
            #     return  HttpResponse("User Does not exist!!!")
    # position = json.loads(request.body)
    # position_lat = latitude

    # position_long = request.POST["longitude"]

    # position_lat = request.POST["latitude"]
    # print position_lat 
    # if position_lat and position_long:
    #     z= "I got lat-"+str(position_lat)+" and long-"+str(position_long)
    
    ctx={"z":z}

    return HttpResponse(json.dumps(ctx), content_type='application/json')
    
    


# def Cab_request(request):

#     i = student.objects.filter(user=request.user).first()
#     z=""
#     if request.method=="GET":
#         if request.GET["trip_id"]:
#             trip_id = request.GET["trip_id"]
#             t_share = trip.objects.filter(id=trip_id).first()
#             my_trip = trip.objects.filter(trip_mode="Cab",trip_city_from=t_share.trip_city_from,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
#             myteam = team.objects.filter(team_memb__contains=i.id,team_category="Cab_Sharing")
#             p=student.objects.filter(id=t_share.trip_memb[0]).first()
#             t_share.trip_memb.append(i.id)
#             t_share.save()
            

#             t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="Cab_Sharing")
#             if t:
#                 m=t.first()
                
#             else:
#                 t_n=p.first_name+"'s' Cab Sharing "+"Troop"
#                 m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Cab_Sharing",team_group=t_share.id)
#                 m.team_memb.append(t_share.trip_memb[0])
#                 m.save()
#             m.team_memb.append(i.id)
#             m.save()

#             if i.id != p.id:
#                 p.n_notif=p.n_notif+1
#                 p.save()
#                 notif_msg = str(i.first_name.split(" ")[0]) + " has joined your Cab Sharing trip. Please contact him soon."
#                 new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Cab_Sharing",purpose_id=t_share.id)
#                 new_notif.save()

#                 if user_device.objects.filter(dev_id=p.id):
#                     ud = user_device.objects.filter(dev_id=p.id).first() 
#                     url='https://fcm.googleapis.com/fcm/send'
#                     data ={ "notification": {"title": "Zeeley Cab Sharing", "text": notif_msg  },  "to" : str(ud.reg_key),
#                         "data":{"team_id":t_share.id}}
#                     header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
#                     r = requests.post(url, data=json.dumps(data), headers=header)
#                     state = "Done"
#             if my_trip:
#                 my_trip.trip_memb.remove(i.id)
#                 my_trip.save()
#                 if not my_trip.trip_memb:
#                     my_trip.delete()
#             if myteam:
#                 myteam.first().team_memb.remove(i.id)
#                 myteam.first().save()
#                 if not myteam.first().team_memb:
#                     myteam.first().delete()
            
            

#             z= "sent"


#     ctx={"z":z}
            

#     return HttpResponse(json.dumps(ctx), content_type='application/json')


    # i = student.objects.filter(user=request.user).first()
    # z=""
    # if request.method=="POST":
    #     cab_id = request.POST["cab_id"]
    #     c_share = cab_share.objects.filter(id=cab_id).first()
    #     my_cab = cab_share.objects.filter(place_from=c_share.place_from,cab_memb__contains=i.id,place_to=c_share.place_to,depart_time=c_share.depart_time).first()
    #     my_cab.delete()
    #     p=student.objects.filter(id=c_share.cab_memb[0]).first()
    #     c_share.cab_memb.append(i.id)
    #     c_share.save()
    #     p.n_notif=p.n_notif+1


        
    #     p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> joined your cab. Please contact him soon.</td></tr><br>"+p.fnotify
    #     p.save()
    #     z= "sent"


    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')




def Cab_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            zeeley = student.objects.filter(id=320).first()
            t_share.trip_invites.append(i.id)
            t_share.save()
            

            

            if i.id != p.id:
                p.n_notif=p.n_notif+1
                p.save()
                zeeley.n_notif = zeeley.n_notif+1
                zeeley.save()
                notif_msg = str(i.first_name.split(" ")[0]) + " has requested to join your Cab trip. Please contact him soon."
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Cab_Sharing",purpose_id=t_share.id)
                new_notif.save()
                z_notif_msg = str(i.first_name.split(" ")[0]) + " has requested to join "+p.first_name+"'s Cab trip. Please contact him soon."
                z_new_notif = notification(notif_from =i.id,notif_to=zeeley.id,notif=z_notif_msg,purpose="Cab_Sharing",purpose_id=t_share.id)
                z_new_notif.save()


                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Request", "text": notif_msg  },  "to" : str(ud.reg_key),
                        "data":{"team_id":t_share.id,"team_name":t_share.trip_name,"new_memb":i.id,"url":"cab"}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"
            

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Cab_accept(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            memb_id = request.GET["memb_id"]
            p=student.objects.filter(id=memb_id).first()
            t_share = trip.objects.filter(id=trip_id).first()
            if t_share:
                if t_share.trip_memb[0] == i.id or i.id==320:
                    my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=p.id,trip_city=t_share.trip_city).first()
                    myteam = team.objects.filter(team_memb__contains=p.id,team_category="Cab_Sharing")
                    t_share.trip_invites.remove(p.id)
                    t_share.trip_memb.append(p.id)
                    t_share.save()
                    capt = student.objects.filter(id=t_share.trip_memb[0]).first()
                    notifying = notification.objects.filter(notif_from=p.id,notif_to=capt.id,purpose='Cab_Sharing',purpose_id=t_share.id)
                    for nify in notifying:
                        nify.delete()

                    t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="Cab_Sharing",team_group=t_share.id)
                    if t:
                        m=t.first()
                        
                    else:
                        t_n=i.first_name+"'s' Cab "+"Troop"
                        m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Cab_Sharing",team_group=t_share.id)
                        m.team_memb.append(t_share.trip_memb[0])
                        m.save()
                    m.team_memb.append(p.id)
                    m.save()

                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " has accepted your request of Cab sharing with "+str()+". Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Cab_Sharing",purpose_id=t_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Accept", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"trip_id":t_share.id,"team_name":m.team_name,"team_id":m.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"
                    if my_trip:
                        if p.id in my_trip.trip_memb:
                            my_trip.trip_memb.remove(p.id)
                            my_trip.save()
                        if not my_trip.trip_memb:
                            my_trip.delete()
                    if myteam:
                        if p.id in myteam.first().team_memb:
                            myteam.first().team_memb.remove(p.id)
                            myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()
                    
                    

                    z= "sent"

                else:
                    z="You don't have permission to accept the request."


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')






def Cab_edit(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method == "GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            c_share = trip.objects.filter(id=int(trip_id)).first()
            if c_share.trip_memb[0]==i.id:

                if request.GET["remark"]:
                    remark = request.GET["remark"]
                    c_share.remark = remark
                    c_share.save()

                if request.GET["memb_remove"]:
                    memb_del = request.GET["memb_remove"]
                    myteam = team.objects.filter(team_group=c_share.id)
                    if myteam:
                        myteam.first().team_memb.remove(int(memb_del))
                        myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()

                    p = student.objects.filter(id=int(memb_del)).first()
                    c_share.trip_memb.remove(p.id)
                    
                    c_share.save()
                    
                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " removed you from his Cab Sharing. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Cab_Sharing",purpose_id=c_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Cab Sharing", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"team_id":c_share.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"

                    z="removed"
                    if not c_share.trip_memb:
                        c_share.delete()
                        z="trip_deleted"
                    elif memb_del==i.id:
                        c_share.trip_admin = c_share.trip_memb[0]
                        c_share.save()

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


    # i=student.objects.filter(user=request.user).first()
    # z=""
    # if request.method == "POST":
    #     cab_id = request.POST["cab_id"]
    #     c_share = cab_share.objects.filter(id=cab_id).first()
    #     if c_share.cab_memb[0]==i.id:
    #         memb_del = request.POST["memb_remove"]
    #         j=student.objects.filter(id=int(memb_del)).first()
    #         c_share.cab_memb.remove(j.id)
    #         c_share.save()
            
    #         if not c_share.cab_memb:
    #             c_share.delete()
    #             z="cab deleted"
    #         else:
    #             z="Removed"

    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')




def Invite_purposely(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['purpose']
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1


                
                p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/"+str(i.id)+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> invited you for "+str(l)+".</td></tr><br>"+p.fnotify
                p.save()


        return HttpResponseRedirect("/home/")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def View_shared(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        purpose = request.GET.get('purpose','')
        if purpose=="Cab_Sharing":
            c_share = cab_share.objects.filter(id=int(team_id)).first()



        
            i=student.objects.filter(user=request.user).first()
            
            
            


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name
            
            a = student.objects.filter(id__in=c_share.cab_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                            
                "i" : i,
                
                "a" : a,
                
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"view_shared.html",context)
        else:
            return HttpResponse("No such purpose found!!!")


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def apply(request):
    p = posts.objects.all()
    for k in p:
        s = student.objects.filter(id=k.identity).first()
        if not s:
            k.delete()
        else:
            k.creator=s
            k.save()

    return HttpResponse("Done")



def Cotravelling(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        trips =[]
        membs = i.f_list
        all_trips = trip.objects.all().exclude(trip_city_from="Cafe").exclude(trip_city_from="Film").exclude(trip_city_from="Quicky").exclude(trip_city_from="Gaming").exclude(trip_mode="Cab").order_by("trip_from")
        near_trips = []
        for tn in all_trips:
            
            if datetime.now(pytz.utc) > tn.trip_from:
                tn.delete()
            else:
                new = []
                new = [x for x in tn.trip_memb if x not in new]
                tn.trip_memb = new
                tn.save()
                membs = membs+tn.trip_memb
                if i.id not in tn.trip_memb:
                    distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
                    if distance <= 15000:
                        near_trips.append(tn)


        membs = list(set(membs))
        your_trips = trip.objects.filter(trip_memb__contains=i.id).exclude(trip_city_from="Cafe").exclude(trip_city_from="Film").exclude(trip_city_from="Quicky").exclude(trip_city_from="Gaming").exclude(trip_mode="Cab").order_by("trip_from")
        count = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        m = intrests.objects.filter(intrest_name__iexact="Travelling").first()
        dist = 15000
        zipped = localite(request,m.intrest_name,dist)



        if request.method=="GET":
            if request.GET["place_from"]:
            
                place_from = request.GET["place_from"]
                place_to = request.GET["place_to"]
                from_lat = request.GET["from_lat"]
                from_long = request.GET["from_long"]
                to_lat = request.GET["to_lat"]
                to_long = request.GET["to_long"]
                max_memb = 5
                remark = request.GET["remark"]
                date_from = change_date_formt2(request.GET["date"])

                date_to = date_from

                # coupon =request.GET["coupon"]

                shared = trip.objects.filter(trip_city_from=place_from,trip_city=place_to,trip_memb__contains =i.id,trip_from=date_from).exclude(trip_mode="Cab")
                for shr in shared:
                    if len(shr.trip_memb) ==1:
                        shr.delete()
                trip_name = i.first_name + "'s Travelling Troop"
                tour = trip(trip_admin=i.id,trip_memb=[i.id],trip_name =trip_name, trip_city_from=place_from,trip_city=place_to,from_latitude=from_lat,from_longitude=from_long,to_latitude=to_lat,to_longitude=to_long,trip_n_membs=max_memb, trip_from=date_from,trip_to=date_to,remark=remark)
                tour.save()
                
                
                
                
                z=""
                # list1 =[]
                # more1 = trip.objects.filter(trip_city_from=tour.trip_city_from,trip_city=tour.trip_city).exclude(trip_memb__contains=i.id).exclude(trip_city_from="Cafe").exclude(trip_city_from="Film").exclude(trip_city_from="Quicky").exclude(trip_city_from="Gaming").exclude(trip_mode="Cab")
                # if more1:
                #     for  k in more1:
                #         list1 = list1 + k.trip_memb

                #     z = '_'.join(str(e) for e in list(set(list1)))

                ctx={"z":z}
            

                return HttpResponse(json.dumps(ctx), content_type='application/json')

        if your_trips:
            trips = trip.objects.filter(trip_city_from=your_trips.last().trip_city_from,trip_city=your_trips.last().trip_city).exclude(trip_memb__contains=i.id).exclude(trip_city_from="Cafe").exclude(trip_city_from="Film").exclude(trip_city_from="Quicky").exclude(trip_city_from="Gaming").exclude(trip_mode="Cab")
        
        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)

        for labe in membs:
            if labe not in java:
                java.append(labe)


        a = student.objects.filter(id__in=java)

        # thisteam = team.objects.filter(team_category=m.intrest_name)
        # ttmemb = []
        # for la in thisteam:
        #     ttmemb = ttmemb+la.team_memb
        # ttmemb = list(set(ttmemb))

        allpost = posts.objects.filter(id__in=m.intrest_posts).order_by("-id").exclude(anonymous=True)
        f=allpost[0:9]
        other = allpost[9:]

        myteam = team.objects.filter(team_memb__contains=i.id)
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "m" : m,
            "your_trips" :your_trips,
            "myteam":myteam,
            "f" : f,
            "other" : other,
            "zip" : zipped,
            # "zip_team" :zipped_team,
            "near_trips" : near_trips,
            "trips" : trips,
            # "l" : l,
            
        }
   
        return render(request,"Cotravelling.html",context)
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def Trip_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            # my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
            # myteam = team.objects.filter(team_memb__contains=i.id,team_category="Travelling")
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            t_share.trip_invites.append(i.id)
            t_share.save()
            

            # t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="Travelling")
            # if t:
            #     m=t.first()
                
            # else:
            #     t_n=p.first_name+"'s' Travelling "+"Troop"
            #     m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Travelling",team_group=t_share.id)
            #     m.team_memb.append(t_share.trip_memb[0])
            #     m.save()
            # m.team_memb.append(i.id)
            # m.save()

            if i.id != p.id:
                p.n_notif=p.n_notif+1
                p.save()
                notif_msg = str(i.first_name.split(" ")[0]) + " has requested to join your travelling trip. Please contact him soon."
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Travelling",purpose_id=t_share.id)
                new_notif.save()

                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Request", "text": notif_msg  },  "to" : str(ud.reg_key),
                        "data":{"team_id":t_share.id,"team_name":t_share.trip_name,"new_memb":i.id,"url":"trip"}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"
            # if my_trip:
            #     my_trip.trip_memb.remove(i.id)
            #     my_trip.save()
            #     if not my_trip.trip_memb:
            #         my_trip.delete()
            # if myteam:
            #     myteam.first().team_memb.remove(i.id)
            #     myteam.first().save()
            #     if not myteam.first().team_memb:
            #         myteam.first().delete()
            
            

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Trip_accept(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            memb_id = request.GET["memb_id"]
            p=student.objects.filter(id=memb_id).first()
            t_share = trip.objects.filter(id=trip_id).first()
            if t_share:
                if t_share.trip_memb[0] == i.id:
                    my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=p.id,trip_city=t_share.trip_city).first()
                    myteam = team.objects.filter(team_memb__contains=p.id,team_category="Travelling")
                    t_share.trip_invites.remove(p.id)
                    t_share.trip_memb.append(p.id)
                    t_share.save()
                    

                    t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="Travelling",team_group=t_share.id)
                    if t:
                        m=t.first()
                        
                    else:
                        t_n=i.first_name+"'s' Travelling "+"Troop"
                        m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Travelling",team_group=t_share.id)
                        m.team_memb.append(t_share.trip_memb[0])
                        m.save()
                    m.team_memb.append(p.id)
                    m.save()

                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " has accepted your travelling request. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Travelling",purpose_id=t_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Accept", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"trip_id":t_share.id,"team_name":m.team_name,"team_id":m.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"
                    if my_trip:
                        if p.id in my_trip.trip_memb:
                            my_trip.trip_memb.remove(p.id)
                            my_trip.save()
                        if not my_trip.trip_memb:
                            my_trip.delete()
                    if myteam:
                        if p.id in myteam.first().team_memb:
                            myteam.first().team_memb.remove(p.id)
                            myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()
                    
                    

                    z= "sent"

                else:
                    z="You don't have permission to accept the request."
            else:
                z="No such trip found."

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')

def trip_edit(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method == "GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            c_share = trip.objects.filter(id=int(trip_id)).first()
            if c_share.trip_memb[0]==i.id:

                if request.GET["remark"]:
                    remark = request.GET["remark"]
                    c_share.remark = remark
                    c_share.save()

                if request.GET["memb_remove"]:
                    memb_del = request.GET["memb_remove"]
                    myteam = team.objects.filter(team_group=c_share.id)
                    if myteam:
                        myteam.first().team_memb.remove(int(memb_del))
                        myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()

                    p = student.objects.filter(id=int(memb_del)).first()
                    c_share.trip_memb.remove(p.id)
                    
                    c_share.save()
                    
                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " removed you from his travelling trip. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Travelling",purpose_id=c_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Traveller", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"team_id":c_share.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"

                    z="removed"
                    if not c_share.trip_memb:
                        c_share.delete()
                        z="trip deleted"
                    elif memb_del==i.id:
                        c_share.trip_admin = c_share.trip_memb[0]
                        c_share.save()
                    

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')

def photo_resize(infile,w,h):
    outfile = infile
    if os.path.isfile(infile):
        try:
            im = Image.open(infile)

            im.thumbnail((w,h),Image.ANTIALIAS)
            im.save(outfile, "JPEG")
            print "kr daal"
        except IOError:
            print "cannot create thumbnail for", infile
    return ("DONE")

def photo_resizing(request):
    all_posts = posts.objects.all()
    for a in all_posts:
        if a.photos > 0:
            if a.profile_picture:
                infile = '/root/ttl/babaS/media_in_env/media_root/file_p'+str(a.identity)+'_'+str(a.photos)+'.jpg'
                photo_resize(infile,800,800)
            elif a.cover_picture:
                infile = '/root/ttl/babaS/media_in_env/media_root/file_c'+str(a.identity)+'_'+str(a.photos)+'.jpg'
                photo_resize(infile,800,800)

            else:
                infile = '/root/ttl/babaS/media_in_env/media_root/file_'+str(a.identity)+'_'+str(a.photos)+'.jpg'
                photo_resize(infile,800,800)

    return HttpResponse("Done")



def more_reg(request):
    if request.user.is_authenticated() :
        i = student.objects.filter(user=request.user).first()
        if request.method=="POST":
            age = request.POST['age']
            hometown = request.POST['hometown']
            organization = request.POST['organization']
            work_as = request.POST['works_as']
            currentplace = request.POST['currentplace']
            year = datetime.today().year - int(age)
            latitude = request.POST['lati']
            longitude = request.POST['longi']
            i.latitude=latitude
            i.longitude=longitude
            mob = int(request.POST['mob'])
            
            i.year=year
            i.hometown=hometown
            i.college = organization
            i.work_as = work_as
            i.current_place = currentplace
            i.save()
            groups = []
            groups.append(organization)

            groups.append(hometown)
            groups.append(currentplace)
            groups.append(work_as+", "+organization)
            for k in groups:
                j = create_group.objects.filter(cg_name=k).first()
                if not j:
                    m=create_group(cg_name=k,cg_lat=latitude,cg_long=longitude,cg_memb_id=[i.id])
                    m.save()
                    p=m.id
                    i.groups.append(p)
                    i.save()

                    enfile='/root/ttl/babaS/media_in_env/media_root/group.jpg'
                    

                    infile='/root/ttl/babaS/media_in_env/media_root/file_g' + str(i.id)+ '_'+ str(p)+".jpg"
                    if infile != enfile:
                        try:
                            im = Image.open(infile)
                            im.thumbnail((400,400),Image.ANTIALIAS)
                            im.save(infile, "JPEG")
                        except IOError:
                            print "cannot create thumbnail for", infile

                    outfile = os.path.splitext(infile)[0] + ".thumbnail"
                    if infile != outfile:
                        try:
                            im = Image.open(infile)
                            im.thumbnail((256,256),Image.ANTIALIAS)
                            im.save(outfile, "JPEG")
                        except IOError:
                            print "cannot create thumbnail for", infile
                else:
                    j.cg_memb_id.append(i.id)
                    j.save()
                    p=j.id
                    i.groups.append(p)
                    i.save()

                if mob == 1:
                    return HttpResponseRedirect('/location/?z="new"')
                else:
                    return HttpResponseRedirect('/interest/')

        return render(request,"morereg.html",{"first_name":i.first_name})
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def upsize(request):
    a = intrests.objects.all()
    for s in a:
        infile = '/home/mohan/lifer/babaS/static_pro/our_static/interests/'+s.intrest_name+'.jpg'
        photo_resize(infile,600,600)
    return HttpResponse("Done")


def Pizza_share(request):
    
    return render(request,"Pizzas.html",{})


def Quicky_share(request):

    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        c = i.id
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        membs = i.f_list
        near_trips =[]
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif

        m = intrests.objects.filter(intrest_name__iexact="Quicky").first()
        dist = 15000
        zipped = localite(request,m.intrest_name,dist)


        tri =trip.objects.filter(trip_city_from='Quicky').order_by("trip_from")
        for tn in tri:
            
            if datetime.now(pytz.utc) > tn.trip_from :
                tn.delete()
            else:
                new = []
                new = [x for x in tn.trip_memb if x not in new]
                tn.trip_memb = new
                tn.save()
                membs = membs+tn.trip_memb
                if i.id not in tn.trip_memb:
                    distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
                    
                    if distance <= 15000:
                        near_trips.append(tn)
        membs = list(set(membs))
        
        my_trip = tri.filter(trip_memb__contains=i.id)
        

        more = []

        if request.method=="GET":
            if request.GET["quicky"]:
                
                t_name = request.GET["quicky"]
                date = request.GET["date"]
                time = request.GET["time"]
                remark = request.GET["remark"]
                d = date.split("/")
                t_from = d[2]+"-"+d[0]+"-"+d[1]+" "+time+":00"
                # q_from=change_date_formt(q_from)
                # t_from = change_date_formt2(date)
                
                
                

                shared = trip.objects.filter(trip_name=t_name,trip_city_from="Quicky",trip_memb__contains =i.id,trip_from=t_from)
                for shr in shared:
                    if len(shr.trip_memb) ==1:
                        shr.delete()
                t=trip(trip_admin=i.id,trip_memb=[i.id],trip_name=t_name,trip_from=t_from,trip_city_from="Quicky",from_latitude=i.latitude,from_longitude=i.longitude,remark=remark)
                
                t.save()

                z=""
                # list1 =[]
                # more1 = tri.filter(trip_name=t.trip_name).exclude(trip_memb__contains=i.id)
                # if more1:
                #     for  k in more1:
                #         list1 =list1+ k.trip_memb

                #     z = '_'.join(str(e) for e in list(set(list1)))

                

                ctx={"z":z}
            

                return HttpResponse(json.dumps(ctx), content_type='application/json')
        if my_trip:
            more = tri.filter(trip_name=my_trip.last().trip_name).exclude(trip_memb__contains=i.id)


        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)

        for labe in membs:
            if labe not in java:
                java.append(labe)


        a = student.objects.filter(id__in=java)

        allpost = posts.objects.filter(id__in=m.intrest_posts).order_by("-id").exclude(anonymous=True)
        f=allpost[0:9]
        other = allpost[9:]

        myteam = team.objects.filter(team_memb__contains=i.id)
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "f" : f,
            "other" : other,
            "your_trip" :my_trip,
            "myteam":myteam,
            "trips":more,
            "near_trips":near_trips,
            "m" : m,
            "zip":zipped,
            
        }


        
        return render(request,"Quicky.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



    # if request.user.is_authenticated():

    #     i=student.objects.filter(user=request.user).first()
    #     c = i.id
    #     count = "p"+str(c)+"_"+str(i.prof_pic[-1])
    #     counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
    #     title = i.first_name
    #     a = all_memb(request)
    #     p=i.n_frqst
    #     q=i.n_msg
    #     r=i.n_notif


    #     tri =quick.objects.all()
    #     for tn in tri:
    #         if timezone.now().date() > tn.trip_from :
    #             tn.delete()

    #     my_quick = tri.filter(quick_memb__contains=i.id)
        

    #     more = tri.exclude(quick_memb__contains=i.id)

    #     if request.method=="POST":
    #         q_int = request.POST['q_name']
    #         date = request.POST["date"]
    #         time = request.POST["time"]
    #         q_from = change_date_formatt(date,time)
            
    #         q=quick(quick_name=q_int,quick_from=q_from)
    #         q.save()
    #         q.quick_memb.append(i.id)
    #         q.save()

            


    #     context = {
    #         "title" : title,
    #         "counter" : counter,
    #         "count" : i.id,
    #         "i" : i,
    #         "a" : a,
    #         "e" : i.f_list,
    #         "p" : p,
    #         "q" : q,
    #         "r" : r,
    #         "your_trip":my_quick,
    #         "trips":more,
            
    #     }


        
    #     return render(request,"Quicky.html",context)



def Quicky_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            t_share.trip_invites.append(i.id)
            t_share.save()
            

            

            if i.id != p.id:
                p.n_notif=p.n_notif+1
                p.save()
                notif_msg = str(i.first_name.split(" ")[0]) + " has requested to join your Quicky trip. Please contact him soon."
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Quicky",purpose_id=t_share.id)
                new_notif.save()

                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Request", "text": notif_msg  },  "to" : str(ud.reg_key),
                        "data":{"team_id":t_share.id,"team_name":t_share.trip_name,"new_memb":i.id,"url":"quicky"}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"
            

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Quicky_accept(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            memb_id = request.GET["memb_id"]
            p=student.objects.filter(id=memb_id).first()
            t_share = trip.objects.filter(id=trip_id).first()
            if t_share:
                if t_share.trip_memb[0] == i.id:
                    my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=p.id,trip_city=t_share.trip_city).first()
                    myteam = team.objects.filter(team_memb__contains=p.id,team_category="Quicky")
                    t_share.trip_invites.remove(p.id)
                    t_share.trip_memb.append(p.id)
                    t_share.save()
                    

                    t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="Quicky",team_group=t_share.id)
                    if t:
                        m=t.first()
                        
                    else:
                        t_n=i.first_name+"'s' Quicky "+"Troop"
                        m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Quicky",team_group=t_share.id)
                        m.team_memb.append(t_share.trip_memb[0])
                        m.save()
                    m.team_memb.append(p.id)
                    m.save()

                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " has accepted your Quicky request. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Quicky",purpose_id=t_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Accept", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"trip_id":t_share.id,"team_name":m.team_name,"team_id":m.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"
                    if my_trip:
                        if p.id in my_trip.trip_memb:
                            my_trip.trip_memb.remove(p.id)
                            my_trip.save()
                        if not my_trip.trip_memb:
                            my_trip.delete()
                    if myteam:
                        if p.id in myteam.first().team_memb:
                            myteam.first().team_memb.remove(p.id)
                            myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()
                    
                    

                    z= "sent"

                else:
                    z="You don't have permission to accept the request."


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')



    

# def Quicky_request(request):

#     i = student.objects.filter(user=request.user).first()
#     z=""
#     if request.method=="GET":
#         if request.GET["trip_id"]:
#             trip_id = request.GET["trip_id"]
#             t_share = trip.objects.filter(id=trip_id).first()
#             my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_name=t_share.trip_name,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
            
#             p=student.objects.filter(id=t_share.trip_memb[0]).first()
#             t_share.trip_memb.append(i.id)
#             t_share.save()
            

#             t = team.objects.filter(team_group=t_share.id,team_category="Quicky")
#             if t:
#                 m=t.first()
                
#             else:
#                 t_n=p.first_name+"'s' Quickhunt "+"Troop"
#                 m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Quicky",team_group=t_share.id)
#                 m.team_memb.append(t_share.trip_memb[0])
#                 m.save()
#             m.team_memb.append(i.id)
#             m.save()
#             if i.id != p.id:
#                 p.n_notif=p.n_notif+1
#                 p.save()
#                 notif_msg = str(i.first_name.split(" ")[0]) + " has joined your quickhunt of "+str(t_share.trip_name)+". Please contact him soon."
#                 new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Quicky",purpose_id=t_share.id)
#                 new_notif.save()

#                 if user_device.objects.filter(dev_id=p.id):
#                     ud = user_device.objects.filter(dev_id=p.id).first() 
#                     url='https://fcm.googleapis.com/fcm/send'
#                     data ={ "notification": {"title": "Zeeley Quickhunt", "text": notif_msg  },  "to" : str(ud.reg_key),
#                         "data":{"team_id":t_share.id}}
#                     header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
#                     r = requests.post(url, data=json.dumps(data), headers=header)
#                     state = "Done"
#             if my_trip:
#                 my_trip.trip_memb.remove(i.id)
#                 my_trip.save()
#                 if not my_trip.trip_memb:
#                     my_trip.delete()
#                 myteam = team.objects.filter(team_group=my_trip.id)
#                 if myteam:
#                     myteam.first().team_memb.remove(i.id)
#                     myteam.first().save()
#                     if not myteam.first().team_memb:
#                         myteam.first().delete()

#             z= "sent"


#     ctx={"z":z}
            

#     return HttpResponse(json.dumps(ctx), content_type='application/json')


    # i = student.objects.filter(user=request.user).first()
    # z=""
    # if request.method=="POST":
    #     quick_id = request.POST["quick_id"]
    #     t_share = quick.objects.filter(id=quick_id).first()
    #     my_quick = quick.objects.filter(quick_name=t_share.quick_name,quick_memb__contains=i.id,quick_from=t_share.quick_from).first()
    #     my_quick.delete()
    #     p=student.objects.filter(id=t_share.quick_memb[0]).first()
    #     t_share.quick_memb.append(i.id)
    #     t_share.save()
    #     p.n_notif=p.n_notif+1


        
    #     p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> joined your quick work of "+t_share.quick_name+". Please contact him soon.</td></tr><br>"+p.fnotify
    #     p.save()
    #     z= "sent"


    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')

def Quicky_edit(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method == "GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            c_share = trip.objects.filter(id=int(trip_id)).first()
            if c_share.trip_memb[0]==i.id:

                if request.GET["remark"]:
                    remark = request.GET["remark"]
                    c_share.remark = remark
                    c_share.save()

                if request.GET["memb_remove"]:
                    memb_del = request.GET["memb_remove"]
                    myteam = team.objects.filter(team_memb__contains=memb_del,team_category="Quicky")
                    if myteam:
                        myteam.first().team_memb.remove(int(memb_del))
                        myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()

                    p = student.objects.filter(id=int(memb_del)).first()
                    c_share.trip_memb.remove(p.id)
                    
                    c_share.save()
                    
                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " removed you from his quickhunt trip. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Quicky",purpose_id=c_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Quickhunt", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"team_id":c_share.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"

                    z="removed"
                    if not c_share.trip_memb:
                        c_share.delete()
                        z="trip deleted"
                    elif memb_del==i.id:
                        c_share.trip_admin = c_share.trip_memb[0]
                        c_share.save()
                    

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')

    # i=student.objects.filter(user=request.user).first()
    # z=""
    # if request.method == "POST":
    #     quick_id = request.POST["quick_id"]
    #     c_share = quick.objects.filter(id=quick_id).first()
    #     if c_share.quick_memb[0]==i.id:
    #         memb_del = request.POST["memb_remove"]
    #         p=student.objects.filter(id=int(memb_del)).first()
    #         c_share.quick_memb.remove(p.id)
    #         c_share.save()
            
            

    #         p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> removed you from his quick work of "+c_share.quick_name+". Please contact him soon.</td></tr><br>"+p.fnotify
    #         p.save()
    #         if not c_share.quick_memb:
    #             c_share.delete()
    #             z="quick deleted"
    #         else:
    #             c_share.pk=None
    #             c_share.save()
    #             c_share.quick_memb=[p.id]
    #             c_share.save()
    #             z="Removed"

    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')

# def Rest_share(request):
#     if request.user.is_authenticated():

#         i=student.objects.filter(user=request.user).first()
#         c = i.id
#         count = "p"+str(c)+"_"+str(i.prof_pic[-1])
#         counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
#         title = i.first_name
#         a = all_memb(request)
#         p=i.n_frqst
#         q=i.n_msg
#         r=i.n_notif


#         tri =trip.objects.filter(trip_city_from='Cafe')
#         for tn in tri:
#             if datetime.now(pytz.utc) > tn.trip_from :
#                 tn.delete()

#         my_trip = tri.filter(trip_memb__contains=i.id)
        

#         more = []

#         if request.method=="POST":
#             t_mode = request.POST["meal"]
#             t_name = request.POST["restaurant"]
#             date = request.POST["date"]
#             d=date.split("/")
#             # q_from=change_date_formt(q_from)
#             t_from = d[2]+'-'+d[0]+'-'+d[1]
            
#             t_memb = request.POST["food_type"]
#             t=trip(trip_name=t_name,trip_from=t_from,trip_mode=t_mode,trip_n_membs=t_memb,trip_city_from="Cafe")
#             t.trip_memb.append(i.id)
#             t.save()

#             more = tri.filter(trip_name=t_name,trip_mode=t_mode)


#         context = {
#             "title" : title,
#             "counter" : counter,
#             "count" : i.id,
#             "i" : i,
#             "a" : a,
#             "e" : i.f_list,
#             "p" : p,
#             "q" : q,
#             "r" : r,
#             "your_trip" :my_trip,"myteam":myteam,
#             "trips":more,
            
#         }


        
#         return render(request,"Restaurants.html",context)

#     else:
#         state = "Login First..."
        # messages.success(request,state)
        # return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def Rest_share(request):
    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        c = i.id
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        membs = i.f_list
        near_trips =[]
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif

        m = intrests.objects.filter(intrest_name__iexact="Restaurant").first()
        dist = 15000
        zipped = localite(request,m.intrest_name,dist)


        tri =trip.objects.filter(trip_city_from='Cafe').order_by("trip_from")
        for tn in tri:
            
            if datetime.now(pytz.utc) > tn.trip_from :
                tn.delete()
            else:
                new = []
                new = [x for x in tn.trip_memb if x not in new]
                tn.trip_memb = new
                
                tn.save()
                membs = membs+tn.trip_memb
                if i.id not in tn.trip_memb:
                    distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
                    if distance <= 15000:
                        near_trips.append(tn)
        membs = list(set(membs))
        
        my_trip = tri.filter(trip_memb__contains=i.id)
        

        more = []

        if request.method=="GET":
            if request.GET["restaurant"]:
                
                t_name = request.GET["restaurant"]
                date = request.GET["date"]
                remark = request.GET["remark"]
                d=date.split("/")
                # q_from=change_date_formt(q_from)
                t_from = change_date_formt2(date)
                
                f_type = request.GET["food_type"]

                shared = trip.objects.filter(trip_name=t_name,trip_city_from="Cafe",trip_memb__contains =i.id,trip_from=t_from)
                for shr in shared:
                    if len(shr.trip_memb) ==1:
                        shr.delete()
                t=trip(trip_admin=i.id,trip_memb=[i.id],trip_name=t_name,trip_from=t_from,trip_n_membs=int(f_type),trip_city_from="Cafe",from_latitude=i.latitude,from_longitude=i.longitude,remark=remark)
                
                t.save()
                z=""
                # list1 =[]
                # more1 = tri.filter(trip_name=t.trip_name).exclude(trip_memb__contains=i.id)
                # if more1:
                #     for  k in more1:
                #         list1 =list1+ k.trip_memb

                #     z = '_'.join(str(e) for e in list(set(list1)))
                

                ctx={"z":z}
            

                return HttpResponse(json.dumps(ctx), content_type='application/json')
        if my_trip:
            more = tri.filter(trip_name=my_trip.last().trip_name).exclude(trip_memb__contains=i.id)


        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)

        for labe in membs:
            if labe not in java:
                java.append(labe)


        a = student.objects.filter(id__in=java)

        allpost = posts.objects.filter(id__in=m.intrest_posts).order_by("-id").exclude(anonymous=True)
        f=allpost[0:9]
        other = allpost[9:]



        myteam = team.objects.filter(team_memb__contains=i.id)
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "f" : f,
            "other" : other,
            "your_trip" :my_trip,
            "myteam":myteam,
            "trips":more,
            "near_trips":near_trips,
            "m" : m,
            "zip":zipped,
            
        }


        
        return render(request,"Restaurants.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

    
    # if request.user.is_authenticated():

    #     i=student.objects.filter(user=request.user).first()
    #     c = i.id
    #     count = "p"+str(c)+"_"+str(i.prof_pic[-1])
    #     counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
    #     title = i.first_name
    #     membs = i.f_list
    #     near_trips =[]
    #     p=i.n_frqst
    #     q=i.n_msg
    #     r=i.n_notif
    #     m = intrests.objects.filter(intrest_name__iexact="Restaurant").first()
    #     dist = 500
    #     zipped = localite(request,m.intrest_name,dist)


    #     tri =trip.objects.filter(trip_city_from='Cafe')
    #     for tn in tri:
    
                
    #         if i.id not in tn.trip_memb:
    #             distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
    #             if distance <= 15000:
    #                 near_trips.append(tn)

    #         if datetime.now(pytz.utc) > tn.trip_from :
    #             tn.delete()

    #     membs = list(set(membs))
    #     a = student.objects.filter(id__in=membs)
    #     my_trip = tri.filter(trip_memb__contains=i.id)
        

    #     more = []

    #     if request.method=="GET":
    #         if request.GET["restaurant"]:
                
    #             t_name = request.GET["restaurant"]
    #             date = request.GET["date"]
    #             remark = request.GET["remark"]
    #             d=date.split("/")
    #             # q_from=change_date_formt(q_from)
    #             t_from = change_date_formt2(date)
                
    #             f_type = request.GET["food_type"]
    #             t=trip(trip_name=t_name,trip_from=t_from,trip_n_membs=int(f_type),trip_city_from="Cafe",from_latitude=i.latitude,from_longitude=i.longitude,remark=remark)
    #             t.trip_memb.append(i.id)
    #             t.save()

                
    #             z="Done"

    #             ctx={"z":z}
            

    #             return HttpResponse(json.dumps(ctx), content_type='application/json')
    #     if my_trip:
    #         more = tri.filter(trip_name=my_trip.last().trip_name)

    #     context = {
    #         "title" : title,
    #         "counter" : counter,
    #         "count" : i.id,
    #         "i" : i,
    #         "a" : a,
    #         "e" : i.f_list,
    #         "p" : p,
    #         "q" : q,
    #         "r" : r,
    #         "your_trip" :my_trip,"myteam":myteam,
    #         "trips":more,
    #         "near_trips":near_trips,
    #         "m" : m,
    #         "zipped":zipped,
            
    #     }


        
    #     return render(request,"Restaurants.html",context)

    # else:
    #     state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


# def Rest_request(request):
#     i = student.objects.filter(user=request.user).first()
#     z=""
#     if request.method=="GET":
#         if request.GET["trip_id"]:
#             trip_id = request.GET["trip_id"]
#             t_share = trip.objects.filter(id=trip_id).first()
#             my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_name=t_share.trip_name,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
#             my_trip.delete()
#             p=student.objects.filter(id=t_share.trip_memb[0]).first()
#             t_share.trip_memb.append(i.id)
#             t_share.save()
#             p.n_notif=p.n_notif+1


            
#             p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> joined your trip to the restaurant "+t_share.trip_name+". Please contact him soon.</td></tr><br>"+p.fnotify
#             p.save()

#             t = team.objects.filter(team_group=t_share.id,team_category="Travelling")
#             if t:
#                 m=t.first()
                
#             else:
#                 t_n=i.first_name+"'s' Restaurant "+"Troop"
#                 m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Restaurant",team_group=t_share.id)
#                 m.team_memb.append(t_share.trip_memb[0])
#                 m.save()
#             m.team_memb.append(i.id)
#             m.save()

#             notif_msg = str(i.first_name) + " has joined your trip to the restaurant "+str(t_share.trip_name)+". Please contact him soon."
#             new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose=i.current_intrest,purpose_id=t_share.id)
#             new_notif.save()

#             if user_device.objects.filter(dev_id=p.id):
#                 ud = user_device.objects.filter(dev_id=p.id).first() 
#                 url='https://fcm.googleapis.com/fcm/send'
#                 data ={ "notification": {"title": "Zeeley Restaurants", "text": notif_msg  },  "to" : str(ud.reg_key),
#                     "data":{"team_id":t_share.id}}
#                 header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
#                 r = requests.post(url, data=json.dumps(data), headers=header)
#                 state = "Done"


#             z= "sent"


#     ctx={"z":z}
            

#     return HttpResponse(json.dumps(ctx), content_type='application/json')

# def Rest_request(request):
#     i = student.objects.filter(user=request.user).first()
#     z=""
#     if request.method=="GET":
#         if request.GET["trip_id"]:
#             trip_id = request.GET["trip_id"]
#             t_share = trip.objects.filter(id=trip_id).first()
#             my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_name=t_share.trip_name,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
            
#             p=student.objects.filter(id=t_share.trip_memb[0]).first()
#             t_share.trip_memb.append(i.id)
#             t_share.save()
            

#             t = team.objects.filter(team_group=t_share.id,team_category="Restaurant")
#             if t:
#                 m=t.first()
                
#             else:
#                 t_n=p.first_name+"'s' Restaurant "+"Troop"
#                 m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Restaurant",team_group=t_share.id)
#                 m.team_memb.append(t_share.trip_memb[0])
#                 m.save()
#             m.team_memb.append(i.id)
#             m.save()
#             if i.id != p.id:
#                 p.n_notif=p.n_notif+1
#                 p.save()
#                 notif_msg = str(i.first_name.split(" ")[0]) + " has joined your trip to the restaurant. Please contact him soon."
#                 new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Restaurant",purpose_id=t_share.id)
#                 new_notif.save()

#                 if user_device.objects.filter(dev_id=p.id):
#                     ud = user_device.objects.filter(dev_id=p.id).first() 
#                     url='https://fcm.googleapis.com/fcm/send'
#                     data ={ "notification": {"title": "Zeeley Restaurants", "text": notif_msg  },  "to" : str(ud.reg_key),
#                         "data":{"team_id":t_share.id}}
#                     header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
#                     r = requests.post(url, data=json.dumps(data), headers=header)
#                     state = "Done"
#             if my_trip:
#                 my_trip.trip_memb.remove(i.id)
#                 my_trip.save()
#                 if not my_trip.trip_memb:
#                     my_trip.delete()
#                 myteam = team.objects.filter(team_group=my_trip.id)
#                 if myteam:
#                     myteam.first().team_memb.remove(i.id)
#                     myteam.first().save()
#                     if not myteam.first().team_memb:
#                         myteam.first().delete()

#             z= "sent"


#     ctx={"z":z}
            

#     return HttpResponse(json.dumps(ctx), content_type='application/json')





def Rest_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            t_share.trip_invites.append(i.id)
            t_share.save()
            

            

            if i.id != p.id:
                p.n_notif=p.n_notif+1
                p.save()
                notif_msg = str(i.first_name.split(" ")[0]) + " has requested to join your Restaurant trip. Please contact him soon."
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Restaurant",purpose_id=t_share.id)
                new_notif.save()

                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Request", "text": notif_msg  },  "to" : str(ud.reg_key),
                        "data":{"team_id":t_share.id,"team_name":t_share.trip_name,"new_memb":i.id,"url":"rest"}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"
            

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Rest_accept(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            memb_id = request.GET["memb_id"]
            p=student.objects.filter(id=memb_id).first()
            t_share = trip.objects.filter(id=trip_id).first()
            if t_share:
                if t_share.trip_memb[0] == i.id:
                    my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=p.id,trip_city=t_share.trip_city).first()
                    myteam = team.objects.filter(team_memb__contains=p.id,team_category="Restaurant")
                    t_share.trip_invites.remove(p.id)
                    t_share.trip_memb.append(p.id)
                    t_share.save()
                    

                    t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="Restaurant",team_group=t_share.id)
                    if t:
                        m=t.first()
                        
                    else:
                        t_n=i.first_name+"'s' Restaurant "+"Troop"
                        m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Restaurant",team_group=t_share.id)
                        m.team_memb.append(t_share.trip_memb[0])
                        m.save()
                    m.team_memb.append(p.id)
                    m.save()

                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " has accepted your Restaurant request. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Restaurant",purpose_id=t_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Accept", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"trip_id":t_share.id,"team_name":m.team_name,"team_id":m.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"
                    if my_trip:
                        if p.id in my_trip.trip_memb:
                            my_trip.trip_memb.remove(p.id)
                            my_trip.save()
                        if not my_trip.trip_memb:
                            my_trip.delete()
                    if myteam:
                        if p.id in myteam.first().team_memb:
                            myteam.first().team_memb.remove(p.id)
                            myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()
                    
                    

                    z= "sent"

                else:
                    z="You don't have permission to accept the request."


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')





def Rest_edit(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method == "GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            c_share = trip.objects.filter(id=int(trip_id)).first()
            if c_share.trip_memb[0]==i.id:

                if request.GET["remark"]:
                    remark = request.GET["remark"]
                    c_share.remark = remark
                    c_share.save()

                if request.GET["memb_remove"]:
                    memb_del = request.GET["memb_remove"]
                    p = student.objects.filter(id=int(memb_del)).first()
                    myteam = team.objects.filter(team_group=c_share.id)
                    if myteam:
                        mineteam=myteam.first()
                        mineteam.team_memb.remove(p.id)
                        mineteam.save()
                        if not mineteam.team_memb:
                            mineteam.delete()

                    
                    c_share.trip_memb.remove(p.id)
                    
                    c_share.save()
                    
                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " removed you from his trip to the restaurant. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Restaurant",purpose_id=c_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Restaurants", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"team_id":t_share.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"

                    z="removed"
                    if not c_share.trip_memb:
                        c_share.delete()
                        z="trip deleted"
                    elif memb_del==i.id:
                        c_share.trip_admin = c_share.trip_memb[0]
                        c_share.save()
                    

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')

    # i=student.objects.filter(user=request.user).first()
    # z=""
    # if request.method == "GET":
    #     if request.GET["trip_id"]:
    #         trip_id = request.GET["trip_id"]
    #         c_share = trip.objects.filter(id=trip_id).first()
    #         if c_share.trip_memb[0]==i.id:
    #             memb_del = request.GET["memb_remove"]
    #             p=student.objects.filter(id=int(memb_del)).first()
    #             c_share.trip_memb.remove(p.id)
    #             c_share.save()
                
                

    #             p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> removed you from his trip to the restaurant "+c_share.trip_name+". Please contact him soon.</td></tr><br>"+p.fnotify
    #             p.save()

    #             notif_msg = str(i.first_name) + " removed you from his trip to the restaurant "+c_share.trip_name+". Please contact him soon."
    #             new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose=i.current_intrest,purpose_id=c_share.id)
    #             new_notif.save()

    #             if user_device.objects.filter(dev_id=p.id):
    #                 ud = user_device.objects.filter(dev_id=p.id).first() 
    #                 url='https://fcm.googleapis.com/fcm/send'
    #                 data ={ "notification": {"title": "Zeeley Restaurants", "text": notif_msg  },  "to" : str(ud.reg_key),
    #                     "data":{"team_id":t_share.id}}
    #                 header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
    #                 r = requests.post(url, data=json.dumps(data), headers=header)
    #                 state = "Done"

    #             if not c_share.trip_memb:
    #                 c_share.delete()
    #                 z="trip deleted"
    #             else:
    #                 c_share.pk=None
    #                 c_share.save()
    #                 c_share.trip_memb=[p.id]
    #                 c_share.save()
    #                 z="Removed"

    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')

def PCGame_share(request):

    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        c = i.id
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        membs = i.f_list
        near_trips =[]
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif

        m = intrests.objects.filter(intrest_name__iexact="Gaming").first()
        dist = 15000
        zipped = localite(request,m.intrest_name,dist)


        tri =trip.objects.filter(trip_city_from='Gaming').order_by("trip_from")
        for tn in tri:
            
            if datetime.now(pytz.utc) > tn.trip_from :
                tn.delete()
            else:
                new = []
                new = [x for x in tn.trip_memb if x not in new]
                tn.trip_memb = new
                tn.save()
                membs = membs+tn.trip_memb
                if i.id not in tn.trip_memb:
                    distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
                    
                    if distance <= 15000:
                        near_trips.append(tn)
        membs = list(set(membs))
        
        my_trip = tri.filter(trip_memb__contains=i.id)
        

        more = []

        if request.method=="GET":
            if request.GET["game"]:
                
                t_name = request.GET["game"]
                date = request.GET["date"]
                time = request.GET["time"]
                remark = request.GET["remark"]
                ip = request.GET["ip"]
                d = date.split("/")
                t_from = d[2]+"-"+d[0]+"-"+d[1]+" "+time+":00"
                # q_from=change_date_formt(q_from)
                # t_from = change_date_formt2(date)
                
                
                

                shared = trip.objects.filter(trip_name=t_name,trip_city=ip,trip_city_from="Gaming",trip_memb__contains =i.id,trip_from=t_from)
                for shr in shared:
                    if len(shr.trip_memb) ==1:
                        shr.delete()
                t=trip(trip_admin=i.id,trip_memb=[i.id],trip_name=t_name,trip_city=ip,trip_from=t_from,trip_city_from="Gaming",from_latitude=i.latitude,from_longitude=i.longitude,remark=remark)
                
                t.save()

                z=""
                # list1 =[]
                # more1 = tri.filter(trip_name=t.trip_name).exclude(trip_memb__contains=i.id)
                # if more1:
                #     for  k in more1:
                #         list1 =list1+ k.trip_memb

                #     z = '_'.join(str(e) for e in list(set(list1)))
            

                return HttpResponse(json.dumps(ctx), content_type='application/json')
        if my_trip:
            more = tri.filter(trip_name=my_trip.last().trip_name).exclude(trip_memb__contains=i.id)

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)


        for labe in membs:
            if labe not in java:
                java.append(labe)


        a = student.objects.filter(id__in=java)

        
        allpost = posts.objects.filter(id__in=m.intrest_posts).order_by("-id").exclude(anonymous=True)
        f=allpost[0:9]
        other = allpost[9:]


        myteam = team.objects.filter(team_memb__contains=i.id)
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "f" : f,
            "other" : other,
            "your_trip" :my_trip,
            "myteam":myteam,
            "trips":more,
            "near_trips":near_trips,
            "m" : m,
            "zip":zipped,
            
        }


        
        return render(request,"PCGaming.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

    # if request.user.is_authenticated():

    #     i=student.objects.filter(user=request.user).first()
    #     c = i.id
    #     count = "p"+str(c)+"_"+str(i.prof_pic[-1])
    #     counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
    #     title = i.first_name
    #     a = all_memb(request)
    #     p=i.n_frqst
    #     q=i.n_msg
    #     r=i.n_notif


    #     tri =team.objects.filter(team_category=i.current_intrest)
    #     my_game = tri.filter(team_memb__contains=i.id)
        

    #     more = []

    #     if request.method=="POST":
            
            
    #         g_n = request.POST['game']
    #         g_server = request.POST['server_address']
    #         print (t_g)
    #         za = team.objects.filter(team_category=i.current_intrest,team_capt=c)
    #         for z in za:
    #             z.delete()
    #         m = team(game_server=g_server,game_name=g_n,team_capt=c,team_category=i.current_intrest)
    #         m.team_memb.append(c)
    #         m.save()
    #         more = tri.filter(game_name=g_n).exclude(team_memb__contains=i.id)


    #     context = {
    #         "title" : title,
    #         "counter" : counter,
    #         "count" : i.id,
    #         "i" : i,
    #         "a" : a,
    #         "e" : i.f_list,
    #         "p" : p,
    #         "q" : q,
    #         "r" : r,
    #         "your_trip":my_game,
    #         "trips":more,
            
    #     }


    #     return render(request,"PCGaming.html",context)
    # else:
    #     state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


# def PCGame_request(request):

#     i = student.objects.filter(user=request.user).first()
#     z=""
#     if request.method=="GET":
#         if request.GET["trip_id"]:
#             trip_id = request.GET["trip_id"]
#             t_share = trip.objects.filter(id=trip_id).first()
#             my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_name=t_share.trip_name,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
            
#             p=student.objects.filter(id=t_share.trip_memb[0]).first()
#             t_share.trip_memb.append(i.id)
#             t_share.save()
            

#             t = team.objects.filter(team_group=t_share.id,team_category="Gaming")
#             if t:
#                 m=t.first()
                
#             else:
#                 t_n=p.first_name+"'s' Gaming "+"Troop"
#                 m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Gaming",team_group=t_share.id)
#                 m.team_memb.append(t_share.trip_memb[0])
#                 m.save()
#             m.team_memb.append(i.id)
#             m.save()
#             if i.id != p.id:
#                 p.n_notif=p.n_notif+1
#                 p.save()
#                 notif_msg = str(i.first_name.split(" ")[0]) + " has joined your gamers gang. Please contact him soon."
#                 new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Gaming",purpose_id=t_share.id)
#                 new_notif.save()

#                 if user_device.objects.filter(dev_id=p.id):
#                     ud = user_device.objects.filter(dev_id=p.id).first() 
#                     url='https://fcm.googleapis.com/fcm/send'
#                     data ={ "notification": {"title": "Zeeley Gaming", "text": notif_msg  },  "to" : str(ud.reg_key),
#                         "data":{"team_id":t_share.id}}
#                     header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
#                     r = requests.post(url, data=json.dumps(data), headers=header)
#                     state = "Done"
#             if my_trip:
#                 my_trip.trip_memb.remove(i.id)
#                 my_trip.save()
#                 if not my_trip.trip_memb:
#                     my_trip.delete()
#                 myteam = team.objects.filter(team_group=my_trip.id)
#                 if myteam:
#                     myteam.first().team_memb.remove(i.id)
#                     myteam.first().save()
#                     if not myteam.first().team_memb:
#                         myteam.first().delete()

#             z= "sent"


#     ctx={"z":z}
            

#     return HttpResponse(json.dumps(ctx), content_type='application/json')


    # i = student.objects.filter(user=request.user).first()
    # z=""
    # if request.method=="POST":
    #     team_id = request.POST["team_id"]
    #     t_share = team.objects.filter(id=team_id).first()
    #     my_team = team.objects.filter(game_name=t_share.team_name,team_memb__contains=i.id,team_from=t_share.team_from).first()
    #     my_team.delete()
    #     p=student.objects.filter(id=t_share.team_memb[0]).first()
    #     t_share.team_memb.append(i.id)
    #     t_share.save()
    #     p.n_notif=p.n_notif+1


        
    #     p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> joined your game "+t_share.game_name+". Please contact him soon.</td></tr><br>"+p.fnotify
    #     p.save()
    #     z= "sent"


    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')  







def PCGame_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            t_share.trip_invites.append(i.id)
            t_share.save()
            

            

            if i.id != p.id:
                p.n_notif=p.n_notif+1
                p.save()
                notif_msg = str(i.first_name.split(" ")[0]) + " has requested to join your PCGame trip. Please contact him soon."
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="PCGame",purpose_id=t_share.id)
                new_notif.save()

                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Request", "text": notif_msg  },  "to" : str(ud.reg_key),
                        "data":{"team_id":t_share.id,"team_name":t_share.trip_name,"new_memb":i.id,"url":"pcgame"}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"
            

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def PCGame_accept(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            memb_id = request.GET["memb_id"]
            p=student.objects.filter(id=memb_id).first()
            t_share = trip.objects.filter(id=trip_id).first()
            if t_share:
                if t_share.trip_memb[0] == i.id:
                    my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=p.id,trip_city=t_share.trip_city).first()
                    myteam = team.objects.filter(team_memb__contains=p.id,team_category="PCGame")
                    t_share.trip_invites.remove(p.id)
                    t_share.trip_memb.append(p.id)
                    t_share.save()
                    

                    t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="PCGame",team_group=t_share.id)
                    if t:
                        m=t.first()
                        
                    else:
                        t_n=i.first_name+"'s' PCGame "+"Troop"
                        m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="PCGame",team_group=t_share.id)
                        m.team_memb.append(t_share.trip_memb[0])
                        m.save()
                    m.team_memb.append(p.id)
                    m.save()

                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " has accepted your PCGame request. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="PCGame",purpose_id=t_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Accept", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"trip_id":t_share.id,"team_name":m.team_name,"team_id":m.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"
                    if my_trip:
                        if p.id in my_trip.trip_memb:
                            my_trip.trip_memb.remove(p.id)
                            my_trip.save()
                        if not my_trip.trip_memb:
                            my_trip.delete()
                    if myteam:
                        if p.id in myteam.first().team_memb:
                            myteam.first().team_memb.remove(p.id)
                            myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()
                    
                    

                    z= "sent"

                else:
                    z="You don't have permission to accept the request."


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')





def PCGame_edit(request):

    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method == "GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            c_share = trip.objects.filter(id=int(trip_id)).first()
            if c_share.trip_memb[0]==i.id:

                if request.GET["remark"]:
                    remark = request.GET["remark"]
                    c_share.remark = remark
                    c_share.save()

                if request.GET["memb_remove"]:
                    memb_del = request.GET["memb_remove"]

                    myteam = team.objects.filter(team_memb__contains=memb_del,team_category="Gaming")
                    if myteam:
                        myteam.first().team_memb.remove(int(memb_del))
                        myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()

                    p = student.objects.filter(id=int(memb_del)).first()
                    c_share.trip_memb.remove(p.id)
                    if memb_del==i.id:
                        c_share.trip_admin = c_share.trip_memb[0]
                    c_share.save()
                    
                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " removed you from his game. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Gaming",purpose_id=c_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Gaming", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"team_id":c_share.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"

                    z="removed"
                    if not c_share.trip_memb:
                        c_share.delete()
                        z="trip deleted"
                    elif memb_del==i.id:
                        c_share.trip_admin = c_share.trip_memb[0]
                        c_share.save()

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')



    # i=student.objects.filter(user=request.user).first()
    # z=""
    # if request.method == "POST":
    #     team_id = request.POST["team_id"]
    #     c_share = team.objects.filter(id=team_id).first()
    #     if c_share.team_memb[0]==i.id:
    #         memb_del = request.POST["memb_remove"]
    #         p=student.objects.filter(id=int(memb_del)).first()
    #         c_share.team_memb.remove(p.id)
    #         c_share.save()
            
            

    #         p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> removed you from his game "+c_share.team_name+". Please contact him soon.</td></tr><br>"+p.fnotify
    #         p.save()
    #         if not c_share.team_memb:
    #             c_share.delete()
    #             z="team deleted"
    #         else:
    #             c_share.pk=None
    #             c_share.save()
    #             c_share.team_memb=[p.id]
    #             c_share.save()
    #             z="Removed"

    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')


def Movie_share(request):

    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        c = i.id
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        membs = i.f_list
        near_trips =[]
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif

        m = intrests.objects.filter(intrest_name__iexact="Film").first()
        dist = 15000
        zipped = localite(request,m.intrest_name,dist)


        tri =trip.objects.filter(trip_city_from='Film').order_by("trip_from")
        for tn in tri:
            
            if datetime.now(pytz.utc) > tn.trip_from :
                tn.delete()
            else:
                new = []
                new = [x for x in tn.trip_memb if x not in new]
                tn.trip_memb = new
                tn.save()
                membs = membs+tn.trip_memb
                if i.id not in tn.trip_memb:
                    distance = haversine(request,i.longitude, i.latitude, tn.from_longitude, tn.from_latitude)
                    if distance <= 15000:
                        near_trips.append(tn)
        membs = list(set(membs))
        
        my_trip = tri.filter(trip_memb__contains=i.id)
        

        more = []

        if request.method=="GET":
            if request.GET["film"]:
                
                t_name = request.GET["film"]
                date = request.GET["date"]
                remark = request.GET["remark"]
                
                # q_from=change_date_formt(q_from)
                t_from = change_date_formt2(date)
                theatre_name = request.GET["theatre"]
                
                

                shared = trip.objects.filter(trip_name=t_name,trip_city_from="Film",trip_memb__contains =i.id,trip_from=t_from)
                for shr in shared:
                    if len(shr.trip_memb) ==1:
                        shr.delete()
                t=trip(trip_admin=i.id,trip_memb=[i.id],trip_name=t_name,trip_city=theatre_name,trip_from=t_from,trip_city_from="Film",from_latitude=i.latitude,from_longitude=i.longitude,remark=remark)
                
                t.save()

                
                z=""
                # list1 =[]
                # more1 = tri.filter(trip_name=t.trip_name).exclude(trip_memb__contains=i.id)
                # if more1:
                #     for  k in more1:
                #         list1 =list1+ k.trip_memb

                #     z = '_'.join(str(e) for e in list(set(list1)))

                ctx={"z":z}
            

                return HttpResponse(json.dumps(ctx), content_type='application/json')
        if my_trip:
            more = tri.filter(trip_name=my_trip.last().trip_name).exclude(trip_memb__contains=i.id)

        java=i.f_list
        mess = message.objects.filter(m_id=i.id).values("m_friend").distinct()
            
        for k in mess:
            if k["m_friend"] not in java:
                java.append(k["m_friend"])

        for ka in i.msg_list:
            if ka not in java:
                java.append(ka)

        for labe in membs:
            if labe not in java:
                java.append(labe)


        a = student.objects.filter(id__in=java)


        allpost = posts.objects.filter(id__in=m.intrest_posts).order_by("-id").exclude(anonymous=True)
        f=allpost[0:9]
        other = allpost[9:]


        myteam = team.objects.filter(team_memb__contains=i.id)
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : java,
            "p" : p,
            "q" : q,
            "r" : r,
            "f" : f,
            "other" : other,
            "your_trip" :my_trip,
            "myteam":myteam,
            "trips":more,
            "near_trips":near_trips,
            "m" : m,
            "zip":zipped,
            
        }


        
        return render(request,"Movies.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



    # if request.user.is_authenticated():

    #     i=student.objects.filter(user=request.user).first()
    #     c = i.id
    #     count = "p"+str(c)+"_"+str(i.prof_pic[-1])
    #     counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
    #     title = i.first_name
    #     a = all_memb(request)
    #     p=i.n_frqst
    #     q=i.n_msg
    #     r=i.n_notif


    #     tri =trip.objects.filter(trip_city_from='Film')
    #     for tn in tri:
    #         if datetime.now(pytz.utc) > tn.trip_from :
    #             tn.delete()

    #     my_trip = tri.filter(trip_memb__contains=i.id)
        

    #     more = []

    #     if request.method=="POST":
    #         t_mode = request.POST["theatre"]
    #         t_name = request.POST["movie"]
    #         date = request.POST["date"]
    #         d=date.split("/")
    #         # q_from=change_date_formt(q_from)
    #         t_from = d[2]+'-'+d[0]+'-'+d[1]
            
    #         t_memb = request.POST["memb"]
    #         t=trip(trip_name=t_name,trip_from=t_from,trip_mode=t_mode,trip_n_membs=t_memb,trip_city_from="Film")
    #         t.trip_memb.append(i.id)
    #         t.save()

    #         more = tri.filter(trip_name=t_name,trip_mode=t_mode).exclude(trip_memb__contains=i.id)


    #     context = {
    #         "title" : title,
    #         "counter" : counter,
    #         "count" : i.id,
    #         "i" : i,
    #         "a" : a,
    #         "e" : i.f_list,
    #         "p" : p,
    #         "q" : q,
    #         "r" : r,
    #         "your_trips":my_trip,
    #         "trips":tri,
            
    #     }


    #     return render(request,"Movies.html",context)
    # else:
    #     state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Movie_request(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            t_share = trip.objects.filter(id=trip_id).first()
            
            p=student.objects.filter(id=t_share.trip_memb[0]).first()
            t_share.trip_invites.append(i.id)
            t_share.save()
            

            

            if i.id != p.id:
                p.n_notif=p.n_notif+1
                p.save()
                notif_msg = str(i.first_name.split(" ")[0]) + " has requested to join your Movie trip. Please contact him soon."
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Film",purpose_id=t_share.id)
                new_notif.save()

                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Request", "text": notif_msg  },  "to" : str(ud.reg_key),
                        "data":{"team_id":t_share.id,"team_name":t_share.trip_name,"new_memb":i.id,"url":"movie"}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"
            

            z= "sent"


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Movie_accept(request):
    i = student.objects.filter(user=request.user).first()
    z=""
    if request.method=="GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            memb_id = request.GET["memb_id"]
            p=student.objects.filter(id=memb_id).first()
            t_share = trip.objects.filter(id=trip_id).first()
            if t_share:
                if t_share.trip_memb[0] == i.id:
                    my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_memb__contains=p.id,trip_city=t_share.trip_city).first()
                    myteam = team.objects.filter(team_memb__contains=p.id,team_category="Film")
                    t_share.trip_invites.remove(p.id)
                    t_share.trip_memb.append(p.id)
                    t_share.save()
                    

                    t = team.objects.filter(team_memb__contains=t_share.trip_memb[0],team_category="Film",team_group=t_share.id)
                    if t:
                        m=t.first()
                        
                    else:
                        t_n=i.first_name+"'s' Movie "+"Troop"
                        m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Film",team_group=t_share.id)
                        m.team_memb.append(t_share.trip_memb[0])
                        m.save()
                    m.team_memb.append(p.id)
                    m.save()

                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " has accepted your Movie request. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Film",purpose_id=t_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Accept", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"trip_id":t_share.id,"team_name":m.team_name,"team_id":m.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"
                    if my_trip:
                        if p.id in my_trip.trip_memb:
                            my_trip.trip_memb.remove(p.id)
                            my_trip.save()
                        if not my_trip.trip_memb:
                            my_trip.delete()
                    if myteam:
                        if p.id in myteam.first().team_memb:
                            myteam.first().team_memb.remove(p.id)
                            myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()
                    
                    

                    z= "sent"

                else:
                    z="You don't have permission to accept the request."


    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')


# def Movie_request(request):

#     i = student.objects.filter(user=request.user).first()
#     z=""
#     if request.method=="GET":
#         if request.GET["trip_id"]:
#             trip_id = request.GET["trip_id"]
#             t_share = trip.objects.filter(id=trip_id).first()
#             my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_name=t_share.trip_name,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
#             myteam = []
#             p=student.objects.filter(id=t_share.trip_memb[0]).first()
#             t_share.trip_memb.append(i.id)
#             t_share.save()
            

#             t = team.objects.filter(team_group=t_share.id,team_category="Film")
#             if t:
#                 m=t.first()
                
#             else:
#                 t_n=p.first_name+"'s' movie "+"Troop"
#                 m = team(team_name=t_n,team_capt=t_share.trip_memb[0],team_category="Film",team_group=t_share.id)
#                 m.team_memb.append(t_share.trip_memb[0])
#                 m.save()
#             m.team_memb.append(i.id)
#             m.save()
#             if i.id != p.id:
#                 p.n_notif=p.n_notif+1
#                 p.save()
#                 notif_msg = str(i.first_name.split(" ")[0]) + " has joined your trip to the movie. Please contact him soon."
#                 new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Film",purpose_id=t_share.id)
#                 new_notif.save()

#                 if user_device.objects.filter(dev_id=p.id):
#                     ud = user_device.objects.filter(dev_id=p.id).first() 
#                     url='https://fcm.googleapis.com/fcm/send'
#                     data ={ "notification": {"title": "Zeeley movies", "text": notif_msg  },  "to" : str(ud.reg_key),
#                         "data":{"team_id":t_share.id}}
#                     header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
#                     r = requests.post(url, data=json.dumps(data), headers=header)
#                     state = "Done"
#             if my_trip:
#                 myteam = team.objects.filter(team_group=my_trip.id)
#                 my_trip.trip_memb.remove(i.id)
#                 my_trip.save()
#                 if not my_trip.trip_memb:
#                     my_trip.delete()
#             if myteam:
#                 myteam.first().team_memb.remove(i.id)
#                 myteam.first().save()
#                 if not myteam.first().team_memb:
#                     myteam.first().delete()

#             z= "sent"


#     ctx={"z":z}
            

#     return HttpResponse(json.dumps(ctx), content_type='application/json')


    # i = student.objects.filter(user=request.user).first()
    # z=""
    # if request.method=="POST":
    #     trip_id = request.POST["trip_id"]
    #     t_share = trip.objects.filter(id=trip_id).first()
    #     my_trip = trip.objects.filter(trip_city_from=t_share.trip_city_from,trip_name=t_share.trip_name,trip_memb__contains=i.id,trip_city=t_share.trip_city,trip_from=t_share.trip_from).first()
    #     my_trip.delete()
    #     p=student.objects.filter(id=t_share.trip_memb[0]).first()
    #     t_share.trip_memb.append(i.id)
    #     t_share.save()
    #     p.n_notif=p.n_notif+1


        
    #     p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> joined your trip to the movie "+t_share.trip_name+". Please contact him soon.</td></tr><br>"+p.fnotify
    #     p.save()
    #     z= "You joined the trip"


    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')


def Movie_edit(request):
    i=student.objects.filter(user=request.user).first()
    z=""
    if request.method == "GET":
        if request.GET["trip_id"]:
            trip_id = request.GET["trip_id"]
            c_share = trip.objects.filter(id=int(trip_id)).first()
            if c_share.trip_memb[0]==i.id:

                if request.GET["remark"]:
                    remark = request.GET["remark"]
                    c_share.remark = remark
                    c_share.save()

                if request.GET["memb_remove"]:
                    memb_del = request.GET["memb_remove"]
                    myteam = team.objects.filter(team_memb__contains=memb_del,team_category="Film")
                    if myteam:
                        myteam.first().team_memb.remove(int(memb_del))
                        myteam.first().save()
                        if not myteam.first().team_memb:
                            myteam.first().delete()

                    p = student.objects.filter(id=int(memb_del)).first()
                    c_share.trip_memb.remove(p.id)
                    
                    c_share.save()
                    
                    
                    
                    if i.id != p.id:
                        p.n_notif=p.n_notif+1
                        p.save()
                        notif_msg = str(i.first_name.split(" ")[0]) + " removed you from his trip to the movie. Please contact him soon."
                        new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose="Film",purpose_id=c_share.id)
                        new_notif.save()

                        if user_device.objects.filter(dev_id=p.id):
                            ud = user_device.objects.filter(dev_id=p.id).first() 
                            url='https://fcm.googleapis.com/fcm/send'
                            data ={ "notification": {"title": "Zeeley Movies", "text": notif_msg  },  "to" : str(ud.reg_key),
                                "data":{"team_id":t_share.id}}
                            header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                            r = requests.post(url, data=json.dumps(data), headers=header)
                            state = "Done"

                    z="removed"
                    if not c_share.trip_memb:
                        c_share.delete()
                        z="trip deleted"
                    elif memb_del==i.id:
                        c_share.trip_admin = c_share.trip_memb[0]
                        c_share.save()
                    

    ctx={"z":z}
            

    return HttpResponse(json.dumps(ctx), content_type='application/json')



    # i=student.objects.filter(user=request.user).first()
    # z=""
    # if request.method == "POST":
    #     trip_id = request.POST["trip_id"]
    #     c_share = trip.objects.filter(id=trip_id).first()
    #     if c_share.trip_memb[0]==i.id:
    #         memb_del = request.POST["memb_remove"]
    #         p=student.objects.filter(id=int(memb_del)).first()
    #         c_share.trip_memb.remove(p.id)
    #         c_share.save()
            
            

    #         p.fnotify="<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_p"+str(i.id)+"_"+str(i.prof_pic[-1])+".thumbnail' style='width:50px;height:50px;border-radius:10px;'></td><td width='85%'><a href='../"+str(i.id)+"' > <b style='color:black;'>"+str(i.first_name)+" "+str(i.last_name)+"</a></b> removed you from his trip to the movie "+c_share.trip_name+". Please contact him soon.</td></tr><br>"+p.fnotify
    #         p.save()
    #         if not c_share.trip_memb:
    #             c_share.delete()
    #             z="trip deleted"
    #         else:
    #             c_share.pk=None
    #             c_share.save()
    #             c_share.trip_memb=[p.id]
    #             c_share.save()
    #             z="Removed"

    # ctx={"z":z}
            

    # return HttpResponse(json.dumps(ctx), content_type='application/json')



def android_msg(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        
        
        j = request.GET.get('to')
        m = message.objects.filter(Q(m_id=c,m_friend=int(j)) | Q(m_id=int(j),m_friend=c))
        d = len(m)
        if d>15:
            e = d-15
            m = m[e:d]
        else:
            m=m[:d]
        
        
        data = serializers.serialize('json', m, fields=('id','m_id','m_photo','m_message','m_friend','post_time'))
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def android_sendmsg(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        
        
        j = request.GET.get('to')
        #j = 6
        print (j)
        k=""
        if request.GET.get('message'):
            k = request.GET.get('message') 
            if k!="\n":
                print (k)
                p = student.objects.filter(id=j).first()
                if i.id not in p.block_msg:
                    l = message(m_id=c,m_friend=j,m_message=k)
                    l.save()
                    if not i.id in p.msg_list:
                        p.msg_list.append(i.id)
                        p.n_msg=p.n_msg+1
                        
                    else:
                        p.msg_list.remove(i.id)
                        p.msg_list.append(i.id)
                        p.n_msg=p.n_msg+1
                    p.fmsg=""
                    for x in p.msg_list:
                        if message.objects.filter(m_id=x,m_friend=j):
                            msgs=message.objects.filter(m_id=x,m_friend=j).last()
                            y=student.objects.filter(id=x).first()
                            p.fmsg="<tr height='50px' class='openchat' value="+str(y.id)+"><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(y.id)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'>"+"<b style='color:black;font-size:13px;'>"+str(y.first_name)+"</b>:<br><div style='font-size:11px;'>"+str(msgs.m_message)+"</div></td></tr>"+p.fmsg
                            #"<tr><td width='15%' style='padding-left:15px;'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:38px;height:38px;border-radius:8px;'></td><td width='85%'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><br>"
                        
                    p.save()
                    if user_device.objects.filter(dev_id=p.id):
                        ud = user_device.objects.filter(dev_id=p.id).first() 
                        url='https://fcm.googleapis.com/fcm/send'
                        data ={ "notification": {"title": "Zeeley Message", "text": i.first_name+" : "+k[:20]  },  "to" : str(ud.reg_key),
                                "data":{"team_id":i.id,"is_photo":msgs.m_photo}}
                        header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                        r = requests.post(url, data=json.dumps(data), headers=header)
                        state = "Done"
                    
            

        


        
        
        m = message.objects.filter(Q(m_id=c,m_friend=j) | Q(m_id=j,m_friend=c)).order_by("-id")[:2]

        data = serializers.serialize('json', m, fields=('id','m_id','m_message','m_friend','post_time','m_photo'))
        
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")






def send_team_image(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        
        
        j = int(request.POST.get('to'))
        #j = 6
        print (j)
        k=""
        if request.FILES.getlist("files"):
            print("aa gaya...")
            # ltime = timezone.now()
            # tz = pytz.timezone('Asia/Kolkata')
            # your_now = ltime.astimezone(tz)
            time = str(datetime.now())
            for count, x in enumerate(request.FILES.getlist("files")):
                def process(f):
                    with open('/root/ttl/babaS/media_in_env/media_root/file_' + time +'.jpg', 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)

                process(x)


            infile = '/root/ttl/babaS/media_in_env/media_root/file_' + time +'.jpg'

            photo_resize(infile,800,800)
            
            outfile = os.path.splitext(infile)[0] + ".thumbnail"
            if infile != outfile:
                try:
                    im = Image.open(infile)
                    im.thumbnail((200,200),Image.ANTIALIAS)
                    im.save(outfile, "JPEG")
                except IOError:
                    print "cannot create thumbnail for", infile
            k="/media/file_"+time+".thumbnail"
            teamc = team.objects.filter(id=j).first()
            for s in teamc.team_memb:
                p = student.objects.filter(id=s).first()
                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first()
                
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Team Message",    "text": i.first_name+" sent a photo. "  },  "to" : str(ud.reg_key),
                        "data":{"team_id":i.id,"is_photo":"True"}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"
                
            l = teamchat(teamc_id=c,teamc_group=j,teamc_message=k)
            l.teamc_photo = True
            l.save()

        m = teamchat.objects.filter(teamc_group=j).order_by("-id")[:2]

        data = serializers.serialize('json', m, fields=('id','teamc_id','teamc_message','teamc_group','post_time','teamc_photo'))
        
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")






@csrf_exempt
def send_image(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        
        
        j = request.POST.get('to')
        #j = 6
        print (j)
        k=""
        if request.FILES.getlist("files"):
            print("aa gaya...")
            # ltime = timezone.now()
            # tz = pytz.timezone('Asia/Kolkata')
            # your_now = ltime.astimezone(tz)
            time = str(datetime.now())
            for count, x in enumerate(request.FILES.getlist("files")):
                def process(f):
                    with open('/root/ttl/babaS/media_in_env/media_root/file_' + time +'.jpg', 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)

                process(x)


            infile = '/root/ttl/babaS/media_in_env/media_root/file_' + time +'.jpg'

            photo_resize(infile,800,800)
            
            outfile = os.path.splitext(infile)[0] + ".thumbnail"
            if infile != outfile:
                try:
                    im = Image.open(infile)
                    im.thumbnail((200,200),Image.ANTIALIAS)
                    im.save(outfile, "JPEG")
                except IOError:
                    print "cannot create thumbnail for", infile
            k="/media/file_"+time+".thumbnail"
            p = student.objects.filter(id=j).first()
            if user_device.objects.filter(dev_id=p.id):
                ud = user_device.objects.filter(dev_id=p.id).first()
            
                url='https://fcm.googleapis.com/fcm/send'
                data ={ "notification": {"title": "Zeeley Message",    "text": i.first_name+" sent a photo. "  },  "to" : str(ud.reg_key),
                    "data":{"team_id":i.id,"is_photo":"True"}}
                header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                r = requests.post(url, data=json.dumps(data), headers=header)
                state = "Done"
                
            l = message(m_id=c,m_friend=j,m_message=k,post_time=time)
            l.m_photo = True
            l.save()

        m = message.objects.filter(Q(m_id=c,m_friend=j) | Q(m_id=j,m_friend=c)).order_by("-id")[:2]

        data = serializers.serialize('json', m, fields=('id','m_id','m_message','m_friend','post_time','m_photo'))
        
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def android_change_interest(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        q = request.GET.get('q','')
        find=intrests.objects.filter(intrest_name__iexact=q).first()
        if find:


            change_interest(request,i,find)
            state="Now Your interest is "+str(find.intrest_name)+" !!!"
            
        else:
            state="No such interest found !!!"
            

        context = {"s":state}


        return HttpResponse(json.dumps(context), content_type='application/json')


    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def scroll_msgs(request):
    i = student.objects.filter(user=request.user).first()

    a = request.GET["a"]
    b = request.GET["b"]
    c=i.id
        
        
    j = request.GET['f']

    m = message.objects.filter(Q(m_id=c,m_friend=j) | Q(m_id=j,m_friend=c)).order_by("-id")[int(a):int(b)]
    data = serializers.serialize('json', m, fields=('id','m_id','m_message','m_friend','post_time','m_photo'))
    return HttpResponse(data, content_type='application/json')
    

    # fl =i.f_list
    # mylist=[Q(identity=i.id),Q(id__in=i.shared_posts)]
    
    # for c in fl:
    #     mylist.append(Q(identity=c))
    # f=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")[int(a):int(b)]

    # data = serializers.serialize('json', f, fields=('id','identity','photos','comments','profile_picture','cover_picture','hits','say','post_time'))



    # return HttpResponse(data, content_type='application/json')



def scroll_team_msgs(request):
    i = student.objects.filter(user=request.user).first()

    a = request.GET["a"]
    b = request.GET["b"]
    c=i.id
        
        
    f = request.GET['f']

    m = teamchat.objects.filter(teamc_group=f).order_by("-id")[int(a):int(b)]
    data = serializers.serialize('json', m, fields=('id','teamc_id','teamc_message','teamc_group','post_time','teamc_photo'))
    return HttpResponse(data, content_type='application/json')

def Create_team(request):
    if request.user.is_authenticated() :
        t_n=""
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            t_n=request.POST['team_name']
            t_l=request.POST['team_logo']
            t_g=request.POST['team_group']
            print (t_g)
            m = team(team_name=t_n,team_capt=c,team_logo=t_l,team_category=i.current_intrest,team_group=t_g)
            m.team_memb.append(c)
            m.save()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            play.p_team=m.id
            play.save()
            

            k=team.objects.filter(team_name=t_n).first()
            return HttpResponseRedirect("/make_team/?q="+str(k.id))


        else:
            return HttpResponse("No team created....")

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def Invitesportee(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        state = ""
        if request.method=='GET':
            j=request.GET["invited"]
            t = team.objects.filter(team_memb__contains=i.id,team_category=i.current_intrest)
            if t:
                m=t.first()
                
            else:
                t_n=i.first_name+" "+i.current_intrest+" "+"Group"
                m = team(team_name=t_n,team_capt=c,team_category=i.current_intrest)
                m.team_memb.append(c)
                m.save()
                
            l=m.id
            
            p=student.objects.filter(id=j).first()
            m.team_invites.append(p.id)
            m.save()
            


            
            
            
            if i.id != p.id:
                p.n_notif=p.n_notif+1
                p.save()
                notif_msg = i.first_name.split(" ")[0] + " invited you for "+i.current_intrest
                new_notif = notification(notif_from =i.id,notif_to=p.id,notif=notif_msg,purpose=i.current_intrest,purpose_id=m.id)
                new_notif.save()

                if user_device.objects.filter(dev_id=p.id):
                    ud = user_device.objects.filter(dev_id=p.id).first() 
                    url='https://fcm.googleapis.com/fcm/send'
                    data ={ "notification": {"title": "Zeeley Invitation", "text": i.first_name+" invited you for "+str(i.current_intrest)  },  "to" : str(ud.reg_key),
                        "data":{"team_id":m.id,"sender_id":i.id,"team_name":m.team_name}}
                    header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                    r = requests.post(url, data=json.dumps(data), headers=header)
                    state = "Done"


            


        context = {"s":state,
                    "team_id":m.id,
                    "team_name":m.team_name,
                    "team_category":m.team_category,
                    }
        return HttpResponse(json.dumps(context), content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def android_registration(request):
    state = ""
    if request.GET:
        
        password=request.GET['password']
        # password1=request.GET['confirm_password']
        email=request.GET['email']
        
        first_name=request.GET['first_name']
        if request.GET['last_name']:
            last_name=request.GET['last_name']
        else:
            last_name = ""


        

        # date=request.GET.get('date')
        # month=request.GET.get('month')
        # year=request.GET.get('year')

        male = request.GET.get('male')

        # female = request.GET.get('male')
        # other = request.GET.get('m')
        

        USER=User.objects.filter(username=email)

        if USER:
            messages.error(request,"User already exist")
            #redirect here
            return HttpResponseRedirect("/")
    
        else:
            User.objects.create_user(username=email,password=password,email=email)
            Us=User.objects.get(username=email)
            ####
            
            # Us.last_name = str(datetime.now())
            # Us.save()

            # a=student.objects.all()
            # p=[]
            # for c in a:
            #     p.append(c.id)
                
            s=student(user=Us,status=password,first_name=first_name,last_name=last_name,post=[])
            s.save()
            s.prof_pic.append("0")
            s.cover_pic.append("0")
            find = []
            if request.GET["interest"]:
                interest = request.GET["interest"]
                find = intrests.objects.filter(intrest_name__exact=interest).first()

            if not find:
                find = intrests.objects.filter(intrest_name__exact="Chat").first()
            change_interest(request,s,find)

            s.tagword = email
            age = request.GET["interest"]
            year = datetime.today().year - int(age)
            s.year = year
            
            if request.GET["contact"]:
                s.phone_number = request.GET["contact"]
                s.save()

            # infile1 = '/root/ttl/babaS/media_in_env/media_root/defaultcover.jpg'
            # outfile1 = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

            
            # if infile1 != outfile1:
            #     try:
            #         im = Image.open(infile1)
            #         im.thumbnail((1600,900),Image.ANTIALIAS)
            #         im.save(outfile1, "JPEG")
            #     except IOError:
            #         print "cannot create thumbnail for", infile1

            
            

            # for c in a:
            #     c.add_f_list.append(s.id)
            #     c.save()
            i = student.objects.filter(id=320).first()

            make_frnd(request,s,i)

            if male == "male":
                s.male = True
                s.save()
            if male=="female":
                s.female = True
                s.save()
            

            if s.male:
                infile = '/root/ttl/babaS/media_in_env/media_root/default.jpg'
                outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
                if infile != outfile:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

                if infile != outfile2:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile2, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

            else:
                infile = '/root/ttl/babaS/media_in_env/media_root/defaultfemale.jpg'
                outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
                
                if infile != outfile:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

                if infile != outfile2:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((250,250),Image.ANTIALIAS)
                        im.save(outfile2, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile


            thumb(outfile)
            # thumb(outfile1)
            thumb(outfile2)

            # res = send_mail("Hallo "+s.first_name+", Welcome to Zeeley","Open this link to activate your account:: www.zeeley.com/mail_confirm/?q="+str(s.id)+"@."+Us.last_name,"zeeleynoreply@gmail.com",[Us.username])


            user = authenticate(username=email, password=password)
            login(request, user)


            # Us.is_active=False
            # Us.save()

            state = "Done"
    context={"state":state}
    return HttpResponse(json.dumps(context), content_type='application/json')



def google_verification(request):

    return render(request,'google102ec01c53289263.html',{})


def android_info(request):
    if request.user.is_authenticated() :
        i = student.objects.filter(user=request.user).first()
        identity = request.GET["id"]
        s = student.objects.filter(id=identity).first()
        year = datetime.today().year - int(s.year)

        context={"first_name":s.first_name,
                "last_name":s.last_name,
                "age":year,
                "gender":s.male,
                "contact":s.phone_number,
                "current_interest": s.current_intrest,
                "latitude" : str(s.latitude),
                "longitude" :str(s.longitude),
    
                "priv_string": priv_string(request,i,s),
                }

        return HttpResponse(json.dumps(context), content_type='application/json')

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def android_trips(request):
    if request.user.is_authenticated() :
        i = student.objects.filter(user=request.user)
        identity = request.GET["id"]
        s = student.objects.filter(id=int(identity)).first()
        
        trips = trip.objects.filter(trip_memb__contains=s.id)

        data = serializers.serialize('json', trips, fields=('id','trip_admin','trip_name','trip_city','trip_city_from','trip_from','trip_mode','trip_to','remark'))
        return HttpResponse(data, content_type='application/json')


        # context={"first_name":s.first_name,
        #         "last_name":s.last_name,
        #         "age":year,
        #         "gender":s.male,
        #         "contact":s.phone_number,
        #         "current_interest": s.current_intrest,
        #         "latitude" : s.latitude,
        #         "longitude" :s.longitude,
    
        #         "priv_string": priv_string(request,i,s),
        #         }

        # return HttpResponse(json.dumps(context), content_type='application/json')

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def android_reg_key(request):
    if request.user.is_authenticated() :
        state = "Not Done"
        key = request.GET["key"]
        s = student.objects.filter(user=request.user).first()
        if user_device.objects.filter(dev_id=s):
            ud= user_device.objects.filter(dev_id=s).first()
            ud.reg_key=key
            ud.save()
            state="Device Id changed"
        else:
            ud = user_device(dev_id=s,reg_key=key)
            ud.save()
            state="Done"

        context={"state":state}
        return HttpResponse(json.dumps(context), content_type='application/json')

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")




def android_teammsg(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""
        if request.method=='GET':
            k = request.GET.get('sluge')
            
            j = request.GET.get('slug')
            
            t = team.objects.filter(id=k).first()
            tcht = teamchat(teamc_id=i.id,teamc_group=t.id,teamc_message=j)
            tcht.save()
            print(tcht.teamc_message)
            for l in t.team_memb:

                p = student.objects.filter(id=l).first()
                if p.id != i.id:
                    if user_device.objects.filter(dev_id=p.id):
                        ud = user_device.objects.filter(dev_id=p.id).first() 
                        url='https://fcm.googleapis.com/fcm/send'
                        data ={ "notification": {"title": "Zeeley Team Message",    "text": i.first_name+" : "+j[:20]  },  "to" : str(ud.reg_key),
                                "data":{"team_id":t.id,"sender_id":i.id,"is_photo":tcht.teamc_photo}}
                        header={'Content-Type':'application/json','Authorization':'key=AIzaSyAyv2u2Jg-EaKwriQsXXBFd5ijFoN4xdFs'}
                        r = requests.post(url, data=json.dumps(data), headers=header)
                        
            
            
        ctx= {'z':z}
        return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def android_privacy(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        z=""
        if request.method=='GET':
            if request.GET["hide_map_all"]:
                hma = int(request.GET["hide_map_all"])
                i.priv_map=hma
                z=z+"hma,"
            if request.GET["hide_dist_all"]:
                hda = int(request.GET["hide_dist_all"])
                i.priv_dist=hda
                z=z+"hda,"

            if request.GET["block_prof"]:
                bp = int(request.GET["block_prof"])
                i.block_prof.append(bp)
                z=z+"bp,"
            if request.GET["block_location"]:
                bl = int(request.GET["block_location"])
                i.block_location.append(bl)
                z=z+"bl,"
            if request.GET["block_msg"]:
                bm = int(request.GET["block_msg"])
                i.block_msg.append(bm)
                z=z+"bm,"
            if request.GET["block_invite"]:
                bi = int(request.GET["block_invite"])
                i.block_invite.append(bi)
                z=z+"bi,"

            if request.GET["unblock_invite"]:
                unbi = int(request.GET["unblock_invite"])
                i.block_invite.remove(unbi)
                z=z+"unbi,"
            if request.GET["unblock_prof"]:
                unbp = int(request.GET["unblock_prof"])
                i.block_prof.remove(unbp)
                z=z+"unbp,"
            if request.GET["unblock_location"]:
                unbl = int(request.GET["unblock_location"])
                i.block_location.remove(unbl)
                z=z+"unbl,"
            if request.GET["unblock_msg"]:
                unbm = int(request.GET["unblock_msg"])
                i.block_msg.remove(unbm)
                z=z+"unbm,"
            
            i.save()
            z=z+"Done"


        ctx= {'z':z}
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def priv_string(request,i,stud):
    s = ""
    s = s+str(stud.priv_map)+str(stud.priv_dist)
    if i.id in stud.block_prof:
        s=s+"1"
    else:
        s=s+"0"
    if i.id in stud.block_location:
        s=s+"1"
    else:
        s=s+"0"
    if i.id in stud.block_msg:
        s=s+"1"
    else:
        s=s+"0"
    if i.id in stud.block_invite:
        s=s+"1"
    else:
        s=s+"0"
    return (s)

def android_teamcht(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""
        
        j = request.GET.get('sluge')
        t = team.objects.filter(id=int(j)).first()
        m = teamchat.objects.filter(teamc_group=t.id)
        
        d = len(m)
        if d>15:
            e = d-15
            m = m[e:d]
        else:
            m=m[:d]
        
        data = serializers.serialize('json', m, fields=('id','teamc_id','teamc_message','teamc_photo','teamc_group','post_time'))
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def android_change_password(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        state =""
        if request.method=="GET":
            if request.GET.get('oldpassword'):
                oldpass = request.GET["oldpassword"]
                newpass = request.GET["newpassword"]
                confpass = request.GET["confpassword"]
                u=User.objects.get(username=request.user.username)
                user = authenticate(username=u.username,password=oldpass)
                if user:
                    if newpass==confpass:
                        user.set_password(newpass)
                        user.save()
                        i.status=newpass
                        i.save()
                        user = authenticate(username=u.username,password=newpass)
                        login(request, user)
                        state="Your password has been successfully changed..."
                        
                        
                    else:
                        state="Confirm password doesn't match!!!"
                        
                        
                        
                else:
                    
                    state="Incorrect old Password !!!"
        ctx= {'z':state}
        return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")                


def leave_team(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        state =""
        if request.method=="GET":
            t_id = request.GET["team_id"]
            t=team.objects.filter(id=t_id).first()
            if t.team_category == "Travelling":
                tri = trip.objects.filter(id=t.team_group)
                if tri:
                    tri = tri.first()
                    tri.trip_memb = list(set(tri.trip_memb))
                    if len(tri.trip_memb)>=2:
                        tri.trip_memb.remove(i.id)
                        tri.save()
                    else:
                        tri.delete()
            if t.team_category== "Restaurant":
                tri = trip.objects.filter(id=t.team_group)
                if tri:
                    tri = tri.first()
                    if len(tri.trip_memb)>=2:
                        tri.trip_memb.remove(i.id)
                        tri.save()
                    else:
                        tri.delete()
            if t.team_category== "Gaming":
                tri = trip.objects.filter(id=t.team_group)
                if tri:
                    tri = tri.first()
                    if len(tri.trip_memb)>=2:
                        tri.trip_memb.remove(i.id)
                        tri.save()
                    else:
                        tri.delete()
            if t.team_category== "Film":
                tri = trip.objects.filter(id=t.team_group)
                if tri:
                    tri = tri.first()
                    if len(tri.trip_memb)>=2:
                        tri.trip_memb.remove(i.id)
                        tri.save()
                    else:
                        tri.delete()

            if t.team_category== "Quicky":
                tri = trip.objects.filter(id=t.team_group)
                if tri:
                    tri = tri.first()
                    if len(tri.trip_memb)>=2:
                        tri.trip_memb.remove(i.id)
                        tri.save()
                    else:
                        tri.delete()

            if t.team_category== "Cab_Sharing":
                tri = trip.objects.filter(id=t.team_group)
                if tri:
                    tri = tri.first()
                    if len(tri.trip_memb)>=2:
                        tri.trip_memb.remove(i.id)
                        tri.save()
                    else:
                        tri.delete()

            t.team_memb.remove(i.id)
            if len(t.team_memb)>=1:
                t.team_capt=t.team_memb[0]
                t.save()
            else:
                t.delete()
            
            state="Done"



        ctx= {'z':state}
        return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")                

def team_info(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        data =[]
        if request.method=="GET":
            if request.GET["team_id"]:
                t_id = request.GET["team_id"]
                t = team.objects.filter(id=t_id).first()
                s = student.objects.filter(id__in=t.team_memb).order_by("first_name") 
            if request.GET["trip_id"]:
                t_id = request.GET["trip_id"]
                t = trip.objects.filter(id=t_id).first()
                s = student.objects.filter(id__in=t.trip_memb).order_by("first_name") 
            b=[]
            for j in s:
                
                j.priv_string = priv_string(request,i,j)

                b.append(j)

            data = serializers.serialize('json', b, fields=('id','first_name','current_intrest','priv_string'))
            
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def team_left_info(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        data =[]
        if request.method=="GET":
            if request.GET["team_id"]:
                t_id = request.GET["team_id"]
                t = team.objects.filter(id=t_id).first()
                team_chat = teamchat.objects.filter(teamc_group=t.id)
                stud = []
                for k in team_chat:
                    if k.teamc_id not in stud:
                        stud.append(k.teamc_id)
                stud = list(set(stud+t.team_memb))
                s = student.objects.filter(id__in=stud).order_by("first_name") 
            if request.GET["trip_id"]:
                t_id = request.GET["trip_id"]
                tr = trip.objects.filter(id=t_id).first()
                stud = []
                t = team.objects.filter(team_group=tr.id)
                if t:
                    t=t.first()
                    team_chat = teamchat.objects.filter(teamc_group=t.id)
                    
                    for k in team_chat:
                        if k.teamc_id not in stud:
                            stud.append(k.teamc_id)
                stud = list(set(stud+tr.trip_memb))
                s = student.objects.filter(id__in=stud).order_by("first_name")  
            b=[]
            for j in s:
                
                j.priv_string = priv_string(request,i,j)

                b.append(j)

            data = serializers.serialize('json', b, fields=('id','first_name','priv_string'))
            
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def Fnotif(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        i.n_notif = 0
        i.save()
        
        notif = notification.objects.filter(notif_to=i.id).order_by("-id")
        data = serializers.serialize('json', notif, fields=('notif_from','notif','purpose','purpose_id',"post_time"))
        

        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def raw_prof_pic(request):
    i = student.objects.filter(user=request.user).first()
    c=i.id
    k=len(i.post)+len(i.prof_pic)+len(i.cover_pic)
    if request.method=="POST":
        for count, x in enumerate(request.FILES.getlist("files")):
            def process(f):
                with open('/root/ttl/babaS/media_in_env/media_root/file_q' + str(c)+'_'+str(k+1)+".jpg", 'wb+') as destination:
                
                    for chunk in f.chunks():
                        destination.write(chunk)
                        

            process(x)

        infile1 = '/media/file_q' + str(c)+'_'+str(k+1)+'.jpg'
    else: 
        infile1 = "Not Done"

    ctx= {'z':infile1}
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Homeband(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="band":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[9:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            bands=band.objects.filter(band_group__in=i.groups)

            

            
            if request.method=='POST':


                if request.POST.get('b_name'): 
                    b_name=request.POST["b_name"]
                    b_des=request.POST["b_des"]
                    b_group = request.POST["team_group"]
                    b=band(band_name=b_name,band_descript=b_des,band_group=b_group)

                    b.band_memb.append(i.id)
                    b.save()
                    instrument=request.POST.getlist("intrument")
                    print instrument
                    for ins in instrument:
                        k = str(ins)
                        num=request.POST[k]
                        print num
                        if k=="guitar":
                            b.band_V_guitar=num
                            b.save()

                        if k=="piano":
                            b.band_V_piano=num
                            b.save()
                        if k=="singer":
                            b.band_V_singer=num
                            b.save()
                        if k=="saxophone":
                            b.band_V_saxophone=num
                            b.save()
                        if k=="violin":
                            b.band_V_violin=num
                            b.save()
                        if k=="bass":
                            b.band_V_bass=num
                            b.save()
                        if k=="drum":
                            b.band_V_drum=num
                            b.save()
                        if k=="flute":
                            b.band_V_flute=num
                            b.save()
                        if k=="tabla":
                            b.band_V_tabla=num
                            b.save()
                        if k=="other":
                            b.band_V_other=num
                            b.band_other = request.POST["b_other"]
                            b.save()

                    

                    for count, x in enumerate(request.FILES.getlist("files")):
                        def process(f):
                            with open('/root/ttl/babaS/media_in_env/media_root/file_band_' +str(b.id)+'.jpg', 'wb+') as destination:
                                for chunk in f.chunks():
                                    destination.write(chunk)

                        process(x)

                        infile='/root/ttl/babaS/media_in_env/media_root/file_band_' +str(b.id)+'.jpg'
                        thumb(infile)




                    state="Your band has been created... Now rock the world with your talent..."
                    messages.success(request,state)
                    return HttpResponseRedirect("/homeband/")
                    




                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            #k = i.shared_posts
            page=intrests.objects.filter(intrest_name=i.current_intrest).first()
            if page:
                pc=page.intrest_category
            else:
                pc="pg"
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "bands" : bands,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" :pc,
                "gr" : gr,
                #"form" : form
            }
            return render(request,"homeband.html",context)

        else:
            state="Make a Band as your Intrest!!!"
            messages.error(request,state)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def Homestartup(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="startup":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            allpost=posts.objects.filter(reduce(operator.or_, mylist)).exclude(anonymous=True).order_by("-id")
            f=allpost[0:9]
            other = allpost[9:]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            bands=startup.objects.filter(startup_group__in=i.groups)

            

            
            if request.method=='POST':


                if request.POST.get('stup_name'): 
                    stup_name=request.POST["stup_name"]
                    stup_des=request.POST["stup_des"]
                    stup_group=request.POST["team_group"]
                    b=startup(startup_name=stup_name,startup_descript=stup_des,startup_group=stup_group)
                    b.startup_memb.append(i.id)
                    b.save()
                    design=request.POST.getlist("design")
                    print design
                    for ins in design:
                        k = str(ins)
                        num=request.POST[k]
                        print num
                        if k=="ceo":
                            b.startup_V_ceo=num
                            b.save()
                        if k=="techop":
                            b.startup_V_techop=num
                            b.save()
                        if k=="sales":
                            b.startup_V_salesmark =num
                            b.save()
                        if k=="hr":
                            b.startup_V_hr =num
                            b.save()
                        if k=="busdev":
                            b.startup_V_busdev=num
                            b.save()
                        if k=="custser":
                            b.startup_V_custser=num
                            b.save()
                        if k=="salesman":
                            b.startup_V_salesman=num
                            b.save()
                        if k=="rnd":
                            b.startup_V_rnd=num
                            b.save()
                        if k=="adm":
                            b.startup_V_adm=num
                            b.save()
                        if k=="other":
                            b.startup_V_other=num
                            b.startup_other = request.POST["stup_other"]
                            b.save()
                    

                    for count, x in enumerate(request.FILES.getlist("files")):
                        def process(f):
                            with open('/root/ttl/babaS/media_in_env/media_root/file_startup_' +str(b.id)+'.jpg', 'wb+') as destination:
                                for chunk in f.chunks():
                                    destination.write(chunk)

                        process(x)

                        infile='/root/ttl/babaS/media_in_env/media_root/file_startup_' +str(b.id)+'.jpg'
                        thumb(infile)

                    state="Your startup is set... Now change the world with your idea..."
                    messages.success(request,state)
                    return HttpResponseRedirect("/homestartup/")

                    




                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = all_memb(request)
            page=intrests.objects.filter(intrest_name=i.current_intrest).first()
            if page:
                pc=page.intrest_category
            else:
                pc="pg"

            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "other" : other,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "bands" : bands,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" :pc,
                "gr" : gr,
                #"form" : form
            }
            return render(request,"homestartup.html",context)

        else:
            state="Make a Startup as your Intrest!!!"
            messages.error(request,state)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



def scroll_following(request):
    if request.user.is_authenticated() :
        s=student.objects.filter(user=request.user).first()
        
        if request.method=="GET":
            me = request.GET["me"]
            i = student.objects.filter(id=int(me)).first()
            follow_from = request.GET["from"]
            a = student.objects.filter(id__in=i.f_list)
            if len(a) >int(follow_from)+12:
                a = a[(len(a)-int(follow_from)-12):(len(a)-int(follow_from))]
            elif len(a) >=int(follow_from):
                a = a[:len(a)-int(follow_from)]
            
            data = serializers.serialize('json', a, fields=('id','first_name','current_intrest','profile_pic'))
            
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def scroll_suggestions(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        if request.method=="GET":
            
            
            follow_from = request.GET["from"]
            a = student.objects.all().exclude(id__in=i.f_list)
            if len(a) >int(follow_from)+12:
                a = a[(len(a)-int(follow_from)-12):(len(a)-int(follow_from))]
            elif len(a) >=int(follow_from):
                a = a[:len(a)-int(follow_from)]

            data = serializers.serialize('json', a, fields=('id','first_name','current_intrest','profile_pic'))
            
        return HttpResponse(data, content_type='application/json')

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def android_team(request):
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        m = team.objects.filter(team_memb__contains=i.id)
        # email = EmailMessage('Hi Mohan', 'Email received..??', to=['getmohan007@gmail.com'])
        # email.send()
        
        # server = smtplib.SMTP('smtp.gmail.com')
        # server.starttls()
        # server.login('zeeley12@gmail.com','raftar12')
        # msg = "Congratulations.... \nDear Sir, \nYou have been appointed as the CDO,ZeeleyInc-2016. You appointment letter is on the way. Please accept the proposal and attend the joining ceremony on the date written on the letter. We welcome your auspicious presence in our company and hope yuo will prove yourself asset to us. Thanking You, ZeeleyInc-2016. "
        # server.sendmail('zeeley12@gmail.com','gaveesh.bhardwaj.civ13@itbhu.ac.in',msg)
        # server.quit()



        data = serializers.serialize('json', m , fields=('id','team_name','team_capt','team_category','team_group',))
        # else:
        #     data= {
        #         'id':m.first().id,
        #         'team_name':m.first().team_name,
        #         'team_capt':m.first().team_capt,
        #         'team_memb':m.first().team_memb,
        #         'team_category':m.first().team_category,
        #         'team_group':m.first().team_group,
        #         'team_invites':m.first().team_invites,
        #         }     
        return HttpResponse(data, content_type='application/json')
    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def near_by(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user = request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = student.objects.filter(id__in=i.f_list)
        m=[]
        zipped=[]
        dist = 15000
        if request.method=="GET":
            if request.GET["interest"]:
                interest = request.GET["interest"]
                m = intrests.objects.filter(intrest_name=interest).first()
                zipped = localite(request,interest,dist)
        else:
            m = intrests.objects.filter(intrest_name=i.current_intrest).first()
            zipped = localite(request,i.current_intrest,dist)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            # "your_trip" :my_trip,"myteam":myteam,
            # "trips":more,
            # "near_trips":near_trips,
            "m" : m.intrest_name,
            "zip":zipped,
            
        }

        return render(request,"nearby.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def notification_panel(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user = request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = student.objects.filter(id__in=i.f_list)
        
        
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            # "your_trip" :my_trip,"myteam":myteam,
            # "trips":more,
            # "near_trips":near_trips,
            
            
        }

        return render(request,"notification.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def msg_panel(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user = request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = student.objects.filter(id__in=i.f_list)
        
        
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            # "your_trip" :my_trip,"myteam":myteam,
            # "trips":more,
            # "near_trips":near_trips,
            
            
        }

        return render(request,"msgpanel.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def android_fblogin(request):
    if request.method=='GET':
        gmail = request.GET['gmail']
        z=""
        if gmail=="":
            fbid = request.GET['fbid']
            gmail = str(fbid)+"@zeeley.com"
        if User.objects.filter(username=str(gmail)):
            Us = User.objects.filter(username=str(gmail)).first()
            i=student.objects.filter(user_id =Us.id )
            if i:
                i= i.first()
                user = authenticate(username=Us.username, password=i.status)
                if not user:
                    return HttpResponse(Us.username+","+Us.password)
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                i.logout=timezone.now()
                i.save()
                j=[]
                m = message.objects.filter(m_id=i.id).values("m_friend").distinct()
                print m
                for k in mess:
                    print k["m_friend"]
                    j.append(k["m_friend"])
                j.append(i.id)
            
                a = student.objects.filter(id__in=j).order_by("-logout")
                b=[]
                for j in a:
                    
                    j.priv_string = priv_string(request,i,j)

                    b.append(j)

                
                
                data = serializers.serialize('json', b, fields=('id',"logout",'first_name','last_name','current_intrest','priv_string'))
                
                return HttpResponse(data, content_type='application/json')

            else:
                Us.delete()
                first_name = request.GET['first_name']
                last_name = request.GET['last_name']
                date=request.GET.get('date')
                month=request.GET.get('month')
                year=request.GET.get('year')

                profile_pic = request.GET['prof_pic']

                male = request.GET.get('male')
                password = str(datetime.now())
                User.objects.create_user(username=gmail,first_name="f",password=password,email=gmail)
                Us=User.objects.get(username=gmail)
                    #### 
                
                    
                s=student(user=Us,status=password,last_name="",first_name=first_name+" "+last_name,date=date,month=month,year=year,GET=[])
                s.save()
                s.prof_pic.append("0")
                s.cover_pic.append("0")
                find = intrests.objects.filter(intrest_name__exact="Chat").first()
                change_interest(request,s,find)
                s.tagword = gmail

                

                # infile1 = '/root/ttl/babaS/media_in_env/media_root/defaultcover.jpg'
                # outfile1 = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

                
                # if infile1 != outfile1:
                #     try:
                #         im = Image.open(infile1)
                #         im.thumbnail((1600,900),Image.ANTIALIAS)
                #         im.save(outfile1, "JPEG")
                #     except IOError:
                #         print "cannot create thumbnail for", infile1

                
                

                # for c in a:
                #     c.add_f_list.append(s.id)
                #     c.save()
                i = student.objects.filter(id=320).first()

                make_frnd(request,s,i)

                if male == "male":
                    s.male = True
                    s.save()
                if male=="female":
                    s.female = True
                    s.save()
                

                outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                urllib.urlretrieve(profile_pic,outfile)
                urllib.urlretrieve(profile_pic,outfile2)   

                # if s.male:
                #     infile = '/root/ttl/babaS/media_in_env/media_root/default.jpg'
                #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                    
                #     if infile != outfile:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                #     if infile != outfile2:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile2, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                # else:
                #     infile = '/root/ttl/babaS/media_in_env/media_root/defaultfemale.jpg'
                #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
                #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                    
                    
                #     if infile != outfile:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                #     if infile != outfile2:
                #         try:
                #             im = Image.open(infile)
                #             im.thumbnail((250,250),Image.ANTIALIAS)
                #             im.save(outfile2, "JPEG")
                #         except IOError:
                #             print "cannot create thumbnail for", infile

                thumb(outfile)
                # thumb(outfile1)
                thumb(outfile2)
                user = authenticate(username=gmail, password=password)
                login(request, user)
                z="Done"

        


            
        else:
            first_name = request.GET['first_name']
            last_name = request.GET['last_name']
            date=request.GET.get('date')
            month=request.GET.get('month')
            year=request.GET.get('year')

            profile_pic = request.GET['prof_pic']

            male = request.GET.get('male')
            password = str(datetime.now())
            User.objects.create_user(username=gmail,first_name="f",password=password,email=gmail)
            Us=User.objects.get(username=gmail)
                #### 
            
            s=student(user=Us,status=password,last_name="",first_name=first_name+" "+last_name,date=date,month=month,year=year,GET=[])
            s.save()
            s.prof_pic.append("0")
            s.cover_pic.append("0")
            find = intrests.objects.filter(intrest_name__exact="Chat").first()
            change_interest(request,s,find)
            s.tagword = gmail

            

            # infile1 = '/root/ttl/babaS/media_in_env/media_root/defaultcover.jpg'
            # outfile1 = '/root/ttl/babaS/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

            
            # if infile1 != outfile1:
            #     try:
            #         im = Image.open(infile1)
            #         im.thumbnail((1600,900),Image.ANTIALIAS)
            #         im.save(outfile1, "JPEG")
            #     except IOError:
            #         print "cannot create thumbnail for", infile1

            
            

            # for c in a:
            #     c.add_f_list.append(s.id)
            #     c.save()
            i = student.objects.filter(id=320).first()

            make_frnd(request,s,i)

            if male == "male":
                s.male = True
                s.save()
            if male=="female":
                s.female = True
                s.save()
            

            outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
            urllib.urlretrieve(profile_pic,outfile)
            urllib.urlretrieve(profile_pic,outfile2)   

            # if s.male:
            #     infile = '/root/ttl/babaS/media_in_env/media_root/default.jpg'
            #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
            #     if infile != outfile:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            #     if infile != outfile2:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile2, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            # else:
            #     infile = '/root/ttl/babaS/media_in_env/media_root/defaultfemale.jpg'
            #     outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'
            #     outfile2 = '/root/ttl/babaS/media_in_env/media_root/file_' + str(s.id) +'.jpg'
                
                
            #     if infile != outfile:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            #     if infile != outfile2:
            #         try:
            #             im = Image.open(infile)
            #             im.thumbnail((250,250),Image.ANTIALIAS)
            #             im.save(outfile2, "JPEG")
            #         except IOError:
            #             print "cannot create thumbnail for", infile

            thumb(outfile)
            # thumb(outfile1)
            thumb(outfile2)
            user = authenticate(username=gmail, password=password)
            login(request, user)
            z="DONE"

        ctx={"state":z}    
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    
def forgot_password(request):
    if request.method=="GET":
        email=request.GET["email"]
        z=""
        if User.objects.filter(username=str(email)):
            Us = User.objects.filter(username=str(email)).first()
            i=student.objects.filter(user_id =Us.id )
            d=str(datetime.now())
            Us.last_name=d
            Us.save()
            link = "zeeley.com/confirm_mail/?q="+d+"@."+str(i.id)+"@."+str(Us.id)

            server = smtplib.SMTP('smtp.gmail.com')
            server.starttls()
            server.login('zeeley12@gmail.com','raftar12')
            msg = "Congratulations.... \nDear Sir, \nPlease click on the link or copy the link and open it to confirm your identity and to get a new password for your zeeley account."+"\n"+link
            server.sendmail('zeeley12@gmail.com',email,msg)
            server.quit()
            state="An email has been sent to your mail. Please check your inbox to confirm your identity and get a new password for your zeeley account."
            messages.success(request,state)
            z="Done"
        else:
            state="Something went wrong. Please contact Zeley Admin."
            messages.success(request,state)
        ctx={"state":z}    
        return HttpResponse(json.dumps(ctx), content_type='application/json')


def confirm_mail(request):
    if request.method=="GET":
        z=""
        otp=""
        q=request.GET["q"]
        r=q.split("@.")
        if r[1]:
            i = student,objects.filter(id=int(r[1])).first()
            Us = User.objects.filter(id=int(r[2])).first()
            if Us.last_name==r[0]:
                state="Your account is verified successfully. Please change your password."
                messages.success(request,state)
                user = authenticate(username=u.username,password=i.status)
                login(request, user)
                Us.last_name=""
                Us.save()
                z="Done"
                otp = i.status
            else:
                state="This link has been used."
                messages.success(request,state)
                
            
        ctx={"state":z,"otp":otp}    
        return HttpResponse(json.dumps(ctx), content_type='application/json')

        



def getprofile_pic_conf(request):
    a = student.objects.all()
    for k in a:
        #profile_pic = 'http://www.zeeley.com:8523/media/file_p' + str(k.id)+"_"+str(k.prof_pic[-1])+'.jpg'
        # profile_pic = 'http://www.zeeley.com/media/file_' + "17" +'.jpg'
        # outfile = '/root/mohan/lifer/'+'1.jpg'
        l=0
    

        if k.prof_pic:
        
            v =  checkUrl('http://www.zeeley.com:8523/media/file_p' + str(k.id)+"_"+str(k.prof_pic.pop())+'.jpg') # True
        else:
            k.prof_pic.append(0)
            v=False


    
        print("Halua")
        if not v:
            l=l+1
            infile = '/root/ttl/babaS/media_in_env/file_' + "28" +'.jpg'
            outfile = '/root/ttl/babaS/media_in_env/media_root/file_p' + str(k.id)+"_"+str(k.prof_pic.pop())+'.jpg'
            with Image.open(infile) as im1:
                result = im1.copy()
                result.save(outfile,"JPEG")

            outfile1 = os.path.splitext(outfile)[0] + ".thumbnail"
            if outfile != outfile1:
                try:
                    im = Image.open(outfile)
                    im.thumbnail((128,128),Image.ANTIALIAS)
                    im.save(outfile1, "JPEG")
                except IOError:
                    print "cannot create thumbnail for", outfile
    

    return HttpResponse("DONE"+str(l))

def valid_url(request,urls):
    val = URLValidator(verify_exists=False)
    try:
        val('http://www.google.com')
        return(True)
    except ValidationError, e:
        print e
        return(False)



import httplib
from urlparse import urlparse

def checkUrl(url):
    p = urlparse(url)
    conn = httplib.HTTPConnection(p.netloc)
    conn.request('HEAD', p.path)
    resp = conn.getresponse()
    return resp.status < 400

def refresh_all_teams(request):
    a = team.objects.all()
    z=""
    for k in a:
        for c in k.team_invites:
            if not notification.objects.filter(notif_to=c,purpose_id=k.id,purpose=k.team_category):
                k.team_invites.remove(c)
                z = z+str(c) 
                k.save()
            else:
                z=z+str(k.team_name)+" has no such\n"

    return HttpResponse(z)

def your_group(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user = request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = student.objects.filter(id__in=i.f_list)
        myteam = team.objects.filter(team_memb__contains=i.id)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            # "your_trip" :my_trip,"myteam":myteam,
            # "trips":more,
            # "near_trips":near_trips,
            # "m" : m.intrest_name,
            # "zip":zipped,
            "myteam":myteam,
            
        }

        return render(request,"your_group.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")

def location(request):
    if request.user.is_authenticated():
        z = request.GET["z"]
        i = student.objects.filter(user = request.user).first()
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name
        a = student.objects.filter(id__in=i.f_list)
        # myteam = team.objects.filter(team_memb__contains=i.id)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "z" : z,
            # "your_trip" :my_trip,"myteam":myteam,
            # "trips":more,
            # "near_trips":near_trips,
            # "m" : m.intrest_name,
            # "zip":zipped,
            # "myteam":myteam,
            
        }

        return render(request,"location.html",context)

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")


def delete_all(request):
    # a = team.objects.filter(id=140).first()
    # a.delete()
    # i=0
    # for k in a:
    #     k.delete()
    #     i=i+1
    return HttpResponse(" Dones")


#+15175681207
def getmail(request):

    context = {
    }

    return render(request,"getmail.html",context)

def delete_account(request):
    blocked =[1006]
    z=""
    for k in blocked:
        if student.objects.filter(id=k):
            s = student.objects.filter(id=k).first()
            sf = student.objects.filter(f_list__contains=k)
            saf = student.objects.filter(accept_f_list__contains=k)
            m = message.objects.filter(Q(m_id=k) | Q(m_friend=k))
            u = User.objects.filter(id=s.user_id).first()
            p = posts.objects.filter(identity=k)
            im = intrests.objects.filter(intrest_memb_id__contains=k)
            # ip = intrests.objects.filter(intrest_posts__contains=k)
            tr = trip.objects.filter(trip_memb__contains=k)
            te = team.objects.filter(team_memb__contains=k)
            tei = team.objects.filter(team_invites__contains=k)
            tri = trip.objects.filter(trip_invites__contains=k)
            pc = posts.objects.filter(comnt__contains=k)
            ph = posts.objects.filter(hitters__contains=k)
            poscom = postcomment.objects.filter(Q(c_identity=k) | Q(c_intity=k))

            for l in sf:
                if k in l.f_list:
                    l.f_list.remove(k)
                    l.save()
            for saf_n in saf:
                if k in saf_n.accept_f_list:
                    saf_n.accept_f_list.remove(k)
                    saf_n.save()
            for m_n in m:
                m_n.delete()
            for im_n in im:
                if k in im_n.intrest_memb_id:
                    im_n.intrest_memb_id.remove(k)
                    im_n.save()
            for tr_n in tr:
                if k in tr_n.trip_memb:
                    tr_n.trip_memb.remove(k)
                    tr_n.save()
            for te_n in te:
                if k in te_n.team_memb:
                    te_n.team_memb.remove(k)
                    te_n.save()
            for tei_n in tei:
                if k in tei_n.team_invites:
                    tei_n.team_invites.remove(k)
                    tei_n.save()
            for tri_n in tri:
                if k in tri_n.trip_memb:
                    tri_n.trip_memb.remove(k)
                    tri_n.save()
            for pc_n in pc:
                if k in pc_n.comnt:
                    pc_n.comnt.remove(k)
                    pc_n.comments =pc_n.comments-1
                    pc_n.save()
            for ph_n in ph:
                if k in ph_n.hitters:
                    ph_n.hitters.remove(k)
                    ph_n.hits =ph_n.hits-1
                    ph_n.save()
            for poscom_n in poscom:
                poscom.delete()
            for p_n in p:
                p_n.delete()
            z = s.first_name+" "+z
            s.delete()
            u.delete()

        else:
            z="No "+z

    return HttpResponse(z)
# 9415256154:Paritosh       
def account_activation(request):
    if request.method=="GET":
        email="mohan.ksingh.civ13@itbhu.ac.in"
        z=""
        if User.objects.filter(username=str(email)):
            Us = User.objects.filter(username=str(email)).first()
            i=student.objects.filter(user_id =Us.id ).first()
            d=str(datetime.now())
            Us.last_name=d
            Us.is_active=False
            Us.save()
            link = "zeeley.com/confirm_mail/?q="+d+"@."+str(i.id)+"@."+str(i.user_id)
            my_mail='zeeley12@gmail.com'
            passw = 'raftar12'
            # server = smtplib.SMTP('smtp.gmail.com')
            # server.ehlo()
            # server.starttls()
            # server.login(my_mail,passw)
            msg = "Congratulations.... \nDear Sir, \nPlease click on the link or copy the link and open it to confirm your identity and to activate your zeeley account."+"\n"+link
            # BODY = '\r\n'.join(['To: %s' % email,'From: %s' % my_mail,'Subject: Zeeley | Account Activation' ,'', msg])
            send_mail('Zeeley | Account Activation',msg,my_mail,[email])                    
                                
                                
            # server.sendmail('zeeley12@gmail.com',[email],BODY)
            # server.quit()
            state="An email has been sent to your mail. Please check your inbox to confirm your identity and get a new password for your zeeley account."
            # messages.success(request,state)
            z=" Done"
        else:
            state="Something went wrong. Please contact Zeley Admin."
            # messages.success(request,state)
        ctx={"state":z}    
        return HttpResponse(state+z)


def confirm_account(request):
    if request.method=="GET":
        z=""
        otp=""
        q=request.GET["q"]
        r=q.split("@.")
        if r[1]:
            if student.objects.filter(id=int(r[1])):
                i = student.objects.filter(id=int(r[1])).first()
                if User.objects.filter(id=int(r[2])):
                    Us = User.objects.filter(id=int(r[2])).first()
                    if Us.last_name==r[0] and Us.id==i.user_id:
                        state="Your account is verified successfully. Please change your password."
                        messages.success(request,state)
                        Us.is_active=True
                        Us.save()
                        user = authenticate(username=u.username,password=i.status)
                        login(request, user)
                        Us.last_name=""
                        Us.save()
                        z="Done"
                        
                    else:
                        state="This link has been used."
                        messages.success(request,state)
                
            
        ctx={"state":z,}    
        return HttpResponse(json.dumps(ctx), content_type='application/json')


def contact(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user = request.user).first()
        i.phone_number=request.GET["contact"]

        i.save()


        return HttpResponse(json.dumps({}), content_type='application/json')

    else:
        state = "Login First..."
        messages.success(request,state)
        return HttpResponseRedirect("/")



def send_sms(request,i,msg):
    # q=request.GET["q"]
    client = TwilioRestClient("AC2d589c7205fef27e92ae62c73b10ebed", "a84241eea03f502b508f156ca3989f38")
    client.messages.create(to="+91"+"9695688685", from_="#+15175681207", 
                       body=msg)
    return "Done"




#+14696153119

def order_cab(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user = request.user).first()
        trip_id = request.GET["trip_id"]
        z=""
        if trip.objects.filter(id=trip_id):

            t_share = trip.objects.filter(id=trip_id).first()
            team_to = team.objects.filter(team_group=t_share.id).first()
            zeeley = student.objects.filter(id=320).first()
            zeeley.n_notif = zeeley.n_notif+1
            zeeley.save()
            z_notif_msg = str(i.first_name.split(" ")[0]) + " has ordered a cab"
            z_new_notif = notification(notif_from =i.id,notif_to=zeeley.id,notif=z_notif_msg,purpose="",purpose_id=t_share.id)
            z_new_notif.save()
            team_to.team_memb.append(zeeley.id)
            team_to.save()
            msg=""
            msg = msg+i.first_name +" has booked a cab from "+str(t_share.trip_city_from)+" to "+str(t_share.trip_city)+" . Members are : "
            for memb in t_share.trip_memb:
                member = student.objects.filter(id=memb).first()
                msg = msg+ "("+member.first_name.split(" ")[0]+","+str(member.phone_number)+"), "

            send_sms(request,zeeley,msg)
            t_share.booking=True
            t_share.save()
            z="Done"

        ctx={"z":z,}    
        return HttpResponse(json.dumps(ctx), content_type='application/json')







