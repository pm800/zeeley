from django.shortcuts import render, render_to_response
from .forms import regs_form
from .forms import student_form,trip_form,quick_form
from .models import student,posts,postcomment,message,create_group,create_city,intrests,create_subjects,team,player,trip,band,groupchat,startup,teamchat,game,quick
from .models import regs
from django.conf import settings
from django.http import HttpResponse
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q
import operator
import os,sys
from PIL import Image
from datetime import datetime
from django.utils import timezone
from django.core import serializers
#from PIL import ThePIL

try:
    from django.utils import simplejson as json
except ImportError:
    import json

import os,sys

# Create your views here.


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def edit(request):
    form = regs_form(request.POST or None)
    head = 'Edit Your Profile'
    title="Welcome"
    active = True
    u = regs.objects.filter(id=2).first()
    if request.method=='POST':
        post = request.POST
        
        u.First_Name = post["First_Name"]

        u.Last_Name = post["Last_Name"]
        u.City = post["City"]

        u.save(update_fields=['First_Name','Last_Name','City'])


    title = u.First_Name+" "+u.Last_Name

    # q = regs.objects.get(id=0)
    # # q.First_Name = i.First_Name
    # q.delete()
    
    #i=form.save(commit=False)
    # # m=i.save()
    # # regs.objects.all()
    # n=i.id
    # fn =i.First_Name
    # ln =i.Last_Name
    # ci =i.City
    # i.id=n
    # i.First_Name=fn
    # i.Last_Name=ln
    # i.City=ci



    # if form.is_valid():
    #     i.First_Name = form.cleaned_data.get("First_Name")
    #     i.Last_Name = form.cleaned_data.get("Last_Name")
    #     i.City = form.cleaned_data.get("City")

    # print i.First_Name

    # #i = form(First_Name="Nitin", Last_Name = "Vishwari", City ="Varanasi")
    # # first_name = form.cleaned_data.get("First_Name")
    # # if not first_name:
    # #     first_name = "User"
    #i.save()
    
    # i.First_Name = first_name
    
    
    # first_name=i.First_Name
    # if first_name:
    

    # q=regs.objects.get(id=1)
    # q.First_Name=title
    # q.save()

    context = {
        "title" : title,
        "counter" : counter,
        "form" : form,
        "head" : head
    }


    # if form.is_valid(): 
    #     instance = form.save(commit=False)
    # # if not instance._name:
    # #     instance.full_name = "mohan"
    #     instance.save()
    #     context ={
    #         "title" : "Thank You!!!"
    #     }
    return render(request,"edit_prof.html",context)
def prof(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        f = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).order_by("-id")[0:9]
        
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        if request.method=='POST':
            
            if request.POST.get('searchbox3'):
                ci = request.POST.get('searchbox3')
                # v = posts.objects.filter(photos=h).first()
                # i.current_intrest = ci
                # i.save()
                # j=intrests.objects.filter(intrest_name=ci).first()
                # if not c in j.intrest_memb_id:
                #     j.intrest_memb_id.append(c)
                #     j.save()


                # if j.intrest_category=="pg" or j.intrest_category=="group"  :
                #     if not player.objects.filter(p_id=c,p_game=ci):
                #         p=player(p_id=c,p_game=ci)
                #         p.save()

                find=intrests.objects.filter(intrest_name=ci).first()
                if find:
                    i.current_intrest = ci
                    i.interest.append(find.id)
                    i.save()
                    

                    if not c in find.intrest_memb_id:
                        find.intrest_memb_id.append(c)
                        find.save()

                    if find.intrest_category=="pg" or find.intrest_category=="group" :
                        if not player.objects.filter(p_id=c,p_game=ci):
                            p=player(p_id=c,p_game=ci)
                            p.save()

                    state="Now Your current interest is "+str(find.intrest_name)
                    messages.success(request,state)


                    return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                else:
                    state="No such interest found !!!"
                    messages.error(request,state)
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


            if request.POST.get('work'):
                work = request.POST.get('work')
                # v = posts.objects.filter(photos=h).first()
                i.work = work
                i.save()
            if request.POST.get('hometown'):
                hometown = request.POST.get('hometown')
                # v = posts.objects.filter(photos=h).first()
                i.hometown = hometown
                i.save()
            if request.POST.get('currentplace'):
                currentplace = request.POST.get('currentplace')
                # v = posts.objects.filter(photos=h).first()
                i.current_place = currentplace
                i.save()

        
        if i.male:
            l="his"

        if i.female:
            l="her"

        active = True
        t = i.post
        
        a = student.objects.filter(id__in=i.f_list)
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        if len(i.cover_pic)>0:
            j=i.cover_pic[len(i.cover_pic)-1]
        else:
            j=0
        if len(i.prof_pic)>0:
            s=i.prof_pic[len(i.prof_pic)-1]
        else:
            s=0

        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"








        
        context = {
            "title" : title,
            "counter" : counter,
            
            "count" :i.id,
            "t" : t,
            "l" : l,
            "f" : f,
            "i" : i,
            "e" : i.f_list,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "s" : s,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "page" : pc,
            #"form" : form
        }


        return render(request,'profile1.html',context)



        #response = render_to_response("profile1.html",context)


        # visits = int(request.COOKIES.get('visits', '0'))

        # # Does the cookie last_visit exist?
        # if 'last_visit' in request.COOKIES:
        #     # Yes it does! Get the cookie's value.
        #     last_visit = request.COOKIES['last_visit']
        #     # Cast the value to a Python date/time object.
        #     last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        #     # If it's been more than a day since the last visit...
        #     if (datetime.now() - last_visit_time).seconds > 10:
        #         # ...reassign the value of the cookie to +1 of what it was before...
        #         response.set_cookie('visits', visits+1)
        #         print visits
        #         # ...and update the last visit cookie, too.
        #         response.set_cookie('last_visit', datetime.now())
        # else:
        #     # Cookie last_visit doesn't exist, so create it to the current date/time.
        #     response.set_cookie('last_visit', datetime.now())
        #     print "set_cookie"








        #return response





    else:
        return HttpResponse("Login first!!!")


    
    


def Upload(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    k=len(i.post)+len(i.prof_pic)+len(i.cover_pic)
    p="/home/raftar/ttl/baba5/media_in_env/media_root/file_p" + str(c)+".jpg"

    #os.rename(p,p+".k"+str(k+1))
    i.prof_pic.append(k+1)
    i.save()
    m=posts(identity=c,photos=k+1,hits=0,comments=0,profile_picture=True)
    m.save()
    for count, x in enumerate(request.FILES.getlist("files")):
        def process(f):
            with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_p' + str(c)+'_'+str(k+1)+".jpg", 'wb+') as destination:
                with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_' + str(c)+'.jpg', 'wb+') as destination1:
                    for chunk in f.chunks():
                        destination.write(chunk)
                        destination1.write(chunk)

        process(x)

        infile = '/home/raftar/ttl/baba5/media_in_env/media_root/file_' + str(c)+'.jpg'

        outfile = os.path.splitext(infile)[0] + ".thumbnail"
        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail((128,128),Image.ANTIALIAS)
                im.save(outfile, "JPEG")
            except IOError:
                print "cannot create thumbnail for", infile

        infile1 = '/home/raftar/ttl/baba5/media_in_env/media_root/file_p' + str(c)+'_'+str(k+1)+'.jpg'

        outfile1 = os.path.splitext(infile1)[0] + ".thumbnail"
        if infile1 != outfile1:
            try:
                im = Image.open(infile1)
                im.thumbnail((128,128),Image.ANTIALIAS)
                im.save(outfile1, "JPEG")
            except IOError:
                print "cannot create thumbnail for", infile1
        


    return HttpResponseRedirect('/prof/')


def Upload_cover(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    l=len(i.post)+len(i.prof_pic)+len(i.cover_pic)
    #p="/home/raftar/ttl/baba5/media_in_env/media_root/file_c" + str(c)+'.jpg'
    #os.rename(p+".0",p+"."+str(l+1))
    i.cover_pic.append(l+1)
    i.save()
    m=posts(identity=c,photos=l+1,hits=0,comments=0,cover_picture=True)
    m.save()
    for count, x in enumerate(request.FILES.getlist("files")):
        def process(f):
            with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_c' + str(c)+'_'+str(l+1)+'.jpg', 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

        process(x)

        infile = '/home/raftar/ttl/baba5/media_in_env/media_root/file_c' + str(c) +'_'+str(l+1) +'.jpg'

        outfile = os.path.splitext(infile)[0] + ".thumbnail"


        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail((300,300),Image.ANTIALIAS)
                im.save(outfile, "JPEG")
            except IOError:
                print "cannot create thumbnail for", infile
        
        do=1
        return HttpResponseRedirect('/covfix/')


def Upload_post(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    t = student.objects.get(id=c)
    p=len(t.post)+len(t.prof_pic)+len(t.cover_pic)
    t.post.append(p+1)
    t.save()
    cat=""
    if request.method=='POST':
        if request.POST.get("category"):
            cat = request.POST["category"]
        
        if request.POST["say"]:
            say= request.POST["say"]
            if cat:
                say=say+" #"+cat+" "
           

            if request.FILES.getlist("files"):

                m=posts(identity=c,photos=p+1,hits=0,comments=0,say=say,category=cat)
                m.save()
            else:
                m=posts(identity=c,photos=-p-1,hits=0,comments=0,say=say,category=cat)
                m.save()

            if "#" in say:
                said=say.split("#")
                saying=said[1].split(" ")
                if intrests.objects.filter(Q(intrest_name__iexact=saying[0])|Q(intrest_name__iexact=cat)):
                    link_intrst=intrests.objects.get(Q(intrest_name__iexact=saying[0])|Q(intrest_name__iexact=cat))
                    link_intrst.intrest_posts.append(m.id)
                    link_intrst.save()

                if create_city.objects.filter(Q(city_name__iexact=saying[0])|Q(city_name__iexact=cat)):
                    link_city=create_city.objects.get(Q(city_name__iexact=saying[0])|Q(city_name__iexact=cat))
                    link_city.city_posts.append(m.id)
                    link_city.save()

                if create_subjects.objects.filter(Q(subjects_name__iexact=saying[0])|Q(subjects_name__iexact=cat)):
                    link_subjects=create_subjects.objects.get(Q(subjects_name__iexact=saying[0])|Q(subjects_name__iexact=cat))
                    link_subjects.subjects_posts.append(m.id)
                    link_subjects.save()
            if request.POST.get("onoffswitch"):
                if request.POST["onoffswitch"]=="on":
                    print request.POST["onoffswitch"]
                    m.anonymous=True
                    m.save()
                else:
                    m.anonymous=False
                    m.save()

            
        elif request.FILES.getlist("files"):
            m=posts(identity=c,photos=p+1,hits=0,comments=0,category=cat)
            m.save()
            if cat:
                if intrests.objects.filter(intrest_name__iexact=cat):
                    link_intrst=intrests.objects.get(Q(intrest_name__iexact=saying[0])|Q(intrest_name__iexact=cat))
                    link_intrst.intrest_posts.append(m.id)
                    link_intrst.save()

                if create_city.objects.filter(city_name__iexact=cat):
                    link_city=create_city.objects.get(Q(city_name__iexact=saying[0])|Q(city_name__iexact=cat))
                    link_city.city_posts.append(m.id)
                    link_city.save()

                if create_subjects.objects.filter(subjects_name__iexact=cat):
                    link_subjects=create_subjects.objects.get(Q(subjects_name__iexact=saying[0])|Q(subjects_name__iexact=cat))
                    link_subjects.subjects_posts.append(m.id)
                    link_subjects.save()
            if request.POST.get("onoffswitch"):
                if request.POST["onoffswitch"]=="on":
                    print request.POST["onoffswitch"]
                    m.anonymous=True
                    m.save()

                else:
                    m.anonymous=False
                    m.save()

        else:
            return HttpResponse("No post to upload")

    
    

    for count, x in enumerate(request.FILES.getlist("files")):
        def process(f):
            with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_' + str(c)+ '_'+ str(p+1)+'.jpg', 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

        process(x)


        infile = '/home/raftar/ttl/baba5/media_in_env/media_root/file_' + str(c) +'_'+str(p+1) +'.jpg'

        outfile = os.path.splitext(infile)[0] + ".thumbnail"
        if infile != outfile:
            try:
                im = Image.open(infile)
                im.thumbnail((128,128),Image.ANTIALIAS)
                im.save(outfile, "JPEG")
            except IOError:
                print "cannot create thumbnail for", infile


    return HttpResponseRedirect('/prof/')

    


def register_user(request):

    form = student_form(request.POST or None)
    title="Welcome"
    context = {
        "form" : form,
        "title" : title
    }


    if request.user.is_authenticated():
        if not request.user.is_superuser:
            return HttpResponseRedirect('/prof/')
        # return render(request,'.html',context)
    #     return HttpResponse("user.is_authenticated!!!")
        # else:
        #     return HttpResponseRedirect('/prof/')
    if request.POST:
        
        password=request.POST['password']
        password1=request.POST['confirm_password']
        email=request.POST['email']
        
        first_name=request.POST['first_name']
        last_name=request.POST['last_name']


        

        date=request.POST.get('date')
        month=request.POST.get('month')
        year=request.POST.get('year')

        male = request.POST.get('male')

        # female = request.POST.get('male')
        # other = request.POST.get('m')
        check = request.POST.get('check')

        USER=User.objects.filter(username=email)

        if USER:
            #messages.error(request,"User already exist")
            #redirect here
            return HttpResponse("User already exist!!!")
        if password==password1:
            if check:
                User.objects.create_user(username=email,password=password,email=email)
                Us=User.objects.get(username=email)
                #### 
                a=student.objects.all()
                p=[]
                for c in a:
                    p.append(c.id)
                    
                s=student(user=Us,first_name=first_name,last_name=last_name,date=date,month=month,year=year,post=[],add_f_list=p)
                s.save()
                s.prof_pic.append("0")
                s.cover_pic.append("0")
                

                infile1 = '/home/raftar/ttl/baba5/media_in_env/media_root/defaultcover.jpg'
                outfile1 = '/home/raftar/ttl/baba5/media_in_env/media_root/file_c' + str(s.id) +'_'+str(0) +'.jpg'

                
                if infile1 != outfile1:
                    try:
                        im = Image.open(infile1)
                        im.thumbnail((1600,900),Image.ANTIALIAS)
                        im.save(outfile1, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile1

                
                

                for c in a:
                    c.add_f_list.append(s.id)
                    c.save()
                
                if male == "male":
                    s.male = True
                    s.save()
                if male=="female":
                    s.female = True
                    s.save()
                

                if s.male:
                    infile = '/home/raftar/ttl/baba5/media_in_env/media_root/default.jpg'
                    outfile = '/home/raftar/ttl/baba5/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'

                    
                    if infile != outfile:
                        try:
                            im = Image.open(infile)
                            im.thumbnail((250,250),Image.ANTIALIAS)
                            im.save(outfile, "JPEG")
                        except IOError:
                            print "cannot create thumbnail for", infile
                else:
                    infile = '/home/raftar/ttl/baba5/media_in_env/media_root/defaultfemale.jpg'
                    outfile = '/home/raftar/ttl/baba5/media_in_env/media_root/file_p' + str(s.id) +'_'+str(0) +'.jpg'

                    
                    if infile != outfile:
                        try:
                            im = Image.open(infile)
                            im.thumbnail((250,250),Image.ANTIALIAS)
                            im.save(outfile, "JPEG")
                        except IOError:
                            print "cannot create thumbnail for", infile

                thumb(outfile)
                thumb(outfile1)
                user = authenticate(username=email, password=password)
                login(request, user)
                return HttpResponseRedirect("/joingroup/")



            else :
               return HttpResponse("Check out the terms and conditions!!!") 

    return render(request,"home.html",context)


def login_user(request):
    state = ""
    username = password = ''
    if request.user.is_authenticated():
        #state="Already logged in, "+request.user.username
        if request.user.is_superuser:
            return HttpResponseRedirect('/admin')
        # profile = Profile.objects.get(user=request.user)
        # if(profile.level==1):
        #     return HttpResponseRedirect(reverse('programme_list', args=(str(dept.objects.filter(head=profile)[0].dept_code),)))
        return HttpResponseRedirect('/prof/')
    if request.POST:
        username = request.POST['usernam']
        password = request.POST['pass']

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_superuser:
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                i.online=True
                i.save()

                return HttpResponseRedirect('/admin')
            elif user.is_active:
                login(request, user)
                i=student.objects.filter(user=request.user).first()
                i.online=True
                i.save()
                # profile = Profile.objects.get(user=request.user)
                # if profile.level==1:
                #     return HttpResponseRedirect(reverse('programme_list', args=(str(dept.objects.filter(head=profile)[0].dept_code),)))
                # elif profile.level==8:
                #     return HttpResponseRedirect('/Faculty/Dashboard')
                return HttpResponseRedirect('/prof/')
            else:
                # state = "Your account is not active, please contact the site admin."
                # messages.error(request,state)
                return HttpResponse("Your account is not active, please contact the site admin.")
        else:
            # state = "Your username and/or password were incorrect."
            # messages.error(request,state)
            return HttpResponse("Your username and/or password were incorrect.")


    return render(request, 'login.html',{})


def logout_user(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        i.online=False
        i.logout=timezone.now()
        i.save()
        logout(request)
        state="Thank You for Stopping by !!!"
        messages.success(request,state)
    else:
        state="Oops ! You are already logged out ! Try logging in again"
        messages.error(request,state)
    return render(request, 'home.html')



def Hit(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    k=""

    if request.method=='POST':
        h = str(request.POST.get('slug'))
        j = str(request.POST.get('sluge'))


        v = posts.objects.filter(identity=j,photos=h).first()
        
        
        if c in v.hitters:
            v.hits = v.hits-1
            i=v.hitters.index(c)
            del v.hitters[i]
            k="Hit"
            
            
            
            
        else:
            v.hits = v.hits+1
            v.hitters.append(c)
            k="Unhit"
            if j!=str(c):
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1

                p.fnotify="<tr><td width='15%'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td width='70%'><a href='../"+str(c)+"' > <b style='color:black;'>"+str(i.first_name)+"</b></a> hitted your post.</td><td width='15%'><img src='/media/file_"+str(v.identity)+"_"+str(v.photos)+".thumbnail' style='width:50px;height:50px;' onclick='port(this)' ></td></tr><br>"+p.fnotify
                p.save()
                
        v.save()
        
        z=str(v.hits)
        ctx= {'z':z,'k':k,}
    
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def comment(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id

    if request.method=='POST':
        h = request.POST.get('coment')
        k = request.POST.get('slug')
        j = request.POST.get('sluge')
        s = posts.objects.filter(identity=j,photos=k).first()
        m = postcomment(c_identity=i.id,c_photos=s.id,c_say=h,c_intity=j)
        m.save()
        s.comments=s.comments+1
        s.comnt.append(c)
        s.save()
        n=s.comments
        l = student.objects.filter(id=m.c_identity).first()
        z="/media/file_"+str(l.id)+".thumbnail"+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
        if j!=str(c):
            p=student.objects.filter(id=j).first()
            p.n_notif=p.n_notif+1

            p.fnotify="<tr><td width='15%'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td width='85%'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td></tr><br>"+p.fnotify
            p.save()

    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z,'n':n}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def opine(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    if request.method=='POST':
        j = request.POST.get('sluge')
        k = request.POST.get('slug')
        s = posts.objects.filter(identity=j,photos=k).first()
        m = postcomment.objects.filter(c_intity=j,c_photos=s.id)
        if m:
            for c in m:
                l = student.objects.filter(id=c.c_identity).first()
                z=z+"/media/file_"+str(l.id)+".thumbnail"+"@#$"+l.first_name+" "+l.last_name+"@#$"+c.c_say+"@#$@#$"
            
        else :
            z="NO"


    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z}
    # ctx = 'yes'
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def pr(request,prof_id=5):

    if request.user.is_authenticated() :
    
        b= student.objects.filter(user=request.user).first()
        

        
        i=student.objects.filter(id=prof_id).first()
        if not i:
            return HttpResponse("No such Url exists!!!")

        if i.id==b.id:
            return HttpResponseRedirect("/prof/")

        
        e=student.objects.filter(id__in=b.f_list)
        gr = create_group.objects.filter(cg_memb_id__contains=i.id)

        grm=[]
        for k in gr:
            print k.cg_memb_id
            grm=grm+[x for x in k.cg_memb_id if x not in grm]
        grm = grm+[x for x in i.f_list if x not in grm]

        print grm

        print i
        
        if i.priv_prof==1:
            
            
            
            a=student.objects.filter(id__in=i.f_list)
            c = i.id
            f = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).order_by("-id")[0:9]
            counter = "p"+str(b.id)+"_"+str(b.prof_pic[-1])
            bomb = "p"+str(i.id)+"_"+str(i.prof_pic[-1])
            title = b.first_name+" "+b.last_name
            if i.male:
                l="his"

            if i.female:
                l="her"

            
            t = i.post

            if len(i.cover_pic)>0:
                j=i.cover_pic[len(i.cover_pic)-1]
            else:
                j=0

            page=intrests.objects.filter(intrest_name=b.current_intrest).first()
            if page:
                pc=page.intrest_category
            else:
                pc="pg"
            

            
            context = {
                "title" : title,
                "counter" : counter,
                
                "count" : b.id,
                "bomb" : bomb,
                "grm" : grm,
                "t" : t,
                "l" : l,
                "f" : f,
                "i" : i,
                "j" : j,
                "af" : a,
                "a" : e,
                "e" : b.f_list,
                "b" : b,
                "page" : pc,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                #"form" : form
            }
            return render(request,"profile10.html",context)

        if i.priv_prof==2:
            if i.id in b.f_list:
                a=student.objects.filter(id__in=i.f_list)
                
                c = i.id
                f = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).order_by("-id")[0:9]
                count = str(b.id)
                title = b.first_name+" "+b.last_name
                if i.male:
                    l="his"

                if i.female:
                    l="her"

                
                t = i.post

                if len(i.cover_pic)>0:
                    j=i.cover_pic[len(i.cover_pic)-1]
                else:
                    j=0

                page=intrests.objects.filter(intrest_name=b.current_intrest).first()
                if page:
                    pc=page.intrest_category
                else:
                    pc="pg"
                

                
                context = {
                    "title" : title,
                    "counter" : counter,
                    "count" : i.id,
                    "t" : t,
                    "grm" : grm,
                    "l" : l,
                    "f" : f,
                    "i" : i,
                    "j" : j,
                    "af" : af,

                    "a" : e,
                    "e" : b.f_list,
                    "page" : pc,
                    "cov_r":i.cov_r,
                    "cov_g":i.cov_g,
                    "cov_b":i.cov_b,
                    #"form" : form
                }
                return render(request,"profile10.html",context)
            else:
                state="Sorry you have no permission to access this profile!!!"
                messages.error(request,state)
                return render(request,"profile10.html",{})
                
        if i.priv_prof == 3:
            
            if b.id in grm:
                a=student.objects.filter(id__in=i.f_list)
                
                c = i.id
                f = posts.objects.filter(Q(identity=c)|Q(id__in=i.shared_posts)).order_by("-id")[0:9]
                count = str(b.id)
                title = b.first_name+" "+b.last_name
                if i.male:
                    l="his"

                if i.female:
                    l="her"

                
                t = i.post

                if len(i.cover_pic)>0:
                    j=i.cover_pic[len(i.cover_pic)-1]
                else:
                    j=0

                page=intrests.objects.filter(intrest_name=b.current_intrest).first()
                if page:
                    pc=page.intrest_category
                else:
                    pc="pg"
                

                
                context = {
                    "title" : title,
                    "counter" : counter,
                    "count" : i.id,
                    "grm" : grm,
                    "b" : b,
                    "t" : t,
                    "l" : l,
                    "f" : f,
                    "i" : i,
                    "j" : j,
                    "af" : a,
                    "a" : e,
                    "e" : b.f_list,
                    "page" : pc,
                    "cov_r":i.cov_r,
                    "cov_g":i.cov_g,
                    "cov_b":i.cov_b,
                    #"form" : form
                }
                return render(request,"profile10.html",context)

            else:
                state="Sorry you have no permission to access this profile!!!"
                messages.error(request,state)
                return render(request,"profile10.html",{})
    else:
        return HttpResponse("Login first!!!")



def all_pr(request):
    a=student.objects.all()
    i=student.objects.filter(user=request.user).first()
    c = i.id
    f = posts.objects.filter(identity=c)
    count = "p"+str(c)+"_"+str(i.prof_pic[-1])
    counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
    title = i.first_name+" "+i.last_name
    t = i.post
    context = {
        "title" : title,
        "counter" : counter,
        "count" : i.id,
        "t" : t,
        #"l" : l,
        "f" : f,
        "i" : i,
        "a" : a,
        "b" : i.add_f_list,
        "d" : i.accept_f_list,
        "e" : i.f_list,
        #"form" : form
    }



    return render(request,"profile9.html",context)

def send_request(request,req_id=1):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        j=student.objects.filter(id=req_id).first()

        if j.id in i.add_f_list:
            j.accept_f_list.append(i.id)
            #i.sent_f_list.append(j.id)
            l=j.add_f_list.index(i.id)
            del j.add_f_list[l]
            k=i.add_f_list.index(j.id)
            del i.add_f_list[k]
            j.n_frqst=j.n_frqst+1
            i.save()
            j.save()

        state="Your request has been sent..."
        messages.success(request,state)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponse ("Login first!!!")

def accept_request(request,req_id=1):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        j=student.objects.filter(id=req_id).first()
        if j.id in i.accept_f_list:
            j.f_list.append(i.id)
            i.f_list.append(j.id)
            # l=j.sent_f_list.index(i.id)
            # del j.sent_f_list[l]
            k=i.accept_f_list.index(j.id)
            del i.accept_f_list[k]
            i.save()
            j.save()

        return HttpResponseRedirect('/friends/?q='+str(i.id))
    else:
        return HttpResponse ("Login first!!!")

def del_request(request,req_id=1):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        j=student.objects.filter(id=req_id).first()
        if i.id in j.accept_f_list:
            k=j.accept_f_list.index(i.id)
            del j.accept_f_list[k]
            j.add_f_list.append(i.id)
            j.save()
            i.add_f_list.append(j.id)
            i.save()
            

        return HttpResponseRedirect('/friends/?q='+str(i.id))

    else:
        return HttpResponse ("Login first!!!")

def interests(request):
    if request.user.is_authenticated():
        a=student.objects.all()
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(identity=c)
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        t = i.post
        j = intrests.objects.all()
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "t" : t,
            #"l" : l,
            "j" : j,
            "f" : f,
            "i" : i,
            "a" : a,
            "b" : i.add_f_list,
            "d" : i.accept_f_list,
            "e" : i.f_list,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            #"form" : form
        }



        return render(request,"interest.html",context)

    else:
        return HttpResponse ("Login first!!!")

def photo(request):
    if request.user.is_authenticated():
        q = request.GET.get('q','')
        b= student.objects.filter(user=request.user).first()
        i=student.objects.filter(id=q).first()
        c = b.id
        f = posts.objects.filter(identity=q)
        counter = "p"+str(c)+"_"+str(i.prof_pic[-1])
        title = b.first_name+" "+b.last_name
        a=student.objects.filter(id__in=b.f_list)
        t = i.post
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "t" : t,
            #"l" : l,
            "f" : f,
            "i" : i,
            "a" : a,
            "page" : pc,
            "e" : b.f_list,
            
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            #"form" : form
        }




        return render(request,"photos.html",context)
    else:
        return HttpResponse ("Login first!!!")


def frn(request):
    if request.user.is_authenticated():
        q = request.GET.get('q','')
        
        i=student.objects.filter(user=request.user).first()
        a=student.objects.filter(id__in=i.f_list)
        c = i.id
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        if str(i.id) == q:
            h=student.objects.filter(id__in=i.f_list)
            b=student.objects.filter(id__in=i.add_f_list)
            d=student.objects.filter(id__in=i.accept_f_list)
            #f = posts.objects.filter(identity=c)
            
            t = i.post
            
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                #"l" : l,
                #"f" : f,
                "i" : i,
                "a" : a,
                "b" : b,
                "d" : d,
                "e" : i.f_list,
                "h" : h,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" : pc,
                #"form" : form
            }


            return render(request,"freinds.html",context)
        else:
            s=student.objects.filter(id=int(q)).first()
            h=student.objects.filter(id__in=s.f_list)
            
            #f = posts.objects.filter(identity=c)
            
            
            context = {
                "title" : title,
                "counter" : counter,
                "count" : s.id,
                "i" : s,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "page" : pc,
                "a" : a,
                "e" : i.f_list,
                
                
                
                
                
                "h" : h,
                
                #"form" : form
            }


            return render(request,"freinds.html",context)
    else:
        return HttpResponse ("Login first!!!")



def Msg(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id

    if request.method=='POST':
        z = request.POST.get('slug')
        k = str(z)
        j = request.POST.get('sluge')
        # if "https://" in k:
        #     l= k.split("https://")
        #     s= l[1].split(" ")
        #     s[0] = "<a href='"+s[0]+"'>"+s[0]+"</a>"
        #     l[1]=s.join(" ")
        #     k=l.join("https://")

        m = message(m_id=c,m_friend=j,m_message=k)
        m.save()
        p=student.objects.filter(id=j).first()
        if not i.id in p.msg_list:
            p.msg_list.append(i.id)
            p.n_msg=p.n_msg+1
            
        else:
            p.msg_list.remove(i.id)
            p.msg_list.append(i.id)
            p.n_msg=p.n_msg+1

        p.fmsg=""
        for x in p.msg_list:
            msgs=message.objects.filter(m_id=x,m_friend=j).last()
            y=student.objects.filter(id=x).first()
            p.fmsg="<tr><td width='15%'><img src='/media/file_"+str(y.id)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td width='85%'>"+"<b style='color:black;'>"+str(y.first_name)+"</b>:"+str(msgs.m_message)+"</td><br>"+p.fmsg
            #"<tr><td width='15%'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td width='85%'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><br>"
        
        p.save()
        
        # l = student.objects.filter(id=m.c_identity).first()
        # z="/media/file_"+str(l.id)+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
        z="<div class='full'><p class='sender'>"+m.m_message+"</p></div><br>"

    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')



def Msglist(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""

    if request.method=='POST':
        j = request.POST.get('sluge')
        m = message.objects.filter(Q(m_id=c,m_friend=j) | Q(m_id=j,m_friend=c))
        # n = message.objects.filter(m_id=j,m_friend=c)
        n = student.objects.filter(id=j).first()
        naam = str(n.first_name)+" "+str(n.last_name)
        # z="/media/file_"+str(l.id)+"@#$"+l.first_name+" "+l.last_name+"@#$"+m.c_say+"@#$@#$"
        for l in m:
            if not l.m_admin:
                if l.m_id==i.id:
                    z=z+"<div class='full'><p class='sender'>"+l.m_message+"</p></div><br>"
                else:
                    z=z+"<div class='full'><img src='/media/file_"+str(l.m_id)+".thumbnail' style='width:35px;height:35px;border-radius:35px;'>"+"<p class='receiver'>"+l.m_message+"<br></p></div><br>"
            else:
                z=z+"<div class='full'><center><div class='admin'>"+l.m_message+"</div></center></div>"


        # for k in n:
        #     z=z+"<img src='/media/file_"+str(k.m_id)+"' style='width:50px;height:50px;border-radius:50px;'>"+"  "+k.m_message+"<br>"

    # v=postcomment.objects.filter(c_identity=c,c_photos=k)
    # z="yes"
    ctx= {'z':z,'naam':naam}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Frqst(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    i.n_frqst=0
    k = i.accept_f_list
    if len(k):
        for l in k:
            j=student.objects.filter(id=l).first()
            z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a friend request.</td></tr>"

            #<tr><td width="55px">
            # <img src="{%static 'image/pro.jpg'%}" style="width:50px;height:50px;border-radius:50px;"></td><td>
            # Mark Zuckerburg sent you a friend request
            # <br>
            # Accept!!</td>
    else:
        z="No request.."
        
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')


def Fmsg(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    if i.fmsg:
        i.n_msg=0
        z = i.fmsg
    # if len(k):
    #     for l in k:
    #         j=student.objects.filter(id=l).first()
    #         z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+"' style='width:50px;height:50px;border-radius:50px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a message.</td></tr>"

            #<tr><td width="55px">
            # <img src="{%static 'image/pro.jpg'%}" style="width:50px;height:50px;border-radius:50px;"></td><td>
            # Mark Zuckerburg sent you a friend request
            # <br>
            # Accept!!</td>
    else:
        z="No recent messages"
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Fnotif(request):
    i=student.objects.filter(user=request.user).first()
    c=i.id
    z=""
    k="<tr><td width='15%'>"
    if i.fnotify:
        i.n_notif=0
        z = i.fnotify
        
        s=i.fnotify.split(k)
        if len(s)>6:
            i.fnotify=""
            for n in s[1:5]:
                i.fnotify=i.fnotify+k+n
    # if len(k):
    #     for l in k:
    #         j=student.objects.filter(id=l).first()
    #         z=z+"<tr><td width='55px'><img src='/media/file_"+str(l)+"' style='width:50px;height:50px;border-radius:50px;'></td><td><a href='../"+str(l)+"'>"+str(j.first_name)+" "+str(j.last_name)+"</a> sent you a message.</td></tr>"

            #<tr><td width="55px">
            # <img src="{%static 'image/pro.jpg'%}" style="width:50px;height:50px;border-radius:50px;"></td><td>
            # Mark Zuckerburg sent you a friend request
            # <br>
            # Accept!!</td>
    else:
        z="No recent notifications"
    i.save()
    ctx= {'z':z}
    #ctx = z
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def homepg(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        play=''
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="pg":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by("-id")[0:9]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
                    

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            
            if player.objects.filter(p_id=i.id,p_game=i.current_intrest).first():
                play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
                tm=team.objects.filter(id=play).first()
                if tm:
                    if tm.team_capt==i.id:
                        capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:

                last_visit_time = k.logout.tzinfo
                time=(timezone.now()-k.logout).seconds
                tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = student.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"homepage.html",context)
            

        else:
            state="Make any team game as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homepage.html",{})
            


    else:
        return HttpResponse("Login first!!!")


    
    


def database(request):
    a=posts.objects.all()
    for c in a:
        c.comnt=[]
        c.comments=0
        z=postcomment.objects.filter(c_intity=c.identity,c_photos=c.photos)
        for k in z:

            c.comnt.append(k.c_identity)
        c.comments=len(c.comnt)
        c.save()

    return HttpResponse("database Updated")


def group(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(identity=c)
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        if request.method=='POST':
            group_name=request.POST['group_name']
            group_lat = request.POST['group_lat']
            group_long=request.POST['group_long']
            m=create_group(cg_name=group_name,cg_lat=group_lat,cg_long=group_long,cg_memb_id=[c])
            m.save()
            p=m.id
            i.groups.append(p)
            i.save()
        
            for count, x in enumerate(request.FILES.getlist("files")):
                def process(f):
                    with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_g' + str(c)+ '_'+ str(p)+".jpg", 'wb+') as destination:
                        for chunk in f.chunks():
                            destination.write(chunk)

                process(x)

                infile='/home/raftar/ttl/baba5/media_in_env/media_root/file_g' + str(c)+ '_'+ str(p)+".jpg"
                outfile = os.path.splitext(infile)[0] + ".thumbnail"
                if infile != outfile:
                    try:
                        im = Image.open(infile)
                        im.thumbnail((128,128),Image.ANTIALIAS)
                        im.save(outfile, "JPEG")
                    except IOError:
                        print "cannot create thumbnail for", infile

                

            state="Your group has been created..."
            messages.success(request,state)
            
            return HttpResponseRedirect("/prof/")

        if i.male:
            l="his"

        if i.female:
            l="her"

        active = True
        t = i.post
        
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "t" : t,
            "l" : l,
            "f" : f,
            "i" : i,
            "e" : i.f_list,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
        return render(request,"group.html",context)
    else:
        return HttpResponse("Login first!!!")
    



def Rgba(request):
    i=student.objects.filter(user=request.user).first()
    if request.method=='POST':
        red=request.POST['r']
        green = request.POST['g']
        blue=request.POST['b']
        i.cov_r=red
        i.cov_g=green
        i.cov_b=blue
        i.save()

    ctx="("+str(i.cov_r)+","+str(i.cov_g)+","+str(i.cov_b)+")"
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def Joingroup(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        g=create_group.objects.exclude(id__in=i.groups)
        if request.method=='POST':
            gr=request.POST.getlist("sel_group")
            print gr
            for k in gr:
                i.groups.append(int(float(k)))
                l=create_group.objects.filter(id=int(float(k))).first()
                l.cg_memb_id.append(c)
                l.save()


            
            i.save()
            state="You have joined the selected groups..."
            messages.success(request,state)
            
            if i.current_intrest:
                return HttpResponseRedirect("/prof/")
            else:
                return HttpResponseRedirect("/interest/")


        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context={
            "p" : p,
            "q" : q,
            "r" : r,
            "g" :g,
            "title" : title,
            "counter" : counter,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }

        return render(request,"nogrp.html",context)

    else:
        return HttpResponse ("Login first!!!")


def Share(request):

    i=student.objects.filter(user=request.user).first()
    c=i.id
    z="NOT DONE"

    if request.method=='POST':
        h = str(request.POST.get('slug'))
        j = str(request.POST.get('sluge'))
        p = posts.objects.filter(identity=j,photos=h).first()
        i.shared_posts.append(p.id)
        i.save()
        z="DONE"


    ctx={"z":z}

    return HttpResponse(json.dumps(ctx), content_type='application/json')




def Addfriend(request):
    if request.user.is_authenticated():

        i=student.objects.filter(user=request.user).first()
        f=student.objects.filter(id__in=i.add_f_list)
        if request.method=='POST':
            gr=request.POST.getlist("sel_group")
            for k in gr:
                j=student.objects.filter(id=k).first()
                j.accept_f_list.append(i.id)
                #i.sent_f_list.append(j.id)
                l=j.add_f_list.index(i.id)
                del j.add_f_list[l]
                k=i.add_f_list.index(j.id)
                del i.add_f_list[k]
                j.n_frqst=j.n_frqst+1
                i.save()
                j.save()

            state="Your requests have been sent..."
            messages.success(request,state)
            
            
            return HttpResponseRedirect("/prof/")




        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context={
            "p" : p,
            "q" : q,
            "r" : r,
            "f" : f,
        }

        return render(request,"nofrnd.html",context)

    else:
        return HttpResponse ("Login first!!!")


def Create_city(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(identity=c)
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        if request.method=='POST':
            city_name=request.POST['city_name']
            city_lat = request.POST['city_lat']
            city_long=request.POST['city_long']
            city_des = request.POST['city_descript']
            m=create_city(city_name=city_name,city_lat=city_lat,city_long=city_long,city_descript=city_des)
            m.save()
            p=m.id
            
        
        for count, x in enumerate(request.FILES.getlist("files")):
            def process(f):
                with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_city' + '_'+ str(p)+".jpg", 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)

            process(x)

            infile='/home/raftar/ttl/baba5/media_in_env/media_root/file_city' + '_'+ str(p)+".jpg"
            thumb(infile)


        if i.male:
            l="his"

        if i.female:
            l="her"

        active = True
        t = i.post
        
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "t" : t,
            "l" : l,
            "f" : f,
            "i" : i,
            "e" : i.f_list,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
        return render(request,"city.html",context)
    else:
        return HttpResponse("Login first!!!")
    



def City(request):
    if request.user.is_authenticated():
        city_name = request.GET.get('q','')
        j = create_city.objects.filter(city_name__iexact=city_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(id__in=j.city_posts).order_by("-id")[0:9]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        form = trip_form(request.POST or None)
        if request.method=="POST":
            t_name = request.POST["tourname"]
            t_from = request.POST["trip_from"]
            t_to = request.POST["trip_to"]
            t_mode = request.POST["mode"]
            t_memb = request.POST["memb"]
            t=trip(trip_name=t_name,trip_from=t_from,trip_to=t_to,trip_mode=t_mode,trip_n_membs=t_memb,trip_city=j.city_name)
            t.trip_memb.append(i.id)
            t.save()
            j.city_memb_id.append(i.id)
            j.save()
            state="Your trip has been created.."
            messages.success(request,state)


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            "page": pc,
            "form":form,
        }

        return render(request,"place.html",context)

    else:
        return HttpResponse ("Login first!!!")



def create_new_intrest(request):
#url....new_intrest/int_name.int_category.int_catelog
    if request.user.is_authenticated():
        in_names=request.GET.get('q','')
        in_name=in_names.split(".")
        if in_name:
            m = intrests(intrest_name=in_name[0],intrest_category=in_name[1],intrest_catelog=in_name[2])
            m.save()
            return HttpResponse("DONE")
        else:
            return HttpResponse("Nothing to Create Intrest")
    else:
        return HttpResponse ("Login first!!!")


def Open_intrest(request):
    if request.user.is_authenticated():
        in_name=request.GET.get('q','')
        m = intrests.objects.get(intrest_name__iexact=in_name)
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(id__in=m.intrest_posts).order_by("-id")[0:9]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        k=str(m.intrest_name)+".html"
        l=student.objects.filter(id__in=m.intrest_memb_id)
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "m" : m,
            "f" : f,
            "l" : l,
            "page" : pc,
        }

        return render(request,k,context)

    else:
        return HttpResponse ("Login first!!!")

def Search(request):
    if request.user.is_authenticated():
        if request.method=='POST':
            k=request.POST.get('searchbox1')
            print k
            if student.objects.filter(first_name__iexact=k):
                m=student.objects.get(first_name__iexact=k)
                return HttpResponseRedirect("/"+str(m.id)+"/")
            
            elif intrests.objects.filter(intrest_name__iexact=k):
                n=intrests.objects.get(intrest_name__iexact=k)
                return HttpResponseRedirect("/open_intrest/?q="+str(n.intrest_name))
            
            elif create_city.objects.filter(city_name__iexact=k):
                o=create_city.objects.get(city_name__iexact=k)
                return HttpResponseRedirect("/city/?q="+str(o.city_name))

            elif create_subjects.objects.filter(subjects_name__iexact=k):
                o=create_subjects.objects.get(subjects_name__iexact=k)
                return HttpResponseRedirect("/subject/?q="+str(o.subjects_name))
            else:
                return HttpResponse("No match found...")
    else:
        return HttpResponse ("Login first!!!")

        

def Searching(request):
    if request.user.is_authenticated:
        z=""
        s=""
        srcnt=0
        if request.method=='POST':
            k=request.POST.get('typed')
            print k

            if student.objects.filter(first_name__istartswith=k):
                m=student.objects.filter(first_name__istartswith=k)
                s=m.first().first_name
                for c in m:
                    srcnt=srcnt+1
                    z=z+"<a href='/"+str(c.id)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/"+str(c.id)+"\"' style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/file_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.first_name)+" "+str(c.last_name)+"</b></h4>Person<br>16 Mutual Friends</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"


            if intrests.objects.filter(intrest_name__istartswith=k):
                n=intrests.objects.filter(intrest_name__istartswith=k)
                if not s:
                    s=n.first().intrest_name
                for c in n:
                    srcnt=srcnt+1
                    z=z+"<a href='/open_intrest/?q="+str(c.intrest_name)+"'><div><tr id='srch"+str(srcnt)+"' onclick='document.location=\"/open_intrest/?q="+str(c.intrest_name)+"\"' style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/intrest/"+str(c.intrest_name)+".thumbnail' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.intrest_name)+"</b></h4>Intrest<br>"+str(len(c.intrest_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"
        
                

            if create_city.objects.filter(city_name__istartswith=k):
                o=create_city.objects.filter(city_name__istartswith=k)
                if not s:
                    s=o.first().city_name
                for c in o:
                    srcnt=srcnt+1
                    z=z+"<a href='/city/?q="+str(c.city_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/city/?q="+str(c.city_name)+"\"' style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/file_city_"+str(c.id)+".thumbnail"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.city_name)+"</b></h4>Intrest<br>"+str(len(c.city_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"

            if create_subjects.objects.filter(subjects_name__istartswith=k):
                o=create_subjects.objects.filter(subjects_name__istartswith=k)
                if not s:
                    s=o.first().subjects_name
                for c in o:
                    srcnt=srcnt+1
                    z=z+"<a href='/subject/?q="+str(c.subjects_name)+"'><div><tr id='srch"+str(srcnt)+"'  onclick='document.location=\"/subject/?q="+str(c.subjects_name)+"\"' style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/file_sub_"+str(c.id)+".jpg"+"' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.subjects_name)+"</b></h4>Study<br>"+str(len(c.subjects_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"



            
            
            
            ctx= {'z':z,'s':s,}
            
            return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        return HttpResponse ("Login first!!!")


def Searching_Intrest(request):
    if request.user.is_authenticated():
        z=""
        s=""
        
        if request.method=='POST':
            k=request.POST.get('typed')
            print k

            if intrests.objects.filter(intrest_name__istartswith=k):
                n=intrests.objects.filter(intrest_name__istartswith=k)
                s=n.first().intrest_name
                
                for c in n:
                    z=z+"<div><tr style='border:1px solid black;'><td style='width:16%;overflow:hidden;padding:3px;'><img src='/media/intrest/"+str(c.intrest_name)+".jpg' class='img-responsive' style='width:60px;height:60px;border-radius:60px;' ></td><td style='width:84%;overflow:hidden;padding:3px;'><h4 style='margin-bottom:2px;'><b>"+str(c.intrest_name)+"</b></h4>Intrest<br>"+str(len(c.intrest_memb_id))+" Members</td><td style='width:6%;overflow:hidden;padding:3px;'><span class='glyphicon glyphicon-search'></span></td></tr></div></a>"
        


        ctx= {'z':z,'s':s,}
        
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        return HttpResponse ("Login first!!!")
    
    


def Open(request):
    if  request.user.is_authenticated():
        name=request.GET.get('q','')
        if intrests.objects.filter(intrest_name__iexact=name):
            return HttpResponseRedirect("/open_intrest/?q="+name)
        elif create_city.objects.filter(city_name__iexact=name):
            return HttpResponseRedirect("/city/?q="+name)
        

        else :
            return HttpResponse("No such page...")
    else:
        return HttpResponse ("Login first!!!")





def add_subjects(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(identity=c)
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        if request.method=='POST':
            subject_name = request.POST['subject_name']
            subject_des = request.POST['subject_descript']

            m=create_subjects(subjects_name=subject_name,subjects_descript=subject_des)
            m.save()
            p=m.id
            
        
        for count, x in enumerate(request.FILES.getlist("files")):
            def process(f):
                with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_sub' + '_'+ str(p)+".jpg", 'wb+') as destination:
                    for chunk in f.chunks():
                        destination.write(chunk)

            process(x)   
            infile='/home/raftar/ttl/baba5/media_in_env/media_root/file_sub' + '_'+ str(p)+".jpg"
            thumb(infile)
        active = True
        t = i.post
        
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "t" : t,
            "f" : f,
            "i" : i,
            "e" : i.f_list,
            "a" : a,
            "p" : p,
            "q" : q,
            "r" : r,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
        }
    else:
        return HttpResponse("Login first!!!")
    return render(request,"newsub.html",context)


def Subject(request):
    if request.user.is_authenticated():
        sub_name = request.GET.get('q','')
        j = create_subjects.objects.filter(subjects_name__iexact=sub_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(id__in=j.subjects_posts).order_by("-id")[0:9]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            "a" : a,
            "e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            "page" :pc,
        }

        return render(request,"subject.html",context)

    else:
        return HttpResponse ("Login first!!!")

def Create_team(request):
    if request.user.is_authenticated() :
        t_n=""
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            t_n=request.POST['team_name']
            t_l=request.POST['team_logo']
            t_g=request.POST['team_group']
            print (t_g)
            m = team(team_name=t_n,team_capt=c,team_logo=t_l,team_category=i.current_intrest,team_group=t_g)
            m.team_memb.append(c)
            m.save()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            play.p_team=m.id
            play.save()
            

            k=team.objects.filter(team_name=t_n).first()
            return HttpResponseRedirect("/make_team/?q="+str(k.id))


        else:
            return HttpResponse("No team created....")

    else:
        return HttpResponse ("Login first!!!")

def Make_team(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if team.objects.filter(id=team_id):
            t=team.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            c = i.id
            b=[]
            
            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name
            gr=create_group.objects.filter(id=t.team_group).first()
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            memb = g.intrest_memb_id
            for p in memb:
                if p in gr.cg_memb_id:
                    playe=player.objects.filter(p_id=p).first()
                    if not playe.p_team:
                        b.append(p)

            a = student.objects.filter(id__in=b)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"createteam.html",context)
        else:
            state="Make any team game as ur Intrest!!!"
            messages.error(request,state)
            return render(request,"createteam.html",{})

    else:
        return HttpResponse("Login first!!!")
    

def Invite(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['teamid']
            m = team.objects.filter(id=l).first()
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1


                p.notify=""
                p.fnotify="<tr><td width='15%'><img src='/static/"+str(m.team_logo)+"' style='width:70%;height:50px;border-radius:50px;'></td><td width='85%'><a href='../view_team/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.team_name)+"</a></b> invited you to play in their team.<br> <a href='../accept/?q=Invite."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a></td></tr><br>"+p.fnotify
                p.save()


        return HttpResponseRedirect("/homepg/")

    else:
        return HttpResponse ("Login first!!!")

def View_team(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if team.objects.filter(id=team_id):
            t=team.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            c = i.id
            if request.method=='POST':
                k=request.POST['player']
                if play.p_team:
                    
                    state="Leave pevious team first!!!"
                    messages.error(request,state)
                    return render(request,"visit.html",{})

                    return render(request,"visit.html",{})
                else:
                    if not c in t.team_memb:
                        t.team_memb.append(c)
                        t.save()
                        print(t.id)
                        play.p_team=t.id
                        play.save()
                    return HttpResponseRedirect("/homepg/")


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            a = student.objects.filter(id__in=t.team_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"visit.html",context)
        else:
            return HttpResponse("No such team found!!!")


    else:
        return HttpResponse("Login first!!!")


def Homedouble(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="double":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by("-id")[0:9]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            teams=team.objects.all()

            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')

                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = student.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
 
                "f" : f,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                
                "gr" : gr,
                #"form" : form
            }
            return render(request,"homecafe.html",context)

        else:
            return HttpResponse("Make a single or double player game as your Intrest!!!")



    else:
        return HttpResponse("Login first!!!")




    
    

def Hometour(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="tour":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by("-id")[0:9]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            teams=team.objects.all()
            cities = create_city.objects.all()
            tri =trip.objects.all()
            a = student.objects.all()

            t_ch=""

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

                if request.POST["t_chosen"]:
                    t_c=request.POST["t_chosen"]
                    t_ch=trip.objects.filter(id=t_c).first()
                    return HttpResponseRedirect("/viewtraveller/?q="+str(t_c))

                    

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "cities":cities,
                "gr" : gr,
                "trip": tri,
                "t_ch":t_ch,
                
                #"form" : form
            }

            return render(request,"hometour.html",context)

        else:
            return HttpResponse("Make Travelling as ur Intrest!!!")

    else:
        return HttpResponse("Login first!!!")

    

def Invitechallenger(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            l=request.POST.get('game')
            print k
            
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1
                p.fnotify="<tr><td width='15%'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:70%;height:50px;border-radius:50px;'></td><td width='85%'><a href='../"+str(i.id)+"' > "+"<b style='color:black;'>"+str(i.first_name)+"</b> challenged you to play "+str(l)+" <a href='../accept/?q=Invitechallenger."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a> </td></tr><br>"+p.fnotify
                p.save()
                #"<tr><td width='15%'><img src='/static/"+str(m.team_logo)+".jpg' style='width:70%;height:50px;border-radius:50px;'></td><td width='85%'><a href='../view_team/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.team_name)+"</a></b> invited you to play in their team.<br> <a href='../accept/?q=Invite."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a><br></td>"


        return HttpResponseRedirect("/homedouble/")
    else:
        return HttpResponse ("Login first!!!")

def Invitepartner(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            l=request.POST.get('game')
            print k
            
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1
                p.fnotify="<tr><td width='15%'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td width='85%'>"+"<b style='color:black;'>"+str(i.first_name)+"</b> invited you to play "+str(l)+" as a partner with him <a href='../accept/?q=Invitepartner."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"'> <button>Accept</button></a></td></tr> <br>"+p.fnotify
                #"<tr><td width='15%'><img src='/media/file_"+str(c)+".thumbnail"+"' style='width:50px;height:50px;border-radius:50px;'></td><td width='85%'><b style='color:black;'>"+str(i.first_name)+"</b> commented on your <a href='../prof/#"+str(j)+"."+str(k)+"liked'>post</a> : "+str(h[:15])+"...</td><br>"
                p.save()


        return HttpResponseRedirect("/homedouble/")
    else:
        return HttpResponse ("Login first!!!")


def Leaveteam(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        p=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
        t=team.objects.filter(id=p.p_team).first()
        t.team_memb.remove(i.id)
        t.save()
        m=t.id
        p.p_team=0
        p.save()
        if t.team_capt==c:
            k=player.objects.filter(p_team=m)
            for l in k:
                l.p_team=0
                l.save()
            t.delete()

        return HttpResponseRedirect("/homepg/")
    else:
        return HttpResponse ("Login first!!!")


def Viewtraveller(request):
    if request.user.is_authenticated() :
        trip_id = request.GET.get('q','')
        if trip.objects.filter(id=trip_id):
            t=trip.objects.filter(id=trip_id).first()
            i=student.objects.filter(user=request.user).first()
            
            c = i.id
            if request.method=='POST':
                k=request.POST['traveller']
                if k==i.id:
                    t.trip_memb.append(i.id)
                    t.save()
                    return HttpResponseRedirect("/hometour/")

                
                


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name
            
            a = student.objects.filter(id__in=t.trip_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                            
                "i" : i,
                
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"viewtraveller.html",context)
        else:
            return HttpResponse("No such trip found!!!")


    else:
        return HttpResponse("Login first!!!")

def Accept(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        acpt = request.GET.get('q','')
        print acpt
        acept= acpt.split(".")
        if acept[0]=="Invitepartner":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<center>"+str(i.first_name)+" accepted your request to play "+str(acept[1])+" as your partner</center>"+p.fmsg
                i.fmsg="<center> You accepted "+str(p.first_name)+"'s request to play "+str(acept[1])+"as his partner</center>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="You both are ready to play "+str(acept[1])+" together")
                m.save()

                return HttpResponseRedirect("/homedouble/")

            else :
                state=str(p.first_name)+"has changed his interest."
                messages.error(request,state)
                return HttpResponseRedirect("/homedouble/")


        if acept[0]=="Invitechallenger":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<center>"+str(i.first_name)+" accepted your challenge to play "+str(acept[1])+"</center>"+p.fmsg
                i.fmsg="<center> You accepted "+str(p.first_name)+"'s challenge to play "+str(acept[1])+"</center>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="You both are ready to challenge each other in "+str(acept[1])+". All the best!!!")
                m.save()

                return HttpResponseRedirect("/homedouble/")
            else :
                state=str(p.first_name)+"has changed his interest."
                messages.error(request,state)
                return HttpResponseRedirect("/homedouble/")


        if acept[0]=="Challenge":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<center>"+str(i.first_name)+" accepted your challenge to play "+str(acept[1])+"</center>"+p.fmsg
                i.fmsg="<center> You accepted "+str(p.first_name)+"'s challenge to play "+str(acept[1])+"</center>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="You both are ready to challenge each other in "+str(acept[1])+". All the best!!!")
                m.save()

                return HttpResponseRedirect("/homepg/")
            else :
                state=str(p.first_name)+"'s team doesnot exist now!!! Ooops, You are late..."
                messages.error(request,state)
                return HttpResponseRedirect("/homepg/")
                
        if acept[0]=="Invite":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<center>"+str(i.first_name)+" joined your team to play "+str(acept[1])+".</center>"+p.fmsg
                i.fmsg="<center> You joined "+str(p.first_name)+"'s team to play "+str(acept[1])+"</center>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="Your team is ready to challenge in "+str(acept[1])+". All the best!!!")
                m.save()
                pl=player.objects.filter(p_id=p.id,p_game=p.current_intrest).first()
                t=team.objects.filter(id=pl.p_team).first()
                t.team_memb.append(i.id)
                t.save()

                return HttpResponseRedirect("/homepg/")
            else :
                state=str(p.first_name)+"'s team doesnot exist now!!! Ooops, You are late..."
                messages.error(request,state)
                return HttpResponseRedirect("/homepg/")
                
        


        if acept[0]=="Invitegamer":
            p=student.objects.filter(id=acept[3]).first()
            if p.current_intrest == acept[1]:
                p.n_msg=p.n_msg+1
                i.n_msg=i.n_msg+1
                p.fmsg="<center>"+str(i.first_name)+" joined your team to play "+str(acept[1])+".</center>"+p.fmsg
                i.fmsg="<center> You joined "+str(p.first_name)+"'s team to play "+str(acept[1])+"</center>"+i.fmsg
                p.save()
                i.save()
                m = message(m_id=i.id,m_friend=p.id,m_admin=True,m_message="You are ready to challenge each other in "+str(acept[1])+". All the best!!!")
                m.save()
                pl=player.objects.filter(p_id=p.id,p_game=p.current_intrest).first()
                t=game.objects.filter(id=pl.p_team).first()
                t.game_memb.append(i.id)
                t.save()

                return HttpResponseRedirect("/homegroup/")
            else :
                state=str(p.first_name)+"'s team doesnot exist now!!! Ooops, You are late..."
                messages.error(request,state)
                return HttpResponseRedirect("/homegroup/")
                
        


    else:
        return HttpResponse ("Login first!!!")


def Challenge(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['teamid']
            print l
            m = team.objects.filter(id=l).first()
            for j in k:
                n= team.objects.filter(id=j).first()
                p=student.objects.filter(id=n.team_capt).first()
                p.n_notif=p.n_notif+1
                p.fnotify="<tr><td width='15%'><img src='{"+"%"+"static \'"+str(m.team_logo)+"\'%}' style='width:50px;height:50px;border-radius:50px;'></td><td width='85%'>"+"<a href='../view_team/?q="+str(m.id)+"' <b style='color:black;'>"+str(m.team_name)+"</b></a> challenged you to play "+str(i.current_intrest)+". <a href='../accept/?q=Challenge."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"'> <button>Accept</button></a></td></tr> <br>"+p.fnotify
                p.save()
                #"<tr><td width='15%'><img src='/static/"+str(m.team_logo)+".jpg' style='width:70%;height:50px;border-radius:50px;'></td><td width='85%'><a href='../view_team/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.team_name)+"</a></b> invited you to play in their team.<br> <a href='../accept/?q=Invite."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a><br></td>"


        return HttpResponseRedirect("/homepg/")

    else:
        return HttpResponse ("Login first!!!")

def Homeband(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="band":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by("-id")[0:9]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            bands=band.objects.all()

            

            
            if request.method=='POST':


                if request.POST.get('b_name'): 
                    b_name=request.POST["b_name"]
                    b_des=request.POST["b_des"]
                    b=band(band_name=b_name,band_descript=b_des)
                    b.band_memb.append(i.id)
                    b.save()
                    instrument=request.POST.getlist("intrument")
                    print instrument
                    for ins in instrument:
                        k = str(ins)
                        num=request.POST[k]
                        print num
                        if k=="guitar":
                            b.band_V_guitar=num
                            b.save()
                        if k=="piano":
                            b.band_V_piano=num
                            b.save()
                        if k=="singer":
                            b.band_V_singer=num
                            b.save()
                        if k=="saxophone":
                            b.band_V_saxophone=num
                            b.save()
                        if k=="violin":
                            b.band_V_violin=num
                            b.save()
                        if k=="bass":
                            b.band_V_bass=num
                            b.save()
                        if k=="drum":
                            b.band_V_drum=num
                            b.save()
                        if k=="flute":
                            b.band_V_flute=num
                            b.save()
                        if k=="tabla":
                            b.band_V_tabla=num
                            b.save()
                        if k=="other":
                            b.band_V_other=num
                            b.band_other = request.POST["b_other"]
                            b.save()

                    

                    for count, x in enumerate(request.FILES.getlist("files")):
                        def process(f):
                            with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_band_' +str(b.id)+'.jpg', 'wb+') as destination:
                                for chunk in f.chunks():
                                    destination.write(chunk)

                        process(x)

                        infile='/home/raftar/ttl/baba5/media_in_env/media_root/file_band_' +str(b.id)+'.jpg'
                        thumb(infile)




                    state="Your band has been created... Now rock the world with your talent..."
                    messages.success(request,state)
                    return HttpResponseRedirect("/homeband/")
                    




                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = student.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "bands" : bands,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                
                "gr" : gr,
                #"form" : form
            }
            return render(request,"homeband.html",context)

        else:
            return HttpResponse("Make a Band as your Intrest!!!")



    else:
        return HttpResponse("Login first!!!")

def Band(request):
    if request.user.is_authenticated():
        band_name = request.GET.get('q','')
        j = band.objects.filter(band_name__iexact=band_name).first()
        i=student.objects.filter(user=request.user).first()
        c = i.id
        f = posts.objects.filter(id__in=j.band_posts).order_by("-id")[0:9]
        count = "p"+str(c)+"_"+str(i.prof_pic[-1])
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        #a = student.objects.all()
        p=i.n_frqst
        q=i.n_msg
        r=i.n_notif
        # form = trip_form(request.POST or None)
        # if request.method=="POST":
        #     t_name = request.POST["tourname"]
        #     t_from = request.POST["trip_from"]
        #     t_to = request.POST["trip_to"]
        #     t_mode = request.POST["mode"]
        #     t_memb = request.POST["memb"]
        #     t=trip(trip_name=t_name,trip_from=t_from,trip_to=t_to,trip_mode=t_mode,trip_n_membs=t_memb,trip_city=j.city_name)
        #     t.trip_memb.append(i.id)
        #     t.save()
        #     j.city_memb_id.append(i.id)
        #     j.save()
        #     return HttpResponse("Trip Created!!!")

        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "i" : i,
            #"a" : a,
            #"e" : i.f_list,
            "p" : p,
            "q" : q,
            "r" : r,
            "j" : j,
            "f" : f,
            #"form":form,
        }

        return render(request,"Band.html",context)


    else:
        return HttpResponse ("Login first!!!")


def thumb(infile):
    
        
    

    outfile = os.path.splitext(infile)[0] + ".thumbnail"
    if infile != outfile:
        try:
            im = Image.open(infile)
            im.thumbnail((128,128),Image.ANTIALIAS)
            im.save(outfile, "JPEG")
            print "kr daal"
        except IOError:
            print "cannot create thumbnail for", infile
    return ("DONE")


def thumbing(request):
    pr = posts.objects.filter(profile_picture=1)
    for p in pr:
        s= student.objects.filter(id=p.identity).first()
        for j in s.prof_pic:
            z= 'file_p'+str(p.identity)+'_'+ str(j) +'.jpg'

            infile = os.path.join(BASE_DIR, 'media_in_env','media_root',z)
            thumb(infile)
    return HttpResponse("DONE")





def Groupmsg(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""

        if request.method=='POST':
            k = request.POST.get('sluge')
            
            j = request.POST.get('slug')
            
            grp = create_group.objects.filter(id=k).first()
            gcht = groupchat(gc_id=i.id,gc_group=grp.id,gc_message=j,gc_interest=i.current_intrest)
            gcht.save()
            z="<div class='full'><p class='sender'>"+j+"</p></div>"
            
        ctx= {'z':z}
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        return HttpResponse ("Login first!!!")

def Groupcht(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""

        if request.method=='POST':
            j = request.POST.get('sluge')
            gr = create_group.objects.filter(id=j).first()
            m = groupchat.objects.filter(gc_group=j,gc_interest=i.current_intrest)
            print (gr.cg_name)
            naam=str(gr.cg_name)
            for l in m:
                if not l.admin:
                    if l.gc_id==i.id:
                        z=z+"<div class='full'><p class='sender'>"+l.gc_message+"</p></div><br>"
                    else:
                        z=z+"<img src='/media/file_"+str(l.gc_id)+".thumbnail' style='width:35px;height:35px;border-radius:35px;'>"+"<p class='receiver'>"+l.gc_message+"<br></p><br>"
                else:
                    z=z+"<center clss='admin'>"+l.gc_message+"</center>"


            
        ctx= {'z':z,'naam':naam}
        
        return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        return HttpResponse ("Login first!!!")


def Teammsg(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""

        if request.method=='POST':
            k = request.POST.get('sluge')
            
            j = request.POST.get('slug')
            
            t = team.objects.filter(id=k).first()
            tcht = teamchat(teamc_id=i.id,teamc_group=t.id,teamc_message=j)
            tcht.save()
            print(tcht.teamc_message)
            z="<div class='full'><p class='sender'>"+j+"</p></div>"
            
        ctx= {'z':z}
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        return HttpResponse ("Login first!!!")

def Teamcht(request):
    if request.user.is_authenticated():
        i=student.objects.filter(user=request.user).first()
        c=i.id
        z=""

        if request.method=='POST':
            j = request.POST.get('sluge')
            t = team.objects.filter(id=int(j)).first()
            m = teamchat.objects.filter(teamc_group=t.id)
            
            naam=str(t.team_name)
            for l in m:
                if not l.admin:
                    if l.gc_id==i.id:
                        z=z+"<div class='full'><p class='sender'>"+l.teamc_message+"</p></div><br>"
                    else:
                        z=z+"<img src='/media/file_"+str(l.teamc_id)+".thumbnail' style='width:35px;height:35px;border-radius:35px;'>"+"<p class='receiver'>"+l.gc_message+"<br></p><br>"
                else:
                    z=z+"<center clss='admin'>"+l.teamc_message+"</center>"


            
        ctx= {'z':z,'naam':naam}
        
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    else:
        return HttpResponse ("Login first!!!")







def sendnote(request):

    i=student.objects.filter(user=request.user).first()
    i.logout=timezone.now()
    
    i.save()
    z=""
    if request.method=="POST":
        m=str(i.n_msg)
        n=str(i.n_notif)
        f=str(i.n_frqst)
        l=str(message.objects.all().last().m_id)

        for c in i.f_list:
            k = student.objects.filter(id=c).first()
            
            

            time=(timezone.now()-k.logout).seconds
            
            if time <= 60:
                z=z+"@"+str(c)

        ctx= {'f':f,'n':n, 'm':m,'l':l,'z':z}
    
        return HttpResponse(json.dumps(ctx), content_type='application/json')




def Homestartup(request):
    
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        
        c = i.id
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="startup":
            
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by("-id")[0:9]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.get(intrest_name=i.current_intrest)
            bands=band.objects.all()

            

            
            if request.method=='POST':


                if request.POST.get('stup_name'): 
                    stup_name=request.POST["stup_name"]
                    stup_des=request.POST["stup_des"]
                    b=startup(startup_name=stup_name,startup_descript=stup_des)
                    b.startup_memb.append(i.id)
                    b.save()
                    design=request.POST.getlist("design")
                    print design
                    for ins in design:
                        k = str(ins)
                        num=request.POST[k]
                        print num
                        if k=="ceo":
                            b.startup_V_ceo=num
                            b.save()
                        if k=="techop":
                            b.startup_V_techop=num
                            b.save()
                        if k=="sales":
                            b.startup_V_salesmark =num
                            b.save()
                        if k=="hr":
                            b.startup_V_hr =num
                            b.save()
                        if k=="busdev":
                            b.startup_V_busdev=num
                            b.save()
                        if k=="custser":
                            b.startup_V_custser=num
                            b.save()
                        if k=="salesman":
                            b.startup_V_salesman=num
                            b.save()
                        if k=="rnd":
                            b.startup_V_rnd=num
                            b.save()
                        if k=="adm":
                            b.startup_V_adm=num
                            b.save()
                        if k=="other":
                            b.startup_V_other=num
                            b.startup_other = request.POST["stup_other"]
                            b.save()
                    

                    for count, x in enumerate(request.FILES.getlist("files")):
                        def process(f):
                            with open('/home/raftar/ttl/baba5/media_in_env/media_root/file_startup_' +str(b.id)+'.jpg', 'wb+') as destination:
                                for chunk in f.chunks():
                                    destination.write(chunk)

                        process(x)

                        infile='/home/raftar/ttl/baba5/media_in_env/media_root/file_startup_' +str(b.id)+'.jpg'
                        thumb(infile)

                    state="Your startup is set... Now change the world with your idea..."
                    messages.success(request,state)
                    return HttpResponseRedirect("/homestartup/")

                    




                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = student.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "l" : l,
                "f" : f,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "bands" : bands,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                
                "gr" : gr,
                #"form" : form
            }
            return render(request,"homestartup.html",context)

        else:
            return HttpResponse("Make a Startup as your Intrest!!!")



    else:
        return HttpResponse("Login first!!!")

def thumbonly(request):

    
    
    infile='/home/raftar/ttl/baba5/media_in_env/media_root/file_g19_11'+'.jpg'
    outfile = os.path.splitext(infile)[0] + ".thumbnail"
    if infile != outfile:
        try:
            im = Image.open(infile)
            im.thumbnail((128,128),Image.ANTIALIAS)
            im.save(outfile, "JPEG")
            print ("kr diyaa"+str(infile))
        except IOError:
            print "cannot create thumbnail for", infile


    return HttpResponse("DONE")



def analyze( imgFile ):
    # open the image
    img = Image.open(imgFile)

    # grab width and height
    #width, height = img.size
    # make a list of all pixels in the image
    pixels = img.convert('RGB')
    data = []
    for x in range(img.width):
        for y in range(img.height):
            cpixel = pixels.getpixel((x, y))
            data.append(cpixel)
    r = 0
    g = 0
    b = 0
    counter = 0

    # loop through all pixels
    # if alpha value is greater than 200/255, add it to the average
    # (note: could also use criteria like, if not a black pixel or not a white pixel...)
    for x in range(len(data)):
        r+=data[x][0]
        g+=data[x][1]
        b+=data[x][2]
        counter+=1;

    # compute average RGB values
    rAvg = r/counter
    gAvg = g/counter
    bAvg = b/counter

    return (rAvg, gAvg, bAvg)



def updatergb(request):
    a =student.objects.filter(user=request.user).first()
        
    
    k=a.cover_pic
    if k:
        infile='/home/raftar/ttl/baba5/media_in_env/media_root/file_c'+str(a.id)+'_' +str(k[-1])+'.jpg'
        a.cov_r=analyze(infile)[0]
        a.cov_g=analyze(infile)[1]
        a.cov_b=analyze(infile)[2]
        a.save()
    return HttpResponseRedirect('/prof/')

def defaulttheme(request):
    if request.user.is_authenticated():
        a =student.objects.filter(user=request.user).first()
        k=a.cover_pic
        if k:
            a.cov_r=22
            a.cov_g=57
            a.cov_b=76
            a.save()

        return HttpResponseRedirect('/prof/')
    else:
        return HttpResponse ("Login first!!!")




def covfix(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        #count = str(i.id)
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        if request.method=="POST":
            scval=request.POST["scval"]
            if i.cov_fix:
                i.cov_fix[0]=scval
            else:
                i.cov_fix.append(scval)


            i.save()

            z="DONE"
            ctx={'z':z}
            return HttpResponse(json.dumps(ctx), content_type='application/json')


        context = {
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "j" : i.cover_pic[-1],
            "k" : i.prof_pic[-1],
            }

        return render(request,"profilecov.html",context)

    else:
        return HttpResponse ("Login first!!!")



def setting(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        #count = str(i.id)
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        if request.method=="POST":
            oldpass = request.POST["oldpassword"]
            newpass = request.POST["newpassword"]
            confpass = request.POST["confpassword"]
            u=User.objects.get(username=request.user.username)
            user = authenticate(username=u.username,password=oldpass)
            if user:
                if newpass==confpass:
                    user.set_password(newpass)
                    user.save()
                    user = authenticate(username=u.username,password=newpass)
                    login(request, user)
                    return HttpResponseRedirect("/prof/")
                else:
                    state="Confirm password doesn't match!!!"
                    messages.error(request,state)
                    return HttpResponseRedirect("/setting/")
                    
            else:
                
                state="Incorrect old Password !!!"
                messages.error(request,state)
                return HttpResponseRedirect("/setting/")


        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"


            
            #return HttpResponse("Password Changed...")

        context = {
            "i" : i,
            "title" : title,
            "counter" : counter,
            "count" : i.id,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "j" : i.cover_pic[-1],
            "k" : i.prof_pic[-1],
            "page" : pc,
            }

        return render(request,"setting.html",context)
    else:
        return HttpResponse ("Login first!!!")

def sports(request):
    if request.user.is_authenticated():
        q = request.GET.get('q','')
        i = student.objects.filter(user=request.user).first()
        t = intrests.objects.filter(intrest_catelog=str(q))
        counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
        title = i.first_name+" "+i.last_name
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"
        context = {
            "counter" : counter,
            "title" : title,
            "i" : i,
            "t" : t,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            "page" : pc,
            
            }

        return render(request,"sports.html",context)

    else:
        return HttpResponse ("Login first!!!")


def study(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        t = create_subjects.objects.all()
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        context = {
            "i" : i,
            "t" : t,
            "page" : pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            
            }

        return render(request,"study.html",context)
    else:
        return HttpResponse ("Login first!!!")


def travel(request):
    if request.user.is_authenticated() :
        i = student.objects.filter(user=request.user).first()
        t = create_city.objects.all()
        page=intrests.objects.filter(intrest_name=i.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        context = {
            "i" : i,
            "t" : t,
            "page" :pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            
            }

        return render(request,"travel.html",context)

    else:
        return HttpResponse ("Login first!!!")

def about(request):
    if request.user.is_authenticated() :
        q = request.GET.get('q','')
        b = student.objects.filter(user=request.user).first()
        i = student.objects.filter(id=q).first()
        counter = 'p'+str(b.id)+'_'+str(b.prof_pic[-1])
        title = b.first_name+" "+b.last_name
            
        u = User.objects.filter(id=i.user_id).first()
        a = student.objects.filter(id__in=b.f_list)
        page=intrests.objects.filter(intrest_name=b.current_intrest).first()
        if page:
            pc=page.intrest_category
        else:
            pc="pg"

        if request.method=="POST":
            if b.id == i.id:
                
                if request.POST["organization"]:
                    i.work=request.POST["organization"]
                if request.POST["place"]:
                    i.current_place=request.POST["place"]

                if request.POST["work"]:
                    i.work_as=request.POST["work"]
                if request.POST["school"]:
                    i.school=request.POST["school"]
                    
                if request.POST["college"]:
                    i.college=request.POST["college"]
                if request.POST["party"]:
                    i.party=request.POST["party"]
                if request.POST["contact"]:
                    i.phone_number=request.POST["contact"]
                if request.POST["hometown"]:
                    i.hometown=request.POST["hometown"]
                i.save()

            else:
                state="You don't have permission to edit !!!"
                messages.error(request,state)     
            

        context = {
            "title" : title,
            "counter" : counter,
            "i" : i,
            "email" : u.email,
            "a" : a,
            "e" : b.f_list,
            "page" : pc,
            "cov_r":i.cov_r,
            "cov_g":i.cov_g,
            "cov_b":i.cov_b,
            
            }

        return render(request,"about.html",context)

    else:
        return HttpResponse ("Login first!!!")




def privacy(request):
    if request.user.is_authenticated() :
        i = student.objects.filter(user=request.user).first()
        if request.method=="POST":
            i.priv_prof = request.POST["priv_prof"]
            i.priv_mess = request.POST["priv_mess"]
            i.priv_frnd = request.POST["priv_frnd"]
            i.priv_email= request.POST["priv_email"]
            i.priv_goups= request.POST["priv_groups"]
            i.save()
            state="Your privacy settings have been changed..."
            messages.success(request,state)
            return HttpResponseRedirect("/prof/")
        else:
            return HttpResponse("Nothing to post!!!")

    else:
        return HttpResponse ("Login first!!!")



def homegroup(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""

        play=''
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="group":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by("-id")[0:9]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    # v = posts.objects.filter(photos=h).first()
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
            if play:
                tm=game.objects.filter(id=play).first()
                if tm:
                    if tm.game_capt==i.id:
                        capt=i.id

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:

                last_visit_time = k.logout.tzinfo
                time=(timezone.now()-k.logout).seconds
                tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = student.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                "play" : play,
                "tm" : tm,
                "list":mylist,

                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                #"form" : form
            }

            return render(request,"homegroup.html",context)
            

        else:
            state="Make any group game as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homegroup.html",{})
            


    else:
        return HttpResponse("Login first!!!")

def homequick(request):
    
    if request.user.is_authenticated() :

        i=student.objects.filter(user=request.user).first()
        i.logout=timezone.now()
        i.save()
        c = i.id
        capt=""
        tm=""
        if intrests.objects.filter(intrest_name=i.current_intrest).first().intrest_category=="quick":
            fl =i.f_list
            mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
            count = c
            for c in fl:
                mylist.append(Q(identity=c))
            f=posts.objects.filter(reduce(operator.or_, mylist)).order_by("-id")[0:9]
            
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name

            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            teams=''



            

            
            if request.method=='POST':
                
                if request.POST.get('searchbox3'):
                    ci = request.POST.get('searchbox3')
                    find=intrests.objects.filter(intrest_name=ci).first()
                    if find:    

                        change_interest(request,i,find)    
                        return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

                    else:
                        state="No such interest found !!!"
                        messages.error(request,state)
                        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

                if request.POST.get('typed'):
                    z=""
                    t_g= request.POST.get('typed')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr onclick='document.location=\"../view_team/?q="+str(n.id)+"\"'><form name='upform' method='post' action='../view_team/?q="+str(n.id)+"' enctype='multipart/form-data' style='display:none;'>{ "+"%"+"csrf_token %}<td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:40%;'>"+str(n.team_name)+"</td><td style='width:35%;'>"+str(len(n.team_memb))+"/11</td><td style='width:15%;'><input type='text' style='display:none;' name='player' value='{{count}}' ><button type='submit'>Join Team</button></td></form></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')


                if request.POST.get('typedd'):
                    z=""
                    t_g= request.POST.get('typedd')
                    print (t_g)
                    if team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id):
                        teams=team.objects.filter(team_category=i.current_intrest,team_group=t_g).exclude(team_capt=i.id)
                        z=""
                        for n in teams:
                            z="<tr><td align='right'  style='width:10%;'><img src='/static/"+str(n.team_logo)+"/' style='height:70px;width:70px;'></td><td style='width:30%;'><a href='../view_team/?q="+str(n.id)+"'>"+str(n.team_name)+"</a></td><td style='width:10%;'>"+str(len(n.team_memb))+"/11</td><td width='5%'><input type='checkbox' name='invited' value='"+str(n.id)+"'></td></tr>"+z

                    else:
                        teams=''
                        z="<center><b> No teams Found</b></center>"
                    ctx={'z':z}
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                    




                if request.POST.get('work'):
                    work = request.POST.get('work')
                    # v = posts.objects.filter(photos=h).first()
                    i.work = work
                    i.save()
                if request.POST.get('hometown'):
                    hometown = request.POST.get('hometown')
                    
                    i.hometown = hometown
                    i.save()
                if request.POST.get('currentplace'):
                    currentplace = request.POST.get('currentplace')
                    
                    i.current_place = currentplace
                    i.save()

            
            if i.male:
                l="his"

            if i.female:
                l="her"

            active = True
            t = i.post

            # play=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first().p_team
            # if play:
            #     tm=team.objects.filter(id=play).first()
            #     if tm.team_capt==i.id:
            #         capt=c

            e=i.f_list

            b = student.objects.filter(id__in=e)
            tim=[]
            for k in b:

                last_visit_time = k.logout.tzinfo
                time=(timezone.now()-k.logout).seconds
                tim.append(time)

            

            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            a = student.objects.all()
            form = quick_form(request.POST or None)
            quik=quick.objects.all()
            #k = i.shared_posts
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "t" : t,
                "page" : g.intrest_category,
                "l" : l,
                "f" : f,
                "i" : i,
                "e" : i.f_list,
                "a" : a,
                "p" : p,
                "q" : q,
                "r" : r,
                "teams" : teams,
                
                "tm" : tm,
                "list":mylist,
                "quick" : quik,
                "g" : g.intrest_memb_id,
                
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
                "capt" : capt,
                "gr" : gr,
                "form" : form
            }

            return render(request,"homequick.html",context)
            

        else:
            state="Make quick event as ur Intrest!!!"
            messages.error(request,state)
        return render(request,"homequick.html",{})
            


    else:
        return HttpResponse("Login first!!!")




def Create_game(request):
    if request.user.is_authenticated() :
        t_n=""
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            t_n=request.POST['team_name']
            t_l=request.POST['team_logo']
            t_g=request.POST['team_group']
            t_lim=request.POST['team_lim']
            print (t_g)
            m = game(game_name=t_n,game_capt=i.id,game_limit=t_lim,game_logo=t_l,game_category=i.current_intrest,game_group=t_g)
            m.game_memb.append(c)
            m.save()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            play.p_team=m.id
            play.save()
            

            k=game.objects.filter(game_name=t_n).first()
            return HttpResponseRedirect("/make_game/?q="+str(k.id))
        else:
            return HttpResponse("No Game created....")

    else:
        return HttpResponse("Login first!!!")


def Make_game(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if game.objects.filter(id=team_id):
            t=game.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            c = i.id
            b=[]
            
            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.Last_Name
            gr=create_group.objects.filter(id=t.game_group).first()
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            memb = g.intrest_memb_id
            for p in memb:
                if p in gr.cg_memb_id:
                    playe=player.objects.filter(p_id=p).first()
                    if not playe.p_team:
                        b.append(p)

            a = student.objects.filter(id__in=b)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"creategame.html",context)
        else:
            return HttpResponse("No such team found!!!")

    else:
        return HttpResponse("Login first!!!")

def Invitegamer(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        if request.method=='POST':
            k=request.POST.getlist("invited")
            print k
            l=request.POST['teamid']
            m = game.objects.filter(id=l).first()
            for j in k:
                p=student.objects.filter(id=j).first()
                p.n_notif=p.n_notif+1


                p.notify=""
                p.fnotify="<tr><td width='15%'><img src='/static/"+str(m.game_logo)+"' style='width:70%;height:50px;border-radius:50px;'></td><td width='85%'><a href='../view_game/?q="+str(m.id)+"' > <b style='color:black;'>"+str(m.game_name)+"</a></b> invited you to play in their team.<br> <a href='../accept/?q=Invitegamer."+str(i.current_intrest)+"."+str(p.id)+"."+str(i.id)+"' style='color:black;'> <button>Accept</button></a></td></tr><br>"+p.fnotify
                p.save()


        return HttpResponseRedirect("/homegroup/")

    else:
        return HttpResponse("Login first!!!")

def View_game(request):
    if request.user.is_authenticated() :
        team_id = request.GET.get('q','')
        if game.objects.filter(id=team_id):
            t=game.objects.filter(id=team_id).first()
            i=student.objects.filter(user=request.user).first()
            play = player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
            c = i.id
            if request.method=='POST':
                k=request.POST['player']
                if play.p_team:
                    return HttpResponse("Leave pevious team first!!!")
                else:
                    if not c in t.game_memb:
                        t.team_memb.append(c)
                        t.save()
                        print(t.id)
                        play.p_team=t.id
                        play.save()
                    return HttpResponseRedirect("/homepg/")


            count = "p"+str(c)+"_"+str(i.prof_pic[-1])
            counter = 'p'+str(i.id)+'_'+str(i.prof_pic[-1])
            title = i.first_name+" "+i.last_name
            gr=create_group.objects.filter(id__in=i.groups)
            g=intrests.objects.filter(intrest_name=i.current_intrest).first()
            active = True
            a = student.objects.filter(id__in=t.game_memb)
            
            
            p=i.n_frqst
            q=i.n_msg
            r=i.n_notif
            context = {
                "title" : title,
                "counter" : counter,
                "count" : i.id,
                "gr" : gr,            
                "i" : i,
                "g" : g.intrest_memb_id,
                "a" : a,
                "t" : t,
                "p" : p,
                "q" : q,
                "r" : r,
                "cov_r":i.cov_r,
                "cov_g":i.cov_g,
                "cov_b":i.cov_b,
            }

            return render(request,"visitgame.html",context)
        else:
            return HttpResponse("No such team found!!!")


    else:
        return HttpResponse("Login first!!!")



def Leavegame(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        c = i.id
        p=player.objects.filter(p_id=i.id,p_game=i.current_intrest).first()
        t=game.objects.filter(id=p.p_team).first()
        t.game_memb.remove(i.id)
        t.save()
        m=t.id
        p.p_team=0
        p.save()
        if t.game_capt==c:
            k=player.objects.filter(p_team=m)
            for l in k:
                l.p_team=0
                l.save()
            t.delete()

        return HttpResponseRedirect("/homegroup/")
    else:
        return HttpResponse("Login first!!!")

def make_quick(request):
    if request.user.is_authenticated():
        i = student.objects.filter(user=request.user).first()
        if request.method=='POST':
            q_int = request.POST ["q_int"]
            q_from = request.POST ["quick_from"]
            q_to = request.POST ["quick_to"]
            #q_g = request.POST ["team_group"]
            q=quick(quick_name=q_int,quick_from=q_from,quick_to = q_to)
            q.save()
            q.quick_memb.append(i.id)
            q.save()
        return HttpResponseRedirect("/homequick/")

    else:
        return HttpResponse("Login first!!!")




def clearnotification(request):
    i = student.objects.filter(user=request.user).first()
    i.fnotify=""
    i.save()

    return HttpResponse("Your notifications have been deleted...")



def scrollpost(request):
    i = student.objects.filter(user=request.user).first()
    a = request.POST["a"]
    b = request.POST["b"]
    fl =i.f_list
    mylist=[Q(identity=c),Q(id__in=i.shared_posts)]
    count = c
    for c in fl:
        mylist.append(Q(identity=c))
    f=posts.objects.filter(reduce(operator.or_, mylist)).order_by("-id")[int(a):int(b)]

    data = serializers.serialize('json', f)



    return HttpResponse(data, content_type='application/json')


def add_as_interest(request):
    if request.user.is_authenticated() :
        i=student.objects.filter(user=request.user).first()
        q = request.GET.get('q','')
        find=intrests.objects.filter(intrest_name__iexact=q).first()
        if find:
            i.current_intrest = find.intrest_name
            i.interest.append(find.id)
            i.save()
            

            if not i.id in find.intrest_memb_id:
                find.intrest_memb_id.append(i.id)
                find.save()

            if find.intrest_category=="pg" or find.intrest_category=="group" :
                if not player.objects.filter(p_id=i.id,p_game=find.intrest_name):
                    p=player(p_id=i.id,p_game=find.intrest_name)
                    p.save()

            state="Now Your current interest is "+str(find.intrest_name)
            messages.success(request,state)


            return HttpResponseRedirect("/home"+str(find.intrest_category)+"/")

        else:
            state="No such interest found !!!"
            messages.error(request,state)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



    else:
        return HttpResponse("Login first!!!")


def change_interest(request,i,find):
    c=i.id
    i.current_intrest = find.intrest_name
    if find.id in i.interest:
        
        i.interest = [x for x in i.interest if x != find.id]
        i.interest.append(find.id)
    else:
        i.interest = [x for x in i.interest if x != find.id]
        i.interest.append(find.id)
    i.save()
    

    if not c in find.intrest_memb_id:
        find.intrest_memb_id.append(c)
        find.save()

    if find.intrest_category=="pg" or find.intrest_category=="group" :
        if not player.objects.filter(p_id=c,p_game=find.intrest_name):
            p=player(p_id=i.id,p_game=find.intrest_name)
            p.save()

    state="Now Your current interest is "+str(find.intrest_name)
    messages.success(request,state)








    