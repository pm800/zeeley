"""ram URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class   -based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import patterns,include, url
from django.conf.urls.static import static
# from django.views.decorators.csrf import csrf_exempt
from django.contrib import admin
from app1 import views
from django.http import HttpResponse

admin.autodiscover()



urlpatterns=[
	url(r'^polls/', include('polls.urls',namespace='polls') ),


    url(r'^$', views.register_user, name='home'),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^contact/$', views.contact, name='contact'),
    # url(r'^', include('registration.backends.default.urls')),
    url(r'^login/', views.login_user, name='login_user'),
    url(r'^logout/$', views.logout_user, name='logout_user'),
    url(r'^prof', views.prof, name='prof'),
    #url(r'^halua/', include('app1.urls')),
    url(r'^(?P<prof_id>\d+)/',views.pr,name='indivprof'),
    url(r'^upload/', views.Upload, name='Upload'),
    url(r'^uploadpost/', views.Upload_post, name='Upload_post'),
    url(r'^hit/', views.Hit, name='Hit'),
    url(r'^msg/', views.Msg, name='Msg'),
    # url(r'^cover/', views.Upload_cover, name='Upload_cover'),
    url(r'^msglist/', views.Msglist, name='Msg'),
    url(r'^comment/', views.comment, name='Comment'),
    url(r'^opine/', views.opine, name='Opine'),
    url(r'^sendrequest/(?P<req_id>\d+)/', views.send_request, name='S_Request'),
    # url(r'^acceptrequest/(?P<req_id>\d+)/', views.accept_request, name='A_Request'),
    # url(r'^deleterequest/(?P<req_id>\d+)/', views.del_request, name='D_Request'),
    #url(r'^accounts/', include('allaccess.urls')),
    # url(r'^edit_prof/', views.edit, name='edit'),
    url(r'^interest', views.interests, name='interests'),
    url(r'^photo', views.photo, name='photo'),
    url(r'^friends', views.frn, name='frn'),
    url(r'^allposts', views.homepg, name='homepg'),
    # url(r'^homechat', views.homechat, name='homechat'),
    url(r'^frqst/', views.Frqst, name='Frqst'),
    
    url(r'^chatmsg/', views.Chatmsg, name='chatmsg'),
    url(r'^fmsg/', views.Fmsg, name='Fmsg'),
    url(r'^fnotif/', views.Fnotif, name='Fnotif'),
    url(r'^data/', views.database, name='Database'),
    url(r'^group/', views.group, name='group'),
    url(r'^rgba/', views.Rgba, name='Rgba'),
    url(r'^joingroup/', views.Joingroup, name='Joingroup'),
    url(r'^share/', views.Share, name='Share'),
    url(r'^addfriend/', views.Addfriend, name='Addfriend'),
    url(r'^create_city/', views.Create_city, name='Create_city'),
    url(r'^city/',views.City,name='City'),
    url(r'^new_intrest/',views.create_new_intrest,name='create_new_intrest'),
    url(r'^open_intrest/',views.Open_intrest,name='Open_intrest'),
    url(r'^search/',views.Search,name='Search'),
    url(r'^searching/',views.Searching,name='Searching'),
    url(r'^open/',views.Open,name='Open'),
    url(r'^add_sub/',views.add_subjects,name='add_subject'),
    url(r'^subject/',views.Subject,name='Subject'),#get: subject_name
    url(r'^create_team/',views.Create_team,name='Create_team'),
    url(r'^searching_intrest/',views.Searching_Intrest,name='Searching_Intrest'),
    url(r'^make_team/',views.Make_team,name='Make_team'),# GET: team id
    url(r'^invite/',views.Invite,name='Invite'),#POST['teamid'] and POST.getlist("invited") # list ids of students
    url(r'^view_team/',views.View_team,name='View_team'),
    # url(r'^homedouble/',views.Homedouble,name='Homedouble'),
    # url(r'^hometour/',views.Hometour,name='Hometour'),
    # url(r'^invitechallenger/',views.Invitechallenger,name='Invitechallenger'),
    # url(r'^invitepartner/',views.Invitepartner,name='Invitepartner'),
    # url(r'^leaveteam/',views.Leaveteam,name='Leaveteam'),
    # url(r'^viewtraveller/',views.Viewtraveller,name='Viewtraveller'),
    url(r'^accept/',views.Accept,name='Accept'),
    # url(r'^challenge/',views.Challenge,name='Challenge'),
    url(r'^homeband/',views.Homeband,name='Homeband'),
    url(r'^band/',views.Band,name='Band'),
    url(r'^startups/',views.Startups,name='Startups'),
    url(r'^bands/',views.Bands,name='Bands'),
    url(r'^entrepreneurship/',views.Startup,name='Startup'),
    url(r'^thumb/',views.thumbonly,name='thumbonly'),
    url(r'^groupcht/',views.Groupcht,name='Groupcht'),
    url(r'^groupmsg/',views.Groupmsg,name='Groupmsg'),
    url(r'^teamcht/',views.Teamcht,name='Teamcht'),
    url(r'^teammsg/',views.Teammsg,name='Teammsg'),
    url(r'^sendnote/',views.sendnote,name='sendnote'),
    url(r'^homestartup/',views.Homestartup,name='Homestartup'),
    url(r'^updatergb/',views.updatergb,name='updatergb'),
    url(r'^thumbing/',views.thumbing,name='thumbing'),
    url(r'^defaulttheme/',views.defaulttheme,name='defaulttheme'),
    url(r'^covfix/',views.covfix,name='covfix'),
    url(r'^setting/',views.setting,name='setting'),
    url(r'^view_interest/',views.sports,name='sport'),
    url(r'^study/',views.study,name='study'),
    url(r'^travel/',views.travel,name='travel'),
    url(r'^about/',views.about,name='about'),
    url(r'^privacy/',views.privacy,name='privacy'),
    # url(r'^homegroup/',views.homegroup,name='homegroup'),
    # url(r'^homequick/',views.homequick,name='homequick'),
    url(r'^create_game/',views.Create_game,name='Create_game'),
    url(r'^make_game/',views.Make_game,name='Make_game'),
    url(r'^invitegamer/',views.Invitegamer,name='Invitegamer'),
    url(r'^view_game/',views.View_game,name='Viewgame'),
    url(r'^leavegame/',views.Leavegame,name='Leavegame'),
    url(r'^make_quick/',views.make_quick,name='make_quick'),
    url(r'^delnotif/',views.clearnotification,name='clearnotification'),
    url(r'^add_as_interest/',views.add_as_interest,name='add_as_interest'),
    url(r'^film/',views.Film,name='filmy'),
    url(r'^films/',views.Films,name='filmsy'),
    url(r'^hitband/',views.Hitband,name='hitBand'),
    # url(r'^homecafe/',views.homecafe,name='homecafe'),
    url(r'^cafe/',views.Cafe,name='Cafe'),\
    url(r'^cafes/',views.Cafes,name='Cafes'),
    url(r'^scrollpost/',views.scroll_post,name='Scrollpost'),
    url(r'^portpost/',views.port_post,name='portpost'),
    url(r'^studs/',views.studs,name='studs'),
    url(r'^create_restaurant/',views.Create_cafe,name='create_cafe'),
    url(r'^pqrs/',views.pqrs,name='pqrs'),
    # url(r'^homepizza/',views.homepizza,name='homepizza'),
    url(r'^create_film/',views.Create_film,name='create_film'),
    url(r'^joinband/',views.Join_band,name='join_band'),
    url(r'^home/',views.pageredirect,name='pageredirect'),
    # url(r'^homelan/',views.homelan,name='homelan'),
    url(r'^create_game_team/',views.Create_game_team,name='create_game_team'),

    url(r'^create_pizza_family/',views.make_pizza_family,name='create_pizza_family'),

    url(r'^viewquickster/',views.Viewquickster,name='viewquickster'),
    url(r'^view_gamers/',views.View_gamers,name='viewgamers'),
    url(r'^view_pizza/',views.view_pizza,name='viewpizza'),
    url(r'^make_all_groups/',views.Make_allgroups,name='make_all_groups'),#get:q = interest_name.model's id ( pizza or team)

    url(r'^invitefriends/',views.Invitefriends,name='invitefriends'),
    # url(r'^homemovie/',views.Homemovie,name='homemovie'),
    url(r'^leavedouble/',views.Leavedouble,name='Leavedouble'),
    url(r'^view_watcher/',views.Viewwatcher,name='viewwatcher'),


    # url(r'^homeservice/',views.servicepage,name='Serve'),
    url(r'^feedback/',views.Feedback,name='Feedback'),
    url(r'^view_fooder/',views.Viewfooder,name='viewfooder'),
    #url(r'^view_online_user/',views.get_all,name='get_all'),
    url(r'^add_admin_to_all/',views.admin_frnd,name='Admin_frnd'),
    url(r'^delpost/',views.delete_post,name='deletepost'),
    url(r'^delcomment/',views.delete_comment,name='deletecomm'),
    url(r'^delmessage/',views.delete_message,name='deletemess'),
    # url(r'^tagstuds/',views.tag_studs,name='tagstuds'),
    # url(r'^tagcities/',views.tag_cities,name='tagcities'),
    #url(r'^updatefn/',views.updatefirstname,name='updatefirstname'),    
    # url(r'^tagstartups/',views.tag_startups,name='tagintrest'),
    url(r'^checktag/',views.check_tag,name='tagcheck'),
    url(r'^leavetour/',views.Leave_tour,name='leavetour'),
    url(r'^fblogin/',views.fblogin,name='fblogin'),
    url(r'^delxyz/',views.delxyz,name='dellogin'),
    url(r'^sujhav/',views.View_Feedback,name='Viewfeed'),
    
    url(r'^nidhi/',views.nidhi,name='nidhi'),

    url(r'^mail_confirm/',views.mail_confirmation,name='mail_confirmation'),

    url(r'^positionupdate/',views.Positionupdate,name='positionupdate'),

    url(r'^android_login2/',views.android_login2,name='android_login2'),
    
    url(r'^android_position/',views.android_position,name='android_position'),
    url(r'^android_login/',views.android_login,name='android_login'),
    url(r'^cab_sharing/',views.Cab_sharing,name='cab_sharing'),
    url(r'^cotravelling/',views.Cotravelling,name='cotravelling'),
    url(r'^cab_request/',views.Cab_request,name='Cab_request'),
    url(r'^cab_edit/',views.Cab_edit,name='cab_edit'),
    url(r'^apply/',views.apply,name='apply'),  
    url(r'^trip_request/',views.Trip_request,name='Trip_request'), 
    url(r'^upsize/',views.upsize,name='upsize'), 
    url(r'^more_register/',views.more_reg,name='morereg'), 
    url(r'^fotos_resizing/',views.photo_resizing,name='photo_resizing'),  
    # url(r'^pizza_share/',views.Pizza_share,name='pizza_share'),
    url(r'^rest_share/',views.Rest_share,name='rest_share'),
    url(r'^quicky_share/',views.Quicky_share,name='quicky_share'),
    url(r'^pcgame_share/',views.PCGame_share,name='pcgame_share'),
    url(r'^movie_share/',views.Movie_share,name='movie_share'),
    # url(r'^pizza_request/',views.Pizza_request,name='pizza_request'),
    url(r'^rest_request/',views.Rest_request,name='rest_request'),
    url(r'^quicky_request/',views.Quicky_request,name='quicky_request'),
    url(r'^pcgame_request/',views.PCGame_request,name='pcgame_request'),
    url(r'^movie_request/',views.Movie_request,name='movie_request'),
    # url(r'^pizza_edit/',views.Pizza_edit,name='pizza_edit'),
    url(r'^rest_edit/',views.Rest_edit,name='rest_edit'),
    url(r'^quicky_edit/',views.Quicky_edit,name='quicky_edit'),
    url(r'^pcgame_edit/',views.PCGame_edit,name='pcgame_edit'),
    url(r'^movie_edit/',views.Movie_edit,name='movie_edit'),
    url(r'^trip_edit/',views.trip_edit,name='Trip_edit'),
    url(r'^android_message/',views.android_msg,name='Android_messs'),
    url(r'^android_sendmessage/',views.android_sendmsg,name='Android_sendmesss'),
    url(r'^send_image/',views.send_image,name='send_image'),
    url(r'^send_team_image/',views.send_team_image,name='send_team_image'),
    url(r'^android_change_interest/',views.android_change_interest,name='android_change_interest'),

    url(r'^scroll_msgs/',views.scroll_msgs,name='scroll_msgs'),
    url(r'^scroll_team_msgs/',views.scroll_team_msgs,name='scroll_team_msgs'),

    url(r'^invitesportee/',views.Invitesportee,name='invite_sportee'),
    
    url(r'^android_registration/',views.android_registration,name='and_reg'),
    url(r'^android_info/',views.android_info,name='android_info'),
    url(r'^android_trips/',views.android_trips,name='android_trips'),
    url(r'^android_team/',views.android_team,name='android_team'),
    url(r'^android_reg_key/',views.android_reg_key,name='android_reg_key'),
    url(r'^android_privacy/',views.android_privacy,name='android_privacy'),
    url(r'^android_teamcht/',views.android_teamcht,name='android_teamcht'),
    url(r'^android_team_message/',views.android_teamcht,name='Android_teammesss'),
    url(r'^android_send_team_message/',views.android_teammsg,name='Android_sendteammesss'),
    url(r'^android_change_password/',views.android_change_password,name='android_change_password'),
    url(r'^leave_team/',views.leave_team,name='leave_team'),
    url(r'^team_info/',views.team_info,name='team_info'),
    url(r'^scroll_following/',views.scroll_following,name='scroll_following'),
    url(r'^scroll_suggestions/',views.scroll_suggestions,name='scroll_suggestions'),
    url(r'^near_by/',views.near_by,name='near_by'),
    url(r'^android_fblogin/',views.android_fblogin,name='android_fblogin'),
    # url(r'^android_notify', include('gcm.urls')),


    url(r'^google102ec01c53289263.html',views.google_verification,name='google_verif'),
    # url(r'^google57313dfadcf8ab59.html/',views.google_verification,name='google_verif'),
    url(r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /admin/", content_type="text/plain")),
    url(r'^sitemap.xml$', lambda r: HttpResponse("<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'><!-- created with Free Online Sitemap Generator www.xml-sitemaps.com --><url><loc>http://www.zeeley.com/</loc><changefreq>weekly</changefreq></url></urlset>", content_type="text/plain")),

    url(r'^trip_accept/',views.Trip_accept,name='trip_accept'),
    url(r'^rest_accept/',views.Rest_accept,name='Rest_accept'),
    url(r'^movie_accept/',views.Movie_accept,name='Movie_accept'),
    url(r'^cab_accept/',views.Cab_accept,name='Cab_accept'),
    url(r'^quicky_accept/',views.Quicky_accept,name='Quicky_accept'),
    url(r'^pcgame_accept/',views.PCGame_accept,name='pcgame_accept'),


    url(r'^delete_all/',views.delete_all,name='delete_all'),


    url(r'^confirm_mail/',views.confirm_mail,name='confirm_maiL'),
    url(r'^forgot_password/',views.forgot_password,name='forgot_passworD'),
    #url(r'^accounts/', include('allauth.urls')),
    url(r'^notification/',views.notification_panel,name='notification_panel'),
    url(r'^msgpanel/',views.msg_panel,name='msgpanel'),
    #url(r'^covchange/',views.covchange,name='covchange'),
    url(r'^raw_prof_pic/',views.raw_prof_pic,name='raw_prof_pic'),
    url(r'^your_group/',views.your_group,name='your_group'),
    url(r'^location/',views.location,name='location'),
    url(r'^refresh_all_teams/',views.refresh_all_teams,name='refresh_all_teams'),
    url(r'^team_left_info/',views.team_left_info,name='team_left_info'),

    url(r'^getprofile_pic_conf/',views.getprofile_pic_conf,name='prof_pic_conf'),
    url(r'^getmail',views.getmail,name='getmail'),


    #+++url(r'^mmm',views.mmm),

]

#urlpatterns = patterns('', ... (r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", mimetype="text/plain")) )


if settings.DEBUG :
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)


